﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator.VitalParameters
{
    class CapillarGlucoseLevel : VitalParameter
    {
        public override VitalParameterType type { get { return VitalParameterType.CapillarBloodGlucose; } }

        public CapillarGlucoseLevel(float? averageGlucoseLevel, bool doCalculateContinuousValue = true) : base(averageGlucoseLevel, doCalculateContinuousValue) { }
        //public CapillarGlucoseLevel(float averageGlucoseLevel) : this(averageGlucoseLevel, null) { }

        public override void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("value");
            }
            if(value < 0f)
            {
                throw new ArgumentException("Glucose level is out of physiological bounds (smaller then 0 mg/dl)", "referenceValue");
            }

            referenceValue = Convert.ToSingle(value);
        }

        public override int calculateContinuousThreadPause()
        {
            return 500;
        }
        public override float calculateContinuousValue()
        {
            return referenceValue * (1 + bellShapedRandomNumber(0.0, 0.003));
        }
    }
}
