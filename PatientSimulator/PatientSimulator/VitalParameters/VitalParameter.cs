﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

using PatientSimulator.Exceptions;

namespace PatientSimulator.VitalParameters
{
    //public enum VitalParameterType { BloodPressure, BreathRate, VenousBloodOxygenPressure, VenousBloodCarbondioxidePressure, Weight, Height, CoreTemperature, CapillarBloodGlucose }
    // Now in Globals.cs

    abstract class VitalParameter
    {
        public List<CurrentVitalValue> continuousValue { get; protected set; }
        public virtual float referenceValue { get; protected set; }
        public abstract VitalParameterType type { get; }  // every VitalParameter has to declare its own type, so CurrentVitalValue has a reference

        private Thread continuousCalculation;
        private bool abortContinuousValueThread = false;
        /// <summary>
        /// Define readonly whether the continuous values should be calculated or not.
        /// If false, the additional Thread will not be started and a NonExistentContinuousValueException is thrown when trying to access the continuous value list.
        /// </summary>
        protected virtual bool doCalculateContinuousValue { get; } = true;

        protected Random rand = new Random();

        #region Constructors
        /// <summary>
        /// Create this vitalParameter and use its default continuous value calculation option.
        /// </summary>
        /// <param name="averageParameterValue"></param>
        /// <param name="doCalculateContinuousValue"></param>
        public VitalParameter(float? averageParameterValue, bool doCalculateContinuousValue)  // Perform a null check later
        {
            _init_setReferenceValue(averageParameterValue);
            continuousValue = new List<CurrentVitalValue>();
            this.doCalculateContinuousValue &= doCalculateContinuousValue;

            if (this.doCalculateContinuousValue)
            {
                continuousCalculation = new Thread(VitalParameter.continuousValueThread);
                continuousCalculation.Start(new ThreadParameters(this));
            }
        }

        /// <summary>
        /// Create this vitalParameter and define the continuous value calculation option.
        /// </summary>
        /// <param name="averageParameterValue"></param>
        /// <param name="doCalculateContinuousValue"></param>
        //public VitalParameter(float? averageParameterValue, bool doCalculateContinuousValue = true)  // Perform a null check later
        //{
        //    _init_setReferenceValue(averageParameterValue);
        //    continuousValue = new List<CurrentVitalValue>();
        //    this.doCalculateContinuousValue &= doCalculateContinuousValue;  // Use the & operator, so a preexistent false (eg. at Weight) cannot be turned into true
        //                                                                    // Is used in the case of missing rights (when no continuous values should be created)

        //    if (doCalculateContinuousValue)
        //    {
        //        continuousCalculation = new Thread(VitalParameter.continuousValueThread);
        //        continuousCalculation.Start(new ThreadParameters(this));
        //    }
        //}

        #endregion

        #region Setter and Gettter Methods
        public void addContinuousValue(CurrentVitalValue vitalValue)
        {
            if (!doCalculateContinuousValue)
            {
                throw new NonExistentContinuousValueException("Illegaly trying to write a continuous value: Continuous Values are disabled for this Vital Parameter.", type, referenceValue);
            }
            if (vitalValue == null || vitalValue.timestamp == null)
            {
                throw new ArgumentNullException(vitalValue == null ? "vitalValue" : "vitalValue.timestamp", "Failed to add the dynamic vital value to the vital parameter list.");
            }

            lock (Locks.ThreadCalculationLocks[3])
            {
                continuousValue.Add(vitalValue);
            }
        }
        public List<CurrentVitalValue> getContinuousValues(int pastSecounds)  // Get all continuous values for the last time (until now) 
        {

            if (!doCalculateContinuousValue)
            {
                throw new NonExistentContinuousValueException("Illegaly trying to read a continuous value intervall: Continuous Values are disabled for this Vital Parameter.", type, referenceValue);
            }

            List<CurrentVitalValue> copyContinuousValues;
            lock (Locks.ThreadCalculationLocks[3])
            {
                copyContinuousValues = new List<CurrentVitalValue>(continuousValue);  // Create this to work with a copy of the List, therefor no error will be thrown when teh list is changed during this operation
            }
            
            pastSecounds = Math.Abs(pastSecounds);
            int m = pastSecounds / 60; // Careful: has to be integer, so all decimals will vanish (those represent the time above the full minute)
            int h = m / 60;
            int d = h / 24;

            DateTime collectDataSince = DateTime.Now.Subtract(new TimeSpan(d, h % 24, m % 60, pastSecounds % 60));
            IEnumerable<CurrentVitalValue> continuousValueSince = from item in copyContinuousValues where DateTime.Compare(item.timestamp, collectDataSince) >= 0 select item;

            return continuousValueSince.ToList();
        }
        public CurrentVitalValue getLastContinuousValue()
        {

            if (!doCalculateContinuousValue)
            {
                throw new NonExistentContinuousValueException("Illegaly trying to read a continuous value: Continuous Values are disabled for this Vital Parameter.", type, referenceValue);
            }

            List<CurrentVitalValue> copyContinuousValues;
            lock (Locks.ThreadCalculationLocks[3])
            {
                copyContinuousValues = new List<CurrentVitalValue>(continuousValue);  // Create this to work with a copy of the List, therefor no error will be thrown when teh list is changed during this operation
            }
            return copyContinuousValues.Count == 0 ? null : copyContinuousValues.Last();
        }
        public virtual void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("value");
            }

            referenceValue = Convert.ToSingle(value);
        }

        private void _init_setReferenceValue(float? value)  // use in constructors only, to ensure that the reference value can be set without interferring with dependencies on derived classes
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("value");
            }

            referenceValue = Convert.ToSingle(value);
        }
        #endregion

        #region Continuous Calculation (Thread)
        public static void continuousValueThread (object threadParameter)
        {
            Thread.Sleep(50);

            ThreadParameters parentCallback = null;
            int sleepTime = 1000;
            bool runnableThread = false;
            try
            {
                parentCallback = (ThreadParameters)threadParameter;
                runnableThread = !parentCallback.vitalParameter.abortContinuousValueThread;  // probe the object, if it is realy accessible
            }
            catch (ThreadAbortException e)  // Thread abort exception must be thrown, so the thread can exit
            {
                throw e;
            }
            catch (InvalidCastException e)  // Will be thrown in case the explicit conversion of threadParametes fails because threadParameter is not of type ThreadParameters
            {
                Logger.log(LogLevel.ERROR, "One thread calculating the current vital parameter values was aborted, because the reference VitalParameter class is not accessible.\n" + e.StackTrace, LogSource.VITALPARAMETER);
            }

            while (runnableThread && !parentCallback.vitalParameter.abortContinuousValueThread)
            {
                try
                {
                    float continuousValue;
                    lock (Locks.ThreadCalculationLocks[0])
                    {
                        continuousValue = parentCallback.vitalParameter.calculateContinuousValue();
                    }
                    CurrentVitalValue recentContinuousValue = new CurrentVitalValue(continuousValue, parentCallback.vitalParameter.type);
                    lock (Locks.ThreadCalculationLocks[1])
                    {
                        parentCallback.vitalParameter.addContinuousValue(recentContinuousValue);
                    }
                    //Debug.WriteLine("At {0} value {1}", parentCallback.vitalParameter.getLastContinuousValue()?.type, parentCallback.vitalParameter.getLastContinuousValue()?.value);
                    lock (Locks.ThreadCalculationLocks[2])
                    {
                        sleepTime = parentCallback.vitalParameter.calculateContinuousThreadPause();
                    }
                    // When The continuous value is created correctly, invoke the continuousValueCreated event
                    //if (recentContinuousValue.type.Equals(VitalParameterType.CapillarBloodGlucose)) { Debug.WriteLine(String.Format("VitalParaemeter: At {0} for {1} recorded Value {2}", DateTime.Now.ToString("HH:mm:ss.ff"), recentContinuousValue.type, recentContinuousValue.value)); }
                    parentCallback.vitalParameter.continuousValueCreated?.Invoke(recentContinuousValue, new VitalParameterEventArgs(parentCallback.vitalParameter.type));
                }
                catch (ThreadAbortException e)  // Thread abort exception must be thrown, so the thread can exit
                {
                    throw e;
                }
                // Write the exception message genericly, as the messega will be provided by the fail of the used function.
                catch (NullReferenceException e)
                {
                    if(parentCallback.vitalParameter == null)
                    {
                        Logger.log(LogLevel.ERROR, "The Thread is terminated because the VitalParmeter reference is inaccessible", LogSource.VITALPARAMETER);
                        runnableThread = false;
                    }
                    else
                    {
                        Logger.log(LogLevel.WARNING, String.Format("{0}\n{1}", e.Message, e.StackTrace), LogSource.VITALPARAMETER, parentCallback.vitalParameter.type);
                    }
                }
                catch (ArgumentException e) // Throw an argument exception when calculation of vital Parameters failed or was not correctly omitted to CurrentVitalValue  // As ArgumentNullException derives from ArgumentException, both will be called here
                {
                    Logger.log(LogLevel.WARNING, String.Format("{0}\n{1}", e.Message, e.StackTrace), LogSource.VITALPARAMETER, parentCallback.vitalParameter.type);
                }
                catch (VitalParameterNullReferenceException e)
                {
                    if(e.Type != null)
                    {
                        Logger.log(LogLevel.WARNING, String.Format("{0}\n{1}", e.Message, e.StackTrace), LogSource.VITALPARAMETER, (VitalParameterType)e.Type);
                    }
                    else
                    {
                        Logger.log(LogLevel.WARNING, String.Format("{0}\n{1}", e.Message, e.StackTrace), LogSource.VITALPARAMETER);
                    }
                }
                catch (NonExistentContinuousValueException e)
                {
                    if(e.Type != null)
                    {
                        Logger.log(LogLevel.WARNING, e.Message, LogSource.VITALPARAMETER, (VitalParameterType)e.Type);
                    }
                    else
                    {
                        Logger.log(LogLevel.WARNING, e.Message, LogSource.VITALPARAMETER);
                    }
                }
                catch (Exception e)
                {
                    Logger.log(LogLevel.ERROR, String.Format("{0}\n{1}", e.Message, e.StackTrace), LogSource.VITALPARAMETER, parentCallback.vitalParameter.type);
                }

                Thread.Sleep(sleepTime);
            }
        }
        public virtual float calculateContinuousValue()
        {
            return referenceValue * (1f + bellShapedRandomNumber(0.0, 0.003));   // distribute the calculated Value with anormal distribution, so that ~99% of values lie within referenceValue +- 1%
        }
        /// <summary>
        /// Returns the pause time until the next continuous vital parameter should be calculated.
        /// </summary>
        /// <returns>The recomended pause time of the current thread in millisecounds.</returns>
        public virtual int calculateContinuousThreadPause()  // Might be overwritten by derived classes, as VP are more complicated (eg. a new heartRate value can only be meassured after the Heart beats, which will take as much time as the heartRate defines)
        {
            return 1000;
        }
        protected float bellShapedRandomNumber(double mean, double stdDerivation)  // Simulate a standard normal distribution by (pseudo) uniform random number generator and sinus * logarithmic functions. It will at least be bell shaped.
        {
            double[] uniformNumber = new double[] { 1.0 - rand.NextDouble(), 1.0 - rand.NextDouble() };
            return (float)(mean + stdDerivation * (Math.Sqrt(-2.0 * Math.Log(uniformNumber[0])) * Math.Sin(2.0 * Math.PI * uniformNumber[1])));
        }
        #endregion

        #region Triggers
        public delegate void VitalParameterEventHandler(object source, VitalParameterEventArgs e);

        public event VitalParameterEventHandler continuousValueCreated;   // Is invoked only when a continuous Value is created and caught from the PatientModel. source is allways the CurrentVitalValue instance!
        #endregion

        protected bool simpleNullCheck(float? value)  // Returns true if value is either null or empty
        {
            return (value == null);
        }

        public void ThreadAbort()
        {
            abortContinuousValueThread = true;
            Logger.log(LogLevel.INFO, "Starting to exit the continuous vital parameter value creation thread on a clean exit.", LogSource.GENERAL, this.type);
            while(doCalculateContinuousValue && continuousCalculation.IsAlive) { }  // Stay on hold, until the thread is finished, to not break up any reference connection with needed objects in OnCleanExit() while they might still be needed
            OnCleanExit();
        }
        public void DirtyThreadAbort()
        {
            abortContinuousValueThread = true;
            continuousCalculation?.Abort();
            Logger.log(LogLevel.INFO, "Starting to exit the continuous vital parameter value creation thread.", LogSource.GENERAL, this.type);
            OnCleanExit();
        }
        /// <summary>
        /// Is invoked at this.ThreadAbort() only and is aimed to close all open pointers, connections to other objects, etc.
        /// </summary>
        protected virtual void OnCleanExit() { }

        private class ThreadParameters  // must be used in the thread, as explicit conversion is not possible for the abstract class VitalParameter, it will be wraped
        {
            public VitalParameter vitalParameter { get; private set; }  // Ability of reference passing successfully tested, the thread will write all changes to the original instance

            public ThreadParameters(VitalParameter vital)
            {
                vitalParameter = vital;
            }
        }
    }
}
