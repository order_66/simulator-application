﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator.VitalParameters
{
    class BloodOxygenPressure : VitalParameter
    {
        public override VitalParameterType type { get { return VitalParameterType.VenousBloodOxygenPressure; } }

        public BloodOxygenPressure(float? averageOxygenPressure, bool doCalculateContinuousValue = true) : base(averageOxygenPressure, doCalculateContinuousValue) { }
        //public CapillarGlucoseLevel(float averageOxygenPressure) : this(averageOxygenPressure, null) { }

        public override void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("value");
            }
            if (value < 0f)
            {
                throw new ArgumentException("Venous oxygen pressure is out of physiological bounds (smaller then 0 mmHg)", "referenceValue");
            }

            referenceValue = Convert.ToSingle(value);
        }

        public override int calculateContinuousThreadPause()
        {
            return 500;
        }
        public override float calculateContinuousValue()
        {
            return referenceValue * (1 + bellShapedRandomNumber(0.0, 0.003));
        }
    }
}
