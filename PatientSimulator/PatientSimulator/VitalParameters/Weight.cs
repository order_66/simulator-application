﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator.VitalParameters
{
    class Weight : VitalParameter
    {
        public override VitalParameterType type { get { return VitalParameterType.Weight; } }
        protected override bool doCalculateContinuousValue { get; } = false;

        //public Weight(float[] averageWeightKG) : this(averageWeightKG, null) { }
        public Weight(float? averageWeightKG, bool doCalculateContinuousValue = true) : base(averageWeightKG, doCalculateContinuousValue) { }

        public override void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("value");
            }
            if(value < 0f || value > 1000f)
            {
                
                throw new ArgumentException("Weight is out of physiological bounds (smaller then 0 or greater then 1000 kg)", "referenceValue");
            }

            referenceValue = Convert.ToSingle(value);
        }
    }
}
