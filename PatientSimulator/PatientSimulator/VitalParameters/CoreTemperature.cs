﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator.VitalParameters
{
    class CoreTemperature : VitalParameter
    {
        public override VitalParameterType type { get { return VitalParameterType.CoreTemperature; } }

        //public CoreTemperature(float[] averageCoreTemperatureCel) : this(averageCoreTemperatureCel, null) { }
        public CoreTemperature(float? averageCoreTemperatureCel, bool doCalculateContinuousValue = true) : base(averageCoreTemperatureCel, doCalculateContinuousValue) { }

        public override void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("value");
            }
            if (value < 0f || value > 100f)
            {

                throw new ArgumentException("Weight is out of physiological bounds (smaller then 0 or greater then 1000 kg)", "referenceValue");
            }

            referenceValue = Convert.ToSingle(value);
        }

        public override float calculateContinuousValue()
        {
            float hundredstDegreeRelative = 0.01f / referenceValue;  // 0.01°C relative to one (calculate the random distribution range, see bolow), It should cover almost the whole sample values
            return referenceValue * bellShapedRandomNumber(1f, hundredstDegreeRelative/3f);  // The bodys core temperature usually does not change much, the body can regulate up to 0.01°C accurate
        }
        public override int calculateContinuousThreadPause()
        {
            return 1000;
        }
    }
}
