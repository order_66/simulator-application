﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PatientSimulator.Exceptions;

namespace PatientSimulator.VitalParameters
{
    abstract class ArterialBloodPressure : VitalParameter
    {
        protected float threadHeartRate = 60f; // unit: bpm
        //private float threadSystolicRise = 0.1f; // describes the relative time in one heartbeat the systole needs to rise from minimal to maximal blood pressure
        //private float threadPressureCycleTime = 0f; // The time that passed for the current blood pressure (since the last full diastole) in means of the threadPause

        public ArterialBloodPressure(float? averageBloodPressureMMHG, bool doCalculateContinuousValue) : base(averageBloodPressureMMHG, doCalculateContinuousValue) { }

        public override void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("referenceValue");
            }
            if (value < 0f || value > 1000f)
            {

                throw new ArgumentException("Mean blood pressure is out of physiological bounds (smaller then 0 or greater then 1000 mmHg)", "referenceValue");
            }

            referenceValue = Convert.ToSingle(value);
        }

        public override int calculateContinuousThreadPause() // Should be dependend on heart rate, as it is not implemented we assume a heart rae of constant 60 bpm
        {
            return (int)Math.Round(threadHeartRate/60f * 1000); // 1 values per second (or between two heart beats), expressed in milliseconds
        }
        public override float calculateContinuousValue()  // Will create a sawtooth-function, where the pressure will rise from the diastolic to the systoli pressure and then fall to the diastolic pressure again in the time of one heartbeat
        {
            /*float pressure = 0f;
            float pressureRise = 0f;
            if(threadPressureCycleTime >= threadHeartRate / 60f)
            {
                threadPressureCycleTime -= threadHeartRate / 60f;
            }
            if(threadPressureCycleTime <= (threadSystolicRise * threadHeartRate / 60f))  // This branch will be chosen if the systole is in progress
            {
                pressureRise = (referenceValue[0] - referenceValue[1]) / (threadSystolicRise * threadHeartRate / 60f);
                pressure = referenceValue[1] + threadPressureCycleTime * pressureRise;
            }
            else
            {
                pressureRise = (referenceValue[1] - referenceValue[0]) / ((1-threadSystolicRise) * threadHeartRate / 60f);
                pressure = (referenceValue[0] - pressureRise * (float)Math.Round(threadHeartRate / 60f / 15f)) + pressureRise * threadPressureCycleTime;
            }

            threadPressureCycleTime += (float)Math.Round(threadHeartRate / 60f / 15f); // Add the time for the next interval*/

            return referenceValue * (1 + bellShapedRandomNumber(0,0.01));
        }
    }

    class SystolicBloodPressure : ArterialBloodPressure
    {
        public override VitalParameterType type { get { return VitalParameterType.SystolicBloodPressure; } }
        private DiastolicBloodPressure diastolicBloodPressure;

        public SystolicBloodPressure(float? averageSystolicBloodPressureMMHG, DiastolicBloodPressure diastolicPressureVP, bool doCalculateContinuousValue = true) : base(averageSystolicBloodPressureMMHG, doCalculateContinuousValue)
        {
            if (diastolicPressureVP == null)
            {
                throw new VitalParameterNullReferenceException("Failing the instantiation of the vital parameter because of a missing reference", type, "diastolicPressureVP");
            }
            diastolicBloodPressure = diastolicPressureVP;
        }
        public SystolicBloodPressure(float? averageSystolicBloodPressureMMHG, out DiastolicBloodPressure diastolicPressureVP, float? averageDiastolicBloodPressureMMHG, bool doCalculateContinuousValue = true) : base(averageSystolicBloodPressureMMHG, doCalculateContinuousValue)
        {
            diastolicPressureVP = new DiastolicBloodPressure(averageDiastolicBloodPressureMMHG, this, doCalculateContinuousValue);
            diastolicBloodPressure = diastolicPressureVP;  // Do not check for null, as the instance might not be created yet
        }

        public override void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("referenceValue");
            }
            if (value < 0f || value > 1000f || getDiastolicPressure().referenceValue >= value)
            {

                throw new ArgumentException("Systolic blood pressure is out of physiological bounds (smaller then 0 or greater then 1000 mmHg or smaller then the diastolic blood pressure).", "referenceValue");
            }

            referenceValue = Convert.ToSingle(value);
        }
        protected DiastolicBloodPressure getDiastolicPressure()
        {
            if (diastolicBloodPressure == null)  // As it is a reference to another object, it could be disposed at any time
            {
                throw new VitalParameterNullReferenceException("Missing a reference blood pressure type.", VitalParameterType.DiastolicBloodPressure, "diastolicBloodPressure");
            }
            return diastolicBloodPressure;
        }

        public override float calculateContinuousValue()
        {
            float value = base.calculateContinuousValue();
            float lastDiastolicValue = getDiastolicPressure().referenceValue;
            CurrentVitalValue lastDiastolicContinuousCalc = null;
            try
            {
                lastDiastolicContinuousCalc = getDiastolicPressure().getLastContinuousValue();
            }
            catch (NonExistentContinuousValueException) { }

            if(lastDiastolicContinuousCalc != null)
            {
                lastDiastolicValue = lastDiastolicContinuousCalc.value;
            }
            if (value <= lastDiastolicValue)
            {
                value = lastDiastolicValue * 1.01f;
            }
            return value;
        }

        protected override void OnCleanExit()
        {
            diastolicBloodPressure = null;
            base.OnCleanExit();
        }
    }

    class DiastolicBloodPressure : ArterialBloodPressure
    {
        public override VitalParameterType type { get { return VitalParameterType.DiastolicBloodPressure; } }
        private SystolicBloodPressure systolicBloodPressure;

        public DiastolicBloodPressure(float? averageDiastolicBloodPressureMMHG, SystolicBloodPressure systolicPressureVP, bool doCalculateContinuousValue = true) : base(averageDiastolicBloodPressureMMHG, doCalculateContinuousValue)
        {
            if(systolicPressureVP == null)
            {
                throw new VitalParameterNullReferenceException("Failing the instantiation of the vital parameter because of a missing reference", type, "systolicPressureVP");
            }
            systolicBloodPressure = systolicPressureVP;
        }
        public DiastolicBloodPressure(float? averageDiastolicBloodPressureMMHG, out SystolicBloodPressure systolicPressureVP, float? averageSystolicBloodPressureMMHG, bool doCalculateContinuousValue = true) : base(averageDiastolicBloodPressureMMHG, doCalculateContinuousValue)
        {
            systolicPressureVP = new SystolicBloodPressure(averageSystolicBloodPressureMMHG, this, doCalculateContinuousValue);
            systolicBloodPressure = systolicPressureVP;
        }

        public override void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("referenceValue");
            }
            if (value < 0f || value > 1000f || getSystolicPressure().referenceValue <= value)
            {

                throw new ArgumentException("Diastolic blood pressure is out of physiological bounds (smaller then 0 or greater then 1000 mmHg or greater then the systolic blood pressure).", "referenceValue");
            }

            referenceValue = Convert.ToSingle(value);
        }
        protected SystolicBloodPressure getSystolicPressure()
        {
            if (systolicBloodPressure == null)  // As it is a reference to another object, it could be disposed at any time
            {
                throw new VitalParameterNullReferenceException("Missing a reference blood pressure type.", VitalParameterType.SystolicBloodPressure, "systolicBloodPressure");
            }
            return systolicBloodPressure;
        }

        public override float calculateContinuousValue()
        {
            float value = base.calculateContinuousValue();
            float lastSystolicValue = getSystolicPressure().referenceValue;
            CurrentVitalValue lastSystolicContinuousCalc = null;
            try
            {
                lastSystolicContinuousCalc = getSystolicPressure().getLastContinuousValue();
            }
            catch (NonExistentContinuousValueException) { }

            if (lastSystolicContinuousCalc != null)
            {
                lastSystolicValue = lastSystolicContinuousCalc.value;
            }
            if (value >= lastSystolicValue)
            {
                value = lastSystolicValue * 0.99f;
            }
            return value;
        }

        protected override void OnCleanExit()
        {
            systolicBloodPressure = null;
            base.OnCleanExit();
        }
    }

    class MeanBloodPressure : ArterialBloodPressure
    {
        public override VitalParameterType type { get { return VitalParameterType.MeanBloodPressure; } }
        public override float referenceValue { get { return calculateMeanBloodPressure(); } protected set => base.referenceValue = value; }
        private SystolicBloodPressure systolicBloodPressure;
        private DiastolicBloodPressure diastolicBloodPressure;

        public MeanBloodPressure(float? averageBloodPressureMMHG, SystolicBloodPressure systolicPressureVP, DiastolicBloodPressure diastolicPressureVP, bool doCalculateContinuousValue = true) : base(averageBloodPressureMMHG, doCalculateContinuousValue)
        {
            systolicBloodPressure = systolicPressureVP ?? throw new ArgumentNullException("diastolicPressureVP");
            diastolicBloodPressure = diastolicPressureVP ?? throw new ArgumentNullException("diastolicPressureVP");
        }

        /// <summary>
        /// Deprecated: Calculates the mean blood pressure aut of the available systolic and diastolic pressure.
        /// </summary>
        /// <param name="value">Unused</param>
        public override void setReferenceValue(float? value)
        {
            /*if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("value");
            }
            if (value < 0f || value > 1000f || getDiastolicPressure() >= value || getSystolicPressure() <= value)
            {

                throw new ArgumentException("Mean blood pressure is out of physiological bounds (smaller then 0 or greater then 1000 mmHg)", "referenceValue");
            }*/  // Not considered Neccessary, because the mean pressure is only dependend on the systolic and diastolic pressure

            referenceValue = calculateMeanBloodPressure();
        }
        protected SystolicBloodPressure getSystolicPressure()
        {
            if (systolicBloodPressure == null)  // As it is a reference to another object, it could be disposed at any time
            {
                throw new VitalParameterNullReferenceException("Missing a reference blood pressure to set the mean blood pressure.", VitalParameterType.SystolicBloodPressure, "systolicBloodPressure");
            }
            return systolicBloodPressure;
        }
        protected DiastolicBloodPressure getDiastolicPressure()
        {
            if (diastolicBloodPressure == null)  // As it is a reference to another object, it could be disposed at any time
            {
                throw new VitalParameterNullReferenceException("Missing a reference blood pressure to set the mean blood pressure.", VitalParameterType.DiastolicBloodPressure, "diastolicBloodPressure");
            }
            return diastolicBloodPressure;
        }

        public override float calculateContinuousValue()
        {
            return calculateMeanBloodPressure();
        }
        protected float calculateMeanBloodPressure()
        {
            float lastSystolicValue = getSystolicPressure().referenceValue;
            float lastDiastolicValue = getDiastolicPressure().referenceValue;
            CurrentVitalValue lastSystolicContinuousValue = null;
            CurrentVitalValue lastDiastolicContinuousValue = null;
            try
            {
                lastSystolicContinuousValue = getSystolicPressure().getLastContinuousValue();
            }
            catch (NonExistentContinuousValueException) { }
            try
            {
                lastDiastolicContinuousValue = getDiastolicPressure().getLastContinuousValue();
            }
            catch (NonExistentContinuousValueException) { }


            if(lastSystolicContinuousValue != null)
            {
                lastSystolicValue = lastSystolicContinuousValue.value;
            }
            if(lastDiastolicContinuousValue != null)
            {
                lastDiastolicValue = lastDiastolicContinuousValue.value;
            }
            return (lastSystolicValue + 2f * lastDiastolicValue) / 3f;
            // Source: https://www.nursingcenter.com/ncblog/december-2011/calculating-the-map
        }

        protected override void OnCleanExit()
        {
            systolicBloodPressure = null;
            diastolicBloodPressure = null;
            base.OnCleanExit();
        }
    }
}
