﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator.VitalParameters
{
    class BreathRate : VitalParameter
    {
        public override VitalParameterType type { get { return VitalParameterType.BreathRate; } }
        private int? threadPauseMillis;

        public BreathRate(float? averageBreathRatePerMinute, bool doCalculateContinuousValue = true) : base(averageBreathRatePerMinute, doCalculateContinuousValue)
        {
            threadPauseMillis = (int)Math.Round(1f / (averageBreathRatePerMinute ?? 12f) * 60f * 1000f, 0);
        }

        public override void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("value");
            }
            if (value < 0f || value > 200f)
            {

                throw new ArgumentException("Weight is out of physiological bounds (smaller then 0 or greater then 1000 kg)", "referenceValue");
            }

            referenceValue = Convert.ToSingle(value);
        }

        public override int calculateContinuousThreadPause()
        {
            threadPauseMillis = (int)Math.Round(1f / (referenceValue * bellShapedRandomNumber(1, 0.03)) * 60f * 1000f, 0);
            return (int)(threadPauseMillis <= 0 ? 8000 : threadPauseMillis);
        }
        public override float calculateContinuousValue()
        {
            if(threadPauseMillis == null)  // Important to handle the null error, as the function might be called by the thread befor the first threadPause is assigned in the constructor.
            {
                throw new ArgumentException("Failed to calculate the dynamic breath rate, referencing values are not set yet (pause between breaths).", "threadPauseMillis");
            }
            
            return 1f/((float)threadPauseMillis / 1000f / 60f );
        }
    }
}
