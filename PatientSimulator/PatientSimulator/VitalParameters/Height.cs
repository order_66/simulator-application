﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator.VitalParameters
{
    class Height : VitalParameter
    {
        public override VitalParameterType type { get { return VitalParameterType.Height; } }
        protected override bool doCalculateContinuousValue { get; } = false;

        //public Height(float[] averageHeightM) : this(averageHeightM, null) { }
        public Height(float? averageHeightM, bool doCalculateContinuousValue = true) : base(averageHeightM, doCalculateContinuousValue) { }

        public override void setReferenceValue(float? value)
        {
            if (simpleNullCheck(value))
            {
                throw new ArgumentNullException("value");
            }
            if (value < 0f || value > 400f)
            {

                throw new ArgumentException("Height is out of physiological bounds (smaller then 0 or greater then 4 m)", "referenceValue");
            }

            referenceValue = Convert.ToSingle(value);
        }
    }
}
