﻿using PatientSimulator.VitalParameters;
using System;
using System.Linq;
using System.Collections.Generic;

namespace PatientSimulator.Organs
{
    abstract class Organ
    {
        public List<VitalParameter> vitalParameters { get; protected set; }
        public virtual OrganType type { get; protected set; }
        protected virtual VitalParameterType[] allowedVitalTypes { get; set; }
        public Guid uuid { get; protected set; }

        public Organ(List<VitalParameter> parameters, Guid organUUID)  // Use for explicit creation of defined Organs (no multiple use of the class posible), allowedVitalTypes are static set in base class
        {
            setVitalParameters(parameters);
            setUuid(organUUID);
        }
        public Organ(OrganType Otype, List<VitalParameter> parameters, Guid organUUID, VitalParameterType[] allowedVitalParameters) // Use for explicit creation, check eventually for correct values in derived class
        {
            allowedVitalTypes = (VitalParameterType[])allowedVitalParameters.Clone();
            type = Otype;
            setVitalParameters(parameters);  // Do not reference the this() constructor here, as the allowedVitalTypes will be needed by those two functions
            setUuid(organUUID);              //
        }
        //public Organ(OrganType Otype) // Complete this constructor with derived class (Recieve VitalParameters from DB, based on type)
        //{
        //    throw new InvalidOperationException("The class is not ready for a generic constructor (establish a connection to DB first):");

        //    type = Otype;
        //}

        protected void setVitalParameters(List<VitalParameter> vitals)
        {
            if(vitals == null || vitals.Contains(null))  // Do not check for an empty list, as an Organ can contain 0 VP
            {
                throw new ArgumentNullException("vitals");
            }
            foreach(VitalParameter item in vitals)
            {
                if (!allowedVitalTypes.Contains(item.type))
                {
                    throw new ArgumentException(String.Format("The given list of vital parameters does contain at least one parameter that is not allowd in this organ({1}): {0}",item.type.ToString(), this.type.ToString()), "vitals");
                }
            } 

            vitalParameters = vitals;
        }
        public virtual void setUuid(Guid newUuid)
        {
            if(newUuid == null)
            {
                throw new ArgumentNullException("newUuid");
            }

            uuid = newUuid;
        }
        public VitalParameter getVitalParameter(VitalParameterType VPtype)
        {
            if (!hasVitalParameter(VPtype))
            {
                throw new ArgumentException("For the given VitalParameterType, no VitalParameter was found in the Organ.", "VPtype");
            }
            VitalParameter specificParameter = vitalParameters.Find(item => item.type.Equals(VPtype));
            return specificParameter;
        }
        /*public List<CurrentVitalValue> getContinuousValues(VitalParameterType parameter, int pastSecounds)
        {
            List<CurrentVitalValue> continuousValues;
            try
            {
                continuousValues = getVitalParameter(parameter).getContinuousValues(pastSecounds);
            }
            catch(ArgumentException e)
            {
                // Throw an exception, only if no apropriate Parameter could be found
                throw new ArgumentException("For the given VitalParameterType, no VitalParameter was found in the Organ.", "parameter");
            }
            return continuousValues;
        }*/

        public bool hasVitalParameter(VitalParameterType VPtype)
        {
            return vitalParameters.Exists(item => item.type.Equals(VPtype));
        }

        public void ThreadAbort()
        {
            OnCleanExit();
            foreach(VitalParameter item in vitalParameters)
            {
                item.ThreadAbort();
            }
        }
        public void DirtyThreadAbort()
        {
            OnCleanExit();
            foreach (VitalParameter item in vitalParameters)
            {
                item.DirtyThreadAbort();
            }
        }
        protected virtual void OnCleanExit() { }
    }
}
