﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PatientSimulator.VitalParameters;

namespace PatientSimulator.Organs
{
    class Body : Organ
    {
        public override OrganType type { get { return OrganType.Body; } }
        protected override VitalParameterType[] allowedVitalTypes { get { return (VitalParameterType[])Global.AllowedVPsInOrgans[type].Clone(); } }

        public Body(List<VitalParameter> parameters, Guid uuid) : base(parameters, uuid) { }
        

    }
}
