﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatientSimulator.VitalParameters;

namespace PatientSimulator.Organs
{
    class BloodVessel : Organ
    {
        public Blood vesselBlood { get; private set; }

        public BloodVessel(OrganType Otype, List<VitalParameter> parameters, Guid uuid, VitalParameterType[] allowedVitalParameters) : base(Otype, parameters, uuid, allowedVitalParameters) { }
        public BloodVessel(OrganType Otype, List<VitalParameter> parameters, Guid uuid, VitalParameterType[] allowedVitalParameters, Blood vesselBlood) : this(Otype, parameters, uuid, allowedVitalParameters)
        {
            this.vesselBlood = vesselBlood;
        }

        protected override void OnCleanExit()
        {
            vesselBlood = null;
            base.OnCleanExit();
        }
    }
}
