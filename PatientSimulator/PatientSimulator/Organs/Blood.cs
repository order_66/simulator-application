﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatientSimulator.VitalParameters;

namespace PatientSimulator.Organs
{
    class Blood : Organ
    {
        public Blood(OrganType Otype, List<VitalParameter> parameters, Guid uuid, VitalParameterType[] allowedVitalParameters) : base(Otype, parameters, uuid, allowedVitalParameters) { }
    }
}
