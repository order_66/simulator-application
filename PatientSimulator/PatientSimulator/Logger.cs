﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security;

using PatientSimulator.VitalParameters;

namespace PatientSimulator
{
    public enum LogLevel { INFO, WARNING, ERROR }
    public enum LogSource { GENERAL, VITALPARAMETER }  // Vital parameter calls the continuous vale log file, it shall only be used by these extra threads or thier depenendcies

    static class Logger
    {
        private static string logFilePath = @"" + Directory.GetCurrentDirectory() + @"\logs\";  // Has to end at "\"
        private static string logFileGeneral = DateTime.Now.ToString("dd_MM_yyyy-HH-mm-ss") + "_General.log";  // Create a file name containing the Date and time the App started (this string was created).
        private static string logFileContinuousVP = DateTime.Now.ToString("dd_MM_yyyy-HH-mm-ss") + "_VitalParameterValues.log";  // Log all messages from continuous values

        private static List<LoggerStack> stackedLogs = new List<LoggerStack>();
        private static bool writingLogs = false;  // Is activiaed ba the log writing process only during its work-time

        /// <summary>
        /// Deprecated, for compatibility reasons. Will call LogSource.GENERAL
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        public static void log(LogLevel level, string message)
        {
            log(level, message, LogSource.GENERAL);
        }
        public static void log(LogLevel level, string message, LogSource source)
        {
            string file = logFileGeneral;
            switch (source)
            {
                case LogSource.GENERAL: 
                    file = logFileGeneral;
                    break;
                case LogSource.VITALPARAMETER: 
                    file = logFileContinuousVP; 
                    break;
            }
            innerLog(level, message, file);
        }
        /// <summary>
        /// Use this function to provide aditional information for a vital parameter log.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        /// <param name="source"></param>
        /// <param name="type"></param>
        public static void log(LogLevel level, string message, LogSource source, VitalParameterType type)
        {
            log(level, String.Format("(At {0}) {1}", type.ToString(), message), source);
        }

        private static void innerLog(LogLevel level, string message, string file)
        {
            stackedLogs.Add( new LoggerStack(String.Format("[{0}] at {1}: {2}", level.ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ff"), message), String.Format("{0}{1}", logFilePath, file)) );

            if (!writingLogs) { WriteLog(); }
        }

        private static void WriteLog()
        {
            writingLogs = true;
            while (stackedLogs.Count != 0) // The loop will execute untill no log messages are available, in case the messages are coming in faster than they are written the List expands and there will be NO MULTIPLE POINTERS TO THE FILE.
            {
                //Console.WriteLine(stackedLogs.Count);
                try
                {
                    FileInfo logFileInfo = new FileInfo(stackedLogs?[0].logFile);
                    DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                    if (!logDirInfo.Exists) logDirInfo.Create();
                    using (FileStream fileStream = new FileStream(stackedLogs?[0].logFile, FileMode.Append))  // Will create the file if not existing
                    {
                        using (StreamWriter log = new StreamWriter(fileStream))
                        {
                            log.WriteLine(stackedLogs?[0].message);
                        }
                    }
                }
                catch (SecurityException e)
                {
                    Console.WriteLine(String.Format("Failed security while accessing log file path: {0}", stackedLogs?.Count != 0 ? stackedLogs?[0].logFile : "[PathNotAvailable]"));
                    Console.WriteLine(e.StackTrace);
                }
                catch (UnauthorizedAccessException e)
                {
                    Console.WriteLine(String.Format("Failed access while accessing the current log file {0}", stackedLogs?.Count != 0 ? stackedLogs?[0].logFile : "[PathNotAvailable]"));
                    Console.WriteLine(e.StackTrace);
                }
                catch (IOException e)
                {
                    Console.WriteLine(String.Format("Something went wrong while writing the log Message to the current log file at {0}", stackedLogs?.Count != 0 ? stackedLogs?[0].logFile : "[PathNotAvailable]"));
                    Console.WriteLine(e.StackTrace);
                }
                catch(NullReferenceException e)
                {
                    //Console.WriteLine(String.Format("Attempting to access an empty Log Message List Stack.\n{0}", e.StackTrace);
                }
                catch (Exception e)
                {
                    Console.WriteLine(String.Format("{0} caused a break in eriting a log file.", e.InnerException == null ? "[Exception not detectable]" : e.InnerException.ToString()));
                    Console.WriteLine(e.StackTrace);
                }
                if (stackedLogs?.Count != 0) {  stackedLogs?.RemoveAt(0);  } // Remove the Log either way, because it wont be written on the secound try, if the first attempt failed in an exception
            }
            writingLogs = false;
        }

        private class LoggerStack
        {
            public string message;
            public string logFile;

            public LoggerStack(string message, string logFile)
            {
                this.message = message;
                this.logFile = logFile;
            }
        }
    }
}
