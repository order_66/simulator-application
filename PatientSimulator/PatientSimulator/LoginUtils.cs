﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator
{
    class Sha256
    {
        public byte[] GetSha256(string input, string random)
        {
            using (SHA256 shaencr = SHA256.Create())
            {
                string tohash = input + random;
                byte[] bytearray = shaencr.ComputeHash(Encoding.UTF8.GetBytes(tohash));
                return bytearray;
            }
        }
    }
    class StringGenerator
    {
        public string GetRandString(int size)
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789#'*+~?ß´`§$%&=.,<>|";
            char[] stringChars = new char[size];
            Random random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            string finalString = new String(stringChars);


            return finalString;
        }
    }
    class NatCodes
    {
        private Dictionary<string, string> unsorted = new Dictionary<string, string>();
        private Dictionary<string, string> sorted = new Dictionary<string, string>();
        public NatCodes()
        {
            foreach (CultureInfo culture in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo country = new RegionInfo(culture.LCID);

                if (!unsorted.ContainsKey(country.EnglishName))
                {
                    unsorted.Add(country.EnglishName, country.ThreeLetterISORegionName);
                }
            }
            List<string> tempList = new List<string>();
            string outValue;

            foreach (var entry in unsorted)
            {
                tempList.Add(entry.Key);
            }
            tempList.Sort();
            foreach (string entry in tempList)
            {
                if (unsorted.TryGetValue(entry, out outValue))
                {
                    sorted.Add(entry, outValue);
                }
            }
        }

        public Dictionary<string, string> Codes { get => sorted; }
    }
}
