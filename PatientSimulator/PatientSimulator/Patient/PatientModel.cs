﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

using PatientSimulator.VitalParameters;
using PatientSimulator.Organs;
using PatientSimulator.Patient.DatabaseRequestInterface;
using PatientSimulator.Exceptions;

namespace PatientSimulator.Patient
{
    class PatientModel
    {
        private List<Organ> organs = new List<Organ>();
        private List<ICD10Impairment> ICD10Impairments = new List<ICD10Impairment>();
        private bool hiddenIsAlterable;
        public bool isAlterable { get { return hiddenIsAlterable; } set { setIsAlterable(value); } }
        public Guid uuid { get; private set; }
        public DateTime? createdAt { get; private set; }
        private Guid? hiddenRoleModelUuid;
        public Guid? roleModelUuid { get { return hiddenRoleModelUuid; } set { setRoleModelUuid(value); } }  // UUID of the patients model, which is the 'standard value' of this patient
        private string hiddenPatientName;
        public string patientName { get { return hiddenPatientName; } set { setPatientName(value); } }
        private Guid? hiddenOwnerUuid;
        public Guid? ownerUuid { get { return hiddenOwnerUuid; } set { setOwnerUuid(value); } }
        public ViewerRightsType ViewerRights { get { return hiddenViewerRights; } set { setViewerRights(value); } }
        private ViewerRightsType hiddenViewerRights;
        public PatientGender Gender { get { return hiddenGender; } set { setGender(value); } }
        public PatientGender hiddenGender = PatientGender.Unknown;

        /// <summary>
        /// Initiate a <c>PatientModel</c> instance and initiating all contianed Orgnas and Vital Parmeters.
        /// </summary>
        /// <param name="patientParameters"></param>
        /// <exception cref="ArgumentException">If any provided <c>referenceValue</c> parameter is completely out of physiological bounds.</exception>
        /// <exception cref="ArgumentNullException">If any provided parameter (such ase the <c>referenceValue</c>) is unexpectedly <c>null</c>.</exception>
        /// <exception cref="VitalParameterNullReferenceException">If a <c>VitalParameter</c> references another <c>VitalParameter</c> and this reference is unavailable.</exception>
        /// <exception cref="CreateProtectedPatientModelException">If this <c>PatientModel</c> is not marked alterable.</exception>
        public PatientModel(PatientInterface patientParameters)
        {
            hiddenIsAlterable = patientParameters.isAlterable ?? throw new NullReferenceException("PatientInterface.isAlterable");
            uuid = patientParameters.uuid ?? throw new NullReferenceException("PatientInterface.uuid");
            hiddenRoleModelUuid = patientParameters.roleModelUuid ?? throw new NullReferenceException("PatientInterface.roleModelUuid");
            hiddenPatientName = patientParameters.patientName ?? throw new NullReferenceException("PatientInterface.patientName");
            hiddenOwnerUuid = patientParameters.owner?.Uuid ?? throw new NullReferenceException("PatientInterface.owner");
            hiddenViewerRights = patientParameters.viewerRights?.Right ?? throw new NullReferenceException("PatientInterface.viewerRights.Right");
            hiddenGender = patientParameters.gender ?? PatientGender.Unknown;
            createdAt = patientParameters.createdAt;

            if (!isAlterable)
            {
                throw new CreateProtectedPatientModelException("Tried to create a patient model that is not marked alterable. The patient model in question might be a role model that cannot be used in this context.");
            }

            List<VitalParameterInterface> iVitalParmeter = new List<VitalParameterInterface>(patientParameters.vitalParameters);
            List<VitalParameter> parameters = new List<VitalParameter>();
            while(iVitalParmeter.Count != 0)  // First, create all VPs then associate them with different organs
            {
                createVitalParameter(ref iVitalParmeter, ref parameters);
            }
            foreach(OrganInterface iOrgan in patientParameters.organs)
            {
                List<VitalParameter> filteredParameters = parameters.FindAll( item => isMemberOfOrgan(item, iOrgan.type) );
                organs.Add(createOrgan(iOrgan, filteredParameters));
            }
            if(patientParameters.ICD10Impairments != null)
            {
                foreach(ICD10ImpairmentInterface iICD10 in patientParameters.ICD10Impairments)
                {
                    if(!ICD10Impairments.Exists(item => item.Code.Equals(iICD10.code)))   // Create ICD10 only when not exists
                    {
                        ICD10Impairments.Add(createICD10Impairment(iICD10));
                    }
                    else
                    {
                        Logger.log(LogLevel.WARNING, String.Format("ICD10 parameter with code {0} not created, because it already exists.", iICD10.code), LogSource.GENERAL);
                    }
                }
            }
        }

        #region Create Organ and VitalParameter instances
        private void createVitalParameter(ref List<VitalParameterInterface> iVP, ref List<VitalParameter> parameters)
        {
            int i = 0;  // Fix the index, it is the first position and every item will move into place
            List<VitalParameterInterface> queryableVP = new List<VitalParameterInterface>(iVP);
            if (parameters.Exists(item => item.type.Equals(queryableVP[i].type)))
            {
                iVP.RemoveAt(i);
            }
            else
            {
                switch (iVP[i].type)  // in here: make sure to pass all required objects, skip an element that is not ready yet if neccessary
                {
                    case VitalParameterType.SystolicBloodPressure:
                        if(parameters.Exists(item => item.type.Equals(VitalParameterType.DiastolicBloodPressure)))
                        {   // This wont ever be the result, as both syst. and diast. are creted at once.
                            SystolicBloodPressure systolicPressure = new SystolicBloodPressure(iVP[i].referenceValue, (DiastolicBloodPressure)parameters.Find(item => item.type.Equals(VitalParameterType.DiastolicBloodPressure)), allowedContinuousValues());
                            parameters.Add(systolicPressure);
                            systolicPressure.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        }
                        else if(iVP.Exists(item => item.type.Equals(VitalParameterType.DiastolicBloodPressure)))
                        {   // This branch will be called at correct providance
                            DiastolicBloodPressure diastolicPressure;
                            SystolicBloodPressure systolicPressure = new SystolicBloodPressure(iVP[i].referenceValue, out diastolicPressure, iVP.Find(item => item.type.Equals(VitalParameterType.DiastolicBloodPressure)).referenceValue, allowedContinuousValues());
                            parameters.Add(systolicPressure);
                            parameters.Add(diastolicPressure);
                            diastolicPressure.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                            systolicPressure.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                            iVP.RemoveAll(item => item.type.Equals(VitalParameterType.DiastolicBloodPressure));
                        }
                        else 
                        {   // If the diastolic pressure information is not inside the interface list
                            throw new VitalParameterNullReferenceException("Missing a reference blood pressure type.", VitalParameterType.DiastolicBloodPressure);
                        }
                        iVP.RemoveAt(i);
                        break;

                    case VitalParameterType.DiastolicBloodPressure:
                        if (parameters.Exists(item => item.type.Equals(VitalParameterType.SystolicBloodPressure)))
                        {   // This wont ever be the result, as both syst. and diast. are creted at once.
                            DiastolicBloodPressure diastolicPressure = new DiastolicBloodPressure(iVP[i].referenceValue, (SystolicBloodPressure)parameters.Find(item => item.type.Equals(VitalParameterType.SystolicBloodPressure)), allowedContinuousValues());
                            parameters.Add(diastolicPressure);
                            diastolicPressure.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        }
                        else if (iVP.Exists(item => item.type.Equals(VitalParameterType.SystolicBloodPressure)))
                        {   // This branch will always be called
                            SystolicBloodPressure systolicPressure;
                            DiastolicBloodPressure diastolicPressure = new DiastolicBloodPressure(iVP[i].referenceValue, out systolicPressure, iVP.Find(item => item.type.Equals(VitalParameterType.SystolicBloodPressure)).referenceValue, allowedContinuousValues());
                            parameters.Add(diastolicPressure);
                            parameters.Add(systolicPressure);
                            diastolicPressure.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                            systolicPressure.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                            iVP.RemoveAll(item => item.type.Equals(VitalParameterType.SystolicBloodPressure));
                        }
                        else
                        {   // If the diastolic pressure information is not inside the interface list
                            throw new VitalParameterNullReferenceException("Missing a reference blood pressure type.", VitalParameterType.SystolicBloodPressure);
                        }
                        iVP.RemoveAt(i);
                        break;

                    case VitalParameterType.MeanBloodPressure:
                        SystolicBloodPressure systolicPressureForM = (SystolicBloodPressure)parameters.Find(item => (item.type.Equals(VitalParameterType.SystolicBloodPressure)));
                        DiastolicBloodPressure diastolicPressureForM;
                        if (systolicPressureForM != null)   // Enhances the query (searching for one before), bec. neither of those can exist without each other and if they exist, one is already found
                        {
                            diastolicPressureForM = (DiastolicBloodPressure)parameters.Find(item => (item.type.Equals(VitalParameterType.DiastolicBloodPressure)));
                            MeanBloodPressure meanPressure = new MeanBloodPressure(iVP[i].referenceValue, systolicPressureForM, diastolicPressureForM, allowedContinuousValues());
                            parameters.Add(meanPressure);
                            meanPressure.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        }
                        else if(iVP.Exists(item => item.type.Equals(VitalParameterType.SystolicBloodPressure)) && iVP.Exists(item => item.type.Equals(VitalParameterType.DiastolicBloodPressure)))
                        {
                            systolicPressureForM = new SystolicBloodPressure(iVP.Find(item => item.type.Equals(VitalParameterType.SystolicBloodPressure)).referenceValue, out diastolicPressureForM, iVP.Find(item => item.type.Equals(VitalParameterType.DiastolicBloodPressure)).referenceValue, allowedContinuousValues());
                            MeanBloodPressure meanPressure = new MeanBloodPressure(iVP[i].referenceValue, systolicPressureForM, diastolicPressureForM, allowedContinuousValues());
                            parameters.Add(meanPressure);
                            parameters.Add(systolicPressureForM);
                            parameters.Add(diastolicPressureForM);
                            meanPressure.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                            systolicPressureForM.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                            diastolicPressureForM.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                            iVP.RemoveAll(item => item.type.Equals(VitalParameterType.SystolicBloodPressure));
                            iVP.RemoveAll(item => item.type.Equals(VitalParameterType.DiastolicBloodPressure));
                        }
                        else
                        {   // If the diastolic pressure information is not inside the interface list
                            throw new VitalParameterNullReferenceException("Missing the systolic and diastolic blood pressure type.");
                        }
                        iVP.RemoveAt(i);
                        break;

                    case VitalParameterType.BreathRate:
                        BreathRate breath = new BreathRate(iVP[i].referenceValue, allowedContinuousValues());
                        parameters.Add(breath);  // Null check is done inside the VitalParameter class
                        breath.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        iVP.RemoveAt(i);
                        break;

                    case VitalParameterType.CapillarBloodGlucose:
                        CapillarGlucoseLevel glucoseLevel = new CapillarGlucoseLevel(iVP[i].referenceValue, allowedContinuousValues());
                        parameters.Add(glucoseLevel);
                        glucoseLevel.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        iVP.RemoveAt(i);
                        break;

                    case VitalParameterType.CoreTemperature:
                        CoreTemperature coreTemp = new CoreTemperature(iVP[i].referenceValue, allowedContinuousValues());
                        parameters.Add(coreTemp);
                        coreTemp.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        iVP.RemoveAt(i);
                        break;

                    case VitalParameterType.Height:
                        Height height = new Height(iVP[i].referenceValue, allowedContinuousValues());
                        parameters.Add(height);
                        height.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        iVP.RemoveAt(i);
                        break;

                    case VitalParameterType.Weight:
                        Weight weight = new Weight(iVP[i].referenceValue, allowedContinuousValues());
                        parameters.Add(weight);
                        weight.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        iVP.RemoveAt(i);
                        break;

                    case VitalParameterType.VenousBloodCarbondioxidePressure:
                        BloodCarbondioxidePressure carbondioxidePress = new BloodCarbondioxidePressure(iVP[i].referenceValue, allowedContinuousValues());
                        parameters.Add(carbondioxidePress);
                        carbondioxidePress.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        iVP.RemoveAt(i);
                        break;

                    case VitalParameterType.VenousBloodOxygenPressure:
                        BloodOxygenPressure oxygenPress = new BloodOxygenPressure(iVP[i].referenceValue, allowedContinuousValues());
                        parameters.Add(oxygenPress);
                        oxygenPress.continuousValueCreated += new VitalParameter.VitalParameterEventHandler(continuousValueCreatedDelegate);
                        iVP.RemoveAt(i);
                        break;
                    default:
                        throw new InvalidOperationException("VitalParameterInterface.type ran out of options, make sure all VP types are specified when creating the PatientModel.");

                }
            }
        }

        private Organ createOrgan(OrganInterface iOrgan, List<VitalParameter> filteredVPforOrgan)
        {
            switch (iOrgan.type)
            {
                case OrganType.Body:
                    return new Body(filteredVPforOrgan, iOrgan.uuid.Value);  // Null check is done inside the Organ class

                case OrganType.CapillarBlood:
                    return new Blood(OrganType.CapillarBlood, filteredVPforOrgan, iOrgan.uuid.Value, Global.AllowedVPsInOrgans[OrganType.CapillarBlood]);

                case OrganType.Lung:
                    return new Lung(filteredVPforOrgan, iOrgan.uuid.Value);

                case OrganType.PeripheralArtery:
                    return new BloodVessel(OrganType.PeripheralArtery, filteredVPforOrgan, iOrgan.uuid.Value, Global.AllowedVPsInOrgans[OrganType.PeripheralArtery]);

                case OrganType.PeripheralVein:
                    return new BloodVessel(OrganType.PeripheralVein, filteredVPforOrgan, iOrgan.uuid.Value, Global.AllowedVPsInOrgans[OrganType.PeripheralVein]);

                case OrganType.VenousBlood:
                    return new Blood(OrganType.VenousBlood, filteredVPforOrgan, iOrgan.uuid.Value, Global.AllowedVPsInOrgans[OrganType.VenousBlood]);
            }
            throw new InvalidOperationException("OrganInterface.type ran out of options, make sure all Organ types are specified when creating the PatientModel.");
        }

        private ICD10Impairment createICD10Impairment(ICD10ImpairmentInterface iICD10)
        {
            Dictionary<VitalParameterType, ICD10ImpairedValues> impairedValueList = new Dictionary<VitalParameterType, ICD10ImpairedValues>();

            foreach(ICD10ImpairedValueInterface iValue in iICD10.impairedVitalParameters)
            {
                if (!impairedValueList.ContainsKey(iValue.type))
                {
                    try
                    {
                        impairedValueList.Add(iValue.type, new ICD10ImpairedValues(iValue.referenceValue, iValue.impairedValue));
                    }
                    catch(ArgumentNullException e) // Catch also an argument null Exception
                    {
                        Logger.log(LogLevel.ERROR, String.Format("At {0} failed to create the ICD10ImpairedValues for ICD10 code {1} for vital parameter type {2}", e.ParamName, iICD10.code, iValue.type.ToString()), LogSource.GENERAL);
                    }
                }
                else
                {
                    Logger.log(LogLevel.ERROR, String.Format("This VitalParameter {0} is already affected by this ICD10Code ({1}) and is therefore not created.", iValue.type, ToString(), iICD10.code), LogSource.GENERAL);
                }
            }

            return new ICD10Impairment(iICD10.code, impairedValueList);
        }

        private static bool isMemberOfOrgan(VitalParameter vp, OrganType Otype)  // Check, if the given Vital parameter is defined to be a member of the given Organtype. Intended to be used by List<VitalParameter>.Find()
        {
            VitalParameterType[] associatedVP;
            if(Global.AllowedVPsInOrgans.TryGetValue(Otype, out associatedVP))
            {
                return associatedVP.Contains(vp.type);
            }
            
            return false;
        }

        /// <summary>
        /// Determines through the rights and logged in user if continuous values can be created
        /// </summary>
        /// <returns></returns>
        private bool allowedContinuousValues()
        {
            return (hiddenViewerRights == ViewerRightsType.Writing) || (GUI.LoginSystem.UserAuthentificationToken != null ? (GUI.LoginSystem.UserAuthentificationToken.Uuid == hiddenOwnerUuid) : false);
        }
        #endregion

        #region Setter and Getter Methods
        public void setIsAlterable(bool value)
        {
            try
            {
                DBAccess db = new DBAccess();
                db.WritePatientInformation(new PatientInterface(uuid, null, null, null, null, isAlterable, null, null, null, null, null));
                Logger.log(LogLevel.INFO, $"Wrote new isAlterable value for patient with UUID:{uuid} to value {value} to Demographic and Vital Database at {Global.DatabaseHost}.", LogSource.GENERAL);
                hiddenIsAlterable = value;
            }
            catch(PostgresException e)
            {
                string partMessage = "";
                if(e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                {
                    InvokeFailedDBConnectionEvent(this, new EventArgs());
                    partMessage = $" Failed to resolve the Database Server at {Global.DatabaseHost}.";
                }
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.isAlterable to the Database.{partMessage}\n{e.Message}", LogSource.GENERAL);
            }
            catch (ArgumentNullException)
            {
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.isAlterable to the Database because of faulty parameters.", LogSource.GENERAL);
            }
        }

        public void setPatientName(string value)
        {
            try
            {
                DBAccess db = new DBAccess();
                db.WritePatientInformation(new PatientInterface(uuid, null, null, value, null, null, null, null, null, null, null));
                Logger.log(LogLevel.INFO, $"Wrote new patientName value for patient with UUID:{uuid} to value {value} to Demographic and Vital Database at {Global.DatabaseHost}.", LogSource.GENERAL);
                hiddenPatientName = value;
            }
            catch (PostgresException e)
            {
                string partMessage = "";
                if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                {
                    InvokeFailedDBConnectionEvent(this, new EventArgs());
                    partMessage = $" Failed to resolve the Database Server at {Global.DatabaseHost}.";
                }
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.patientName to the Database.{partMessage}\n{e.Message}", LogSource.GENERAL);
            }
            catch (ArgumentNullException)
            {
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.patientName to the Database because of faulty parameters.", LogSource.GENERAL);
            }
        }

        public void setGender(PatientGender value)
        {
            try
            {
                DBAccess db = new DBAccess();
                db.WritePatientInformation(new PatientInterface(uuid, null, null, null, value, null, null, null, null, null, null));
                Logger.log(LogLevel.INFO, $"Wrote new gender value for patient with UUID:{uuid} to value {value} to Demographic and Vital Database at {Global.DatabaseHost}.", LogSource.GENERAL);
                hiddenGender = value;
            }
            catch (PostgresException e)
            {
                string partMessage = "";
                if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                {
                    InvokeFailedDBConnectionEvent(this, new EventArgs());
                    partMessage = $" Failed to resolve the Database Server at {Global.DatabaseHost}.";
                }
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.gender to the Database.{partMessage}\n{e.Message}", LogSource.GENERAL);
            }
            catch (ArgumentNullException)
            {
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.gender to the Database because of faulty parameters.", LogSource.GENERAL);
            }
        }

        public void setOwnerUuid(Guid? value)
        {
            try
            {
                DBAccess db = new DBAccess();
                db.WritePatientInformation(new PatientInterface(uuid, null, null, null, null, null, new UserInterface(value, null, null, null, null, null), null, null, null, null));
                Logger.log(LogLevel.INFO, $"Wrote new ownerName value for patient with UUID:{uuid} to value {value} to Demographic and Vital Database at {Global.DatabaseHost}.", LogSource.GENERAL);
                hiddenOwnerUuid = value;
            }
            catch (PostgresException e)
            {
                string partMessage = "";
                if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                {
                    InvokeFailedDBConnectionEvent(this, new EventArgs());
                    partMessage = $" Failed to resolve the Database Server at {Global.DatabaseHost}.";
                }
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.owner to the Database.{partMessage}\n{e.Message}", LogSource.GENERAL);
            }
            catch (ArgumentNullException)
            {
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.owner to the Database because of faulty parameters.", LogSource.GENERAL);
            }
        }

        public void setViewerRights(ViewerRightsType value)
        {
            try
            {
                DBAccess db = new DBAccess();
                db.WritePatientInformation(new PatientInterface(uuid, null, null, null, null, null, null, new ViewerRightsInterface(value, null), null, null, null));
                Logger.log(LogLevel.INFO, $"Wrote new ownerName value for patient with UUID:{uuid} to value {value} to Demographic and Vital Database at {Global.DatabaseHost}.", LogSource.GENERAL);
                hiddenViewerRights = value;
            }
            catch (PostgresException e)
            {
                string partMessage = "";
                if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                {
                    InvokeFailedDBConnectionEvent(this, new EventArgs());
                    partMessage = $" Failed to resolve the Database Server at {Global.DatabaseHost}.";
                }
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.viewerRights to the Database.{partMessage}\n{e.Message}", LogSource.GENERAL);
            }
            catch (ArgumentNullException)
            {
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.viewerRights to the Database because of faulty parameters.", LogSource.GENERAL);
            }
        }

        public void setRoleModelUuid(Guid? value)
        {
            try
            {
                DBAccess db = new DBAccess();
                db.WritePatientInformation(new PatientInterface(uuid, null, value, null, null, null, null, null, null, null, null));
                Logger.log(LogLevel.INFO, $"Wrote new roleModelUUID value for patient with UUID:{uuid} to value {value} to Demographic and Vital Database at {Global.DatabaseHost}.", LogSource.GENERAL);
                hiddenRoleModelUuid = value;
            }
            catch (PostgresException e)
            {
                string partMessage = "";
                if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                {
                    InvokeFailedDBConnectionEvent(this, new EventArgs());
                    partMessage = $" Failed to resolve the Database Server at {Global.DatabaseHost}.";
                }
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.roleModelUUID to the Database.{partMessage}\n{e.Message}", LogSource.GENERAL);
            }
            catch (ArgumentNullException)
            {
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.roleModelUUID to the Database because of faulty parameters.", LogSource.GENERAL);
            }
        }

        /// <summary>
        /// Writes a new reference value to a specific vital parameter.
        /// </summary>
        /// <param name="value">The new reference value</param>
        /// <param name="vitalParameter">The type of the vital parameter in question</param>
        /// <exception cref="ArgumentException">If the input parameter <c>vitalParmeter</c> is not valid.</exception>
        public void setReferenceValue(float value, VitalParameterType vitalParameter)
        {
            OrganType Otype = getOrganOfVitalParameter(vitalParameter);
            try
            {
                getOrgan(Otype).getVitalParameter(vitalParameter).setReferenceValue(value);
                DBAccess db = new DBAccess();
                db.WriteReferenceValue(new VitalParameterInterface(vitalParameter, value, null), new OrganInterface(Otype, getOrgan(Otype).uuid));
                Logger.log(LogLevel.INFO, $"Wrote new reference value for patient with UUID:{uuid} to value {value} to Demographic and Vital Database at {Global.DatabaseHost}.", LogSource.GENERAL);
            }
            catch (PostgresException e)
            {
                string partMessage = "";
                if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                {
                    InvokeFailedDBConnectionEvent(this, new EventArgs());
                    partMessage = $" Failed to resolve the Database Server at {Global.DatabaseHost}.";
                }
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.referenceValue of {vitalParameter} with a value of {value} to the Database.{partMessage}\n{e.Message}", LogSource.GENERAL);
            }
            catch (ArgumentNullException)
            {
                Logger.log(LogLevel.WARNING, $"Failed to update PatientModel.roleModelUUID of {vitalParameter} with a value of {value} to the Database because of faulty parameters.", LogSource.GENERAL);
            }
        }

        /// <summary>
        /// Returning the current reference value of a specific vital parameter.
        /// </summary>
        /// <param name="vitalParameter">The type of the vital parameter in question</param>
        /// <returns>The current <c>referenceValue</c> of this <c>VitalParmeter</c></returns>
        /// <exception cref="ArgumentException">If the input parameter <c>vitalParmeter</c> is not valid.</exception>
        public float getReferenceValue(VitalParameterType vitalParameter)
        {
            return getOrgan(getOrganOfVitalParameter(vitalParameter)).getVitalParameter(vitalParameter).referenceValue;
        }

        /// <summary>
        /// Search for all continuously created vital values in a specific vital parameter for a specific time span.
        /// </summary>
        /// <param name="pastSeconds">The absolute time span (until now) to search for</param>
        /// <param name="vitalParameter">The type of the vital parameter in question</param>
        /// <returns>All continuous vital values created in the given time span until now.</returns>
        /// <exception cref="ArgumentException">If the input parameter <c>vitalParmeter</c> is not valid.</exception>
        /// <exception cref="NonExistentContinuousValueException">If the creation of continuous vital values are disabled by default</exception>
        public List<CurrentVitalValue> getContinuousValues(int pastSeconds, VitalParameterType vitalParameter)
        {
            return getOrgan(getOrganOfVitalParameter(vitalParameter)).getVitalParameter(vitalParameter).getContinuousValues(pastSeconds);
        }

        /// <summary>
        /// Search for the last continuously created vital value (or <c>null</c> of none exists) in a specific vital parameter.
        /// </summary>
        /// <param name="vitalParameter">The type of the vital parameter in question</param>
        /// <returns>The newest created continuous vital value, or <c>null</c> of none exists</returns>
        /// <exception cref="ArgumentException">If the input parameter <c>vitalParmeter</c> is not valid.</exception>
        /// <exception cref="NonExistentContinuousValueException">If the creation of continuous vital values are disabled by default</exception>
        public CurrentVitalValue getLastContinuousValue(VitalParameterType vitalParameter)
        {
            return getOrgan(getOrganOfVitalParameter(vitalParameter)).getVitalParameter(vitalParameter).getLastContinuousValue();
        }

        /// <summary>
        /// Retrieving the static uuid from an specific organ (as safed in the Demografic and Vital Database)
        /// </summary>
        /// <param name="organ">The type of the organ in question</param>
        /// <returns>The 36 character hexadecimal coded UUID after norm RFC 4122</returns>
        /// <exception cref="ArgumentException">If the input parameter <c>organ</c> is not valid.</exception>
        public Guid getOrganUUID(OrganType organ)
        {
            return getOrgan(organ).uuid;
        }

        public PatientInterface getPatientInterface()
        {
            PatientInterface face = new PatientInterface(uuid, createdAt, roleModelUuid, patientName, Gender, isAlterable, new UserInterface(ownerUuid, null, null, null, null, null), 
                new ViewerRightsInterface(ViewerRights, null), new List<VitalParameterInterface>(), new List<OrganInterface>(), null);
            foreach(Organ organ in organs)
            {
                face.organs.Add(new OrganInterface(organ.type, organ.uuid));
                foreach(VitalParameter vp in organ.vitalParameters)
                {
                    List<CurrentVitalValue> copyContinuousValues = null;
                    lock (Locks.ThreadCalculationLocks[3])
                    {
                        copyContinuousValues = new List<CurrentVitalValue>(vp.continuousValue);  // Create this to work with a copy of the List, therefor no error will be thrown when teh list is changed during this operation
                    }
                    face.vitalParameters.Add(new VitalParameterInterface(vp.type, vp.referenceValue, copyContinuousValues));
                }
            }

            return face;
        }

        private OrganType getOrganOfVitalParameter(VitalParameterType VPtype)
        {
            if (!Global.AllowedVPsInOrgans.ToList().Exists(item => item.Value.Contains(VPtype)))
            {
                throw new ArgumentException("Failed to write a reference Value. The Vital Parameter type is not present in any Organ.");
            }
            return Global.AllowedVPsInOrgans.First(item => item.Value.Contains(VPtype)).Key;
        }
        private Organ getOrgan(OrganType type)
        {
            if(!organs.Exists(item => item.type.Equals(type)))
            {
                throw new ArgumentException("The specified Organ is not present for this patient.");
            }
            return organs.Find(item => item.type.Equals(type));
        }
        #endregion

        #region Events
        public delegate void VitalParmeterEventHandler(object source, VitalParameterEventArgs e);

        /// <summary>
        /// This event is invoked whenever any <c>VitalParameter</c> creates a new continuous value. The passed <c>source</c> is allways a <c>CurrentVitalValue</c> object, representing the created value.
        /// </summary>
        public event VitalParmeterEventHandler continuousValueCreated;
        public event EventHandler FailedDBConnection;  // invoked only when any DB requests fails with an MySqlException. Will be caught by Form1

        private void continuousValueCreatedDelegate(object source, VitalParameterEventArgs e)
        {
            /* Write only to DB if rights are set to writing or the logged in user is the owner of the PatientModel */
            if(ownerUuid == GUI.LoginSystem.UserAuthentificationToken.Uuid || 
                ViewerRights == ViewerRightsType.Writing)
            {
                DBAccess db = new DBAccess();
                CurrentVitalValue cvv = (CurrentVitalValue)source;
                try
                {
                    List<CurrentVitalValue> writeableCVV = new List<CurrentVitalValue>();
                    writeableCVV.Add(cvv);
                    Organ correspOrgan = getOrgan(Global.AllowedVPsInOrgans.First(item => item.Value.Contains(cvv.type)).Key);
                    db.CreateContinuousValues(new VitalParameterInterface(cvv.type, null, writeableCVV), new OrganInterface(correspOrgan.type, correspOrgan.uuid));
                    Logger.log(LogLevel.INFO, $"Wrote newest currentVitalValue of {cvv.type} with a value of {cvv.value} created at {cvv.timestamp.ToString("yyyy.MM.dd HH:mm:ss")} to Demographic and Vital Database at {Global.DatabaseHost}.", LogSource.VITALPARAMETER);
                }
                catch (PostgresException exp)
                {
                    string partMessage = "";
                    if (exp.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                    {
                        InvokeFailedDBConnectionEvent(this, new EventArgs());
                        partMessage = $" Failed to resolve the Database Server at {Global.DatabaseHost}.";
                    }
                    Logger.log(LogLevel.WARNING, $"Failed to insert newest currentVitalValue of {cvv.type} with a value of {cvv.value} created at {cvv.timestamp.ToString("yyyy.MM.dd HH:mm:ss")} to the Database.{partMessage}\n{exp.Message}", LogSource.VITALPARAMETER);
                }
                catch (ArgumentNullException)
                {
                    Logger.log(LogLevel.WARNING, $"Failed to insert newest currentVitalValue of {cvv.type} with a value of {cvv.value} created at {cvv.timestamp.ToString("yyyy.MM.dd HH:mm:ss")} to the Database because of faulty parameters.", LogSource.VITALPARAMETER);
                }
            }

            continuousValueCreated?.Invoke(source, e);
        }
        private void InvokeFailedDBConnectionEvent(object source, EventArgs e)
        {
            FailedDBConnection?.Invoke(source, e);
        }
        #endregion

        /// <summary>
        /// Abort all Threads in any <c>Organ</c> and <c>VitalParameter</c> to prepare a clean exit on the patient model.
        /// </summary>
        public void ThreadAbort()
        {
            foreach(Organ item in organs)
            {
                item.ThreadAbort();
            }
        }
        public void DirtyThreadAbort()
        {
            foreach (Organ item in organs)
            {
                item.DirtyThreadAbort();
            }
        }
    }
}
