﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Globalization;
using Npgsql;


using PatientSimulator.Patient.DatabaseRequestInterface;
using PatientSimulator.VitalParameters;
using PatientSimulator.Organs;
using PatientSimulator.Exceptions;

namespace PatientSimulator.Patient
{
    class DBAccess
    {
        private class DBConnection : IDisposable
        {
            private NpgsqlConnection connection;
            private string connectionString;

            /// <summary>
            /// Establish a connection to the postgreSQL Database to perform queries. Safe handle in <c>using</c> statements.
            /// </summary>
            /// <exception cref="ArgumentException"></exception>
            /// <exception cref="NpgsqlException"></exception>
            public DBConnection()
            {
                Initialize();
            }

            private void Initialize()
            {
                connectionString = $"Host={Global.DatabaseHost};Username={Global.DatabaseUserInformation.Name};Password={Global.DatabaseUserInformation.Password};Database={Global.DatabaseName}";
                //connectionString = "Host=db-n1.ls.technikum-wien.at;Username=LSE_G1;Password=T50ztvGhql0NWIbR5J5fXs86;Database=G1";
                //connectionString = "Host=db-n1.ls.technikum-wien.at;Username=be18b003;Password=ACsarmann913;Database=LSE-G1";

                connection = new NpgsqlConnection(connectionString);
                OpenConnection();
            }
            /// <summary>
            /// Open a connection to the specified postgreSQL database
            /// </summary>
            /// <returns></returns>
            private bool OpenConnection()
            {
                try
                {
                    connection.Open();  // Exceptions will cause the code break, catches expected on higher levels.
                    return true;
                }
                catch (System.Net.Sockets.SocketException e)
                {
                    throw new PostgresException("Connection failed.", "", "", "0800");
                }
                catch (Exception e)
                {
                    connection?.Close();
                    throw e;
                }
            }
            /// <summary>
            /// Close the connection to the postgreSQL database.
            /// </summary>
            /// <returns>true, if the connection is successfully closed.</returns>
            /// <exception cref="NpgsqlException"></exception>
            private bool CloseConnection()
            {
                try
                {
                    connection?.Close();
                    return true;
                }
                catch (NpgsqlException ex)
                {
                    throw ex;
                }
            }

            #region  Insert Statements

            /// <summary>
            /// Insert basic patient data and organ uuids (provided the organs which are referenced do already exist) for a pre-existent user
            /// </summary>
            /// <param name="pFI">Interface containing all patient data with a user as owner (containing at least a valid UUID) and a populated list of organs with valid (pre-existent) UUIDs.</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void InsertBasicPatientData(PatientInterface patientI)
            {
                string query;
                NpgsqlCommand command;

                if (patientI.uuid == null || patientI.owner?.Uuid == null || patientI.patientName == null || patientI.isAlterable == null || patientI.viewerRights == null)
                {
                    throw new ArgumentNullException();
                }

                query = String.Format("INSERT INTO \"patientsimulator\".\"patient\" (\"uuid\", \"model_name\", \"owner_uuid\", \"rights_id\", \"is_alterable\", \"parent_uuid\", {0}{1}{2}",
                        "\"peripheral_vein_uuid\", \"venous_blood_uuid\", \"capillary_blood_uuid\", \"body_uuid\", \"lung_uuid\", \"peripheral_artery_uuid\", \"gender\") ",
                    "VALUES (@uuid, @model_name, @owner_uuid, @rights_id, @is_alterable, @parent_uuid, ",
                        "@peripheral_vein_uuid, @venous_blood_uuid, @capillary_blood_uuid, @body_uuid, @lung_uuid, @peripheral_artery_uuid, @gender);");

                // Connection is already opened with the instantiation of this class
                // Try/catch is disgarded, exception handling is an objective of higher instances.
                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", patientI.uuid.Value));
                    command.Parameters["uuid"].NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;
                    command.Parameters.Add(new NpgsqlParameter<string>("model_name", patientI.patientName));
                    command.Parameters["model_name"].NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    command.Parameters.Add(new NpgsqlParameter<Guid>("owner_uuid", patientI.owner.Uuid.Value));
                    command.Parameters["owner_uuid"].NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;
                    command.Parameters.Add(new NpgsqlParameter<int>("rights_id", Global.TranslateViewerRightsType2DBInteger[patientI.viewerRights.Right]));
                    command.Parameters["rights_id"].NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;
                    command.Parameters.Add(new NpgsqlParameter<bool>("is_alterable", patientI.isAlterable == true));
                    command.Parameters["is_alterable"].NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                    command.Parameters.Add(new NpgsqlParameter<Guid>("parent_uuid", patientI.roleModelUuid.Value));
                    command.Parameters["parent_uuid"].NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;
                    command.Parameters.Add(new NpgsqlParameter<string>("gender", Global.TranslateGender2DBString[patientI.gender ?? PatientGender.Unknown]));
                    command.Parameters["gender"].NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;

                    command.Parameters.Add(new NpgsqlParameter<Guid>("peripheral_vein_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters.Add(new NpgsqlParameter<Guid>("venous_blood_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters.Add(new NpgsqlParameter<Guid>("capillary_blood_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters.Add(new NpgsqlParameter<Guid>("body_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters.Add(new NpgsqlParameter<Guid>("lung_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters.Add(new NpgsqlParameter<Guid>("peripheral_artery_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    /* all organ uuids remain NULL, until filled with values */

                    foreach (OrganInterface item in patientI.organs.FindAll(item => item.uuid != null))
                    {
                        /* Add uuids for all organs which have one defined */
                        switch (item.type)
                        {
                            case OrganType.Body:
                                command.Parameters["body_uuid"].Value = item.uuid.Value;
                                break;
                            case OrganType.CapillarBlood:
                                command.Parameters["capillary_blood_uuid"].Value = item.uuid.Value;
                                break;
                            case OrganType.Lung:
                                command.Parameters["lung_uuid"].Value = item.uuid.Value;
                                break;
                            case OrganType.PeripheralArtery:
                                command.Parameters["peripheral_artery_uuid"].Value = item.uuid.Value;
                                break;
                            case OrganType.PeripheralVein:
                                command.Parameters["peripheral_vein_uuid"].Value = item.uuid.Value;
                                break;
                            case OrganType.VenousBlood:
                                command.Parameters["venous_blood_uuid"].Value = item.uuid.Value;
                                break;
                        }
                    }

                    command.ExecuteNonQuery();   /* At this execution, the exceptions might be thrown */
                }
            }

            /// <summary>
            /// Insert a new organ (into organ table and the referencing derived organ table)
            /// </summary>
            /// <param name="organI">Organ to insert, <c>uuid</c> required</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void InsertOrganValue(OrganInterface organI)
            {
                string organBaseQuery;
                string organDerivedQuery;
                NpgsqlCommand command;

                if (organI.uuid == null)
                {
                    throw new ArgumentNullException();
                }

                organBaseQuery = "INSERT INTO \"patientsimulator\".\"organs\" (\"uuid\") VALUES (@uuid);";
                //), String.Format("INSERT INTO {3}(uuid{1}) VALUES(UUID_TO_BIN('{0}'){2});", OIF.uuid, OIF.type.Equals(OrganType.PeripheralVein) ? ",venous_blood_uuid" : "", OIF.type.Equals(OrganType.PeripheralVein) ? $",NULL" : "", Global.TranslateOType2DemographDB[OIF.type]) };
                organDerivedQuery = $"INSERT INTO \"patientsimulator\".\"{ Global.TranslateOType2DemographDB[organI.type] }\" (\"uuid\") VALUES (@uuid);";

                /* Execute inserts on base organ and derived organs sequencially, a fail on higher instances 
                 * will throw an exception and therefore prevent SQL query errors onlower instances */
                using (command = new NpgsqlCommand(organBaseQuery, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["uuid"].Value = organI.uuid.Value;

                    command.ExecuteNonQuery();
                }

                using (command = new NpgsqlCommand(organDerivedQuery, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["uuid"].Value = organI.uuid.Value;

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Insert a vitalparameter entry and value, linked to an organ and LIONC-Code as referenced by ELGA
            /// </summary>
            /// <param name="vitalI">Vitalparameter to insert, <c>type</c> and <c>creferenceValue</c> required</param>
            /// <param name="organI">Organ linked to this vitalparameter (<c>uuid</c> required). Has to already exist in linked tables</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void InsertReferenceValue(VitalParameterInterface vitalI, OrganInterface organI)
            {
                string query;
                NpgsqlCommand command;

                if (vitalI.referenceValue == null || organI.uuid == null)
                {
                    throw new ArgumentNullException();
                }

                query = $"INSERT INTO \"patientsimulator\".\"reference_vitalparameter\" (\"organ_uuid\",\"vitalparameter_code\",\"parameter_value\") VALUES (@organ_uuid, @vitalparameter_code, @parameter_value);";

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("organ_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters.Add(new NpgsqlParameter<string>("vitalparameter_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                    command.Parameters.Add(new NpgsqlParameter<float>("parameter_value", NpgsqlTypes.NpgsqlDbType.Real));
                    command.Parameters["organ_uuid"].Value = organI.uuid.Value;
                    command.Parameters["vitalparameter_code"].Value = Global.TranslateVPType2ElgaCode[vitalI.type];
                    command.Parameters["parameter_value"].Value = vitalI.referenceValue;

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Insert a link between a patient and his alement (as ICD10 coded disease)
            /// </summary>
            /// <param name="patientI">The patient corresponding to the ICD10 diagnose (field <c>uuid</c> is required)</param>
            /// <param name="diagnoseI">The diagnose in question (<c>code</c> is required)</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void InsertICD10References(PatientInterface patientI, ICD10ImpairmentInterface diagnoseI)
            {
                string query;
                NpgsqlCommand command;

                if (patientI.uuid == null || diagnoseI.code == null)
                {
                    throw new ArgumentNullException();
                }

                query = "INSERT INTO \"patientsimulator\".\"alement_of_icd10\" (\"patient_uuid\", \"icd10_code\") VALUES (@patient_uuid, @icd10_code);";

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("patient_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters.Add(new NpgsqlParameter<string>("icd10_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                    command.Parameters["patient_uuid"].Value = patientI.uuid.Value;
                    command.Parameters["icd10_code"].Value = diagnoseI.code;

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Insert a new continuously created vitalparameter with a specific timestamp
            /// </summary>
            /// <param name="vitalValue">Unambiguously identifyable vitalparmaeter value of specific vitalparameter type and timestamp</param>
            /// <param name="organI">Organ linked to this vitalparameter (<c>uuid</c> is required)</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void InsertContinuousValue(CurrentVitalValue vitalValue, OrganInterface organI)
            {
                string query;
                NpgsqlCommand command;

                if (vitalValue.timestamp == null || organI.uuid == null)
                {
                    throw new ArgumentNullException();
                }

                query = "INSERT INTO \"patientsimulator\".\"continuous_vitalparameter\" (\"organ_uuid\",\"vitalparameter_code\",\"created_at\",\"parameter_value\") VALUES (@organ_uuid, @vitalparameter_code, @created_at, @parameter_value);";

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("organ_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters.Add(new NpgsqlParameter<string>("vitalparameter_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                    command.Parameters.Add(new NpgsqlParameter<DateTime>("created_at", NpgsqlTypes.NpgsqlDbType.Timestamp));
                    command.Parameters.Add(new NpgsqlParameter<float>("parameter_value", NpgsqlTypes.NpgsqlDbType.Real));
                    command.Parameters["organ_uuid"].Value = organI.uuid.Value;
                    command.Parameters["vitalparameter_code"].Value = Global.TranslateVPType2ElgaCode[vitalValue.type];
                    command.Parameters["created_at"].Value = vitalValue.timestamp;
                    command.Parameters["parameter_value"].Value = vitalValue.value;

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Insert multiple new continuously created vitalparameters, each with a specific timestamp
            /// </summary>
            /// <param name="multipleVitalValues">Contains a collection linked continuously created vitalparmaters and thier organs (there, <c>uuid</c> is required)</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void InsertContinuousValue(List<Tuple<CurrentVitalValue, OrganInterface>> multipleVitalValues)
            {
                string query;
                string[] queryInsertParams = new string[4] { "organ_uuid", "vitalparameter_code", "created_at", "parameter_value" };
                string queryInsertValues = $"(@{queryInsertParams[0]}{"{0}"}, @{queryInsertParams[1]}{"{0}"}, @{queryInsertParams[2]}{"{0}"}, @{queryInsertParams[3]}{"{0}"})";
                string queryInsertValuesCase;
                Tuple<CurrentVitalValue, OrganInterface> vitalValueCase;
                NpgsqlCommand command;

                if ((multipleVitalValues?.Exists(item => (item == null || item.Item1 == null || item.Item2 == null || item.Item2.uuid == null)) ?? true) || multipleVitalValues.Count == 0)
                {
                    throw new ArgumentNullException();
                }

                query = "INSERT INTO \"patientsimulator\".\"continuous_vitalparameter\" (\"organ_uuid\",\"vitalparameter_code\",\"created_at\",\"parameter_value\") VALUES ";
                for (int i = 0; i < multipleVitalValues.Count; i++)
                {
                    queryInsertValuesCase = String.Format(queryInsertValues, i);
                    query = String.Format("{0}{1}{2}", query, queryInsertValuesCase, multipleVitalValues.Count == (i + 1) ? ";" : ",");
                }

                using (command = new NpgsqlCommand(query, connection))
                {
                    for (int i = 0; i < multipleVitalValues.Count; i++)
                    {
                        vitalValueCase = multipleVitalValues[i];
                        command.Parameters.Add(new NpgsqlParameter<Guid>($"{queryInsertParams[0]}{i}", NpgsqlTypes.NpgsqlDbType.Uuid));
                        command.Parameters.Add(new NpgsqlParameter<string>($"{queryInsertParams[1]}{i}", NpgsqlTypes.NpgsqlDbType.Varchar));
                        command.Parameters.Add(new NpgsqlParameter<DateTime>($"{queryInsertParams[2]}{i}", NpgsqlTypes.NpgsqlDbType.Timestamp));
                        command.Parameters.Add(new NpgsqlParameter<float>($"{queryInsertParams[3]}{i}", NpgsqlTypes.NpgsqlDbType.Real));
                        command.Parameters[$"{queryInsertParams[0]}{i}"].Value = vitalValueCase.Item2.uuid.Value;
                        command.Parameters[$"{queryInsertParams[1]}{i}"].Value = Global.TranslateVPType2ElgaCode[vitalValueCase.Item1.type];
                        command.Parameters[$"{queryInsertParams[2]}{i}"].Value = vitalValueCase.Item1.timestamp;
                        command.Parameters[$"{queryInsertParams[3]}{i}"].Value = vitalValueCase.Item1.value;
                    }

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Insert multiple new continuously created vitalparameters from the same organ, each with a specific timestamp
            /// </summary>
            /// <param name="multipleVitalValues">Contains a collection of continuously created vitalparmaters linked to one specific organs/param>
            /// <param name="organI">The organ the continuously created values are a member of (there <c>uuid</c> is required)</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void InsertContinuousValue(List<CurrentVitalValue> multipleVitalValues, OrganInterface organI)
            {
                string query;
                string[] queryInsertParams = new string[4] { "organ_uuid", "vitalparameter_code", "created_at", "parameter_value" };
                string queryInsertValues = $"(@{queryInsertParams[0]}{"{0}"}, @{queryInsertParams[1]}{"{0}"}, @{queryInsertParams[2]}{"{0}"}, @{queryInsertParams[3]}{"{0}"})";
                string queryInsertValuesCase;
                CurrentVitalValue vitalValueCase;
                NpgsqlCommand command;

                if ((multipleVitalValues?.Exists(item => (item == null)) ?? true) || multipleVitalValues.Count == 0 || organI.uuid == null)
                {
                    throw new ArgumentNullException();
                }

                query = "INSERT INTO \"patientsimulator\".\"continuous_vitalparameter\" (\"organ_uuid\",\"vitalparameter_code\",\"created_at\",\"parameter_value\") VALUES ";
                for (int i = 0; i < multipleVitalValues.Count; i++)
                {
                    queryInsertValuesCase = String.Format(queryInsertValues, i);
                    query = String.Format("{0}{1}{2}", query, queryInsertValuesCase, multipleVitalValues.Count == (i + 1) ? ";" : ",");
                }

                using (command = new NpgsqlCommand(query, connection))
                {
                    for (int i = 0; i < multipleVitalValues.Count; i++)
                    {
                        vitalValueCase = multipleVitalValues[i];
                        command.Parameters.Add(new NpgsqlParameter<Guid>($"{queryInsertParams[0]}{i}", NpgsqlTypes.NpgsqlDbType.Uuid));
                        command.Parameters.Add(new NpgsqlParameter<string>($"{queryInsertParams[1]}{i}", NpgsqlTypes.NpgsqlDbType.Varchar));
                        command.Parameters.Add(new NpgsqlParameter<DateTime>($"{queryInsertParams[2]}{i}", NpgsqlTypes.NpgsqlDbType.Timestamp));
                        command.Parameters.Add(new NpgsqlParameter<float>($"{queryInsertParams[3]}{i}", NpgsqlTypes.NpgsqlDbType.Real));
                        command.Parameters[$"{queryInsertParams[0]}{i}"].Value = organI.uuid.Value;
                        command.Parameters[$"{queryInsertParams[1]}{i}"].Value = Global.TranslateVPType2ElgaCode[vitalValueCase.type];
                        command.Parameters[$"{queryInsertParams[2]}{i}"].Value = vitalValueCase.timestamp;
                        command.Parameters[$"{queryInsertParams[3]}{i}"].Value = vitalValueCase.value;
                    }

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Insert data for an user into Demographic and Vital DB with optional in-DB-pre-existant company data or person data uuid provided.
            /// </summary>
            /// <param name="userI">The users data to insert (<c>Uuid</c>, <c>Username</c>, <c>PasswordHash</c> and <c>PassordSalt</c> required).</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentException"></exception>
            public void insertUser(UserInterface userI)
            {
                string query;
                NpgsqlCommand command;
                List<NpgsqlParameter> optionalParameters = new List<NpgsqlParameter>();     /* These Parameters are used for personell uuid or insurance Number inserts */

                if (userI.Uuid == null || userI.Username == null || userI.PasswordHash == null || userI.PasswordHash.Length == 0 ||
                    userI.PasswordSalt == null || userI.PasswordSalt.Length == 0 || (userI.PersonData == null && userI.PersonData.Uuid == null))
                {
                    throw new ArgumentNullException();
                }

                query = "INSERT INTO \"management\".\"user\" ({0}) VALUES ({1});";


                if (userI.PersonData != null)
                {
                    query = String.Format(query, "{0}, \"user_personell_uuid\"", "{1}, @user_personell_uuid");
                    optionalParameters.Add(new NpgsqlParameter<Guid>("user_personell_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    optionalParameters.Last().Value = userI.PersonData.Uuid.Value;
                }
                if (userI.InsuranceNumberHash != null && userI.InsuranceNumberHash.Length != 0)
                {
                    if (userI.InsuranceNumberHash.Length != 32)
                    {
                        /* Hash is not the expected 32 bytes long and therefore would not store in varchar(64) hex representation in DB */
                        throw new ArgumentException("Insurence number has to be 32 bytes long, as it should come from a sha256 hash", "userI.InsuranceNumberHash");
                    }
                    query = String.Format(query, "{0}, \"insurance_number_hash\"", "{1}, @insurance_number_hash");
                    optionalParameters.Add(new NpgsqlParameter<string>("insurance_number_hash", NpgsqlTypes.NpgsqlDbType.Varchar));
                    optionalParameters.Last().Value = convertByteArrayToHex(userI.InsuranceNumberHash);
                }
                query = String.Format(query, "\"uuid\", \"username\", \"password_hash\", \"pwd_salt\"", "@uuid, @username, @password_hash, @pwd_salt");

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["uuid"].Value = userI.Uuid.Value;
                    command.Parameters.Add(new NpgsqlParameter<string>("username", NpgsqlTypes.NpgsqlDbType.Varchar));
                    command.Parameters["username"].Value = userI.Username;
                    command.Parameters.Add(new NpgsqlParameter<string>("password_hash", NpgsqlTypes.NpgsqlDbType.Varchar));
                    command.Parameters["password_hash"].Value = convertByteArrayToHex(userI.PasswordHash);
                    command.Parameters.Add(new NpgsqlParameter<string>("pwd_salt", NpgsqlTypes.NpgsqlDbType.Varchar));
                    command.Parameters["pwd_salt"].Value = convertByteArrayToAscii(userI.PasswordSalt);
                    foreach (NpgsqlParameter param in optionalParameters)
                    {
                        command.Parameters.Add(param);
                    }

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Insert data for one specific personalized_data deriving table (either company_data or person_data)
            /// </summary>
            /// <param name="userI">User interface with the field <c>PersonData</c> required (of which <c>PersonData.Uuid</c> is required)</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void insertUserPersonalizedData(UserInterface userI)
            {
                string queryBasePersonalizedData;
                string queryDerivedData;
                NpgsqlCommand command;
                List<NpgsqlParameter> optionalParameters = new List<NpgsqlParameter>();

                if (userI.Uuid == null || userI.PersonData.Uuid == null)
                {
                    throw new ArgumentNullException();
                }

                queryBasePersonalizedData = "INSERT INTO \"management\".\"personalized_data\" (\"uuid\") VALUES (@uuid);";
                queryDerivedData = "INSERT INTO \"management\".{0} ({1}) VALUES ({2});";
                if (userI.PersonData.Person == PersonalizedDataType.PersonData)
                {
                    RealPersonDataInterface userPersonI = (RealPersonDataInterface)userI.PersonData;
                    queryDerivedData = String.Format(queryDerivedData, "\"person_data\"", "\"uuid\"{0}", "@uuid{1}");
                    optionalParameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    optionalParameters.Last().Value = userPersonI.Uuid.Value;
                    if (userPersonI.Firstname != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"firstname\"{0}", ", @firstname{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("firstname", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.Firstname;
                    }
                    if (userPersonI.Lastname != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"lastname\"{0}", ", @lastname{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("lastname", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.Lastname;
                    }
                    if (userPersonI.Titles != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"titles\"{0}", ", @titles{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("titles", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.Titles;
                    }
                    if (userPersonI.Email != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"email\"{0}", ", @email{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("email", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.Email;
                    }
                    if (userPersonI.Birthdate != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"birthdate\"{0}", ", @birthdate{1}");
                        optionalParameters.Add(new NpgsqlParameter<DateTime>("birthdate", NpgsqlTypes.NpgsqlDbType.Date));
                        optionalParameters.Last().Value = userPersonI.Birthdate;
                    }
                    if (userPersonI.NationalityCode != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"nationality_code\"{0}", ", @nationality_code{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("nationality_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.NationalityCode;
                    }
                    if (userPersonI.Employer != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"employer\"{0}", ", @employer{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("employer", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.Employer;
                    }
                    queryDerivedData = String.Format(queryDerivedData, "", "");
                }
                else if (userI.PersonData.Person == PersonalizedDataType.CompanyData)
                {
                    CompanyPersonDataInterface userPersonI = (CompanyPersonDataInterface)userI.PersonData;
                    queryDerivedData = String.Format(queryDerivedData, "\"company_data\"", "\"uuid\"{0}", "@uuid{1}");
                    optionalParameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    optionalParameters.Last().Value = userPersonI.Uuid.Value;
                    if (userPersonI.Name != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"name\"{0}", ", @name{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("name", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.Name;
                    }
                    if (userPersonI.Address != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"address\"{0}", ", @address{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("address", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.Address;
                    }
                    if (userPersonI.ContactEMail != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"contact_email\"{0}", ", @contact_email{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("contact_email", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.ContactEMail;
                    }
                    if (userPersonI.Webaddress != null)
                    {
                        queryDerivedData = String.Format(queryDerivedData, ", \"webaddress\"{0}", ", @webaddress{1}");
                        optionalParameters.Add(new NpgsqlParameter<string>("webaddress", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = userPersonI.Webaddress;
                    }
                    queryDerivedData = String.Format(queryDerivedData, "", "");
                }

                /* Insert the base table UUID */
                using (command = new NpgsqlCommand(queryBasePersonalizedData, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["uuid"].Value = userI.PersonData.Uuid.Value;

                    command.ExecuteNonQuery();
                }

                /* Insert specific information on either one of the personalized tables */
                using (command = new NpgsqlCommand(queryDerivedData, connection))
                {
                    foreach (NpgsqlParameter param in optionalParameters)
                    {
                        command.Parameters.Add(param);
                    }

                    command.ExecuteNonQuery();
                }
            }

            #endregion

            #region Update Statements

            /// <summary>
            /// Update one specific vitalparameter reference value
            /// </summary>
            /// <param name="vitalparameterI">The vitalparameter to update (<c>referenceValue</c> and <c>type</c> required)</param>
            /// <param name="organI">The organ linked to the vitalparameter (<c>uuid</c> required)</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void UpdateReferenceValue(VitalParameterInterface vitalParameterI, OrganInterface organI)
            {
                string query;
                NpgsqlCommand command;

                if (organI.uuid == null || vitalParameterI.referenceValue == null)
                {
                    throw new ArgumentNullException();
                }

                query = "UPDATE \"patientsimulator\".\"reference_vitalparameter\" SET \"parameter_value\" = @parameter_value WHERE \"organ_uuid\" = @organ_uuid AND \"vitalparameter_code\" = @vitalparameter_code;";

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<float>("parameter_value", NpgsqlTypes.NpgsqlDbType.Real));
                    command.Parameters.Add(new NpgsqlParameter<Guid>("organ_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters.Add(new NpgsqlParameter<string>("vitalparameter_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                    command.Parameters["parameter_value"].Value = vitalParameterI.referenceValue;
                    command.Parameters["organ_uuid"].Value = organI.uuid.Value;
                    command.Parameters["vitalparameter_code"].Value = Global.TranslateVPType2ElgaCode[vitalParameterI.type];

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Updates all given basic values of the patient in the database. Paramters of patient set to <c>null</c> will be ignored.
            /// </summary>
            /// <param name="patientI">Provide information to update (<c>uuid</c> required). If any organs are given, only the reference (<c>uuid</c> required) is updated</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException">If some required or all updateable information is not given</exception>
            public void UpdateBasicPatientData(PatientInterface patientI)
            {
                string query;
                List<NpgsqlParameter> querySetParams = new List<NpgsqlParameter>();
                NpgsqlCommand command;

                /* Check, if required fields or all other fields together (no information provided to update) are null */
                if (patientI.uuid == null || (patientI.owner?.Uuid == null && patientI.patientName == null && patientI.isAlterable == null &&
                        patientI.roleModelUuid == null && patientI.viewerRights == null && patientI.organs == null) ||
                    (patientI.organs?.Exists(item => (item != null && item.uuid == null)) ?? false))
                {
                    throw new ArgumentNullException();
                }

                query = "UPDATE \"patientsimulator\".\"patient\" SET {0} WHERE \"uuid\" = @uuid;";

                if (patientI.isAlterable != null)
                {
                    querySetParams.Add(new NpgsqlParameter<bool>("is_alterable", patientI.isAlterable == true));
                    querySetParams.Last().NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                }
                if (patientI.owner?.Uuid != null)
                {
                    querySetParams.Add(new NpgsqlParameter<Guid>("owner_uuid", patientI.owner.Uuid.Value));
                    querySetParams.Last().NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;
                }
                if (patientI.gender != null)
                {
                    querySetParams.Add(new NpgsqlParameter<string>("gender", Global.TranslateGender2DBString[patientI.gender.Value]));
                    querySetParams.Last().NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                }
                if (patientI.roleModelUuid != null)
                {
                    querySetParams.Add(new NpgsqlParameter<Guid>("parent_uuid", patientI.roleModelUuid.Value));
                    querySetParams.Last().NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;
                }
                if (patientI.viewerRights != null)
                {
                    querySetParams.Add(new NpgsqlParameter<int>("rights_id", Global.TranslateViewerRightsType2DBInteger[patientI.viewerRights.Right]));
                    querySetParams.Last().NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;
                }
                if (patientI.patientName != null)
                {
                    querySetParams.Add(new NpgsqlParameter<string>("model_name", patientI.patientName));
                    querySetParams.Last().NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                }
                if (patientI.organs != null && patientI.organs.Count > 0)
                {
                    foreach (OrganInterface organI in patientI.organs)
                    {
                        querySetParams.Add(new NpgsqlParameter<Guid>(Global.TranslateOType2DBPatientReference[organI.type], organI.uuid.Value));
                        querySetParams.Last().NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;
                    }
                }

                query = String.Format(query, $"\"{querySetParams[0].ParameterName}\" = @{querySetParams[0].ParameterName} {"{0}"}");
                for (int i = 1; i < querySetParams.Count; i++)
                {
                    query = String.Format(query, $", \"{querySetParams[i].ParameterName}\" = @{querySetParams[i].ParameterName} {"{0}"}");
                }
                query = String.Format(query, "");

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", patientI.uuid.Value));
                    command.Parameters["uuid"].NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;
                    foreach (NpgsqlParameter param in querySetParams)
                    {
                        command.Parameters.Add(param);
                    }

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Update the user table in the Demographic and Vital DB only.
            /// </summary>
            /// <param name="userI">COntains updateable information and required identifiers (<c>Uuid</c>)</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException">If some required or all updateable information is not given</exception>
            public void updateUser(UserInterface userI)
            {
                string query;
                List<NpgsqlParameter> optionalParameters = new List<NpgsqlParameter>();
                NpgsqlCommand command;

                if (userI.Uuid == null || (userI.Username == null && userI.PasswordHash == null && userI.PasswordSalt == null && userI.InsuranceNumberHash == null && userI.PersonData?.Uuid == null))
                {
                    throw new ArgumentNullException();
                }

                query = "UPDATE \"management\".\"user\" SET {0} WHERE \"uuid\" = @uuid;";

                if (userI.Username != null)
                {
                    optionalParameters.Add(new NpgsqlParameter<string>("username", NpgsqlTypes.NpgsqlDbType.Varchar));
                    optionalParameters.Last().Value = userI.Username;
                }
                if (userI.PasswordHash != null)
                {
                    optionalParameters.Add(new NpgsqlParameter<string>("password_hash", NpgsqlTypes.NpgsqlDbType.Varchar));
                    optionalParameters.Last().Value = convertByteArrayToHex(userI.PasswordHash);
                }
                if (userI.PasswordSalt != null)
                {
                    optionalParameters.Add(new NpgsqlParameter<string>("pwd_salt", NpgsqlTypes.NpgsqlDbType.Varchar));
                    optionalParameters.Last().Value = convertByteArrayToAscii(userI.PasswordSalt);
                }
                if (userI.InsuranceNumberHash != null)
                {
                    optionalParameters.Add(new NpgsqlParameter<string>("insurance_number_hash", NpgsqlTypes.NpgsqlDbType.Varchar));
                    optionalParameters.Last().Value = convertByteArrayToHex(userI.InsuranceNumberHash);
                }
                if (userI.PersonData?.Uuid != null)
                {
                    optionalParameters.Add(new NpgsqlParameter<Guid>("user_personell_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    optionalParameters.Last().Value = userI.PersonData.Uuid.Value;
                }

                query = String.Format(query, $"\"{optionalParameters[0].ParameterName}\" = @{optionalParameters[0].ParameterName} {"{0}"}");
                for (int i = 1; i < optionalParameters.Count; i++)
                {
                    query = String.Format(query, $", \"{optionalParameters[i].ParameterName}\" = @{optionalParameters[i].ParameterName} {"{0}"}");
                }
                query = String.Format(query, "");

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["uuid"].Value = userI.Uuid.Value;
                    foreach (NpgsqlParameter param in optionalParameters)
                    {
                        command.Parameters.Add(param);
                    }

                    command.ExecuteNonQuery();
                }
            }

            /// <summary>
            /// Update either person_Data or company_data table from the Demographic and Vital DB.
            /// </summary>
            /// <param name="personellData">A class implementing the <c>IPersonalizedData</c> interface, containing optional updateable and required(<c>uuid</c>) data</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException">If some required or all updateable information is not given</exception>
            public void updateUserPersonalizedData(IPersonalizedData personellData)
            {
                string query;
                List<NpgsqlParameter> optionalParameters = new List<NpgsqlParameter>();
                NpgsqlCommand command;
                CompanyPersonDataInterface companyI = personellData.Person == PersonalizedDataType.CompanyData ? (CompanyPersonDataInterface)personellData : null;
                RealPersonDataInterface personI = personellData.Person == PersonalizedDataType.PersonData ? (RealPersonDataInterface)personellData : null;

                if (personellData.Uuid == null || (personellData.Person == PersonalizedDataType.PersonData && personI.Firstname == null && personI.Lastname == null &&
                        personI.Titles == null && personI.Email == null && personI.Birthdate == null && personI.NationalityCode == null && personI.Employer == null) ||
                    (personellData.Person == PersonalizedDataType.CompanyData && companyI.Name == null && companyI.Address == null && companyI.ContactEMail == null && companyI.Webaddress == null))
                {
                    throw new ArgumentNullException();
                }

                query = "UPDATE \"management\".\"{0}\" SET {1} WHERE \"uuid\" = @uuid;";
                if (personellData.Person == PersonalizedDataType.CompanyData)
                {
                    query = String.Format(query, "company_data", "{0}");
                    if (companyI.Name != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("name", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = companyI.Name;
                    }
                    if (companyI.Address != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("address", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = companyI.Address;
                    }
                    if (companyI.ContactEMail != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("contact_email", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = companyI.ContactEMail;
                    }
                    if (companyI.Webaddress != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("webaddress", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = companyI.Webaddress;
                    }
                }
                else if (personellData.Person == PersonalizedDataType.PersonData)
                {
                    query = String.Format(query, "person_data", "{0}");
                    if (personI.Firstname != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("firstname", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = personI.Firstname;
                    }
                    if (personI.Lastname != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("lastname", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = personI.Lastname;
                    }
                    if (personI.Titles != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("titles", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = personI.Titles;
                    }
                    if (personI.Email != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("email", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = personI.Email;
                    }
                    if (personI.Birthdate != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<DateTime>("birthdate", NpgsqlTypes.NpgsqlDbType.Date));
                        optionalParameters.Last().Value = personI.Birthdate;
                    }
                    if (personI.NationalityCode != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("nationality_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = personI.NationalityCode;
                    }
                    if (personI.Employer != null)
                    {
                        optionalParameters.Add(new NpgsqlParameter<string>("employer", NpgsqlTypes.NpgsqlDbType.Varchar));
                        optionalParameters.Last().Value = personI.Employer;
                    }
                }

                query = String.Format(query, $"\"{optionalParameters[0].ParameterName}\" = @{optionalParameters[0].ParameterName} {"{0}"}");
                for (int i = 1; i < optionalParameters.Count; i++)
                {
                    query = String.Format(query, $", \"{optionalParameters[i].ParameterName}\" = @{optionalParameters[i].ParameterName} {"{0}"}");
                }
                query = String.Format(query, "");

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["uuid"].Value = personellData.Uuid.Value;
                    foreach (NpgsqlParameter param in optionalParameters)
                    {
                        command.Parameters.Add(param);
                    }

                    command.ExecuteNonQuery();
                }
            }
            #endregion

            #region Delete Statements
            /* Notice: if a referenced primary key would be deleted, any referencing tables are set to "DELETE CASCADE", therefore thier referencing records will be automatically deleted as wll */

            /// <summary>
            /// Delete the patient model, all referenced organs, all links to icd10 coded diseases, all reference and continuous vitalparameters which are related to this patient.
            /// </summary>
            /// <param name="patientI"><c>PatientInterface</c> identifing the entry to delete (<c>uuid</c> required)</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void deletePatientFull(PatientInterface patientI)
            {
                string queryPatient;
                string queryUpdatePatient;
                string querySelectOrgans;
                string queryDeleteOrgans;
                List<string> relatedOrgansUuid = new List<string>();
                NpgsqlCommand command;

                if (patientI.uuid == null)
                {
                    throw new ArgumentNullException();
                }

                queryPatient = "DELETE FROM \"patientsimulator\".\"patient\" WHERE \"uuid\" = @uuid;";
                queryUpdatePatient = "UPDATE \"patientsimulator\".\"patient\" SET \"parent_uuid\" = NULL WHERE \"parent_uuid\" = @parent_uuid;";
                querySelectOrgans = "SELECT \"peripheral_vein_uuid\", \"venous_blood_uuid\", \"capillary_blood_uuid\", \"body_uuid\", \"lung_uuid\", \"peripheral_artery_uuid\" " +
                                        "FROM \"patientsimulator\".\"patient\" WHERE \"uuid\" = @uuid;";
                queryDeleteOrgans = "DELETE FROM \"patientsimulator\".\"organs\" WHERE \"uuid\" = @uuid;";

                using (command = new NpgsqlCommand(querySelectOrgans, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["uuid"].Value = patientI.uuid.Value;

                    NpgsqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        relatedOrgansUuid.Add(reader.IsDBNull(0) ? null : reader.GetGuid(0).ToString("N"));
                        relatedOrgansUuid.Add(reader.IsDBNull(1) ? null : reader.GetGuid(1).ToString("N"));
                        relatedOrgansUuid.Add(reader.IsDBNull(2) ? null : reader.GetGuid(2).ToString("N"));
                        relatedOrgansUuid.Add(reader.IsDBNull(3) ? null : reader.GetGuid(3).ToString("N"));
                        relatedOrgansUuid.Add(reader.IsDBNull(4) ? null : reader.GetGuid(4).ToString("N"));
                        relatedOrgansUuid.Add(reader.IsDBNull(5) ? null : reader.GetGuid(5).ToString("N"));
                    }
                }
                using (command = new NpgsqlCommand(queryUpdatePatient, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("parent_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["parent_uuid"].Value = patientI.uuid.Value;
                    command.ExecuteNonQuery();
                }
                /* Rely on delete cascade of constraints to delete depended entries in alement_of_icd10 table */
                using (command = new NpgsqlCommand(queryPatient, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["uuid"].Value = patientI.uuid.Value;
                    command.ExecuteNonQuery();
                }
                /* Rely on delete cascade of constraints to delete depended entries in vitalparameter and organ tables */
                foreach (string organUuid in relatedOrgansUuid.FindAll(item => (item != null)))
                {
                    using (command = new NpgsqlCommand(queryDeleteOrgans, connection))
                    {
                        command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                        command.Parameters["uuid"].Value = new Guid(organUuid);
                        command.ExecuteNonQuery();
                    }
                }
            }

            /// <summary>
            /// Delete continuous vitalparameters from Demographic and Vital DB
            /// </summary>
            /// <param name="vitalValuesIdentifiers">Identifiers for a vital value in <c>CurrentVitalValue</c> linked to the apropriate organ uuid</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            public void deleteContinuousVitalparameter(List<Tuple<CurrentVitalValue, Guid>> vitalValuesIdentifiers)
            {
                string query;
                NpgsqlCommand command;

                if (vitalValuesIdentifiers?.Exists(item => (item.Item2 == null || item.Item1.timestamp == null)) ?? true)
                {
                    throw new ArgumentNullException();
                }

                query = "DELETE FROM \"patientsimulator\".\"continuous_vitalparameter\" WHERE \"organ_uuid\" = @organ_uuid AND \"vitalparameter_code\" = @vitalparameter_code AND \"created_at\" = @created_at;";

                foreach (Tuple<CurrentVitalValue, Guid> vital in vitalValuesIdentifiers)
                {
                    using (command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.Add(new NpgsqlParameter<Guid>("organ_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                        command.Parameters.Add(new NpgsqlParameter<string>("vitalparameter_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                        command.Parameters.Add(new NpgsqlParameter<DateTime>("created_at", NpgsqlTypes.NpgsqlDbType.Timestamp));
                        command.Parameters["organ_uuid"].Value = vital.Item2;
                        command.Parameters["vitalparameter_code"].Value = Global.TranslateVPType2ElgaCode[vital.Item1.type];
                        command.Parameters["created_at"].Value = vital.Item1.timestamp;

                        command.ExecuteNonQuery();
                    }
                }
            }

            /// <summary>
            /// Delete the references to diseases (identified by ICD10 codes) for spwcific patients
            /// </summary>
            /// <param name="linkedICD10IDentifiers">Of these interfaces <c>uuid</c> and <c>ICD10Impairments.code</c> is required</param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException">Thrown if any required parameters are not provided</exception>
            public void deleteAlementOfICD10(List<PatientInterface> linkedICD10IDentifiers)
            {
                string query;
                NpgsqlCommand command;

                if (linkedICD10IDentifiers?.Exists(item => (item.uuid == null || (item.ICD10Impairments?.Exists(disease => disease.code == null) ?? true))) ?? true)
                {
                    throw new ArgumentNullException();
                }

                query = "DELETE FROM \"patientsimulator\".\"alement_of_icd10\" WHERE \"patient_uuid\" = @patient_uuid AND \"icd10_code\" = @icd10_code;";

                foreach (PatientInterface patient in linkedICD10IDentifiers)
                {
                    foreach (ICD10ImpairmentInterface icd10 in patient.ICD10Impairments)
                    {
                        using (command = new NpgsqlCommand(query, connection))
                        {
                            command.Parameters.Add(new NpgsqlParameter<Guid>("patient_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                            command.Parameters.Add(new NpgsqlParameter<string>("icd10_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                            command.Parameters["patient_uuid"].Value = patient.uuid.Value;
                            command.Parameters["icd10_code"].Value = icd10.code;

                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            /// <summary>
            /// Deletes either whole user entries or personell data entries, based on the semi-required identifiers provided
            /// </summary>
            /// <param name="userIdentifiers">
            /// Delete the whole user dat if <c>userIdentifiers.Uuid</c> is provided, 
            /// delete only the personell data if <c>userIdentifiers.PersonData.Uuid</c> is provided
            /// </param>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="SQLCommandIncompleteException">Thrown if none of the semi-required parameters are provided in some cases, all other cases were executed</exception>
            public void deleteUser(List<UserInterface> userIdentifiers)
            {
                string queryUser;
                string queryPersonell;
                string querySelectPersonell;
                string queryUpdatePatient;
                int failedstatements = 0;
                NpgsqlCommand command;

                if (userIdentifiers == null || userIdentifiers.Exists(item => item == null))
                {
                    throw new ArgumentNullException();
                }

                queryUser = "DELETE FROM \"management\".\"user\" WHERE \"uuid\" = @uuid;";
                queryPersonell = "DELETE FROM \"management\".\"personalized_data\" WHERE \"uuid\" = @uuid";
                querySelectPersonell = "SELECT \"user_personell_uuid\" FROM \"management\".\"user\" WHERE \"uuid\" = @uuid;";
                queryUpdatePatient = "UPDATE \"patientsimulator\".\"patient\" SET \"owner_uuid\" = NULL WHERE \"owner_uuid\" = @uuid;";

                foreach (UserInterface userI in userIdentifiers)
                {
                    if (userI.Uuid != null)
                    {
                        using (command = new NpgsqlCommand(queryUpdatePatient, connection))
                        {
                            command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                            command.Parameters["uuid"].Value = userI.Uuid.Value;
                            command.ExecuteNonQuery();
                        }

                        using (command = new NpgsqlCommand(querySelectPersonell, connection))
                        {
                            command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                            command.Parameters["uuid"].Value = userI.Uuid.Value;
                            NpgsqlDataReader reader = command.ExecuteReader();
                            userI.PersonData = new CompanyPersonDataInterface(reader.IsDBNull(0) ? (Guid?)null : reader.GetGuid(0), null, null, null, null);
                        }

                        using (command = new NpgsqlCommand(queryUser, connection))
                        {
                            command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                            command.Parameters["uuid"].Value = userI.Uuid.Value;
                            command.ExecuteNonQuery();
                        }

                        if (userI.PersonData.Uuid != null)
                        {
                            using (command = new NpgsqlCommand(queryPersonell, connection))
                            {
                                command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                                command.Parameters["uuid"].Value = userI.PersonData.Uuid.Value;
                                command.ExecuteNonQuery();
                            }
                        }
                    }
                    else if (userI.PersonData?.Uuid != null)
                    {
                        using (command = new NpgsqlCommand(queryPersonell, connection))
                        {
                            command.Parameters.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                            command.Parameters["uuid"].Value = userI.PersonData.Uuid.Value;
                            command.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        failedstatements++;
                    }
                }

                if (failedstatements > 0)
                {
                    throw new SQLCommandIncompleteException(failedstatements);
                }
            }
            #endregion

            #region Select Statements

            private NpgsqlCommand buildSelectCommand(string schema, string table, string[] columns, List<NpgsqlParameter> whereOptions)
            {
                string query = "";
                StringBuilder queryWhereOptions = new StringBuilder();
                NpgsqlCommand command;

                /* Cut bonds to pre existent collections, in case this parameter was already used before */
                whereOptions?.ForEach(item => item.Collection = null);

                if (schema == null || table == null || !(columns != null && columns.Length != 0 && !columns.ToList().Exists(item => (item == null))) ||
                    (whereOptions?.Exists(item => item?.Value == null || item?.ParameterName == null) ?? false))
                {
                    throw new ArgumentNullException();
                }

                if (whereOptions == null || whereOptions.Count == 0)
                {
                    query = String.Format("SELECT \"{0}\" FROM \"{2}\".\"{1}\";", String.Join("\",\"", columns), table, schema);

                    command = new NpgsqlCommand(query, connection);
                }
                else
                {
                    queryWhereOptions.Append($"\"{whereOptions[0].ParameterName}\" = @{whereOptions[0].ParameterName}");
                    for (int i = 1; i < whereOptions.Count; i++)
                    {
                        queryWhereOptions.Append($" AND \"{whereOptions[i].ParameterName}\" = @{whereOptions[i].ParameterName}");
                    }
                    query = String.Format("SELECT \"{0}\" FROM \"{2}\".\"{1}\" WHERE {3};", String.Join("\",\"", columns), table, schema, queryWhereOptions.ToString());

                    command = new NpgsqlCommand(query, connection);
                    foreach (NpgsqlParameter param in whereOptions)
                    {
                        command.Parameters.Add(param);
                    }
                }

                return command;
            }

            /// <summary>
            /// Select multiple rows from a single table and order the output array in the same order as <c>columns</c>.
            /// Multiple where cases are joined with the logic AND operator.
            /// </summary>
            /// <param name="columns">Select from these columns only, in the order they are listed here</param>
            /// <param name="schema">The DB schema, where the table is stored</param>
            /// <param name="table">The specific tale name to select from</param>
            /// <param name="whereOptions">Where clause parameters (<c>parameterName</c> must equal the column name in the table, <c>Value is also required)</c></param>
            /// <returns>An ordered list of all returned rows as array, the order of elemts in this array matches the order in <c>columns</c></returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="SQLNoEntryFoundException"></exception>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            public List<object[]> selectAllRows(string schema, string table, string[] columns, List<NpgsqlParameter> whereOptions)    //private List<object[]> SelectSingle(string schema, string table, string[] columns, List<NpgsqlParameter> whereKeyValues)
            {
                NpgsqlDataReader reader;
                List<object[]> ret = new List<object[]>();
                NpgsqlCommand command;

                /* Cut bonds to pre existent collections, in case this parameter was already used before */
                whereOptions?.ForEach(item => item.Collection = null);

                using (command = buildSelectCommand(schema, table, columns, whereOptions))
                {
                    using (reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            throw new SQLNoEntryFoundException(command.CommandText);
                        }

                        while (reader.Read())
                        {
                            ret.Add(new object[columns.Length]);
                            for (int i = 0; i < ret.Last().Length; i++)
                            {
                                ret.Last()[i] = reader.IsDBNull(i) ? null : reader.GetValue(i);
                            }
                        }
                    }
                }

                return ret;
            }

            /// <summary>
            /// Select one single row from a single table and order the output array in the same order as <c>columns</c>.
            /// Multiple where cases are joined with the logic AND operator.
            /// </summary>
            /// <param name="columns">Select from these columns only, in the order they are listed here</param>
            /// <param name="schema">The DB schema, where the table is stored</param>
            /// <param name="table">The specific table name to select from</param>
            /// <param name="whereOptions">Where clause parameters (<c>parameterName</c> must match the column name in the table, <c>Value is also required)</c></param>
            /// <returns>One ordered array which order of elemnts reflect the order of <c>columns</c></returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="SQLNoEntryFoundException"></exception>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            public object[] selectOneRow(string schema, string table, string[] columns, List<NpgsqlParameter> whereOptions)
            {
                object[] result = new object[columns.Length];
                NpgsqlCommand command;
                NpgsqlDataReader reader;

                /* Cut bonds to pre existent collections, in case this parameter was already used before */
                whereOptions?.ForEach(item => item.Collection = null);

                using (command = buildSelectCommand(schema, table, columns, whereOptions))
                {
                    using (reader = command.ExecuteReader(System.Data.CommandBehavior.SingleRow))
                    {
                        if (!reader.HasRows)
                        {
                            throw new SQLNoEntryFoundException(command.CommandText);
                        }

                        if (reader.Read())  // Get only the first row of results
                        {
                            for (int i = 0; i < result.Length; i++)
                            {
                                result[i] = reader.IsDBNull(i) ? null : reader.GetValue(i);
                            }
                        }
                    }
                }

                return result;
            }

            /// <summary>
            /// Select all vitalparameter reference information from one specific patient.
            /// </summary>
            /// <param name="patientUUID">UUID version 1 of the patient as registered in the Demographic and Vital DB</param>
            /// <returns>A list containing identification (<c>type</c> and <c>organUuid</c>) and information (<c>referenceValue</c>) for all vitalparameters found</returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="SQLNoEntryFoundException"></exception>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            public List<VitalParameterInterface> selectPatientVitalParameters(Guid patientUUID)
            {
                string query;
                NpgsqlCommand command;
                NpgsqlDataReader reader;
                List<VitalParameterInterface> ret = new List<VitalParameterInterface>();

                if (patientUUID == null)
                {
                    throw new ArgumentNullException();
                }

                //string query = String.Format("SELECT reference_vitalparameter.vitalparameter_code, reference_vitalparameter.parameter_value FROM (SELECT peripheral_vein_uuid as uuid FROM patient WHERE patient.uuid = UUID_TO_BIN('{0}') UNION SELECT venous_blood_uuid as uuid FROM patient WHERE patient.uuid = UUID_TO_BIN('{0}') UNION SELECT capillary_blood_uuid as uuid FROM patient WHERE patient.uuid = UUID_TO_BIN('{0}') UNION SELECT body_uuid as uuid FROM patient WHERE patient.uuid = UUID_TO_BIN('{0}') UNION SELECT lung_uuid as uuid FROM patient WHERE patient.uuid = UUID_TO_BIN('{0}') UNION SELECT peripheral_artery_uuid as uuid FROM patient WHERE patient.uuid = UUID_TO_BIN('{0}')) as patient_organs_uuids INNER JOIN organs ON patient_organs_uuids.uuid = organs.uuid INNER JOIN reference_vitalparameter ON organs.uuid = reference_vitalparameter.organ_uuid;", patientUUID);
                query = "SELECT \"patientsimulator\".\"reference_vitalparameter\".\"vitalparameter_code\", \"patientsimulator\".\"reference_vitalparameter\".\"parameter_value\" " +
                            "FROM (SELECT \"peripheral_vein_uuid\" as \"uuid\" FROM \"patientsimulator\".\"patient\" WHERE \"patientsimulator\".\"patient\".\"uuid\" = @patient_uuid " +
                                "UNION SELECT \"venous_blood_uuid\" as \"uuid\" FROM \"patientsimulator\".\"patient\" WHERE \"patient\".\"uuid\" = @patient_uuid " +
                                "UNION SELECT \"capillary_blood_uuid\" as \"uuid\" FROM \"patientsimulator\".\"patient\" WHERE \"patientsimulator\".\"patient\".\"uuid\" = @patient_uuid " +
                                "UNION SELECT \"body_uuid\" as \"uuid\" FROM \"patientsimulator\".\"patient\" WHERE \"patientsimulator\".\"patient\".\"uuid\" = @patient_uuid " +
                                "UNION SELECT \"lung_uuid\" as \"uuid\" FROM \"patientsimulator\".\"patient\" WHERE \"patientsimulator\".\"patient\".\"uuid\" = @patient_uuid " +
                                "UNION SELECT \"peripheral_artery_uuid\" as \"uuid\" FROM \"patientsimulator\".\"patient\" WHERE \"patientsimulator\".\"patient\".\"uuid\" = @patient_uuid) as \"patient_organs_uuids\" " +
                            "INNER JOIN \"patientsimulator\".\"organs\" ON \"patient_organs_uuids\".\"uuid\" = \"patientsimulator\".\"organs\".\"uuid\" " +
                            "INNER JOIN \"patientsimulator\".\"reference_vitalparameter\" ON \"patientsimulator\".\"organs\".\"uuid\" = \"patientsimulator\".\"reference_vitalparameter\".\"organ_uuid\";";

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("patient_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["patient_uuid"].Value = patientUUID;

                    using (reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            throw new SQLNoEntryFoundException(command.CommandText);
                        }

                        while (reader.Read())
                        {
                            string elgaCode = (string)reader.GetString(0);
                            ret.Add(new VitalParameterInterface(Global.TranslateVPType2ElgaCode.ToList().Find(item => item.Value == elgaCode).Key, reader.IsDBNull(1) ? null : (float?)reader.GetFloat(1), null));
                        }
                    }
                }

                return ret;
            }

            /// <summary>
            /// Select a list of all ICD10 coded diagnoses (and vital parameter impairments) linked to a specific patient.
            /// </summary>
            /// <param name="patientUuid">UUID version 1 of the specific patient (required)</param>
            /// <returns>
            /// A list of all <c>string</c> ICD10 diagnosis code <c>int</c> version year of the ICD10 system, 
            /// all related impaired vitalparameters (in <c>VitalParameterType</c> type, <c>float</c> standard value and <c>float</c> impaired value).
            /// </returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="SQLNoEntryFoundException"></exception>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            public List<Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>> selectICD10OfPatient(Guid patientUuid)
            {
                string query;
                NpgsqlCommand command;
                NpgsqlDataReader reader;
                List<Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>> ret = new List<Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>>();

                if (patientUuid == null)
                {
                    throw new ArgumentNullException();
                }

                query = "SELECT \"code_icd10\", \"version_year\", \"vitalparameter_code\", \"ordinary_value\", \"impaired_value\" FROM \"patientsimulator\".\"physical_status_icd10\" " +
                            "INNER JOIN \"patientsimulator\".\"impairing_physical_change\" ON \"physical_status_icd10\".\"code_icd10\" = \"impairing_physical_change\".\"icd10_code\" " +
                            "INNER JOIN \"patientsimulator\".\"alement_of_icd10\" ON \"physical_status_icd10\".\"code_icd10\" = \"alement_of_icd10\".\"icd10_code\" " +
                            "WHERE \"alement_of_icd10\".\"patient_uuid\" = @patient_uuid ORDER BY \"code_icd10\";";

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<Guid>("patient_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    command.Parameters["patient_uuid"].Value = patientUuid;

                    using (reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            throw new SQLNoEntryFoundException(command.CommandText);
                        }

                        string currentDiagnose = "";
                        while (reader.Read())
                        {
                            if (!currentDiagnose.Equals(reader.GetString(0)))
                            {
                                currentDiagnose = reader.GetString(0);
                                ret.Add(new Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>(currentDiagnose,
                                            reader.IsDBNull(1) ? 0 : reader.GetInt32(1), new List<Tuple<VitalParameterType, float, float>>()));
                            }
                            string elgaCode = reader.GetString(2);
                            ret.Last().Item3.Add(new Tuple<VitalParameterType, float, float>(Global.TranslateVPType2ElgaCode.ToList().Find(item => item.Value == elgaCode).Key,
                                reader.IsDBNull(3) ? 0f : reader.GetFloat(3), reader.IsDBNull(4) ? 0f : reader.GetFloat(4)));
                        }
                    }
                }

                return ret;
            }

            /// <summary>
            /// Select a list of all known ICD10 diagnosis (and vital parameter impairments) in the Demographic and Vital DB.
            /// </summary>
            /// <returns>
            /// A list of all <c>string</c> ICD10 diagnosis code <c>int</c> version year of the ICD10 system, 
            /// all related impaired vitalparameters (in <c>VitalParameterType</c> type, <c>float</c> standard value and <c>float</c> impaired value).
            /// </returns>
            /// <exception cref="SQLNoEntryFoundException"></exception>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            public List<Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>> selectAllICD10()
            {
                string query;
                NpgsqlCommand command;
                NpgsqlDataReader reader;
                List<Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>> ret = new List<Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>>();

                query = "SELECT \"code_icd10\", \"version_year\", \"vitalparameter_code\", \"ordinary_value\", \"impaired_value\" FROM \"patientsimulator\".\"physical_status_icd10\" " +
                            "INNER JOIN \"patientsimulator\".\"impairing_physical_change\" ON \"physical_status_icd10\".\"code_icd10\" = \"impairing_physical_change\".\"icd10_code\" " +
                            "ORDER BY \"code_icd10\";";

                using (command = new NpgsqlCommand(query, connection))
                {
                    using (reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            throw new SQLNoEntryFoundException(command.CommandText);
                        }

                        string currentDiagnose = "";
                        while (reader.Read())
                        {
                            if (!currentDiagnose.Equals(reader.GetString(0)))
                            {
                                currentDiagnose = reader.GetString(0);
                                ret.Add(new Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>(currentDiagnose,
                                            reader.IsDBNull(1) ? 0 : reader.GetInt32(1), new List<Tuple<VitalParameterType, float, float>>()));
                            }
                            string elgaCode = reader.GetString(2);
                            ret.Last().Item3.Add(new Tuple<VitalParameterType, float, float>(Global.TranslateVPType2ElgaCode.ToList().Find(item => item.Value == elgaCode).Key,
                                reader.IsDBNull(3) ? 0f : reader.GetFloat(3), reader.IsDBNull(4) ? 0f : reader.GetFloat(4)));
                        }
                    }
                }

                return ret;
            }

            /// <summary>
            /// Select one ICD10 coded diagnosis and all impaired vitalparameters from this diagnosis.
            /// </summary>
            /// <param name="codeIcd10">Required, the code representing the diagnosis in ICD10 system</param>
            /// <returns>
            /// The <c>int</c> version year of the ICD10 system, all related impaired vitalparameters (in <c>VitalParameterType</c> type, <c>float</c> standard value and <c>float</c> impaired value)
            /// </returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="SQLNoEntryFoundException"></exception>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            public Tuple<int, List<Tuple<VitalParameterType, float, float>>> selectICD10(string codeIcd10)
            {
                string query;
                NpgsqlCommand command;
                NpgsqlDataReader reader;
                int retVersionYear = 0;
                List<Tuple<VitalParameterType, float, float>> retDiagnoseImpairment = new List<Tuple<VitalParameterType, float, float>>();

                if (codeIcd10 == null)
                {
                    throw new ArgumentNullException();
                }

                query = "SELECT \"version_year\", \"vitalparameter_code\", \"ordinary_value\", \"impaired_value\" FROM \"patientsimulator\".\"physical_status_icd10\" " +
                            "INNER JOIN \"patientsimulator\".\"impairing_physical_change\" ON \"physical_status_icd10\".\"code_icd10\" = \"impairing_physical_change\".\"icd10_code\" " +
                            "WHERE \"code_icd10\" = @code_icd10;";

                using (command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter<string>("code_icd10", NpgsqlTypes.NpgsqlDbType.Varchar));
                    command.Parameters["code_icd10"].Value = codeIcd10;

                    using (reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            throw new SQLNoEntryFoundException(command.CommandText);
                        }

                        while (reader.Read())
                        {
                            retVersionYear = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                            string elgaCode = reader.GetString(1);
                            retDiagnoseImpairment.Add(new Tuple<VitalParameterType, float, float>(Global.TranslateVPType2ElgaCode.ToList().Find(item => item.Value == elgaCode).Key,
                                reader.IsDBNull(2) ? 0f : reader.GetFloat(2), reader.IsDBNull(3) ? 0f : reader.GetFloat(3)));
                        }
                    }
                }

                return new Tuple<int, List<Tuple<VitalParameterType, float, float>>>(retVersionYear, retDiagnoseImpairment);
            }

            /// <summary>
            /// Select the current (highest) version provided by the table management.version
            /// </summary>
            /// <returns>A collection containing (in this order) UUID v1, major version, minor version, alpha status, beta status and date of creation from the selected version</returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="SQLNoEntryFoundException"></exception>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            public Tuple<Guid, int, int, bool, bool, DateTime> SelectCurrentVersion()
            {
                string query;
                NpgsqlDataReader reader;
                NpgsqlCommand command;
                Tuple<Guid, int, int, bool, bool, DateTime> ret = null;

                query = "SELECT \"uuid\", \"major_version\", \"minor_version\", \"is_alpha\", \"is_beta\", \"published_at\" " +
                            "FROM \"management\".\"version\" WHERE (\"major_version\", \"minor_version\") = " +
                                "(SELECT \"major_version\", max(\"minor_version\") " +
                                 "FROM \"management\".\"version\" GROUP BY \"major_version\" ORDER BY \"major_version\" DESC LIMIT 1);";

                using (command = new NpgsqlCommand(query, connection))
                { 
                    using (reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            throw new SQLNoEntryFoundException(command.CommandText);
                        }

                        if (reader.Read())
                        {
                            ret = new Tuple<Guid, int, int, bool, bool, DateTime>(reader.GetGuid(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetBoolean(3), reader.GetBoolean(4), reader.GetDateTime(5));
                        }
                    }
                }

                return ret;
            }

            /// <summary>
            /// Serches the Demographic and Vital DB for the specified entrys to determine whether they exist or not.
            /// </summary>
            /// <param name="schema">The schema name to search in</param>
            /// <param name="table">The table name (in the specified schema) to search in</param>
            /// <param name="identifiers">
            /// Contains the columns and values to search for in the table (exact match), where <c>ParameterName</c> has to mach the column name.
            /// Multiple conditions are linked with AND operator on the same table.
            /// </param>
            /// <returns>Whether the specified entry was found in the table or not</returns>
            /// <exception cref="ArgumentNullException"></exception>
            /// <exception cref="NpgsqlException"></exception>
            /// <exception cref="PostgresException"></exception>
            public bool selectEntryExists(string schema, string table, List<NpgsqlParameter> identifiers)
            {
                string query;
                NpgsqlCommand command;
                bool ret;

                if (schema == null || table == null || identifiers == null || identifiers.Count == 0)
                {
                    throw new ArgumentNullException();
                }
                /* Cut bonds to pre existent collections, in case this parameter was already used before */
                identifiers?.ForEach(item => item.Collection = null);

                query = String.Format("SELECT EXISTS (SELECT 1 FROM \"{0}\".\"{1}\" WHERE {2});", schema, table, "{0}");
                query = String.Format(query, $"\"{identifiers[0].ParameterName}\" = @{identifiers[0].ParameterName}{"{0}"}");
                for (int i = 1; i < identifiers.Count; i++)
                {
                    query = String.Format(query, $" && \"{identifiers[i].ParameterName}\" = @{identifiers[i].ParameterName}{"{0}"}");
                }
                query = String.Format(query, "");

                using (command = new NpgsqlCommand(query, connection))
                {
                    foreach (NpgsqlParameter param in identifiers)
                    {
                        command.Parameters.Add(param);
                    }

                    ret = (bool)command.ExecuteScalar();
                }

                return ret;
            }
            #endregion

            #region Static converters

            /// <summary>
            /// Converts a given byte array to a sequence of ASCII coded characters
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static string convertByteArrayToAscii(byte[] data)
            {
                StringBuilder ret = new StringBuilder();

                for (int i = 0; i < data.Length; i++)
                {
                    ret.Append((char)data[i]);
                }

                return ret.ToString();
            }

            /// <summary>
            /// Convert a given array to a hexadecimal representaion of the contained values
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static string convertByteArrayToHex(byte[] data)
            {
                StringBuilder ret = new StringBuilder();

                for (int i = 0; i < data.Length; i++)
                {
                    ret.AppendFormat("{0:x2}", data[i]);
                }

                return ret.ToString();
            }

            /// <summary>
            /// Converts a given string into an array of ASCII coded characters.
            /// </summary>
            /// <param name="asciiCode"></param>
            /// <returns>An array of the same length as the provided string</returns>
            public static byte[] convertAsciiToByteArray(string asciiCode)
            {
                byte[] ret = new byte[asciiCode.Length];

                for (int i = 0; i < asciiCode.Length; i++)
                {
                    ret[i] = Convert.ToByte(asciiCode.ElementAt(i));
                }

                return ret;
            }

            /// <summary>
            /// Converts a given hexadecimal sequence into a byte array, where one byte represents two consecutive hex values.
            /// </summary>
            /// <param name="hexHash">The hexadecimal sequence, must not contain prefixes or any delimiters</param>
            /// <returns>An array containing 1/4 the elemts the given string did</returns>
            public static byte[] convertHexToByteArray(string hexHash)
            {
                byte[] ret;

                if ((hexHash.Length % 2) != 0)
                {
                    throw new ArgumentException("The character chain provided has an uneven Number of elements.", "hexHash");
                }

                ret = new byte[hexHash.Length / 2];
                for (int index = 0; index < ret.Length; index++)
                {
                    string byteValue = hexHash.Substring(index * 2, 2);
                    ret[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                }

                return ret;
            }
            #endregion

            #region Finalizer and disposable methods
            public void Dispose()
            {
                Dispose(true);  // dispose both managed and unamanged objects
                GC.SuppressFinalize(true);  // Suppress calling the finalize method (destructor), since all resources have been freed
            }
            ~DBConnection()  // Finalizer method called by the garbage collector (GC)
            {
                Dispose(false);  // dispose unamanged objects only. Since the GC is running, all handeled resources are/will be disposed
            }
            protected void Dispose(bool itIsSafeToAlsoFreeManagedObjects)
            {
                // Put the unamanged objects here

                //Put the managed objects here
                if (itIsSafeToAlsoFreeManagedObjects)
                {
                    CloseConnection();
                    connection = null;
                }
            }
            #endregion
        }




        /// <summary>
        /// Extensively reads all information for one specific patient (including user, viewer rights and ICD10 diagnoses)
        /// </summary>
        /// <param name="patientIdentifier">Patient interface, required to contain <c>uuid</c> as patient entry identifier</param>
        /// <returns>
        /// A <c>PatientInterface</c> containing all related data (including <c>organs</c>, <c>vitalparameters</c>, <c>user</c> and <c>ICD10Impairments</c>).
        /// Note that from <c>user</c> any secure information (<c>PassowrdHash</c>, <c>PasswordSalt</c> and <c>InsurenceNumberHash</c>) is withheld.
        /// </returns>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public PatientInterface ReadFullPatient(PatientInterface patientIdentifier)
        {
            DBConnection db;
            PatientInterface patientI = new PatientInterface();

            object[] patientInformation = null;
            object[] userInformation = null;
            object[] userPersonInformation = null;
            object[] viewerRightsInformation = null;
            List<Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>> icd10DiagnoseInformation = null;

            if (patientIdentifier.uuid == null)
            {
                throw new ArgumentNullException("pIFInput", "Cannot read the patient from DB, no uuid provided.");
            }

            patientI.uuid = patientIdentifier.uuid;
            patientI.organs = new List<OrganInterface>();
            patientI.vitalParameters = new List<VitalParameterInterface>();
            patientI.ICD10Impairments = new List<ICD10ImpairmentInterface>();

            using (db = new DBConnection())
            {
                List<NpgsqlParameter> whereClause = new List<NpgsqlParameter>();
                whereClause.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                whereClause.Last().Value = patientI.uuid;

                patientInformation = db.selectOneRow("patientsimulator", "patient", new string[] {
                    "created_at", "model_name", "owner_uuid", "rights_id", "is_alterable",
                    "parent_uuid", "peripheral_vein_uuid", "venous_blood_uuid", "capillary_blood_uuid",
                    "body_uuid", "lung_uuid", "peripheral_artery_uuid", "gender" },
                    whereClause);

                patientI.vitalParameters = db.selectPatientVitalParameters(patientI.uuid.Value);

                patientI.owner = new UserInterface();
                patientI.owner.Uuid = patientInformation[2] != null ? (Guid?)patientInformation[2] : null;
                if (patientI.owner.Uuid != null)
                {
                    whereClause = new List<NpgsqlParameter>();
                    whereClause.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    whereClause.Last().Value = patientI.owner.Uuid.Value;
                    userInformation = db.selectOneRow("management", "user", new string[] { "username", "user_personell_uuid" }, whereClause);

                    if (userInformation[1] != null)
                    {
                        whereClause = new List<NpgsqlParameter>();
                        whereClause.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                        whereClause.Last().Value = (Guid)userInformation[1];
                        if (db.selectEntryExists("management", "person_data", whereClause))
                        {
                            /* user personell data is a person_data entry */
                            userPersonInformation = db.selectOneRow("management", "person_data", new string[] { "firstname", "lastname", "titles", "email", "birthdate", "nationality_code", "employer" }, whereClause);
                            if (userPersonInformation != null && userPersonInformation.Length == 7)
                            {
                                patientI.owner.PersonData = new RealPersonDataInterface((Guid?)userInformation[1],
                                    userPersonInformation[0] != null ? (string)userPersonInformation[0] : null,
                                    userPersonInformation[1] != null ? (string)userPersonInformation[1] : null,
                                    userPersonInformation[2] != null ? (string)userPersonInformation[2] : null,
                                    userPersonInformation[3] != null ? (string)userPersonInformation[3] : null,
                                    userPersonInformation[4] != null ? (DateTime?)userPersonInformation[4] : null,
                                    userPersonInformation[5] != null ? (string)userPersonInformation[5] : null,
                                    userPersonInformation[6] != null ? (string)userPersonInformation[6] : null);
                            }
                            else
                            {
                                patientI.owner.PersonData = new RealPersonDataInterface((Guid)userInformation[1], null, null, null, null, null, null, null);
                            }
                        }
                        else if (db.selectEntryExists("management", "company_data", whereClause))
                        {
                            /* user personell data is a copmany_data entry */
                            userPersonInformation = db.selectOneRow("management", "company_data", new string[] { "name", "address", "contact_email", "webaddress" }, whereClause);
                            if (userPersonInformation != null && userPersonInformation.Length == 4)
                            {
                                patientI.owner.PersonData = new CompanyPersonDataInterface((Guid?)userInformation[1],
                                    userPersonInformation[0] != null ? (string)userPersonInformation[0] : null,
                                    userPersonInformation[1] != null ? (string)userPersonInformation[1] : null,
                                    userPersonInformation[2] != null ? (string)userPersonInformation[2] : null,
                                    userPersonInformation[3] != null ? (string)userPersonInformation[3] : null);
                            }
                            else
                            {
                                patientI.owner.PersonData = new CompanyPersonDataInterface((Guid?)userInformation[1], null, null, null, null);
                            }
                        }
                        else
                        {
                            patientI.owner.PersonData = null;
                        }
                    }
                    else
                    {
                        patientI.owner.PersonData = null;
                    }
                }

                patientI.viewerRights = patientInformation[3] != null ? new ViewerRightsInterface() : null;
                if (patientI.viewerRights != null)
                {
                    patientI.viewerRights.Right = Global.TranslateViewerRightsType2DBInteger.ToList().Find(item => item.Value == (int)patientInformation[3]).Key;
                    whereClause = new List<NpgsqlParameter>();
                    whereClause.Add(new NpgsqlParameter<int>("id", (int)patientInformation[3]));
                    whereClause.Last().NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;
                    viewerRightsInformation = db.selectOneRow("management", "viewer_rights", new string[] { "description" }, whereClause);
                }

                try
                {
                    icd10DiagnoseInformation = db.selectICD10OfPatient(patientI.uuid.Value);
                }
                catch (SQLNoEntryFoundException)
                {
                    icd10DiagnoseInformation = new List<Tuple<string, int, List<Tuple<VitalParameterType, float, float>>>>();
                }
            }

            if (patientInformation == null || patientInformation.Length == 0)
            {
                throw new ArgumentException("Cannot read the patient from DB, given uuid not found in database.", "patientIdentifier.uuid");
            }

            patientI.patientName = patientInformation[1] != null ? (string)patientInformation[1] : null;
            patientI.roleModelUuid = patientInformation[5] != null ? (Guid?)patientInformation[5] : null;
            patientI.isAlterable = patientInformation[4] != null ? (bool?)patientInformation[4] : null;
            patientI.createdAt = patientInformation[0] != null ? (DateTime?)patientInformation[0] : null;
            patientI.gender = patientInformation[12] != null ? (PatientGender?)Global.TranslateGender2DBString.ToList().Find(item => item.Value.Equals((string)patientInformation[12])).Key : null;

            patientI.organs.Add(new OrganInterface(OrganType.PeripheralVein, patientInformation[6] != null ? (Guid?)patientInformation[6] : null));
            patientI.organs.Add(new OrganInterface(OrganType.VenousBlood, patientInformation[7] != null ? (Guid?)patientInformation[7] : null));
            patientI.organs.Add(new OrganInterface(OrganType.CapillarBlood, patientInformation[8] != null ? (Guid?)patientInformation[8] : null));
            patientI.organs.Add(new OrganInterface(OrganType.Body, patientInformation[9] != null ? (Guid?)patientInformation[9] : null));
            patientI.organs.Add(new OrganInterface(OrganType.Lung, patientInformation[10] != null ? (Guid?)patientInformation[10] : null));
            patientI.organs.Add(new OrganInterface(OrganType.PeripheralArtery, patientInformation[11] != null ? (Guid?)patientInformation[11] : null));
            foreach (OrganInterface organI in patientI.organs)
            {
                // remove organs with empty uuids
                if (organI.uuid == null)
                {
                    patientI.organs.Remove(organI);
                }
            }

            if (userInformation != null)
            {
                patientI.owner.Username = userInformation[0] != null ? (string)userInformation[0] : null;
            }


            if (viewerRightsInformation != null && viewerRightsInformation.Length == 1)
            {
                patientI.viewerRights.Description = viewerRightsInformation[0] != null ? (string)viewerRightsInformation[0] : null;
            }
            else
            {
                patientI.viewerRights = null;
            }

            foreach (Tuple<string, int, List<Tuple<VitalParameterType, float, float>>> icd10 in icd10DiagnoseInformation)
            {
                if (icd10.Item1 != null)
                {
                    patientI.ICD10Impairments.Add(new ICD10ImpairmentInterface());
                    patientI.ICD10Impairments.Last().code = icd10.Item1;
                    patientI.ICD10Impairments.Last().VersionYear = icd10.Item2;
                    foreach (Tuple<VitalParameterType, float, float> impairment in icd10.Item3)
                    {
                        patientI.ICD10Impairments.Last().impairedVitalParameters.Add(new ICD10ImpairedValueInterface(impairment.Item1, impairment.Item2, impairment.Item3));
                    }
                }
            }
            if (icd10DiagnoseInformation.Count == 0)
            {
                patientI.ICD10Impairments = null;
            }

            return patientI;
        }

        /// <summary>
        /// Requests basic information from all patient models existing in the Demographic and Vital DB
        /// </summary>
        /// <param name="isAlterable">Determines, whether alterable or non-alterable (models only) patients should be selectd from DB</param>
        /// <returns>
        /// A list of <c>PatientInterface</c> each with the attributes <c>uuid</c>, <c>createdAt</c>, <c>patientName</c>, <c>owner.Uuid</c>, <c>viewerRights.Id</c>, <c>isAlterable</c>, <c>modelUuid</c>
        /// </returns>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public List<PatientInterface> ReadAllPatientInformation(bool isAlterable)
        {
            DBConnection db;
            List<NpgsqlParameter> parameter = new List<NpgsqlParameter>();
            List<PatientInterface> patientRecords = new List<PatientInterface>();
            List<object[]> queryResults;

            using (db = new DBConnection())
            {
                parameter.Add(new NpgsqlParameter<bool>("is_alterable", isAlterable));
                queryResults = db.selectAllRows("patientsimulator", "patient", new string[] { "uuid", "created_at", "model_name", "owner_uuid", "rights_id", "is_alterable", "parent_uuid", "gender" }, parameter);
            }
            if (queryResults == null || queryResults.Count == 0)
            {
                return null;
            }

            foreach (object[] item in queryResults)
            {
                patientRecords.Add(new PatientInterface(item[0] != null ? (Guid?)item[0] : null,
                                                        item[1] != null ? (DateTime?)item[1] : null,
                                                        item[6] != null ? (Guid?)item[6] : null,
                                                        item[2] != null ? (string)item[2] : null,
                                                        item[7] != null ? (PatientGender?)Global.TranslateGender2DBString.ToList().Find(pg => pg.Value.Equals((string)item[7])).Key : null,
                                                        item[5] != null ? (bool?)item[5] : null,
                                                        item[3] != null ? new UserInterface((Guid?)item[3], null, null, null, null, null) : null,
                                                        item[4] != null ? new ViewerRightsInterface(Global.TranslateViewerRightsType2DBInteger.ToList().Find(trans => trans.Value == (int)item[4]).Key, null) : null,
                                                        null, null, null));
            }

            return patientRecords;
        }

        /// <summary>
        /// Requests basic information from all patient models existing in the Demographic and Vital DB
        /// </summary>
        /// <returns>
        /// A list of <c>PatientInterface</c> each with the attributes <c>uuid</c>, <c>createdAt</c>, <c>patientName</c>, <c>owner.Uuid</c>, <c>viewerRights.Id</c>, <c>isAlterable</c>, <c>modelUuid</c>
        /// </returns>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        [Obsolete("This function is cept for compatibility. Instead use overload ReadAllPatientInformation(bool).")]
        public List<PatientInterface> ReadAllPatientInformation()
        {
            DBConnection db;
            List<NpgsqlParameter> parameter = new List<NpgsqlParameter>();
            List<PatientInterface> patientRecords = new List<PatientInterface>();
            List<object[]> queryResults;

            using (db = new DBConnection())
            {
                queryResults = db.selectAllRows("patientsimulator", "patient", new string[] { "uuid", "created_at", "model_name", "owner_uuid", "rights_id", "is_alterable", "parent_uuid", "gender" }, null);
            }
            if (queryResults == null || queryResults.Count == 0)
            {
                return null;
            }

            foreach (object[] item in queryResults)
            {
                patientRecords.Add(new PatientInterface(item[0] != null ? (Guid?)item[0] : null,
                                                        item[1] != null ? (DateTime?)item[1] : null,
                                                        item[6] != null ? (Guid?)item[6] : null,
                                                        item[2] != null ? (string)item[2] : null,
                                                        item[7] != null ? (PatientGender?)Global.TranslateGender2DBString.ToList().Find(pg => pg.Value.Equals((string)item[7])).Key : null,
                                                        item[5] != null ? (bool?)item[5] : null,
                                                        item[3] != null ? new UserInterface((Guid?)item[3], null, null, null, null, null) : null,
                                                        item[4] != null ? new ViewerRightsInterface(Global.TranslateViewerRightsType2DBInteger.ToList().Find(trans => trans.Value == (int)item[4]).Key, null) : null,
                                                        null, null, null));
            }

            return patientRecords;
        }

        /// <summary>
        /// Reads the current version in use of the Demographic and Vital Database.
        /// </summary>
        /// <returns>Returns fully filled <c>VersionInterface</c> with the current DB version.</returns>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        public VersionInterface ReadVersion()
        {
            DBConnection db;
            Tuple<Guid, int, int, bool, bool, DateTime> result;

            using (db = new DBConnection())
            {
                result = db.SelectCurrentVersion();
            }
            if (result == null)
            {
                return null;
            }

            return new VersionInterface(result.Item1, result.Item2, result.Item3, result.Item4, result.Item5, result.Item6);
        }

        /// <summary>
        /// Read all continuously created values of a specific Vitalparameter from the Demographic and Vital DB.
        /// </summary>
        /// <param name="vitalI">The vitalparameter this continuous value is a memeber of (<c>type</c> required).</param>
        /// <param name="organI">The organ (<c>uuid</c> is required) the vitalparameter (and therefore consequently all continously created values too) is a member of.</param>
        /// <returns>A <c>VitalParameterInterface</c> identified by its <c>type</c> containing a list of all found <c>CurrentVitalValue</c>s (whos parameter_value is not null).</returns>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public VitalParameterInterface ReadContinuousValues(VitalParameterInterface vitalI, OrganInterface organI)
        {
            return ReadContinuousValues(vitalI, organI);
        }

        /// <summary>
        /// Read all continuously created values of a specific Vitalparameter from the Demographic and Vital DB.
        /// </summary>
        /// <param name="vitalI">The vitalparameter this continuous value is a memeber of (<c>type</c> required).</param>
        /// <param name="organUuid">The identifier (<c>uuid</c>) of the organ the vitalparameter (and therefore consequently all continously created values too) is a member of.</param>
        /// <returns>A <c>VitalParameterInterface</c> identified by its <c>type</c> containing a list of all found <c>CurrentVitalValue</c>s (whos parameter_value is not null).</returns>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        [Obsolete("This function is held up for compatibility reasons, use overload ReadContinuousValues(VitalParameterInterface, OrganInterface) instead.")]
        public VitalParameterInterface ReadContinuousValues(VitalParameterInterface vitalI, string organUuid)
        {
            DBConnection db;
            List<NpgsqlParameter> whereConditions = new List<NpgsqlParameter>();
            List<object[]> selectedContinuousValues;
            VitalParameterInterface result = new VitalParameterInterface();

            if (organUuid == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                whereConditions.Add(new NpgsqlParameter<Guid>("organ_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                whereConditions.Last().Value = new Guid(organUuid);
                whereConditions.Add(new NpgsqlParameter<string>("vitalparameter_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                whereConditions.Last().Value = Global.TranslateVPType2ElgaCode[vitalI.type];

                selectedContinuousValues = db.selectAllRows("patientsimulator", "continuous_vitalparameter", new string[] { "created_at", "parameter_value" }, whereConditions);
            }
            if (selectedContinuousValues == null || selectedContinuousValues.Count == 0)
            {
                return null;
            }

            /* Only select the values which are not null */
            foreach (object[] val in selectedContinuousValues.FindAll(item => item[1] != null))
            {
                result.continuousValues.Add(new CurrentVitalValue((DateTime)val[0], (float)val[1], vitalI.type));
            }
            result.type = vitalI.type;

            return result;
        }

        /// <summary>
        /// Select one specific reference Value from the Demographic and Vital DB.
        /// </summary>
        /// <param name="vitalI">Defining the vitalparameter to select (<c>type</c> required).</param>
        /// <param name="organI">The identifier (<c>uuid</c> required) of the organ the vitalparameter is a member of.</param>
        /// <returns>A <c>VitalPArameterInterface</c> containing <c>type</c> and the requested <c>referenceValue</c>.</returns>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public VitalParameterInterface ReadReferenceValue(VitalParameterInterface vitalI, OrganInterface organI)
        {
            return ReadReferenceValue(vitalI, organI);
        }

        /// <summary>
        /// Select one specific reference Value from the Demographic and Vital DB.
        /// </summary>
        /// <param name="vitalI">Defining the vitalparameter to select (<c>type</c> required).</param>
        /// <param name="organUuid">The identifier (<c>uuid</c>) of the organ the vitalparameter is a member of.</param>
        /// <returns>A <c>VitalPArameterInterface</c> containing <c>type</c> and the requested <c>referenceValue</c>.</returns>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        [Obsolete("This function is held up for compatibility reasons, use overload ReadReferenceValue(VitalParameterInterface, OrganInterface) instead.")]
        public VitalParameterInterface ReadReferenceValue(VitalParameterInterface vitalI, string organUuid)
        {
            DBConnection db;
            List<NpgsqlParameter> whereConditions = new List<NpgsqlParameter>();
            object[] selectedVitalParameter;
            VitalParameterInterface result = new VitalParameterInterface();

            if (organUuid == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                whereConditions.Add(new NpgsqlParameter<Guid>("organ_uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                whereConditions.Last().Value = new Guid(organUuid);
                whereConditions.Add(new NpgsqlParameter<string>("vitalparameter_code", NpgsqlTypes.NpgsqlDbType.Varchar));
                whereConditions.Last().Value = Global.TranslateVPType2ElgaCode[vitalI.type];

                selectedVitalParameter = db.selectOneRow("patientsimulator", "reference_vitalparameter", new string[] { "parameter_value" }, whereConditions);
            }

            if (selectedVitalParameter == null)
            {
                return null;
            }
            result.type = vitalI.type;
            result.referenceValue = selectedVitalParameter[0] != null ? (float?)selectedVitalParameter[0] : null;

            return result;
        }

        /// <summary>
        /// Not implemented yet.
        /// </summary>
        /// <returns></returns>
        public List<VitalParameterInterface> ReadAllReferenceValues()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Select all basic information for one specific patient from the Demographic and Vital DB.
        /// </summary>
        /// <param name="patientI">Specify the requested patient (<c>uuid</c> required).</param>
        /// <returns>
        /// The requested <c>PatientInterface</c>, containing <c>uuid</c>, <c>createdAt</c>, <c>patientName</c>, <c>roleModelUuid</c>, <c>isAlterable</c>, 
        /// <c>owner.Uuid</c>, <c>viewerRights.Id</c> and all <c>organ.uuid</c>s.
        /// </returns>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public PatientInterface ReadPatientInformation(PatientInterface patientI)
        {
            DBConnection db;
            List<NpgsqlParameter> whereCondition = new List<NpgsqlParameter>();
            object[] selectPatient;
            PatientInterface result = new PatientInterface();

            if (patientI.uuid == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                whereCondition.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                whereCondition.Last().Value = patientI.uuid.Value;

                selectPatient = db.selectOneRow("patientsimulator", "patient", new string[] {
                    "created_at", "model_name", "owner_uuid", "rights_id", "is_alterable",
                    "parent_uuid", "peripheral_vein_uuid", "venous_blood_uuid", "capillary_blood_uuid",
                    "body_uuid", "lung_uuid", "peripheral_artery_uuid", "gender" },
                    whereCondition);
            }
            if (selectPatient == null || selectPatient.Length != 12)
            {
                return null;
            }

            result.uuid = patientI.uuid;
            result.createdAt = selectPatient[0] != null ? (DateTime?)selectPatient[0] : null;
            result.patientName = selectPatient[1] != null ? (string)selectPatient[1] : null;
            result.roleModelUuid = selectPatient[5] != null ? (Guid?)selectPatient[5] : null;
            result.isAlterable = selectPatient[4] != null ? (bool?)selectPatient[4] : null;
            result.gender = selectPatient[12] != null ? (PatientGender?)Global.TranslateGender2DBString.ToList().Find(item => item.Value.Equals((string)selectPatient[12])).Key : null;
            result.owner = new UserInterface();
            result.owner.Uuid = selectPatient[2] != null ? (Guid?)selectPatient[2] : null;
            result.viewerRights = selectPatient[3] != null ? new ViewerRightsInterface(Global.TranslateViewerRightsType2DBInteger.ToList().Find(item => item.Value == (int)selectPatient[3]).Key, null) : null;
            result.organs.Add(new OrganInterface(OrganType.Body, selectPatient[9] != null ? (Guid?)selectPatient[9] : null));
            result.organs.Add(new OrganInterface(OrganType.CapillarBlood, selectPatient[8] != null ? (Guid?)selectPatient[8] : null));
            result.organs.Add(new OrganInterface(OrganType.Lung, selectPatient[10] != null ? (Guid?)selectPatient[10] : null));
            result.organs.Add(new OrganInterface(OrganType.PeripheralArtery, selectPatient[11] != null ? (Guid?)selectPatient[11] : null));
            result.organs.Add(new OrganInterface(OrganType.PeripheralVein, selectPatient[6] != null ? (Guid?)selectPatient[6] : null));
            result.organs.Add(new OrganInterface(OrganType.VenousBlood, selectPatient[7] != null ? (Guid?)selectPatient[7] : null));
            result.organs = result.organs.FindAll(item => item.uuid != null);    /* Filter all non-existent organs (those with no uuid in the DB) */

            return result;
        }

        /// <summary>
        /// Update changes on a specific patient interface, its vitalparameters and continuous values to the DB.
        /// </summary>
        /// <param name="pIFInput"><c>PatientInterface</c> containing updateable information (not null), <c>uuid</c> is required</param>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void WriteFullPatient(PatientInterface patientI)
        {
            DBConnection db;

            if (patientI.uuid == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                foreach (OrganInterface organI in patientI.organs)
                {
                    foreach (VitalParameterInterface vp in patientI.vitalParameters.FindAll(item => Global.AllowedVPsInOrgans[organI.type].Contains(item.type)))
                    {
                        if (vp.referenceValue != null)
                        {
                            db.UpdateReferenceValue(vp, organI);
                        }
                        if (vp.continuousValues != null && vp.continuousValues.Count > 0)
                        {
                            db.InsertContinuousValue(vp.continuousValues, organI);
                        }
                    }
                }

                db.UpdateBasicPatientData(patientI);
            }
        }

        /// <summary>
        /// Inserts full patient, corresponding organs and ICD10 impairments into DB.
        /// </summary>
        /// <param name="patientI">
        /// <c>PatientInterface</c>, required inputs (<c>uuid</c>, <c>viewerRights.Id</c>, <c>owner.Uuid</c>, <c>patientName</c> and <c>isAlterable</c>) will be written to the DB,
        /// from <c>organs</c> and <c>icd10impairments</c> every existing elemnt is written to the DB.
        /// </param>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void CreateFullPatient(PatientInterface patientI)
        {
            DBConnection db;

            if (patientI.uuid == null || patientI.viewerRights == null || patientI.owner?.Uuid == null || patientI.patientName == null || patientI.isAlterable == null ||
                (patientI.organs?.Exists(item => item.uuid == null) ?? false) || (patientI.ICD10Impairments?.Exists(item => item.code == null) ?? false))
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                foreach (OrganInterface organI in patientI.organs)
                {
                    db.InsertOrganValue(organI);

                    foreach (VitalParameterInterface vp in patientI.vitalParameters.FindAll(item => Global.AllowedVPsInOrgans[organI.type].Contains(item.type)))
                    {
                        db.InsertReferenceValue(vp, organI);
                    }
                }

                db.InsertBasicPatientData(patientI);
                
                if(patientI.ICD10Impairments != null)
                {
                    foreach (ICD10ImpairmentInterface impairment in patientI.ICD10Impairments)
                    {
                        db.InsertICD10References(patientI, impairment);
                    }
                }
            }
        }

        /// <summary>
        /// Insert one or more new <c>CurrentVitalValue</c> as continuously created value into the DB.
        /// </summary>
        /// <param name="vitalParameterI">Contains the <c>CurrentVitalValue</c> ti insert</param>
        /// <param name="organI">Contains the required <c>uuid</c> for this type of vital parameter</param>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void CreateContinuousValues(VitalParameterInterface vitalParameterI, OrganInterface organI)
        {
            DBConnection db;

            if (vitalParameterI.continuousValues == null || vitalParameterI.continuousValues.Count == 0 || organI.uuid == null || !Global.AllowedVPsInOrgans[organI.type].Contains(vitalParameterI.type))
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                db.InsertContinuousValue(vitalParameterI.continuousValues, organI);
            }
        }

        /// <summary>
        /// Update one specific vital parameters reference value in the DB.
        /// </summary>
        /// <param name="vitalParameterI">Contains the required <c>type</c> and <c>referenceValue</c> to update</param>
        /// <param name="organI">Contains the required <c>uuid</c> for this type of vital parameter</param>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void WriteReferenceValue(VitalParameterInterface vitalParameterI, OrganInterface organI)
        {
            DBConnection db;

            if (vitalParameterI.referenceValue == null || organI.uuid == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                db.UpdateReferenceValue(vitalParameterI, organI);
            }
        }

        /// <summary>
        /// Update only the direct information of one patient in the DB.
        /// </summary>
        /// <param name="patientI">Contains the updateable fields and required <c>uuid</c></param>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void WritePatientInformation(PatientInterface patientI)
        {
            DBConnection db;

            if (patientI.uuid == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                db.UpdateBasicPatientData(patientI);
            }
        }

        /// <summary>
        /// Checks, if the Demographic and Vital DB already contains any User with either a given UUID, username or insurance number.
        /// </summary>
        /// <param name="userI"><c>UserInterface</c> containing either <c>Uuid</c>, <c>Username</c> or <c>InsuranceNumberHash</c> as unique identifiers</param>
        /// <returns>Returns true if any User with at least one of the given identifiers is registered in the DB.</returns>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public bool IsUserExisting(UserInterface userI)
        {
            DBConnection db;
            List<NpgsqlParameter> identifier = new List<NpgsqlParameter>();
            bool result = false;

            if (userI.InsuranceNumberHash == null && userI.Uuid == null && userI.Username == null)
            {
                throw new ArgumentNullException();
            }

            if (userI.Username != null)
            {
                identifier.Add(new NpgsqlParameter<string>("username", NpgsqlTypes.NpgsqlDbType.Varchar));
                identifier.Last().Value = userI.Username;
            }
            if (userI.Uuid != null)
            {
                identifier.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                identifier.Last().Value = userI.Uuid.Value;
            }
            if (userI.InsuranceNumberHash != null)
            {
                identifier.Add(new NpgsqlParameter<string>("insurance_number_hash", NpgsqlTypes.NpgsqlDbType.Varchar));
                identifier.Last().Value = DBConnection.convertByteArrayToHex(userI.InsuranceNumberHash);
            }

            using (db = new DBConnection())
            {
                foreach(NpgsqlParameter item in identifier)
                {
                    result |= db.selectEntryExists("management", "user", new List<NpgsqlParameter>() { item });
                }
            }

            return result;
        }

        /// <summary>
        /// Read basic information (<c>Uuid</c> and <c>Username</c>) from all user entries present in the DB.
        /// </summary>
        /// <returns>List of <c>UserInterface</c>, each entry containing one user entry from the DB</returns>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public List<UserInterface> ReadAllUsers()
        {
            DBConnection db;
            List<object[]> result = new List<object[]>();
            List<UserInterface> resultUserI = new List<UserInterface>();

            using(db = new DBConnection())
            {
                result = db.selectAllRows("management", "user", new string[] { "uuid", "username" }, null);
            }

            foreach(object[] user in result)
            {
                resultUserI.Add(new UserInterface( user[0] != null ? (Guid?)user[0] : null,
                                            user[1] != null ? (string)user[1] : null, null, null, null, null));
            }

            return resultUserI;
        }

        /// <summary>
        /// Read the full user information (including personal information) based on a given username, uuid or social insurance number.
        /// </summary>
        /// <param name="userI"><c>UserInterface</c> containing either <c>Uuid</c>, <c>Username</c> or <c>InsuranceNumberHash</c> as unique identifiers.</param>
        /// <returns></returns>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public UserInterface ReadFullUser(UserInterface userI)
        {
            DBConnection db;
            List<NpgsqlParameter> identifier = new List<NpgsqlParameter>();
            List<NpgsqlParameter> personellIdentifier = new List<NpgsqlParameter>();
            object[] result;
            object[] resultPersonell;
            UserInterface resultUserI = new UserInterface();

            if (userI.InsuranceNumberHash == null && userI.Uuid == null && userI.Username == null)
            {
                throw new ArgumentNullException();
            }

            if (userI.Username != null)
            {
                identifier.Add(new NpgsqlParameter<string>("username", NpgsqlTypes.NpgsqlDbType.Varchar));
                identifier.Last().Value = userI.Username;
            }
            else if (userI.Uuid != null)
            {
                identifier.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                identifier.Last().Value = userI.Uuid.Value;
            }
            else if (userI.InsuranceNumberHash != null)
            {
                identifier.Add(new NpgsqlParameter<string>("insurance_number_hash", NpgsqlTypes.NpgsqlDbType.Varchar));
                identifier.Last().Value = DBConnection.convertByteArrayToHex(userI.InsuranceNumberHash);
            }

            using (db = new DBConnection())
            {
                result = db.selectOneRow("management", "user", new string[] { "uuid", "username", "password_hash", "pwd_salt", "insurance_number_hash", "user_personell_uuid" }, identifier);
                resultUserI.Uuid = result[0] == null ? null : (Guid?)result[0];
                resultUserI.Username = result[1] == null ? null : (string)result[1];
                resultUserI.PasswordHash = result[2] == null ? null : DBConnection.convertHexToByteArray((string)result[2]);
                resultUserI.PasswordSalt = result[3] == null ? null : DBConnection.convertAsciiToByteArray((string)result[3]);
                resultUserI.InsuranceNumberHash = result[4] == null ? null : DBConnection.convertHexToByteArray((string)result[4]);

                if (result[5] != null)
                {
                    personellIdentifier.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    personellIdentifier.Last().Value = (Guid)result[5];

                    if (db.selectEntryExists("management", "person_data", personellIdentifier))
                    {
                        /* Personell data as Person registered */
                        resultPersonell = db.selectOneRow("management", "person_data", new string[] { "firstname", "lastname", "titles", "email", "birthdate", "nationality_code", "employer" }, personellIdentifier);
                        resultUserI.PersonData = new RealPersonDataInterface((Guid?)personellIdentifier.Last().Value,
                                                        resultPersonell[0] == null ? null : (string)resultPersonell[0],
                                                        resultPersonell[1] == null ? null : (string)resultPersonell[1],
                                                        resultPersonell[2] == null ? null : (string)resultPersonell[2],
                                                        resultPersonell[3] == null ? null : (string)resultPersonell[3],
                                                        resultPersonell[4] == null ? null : (DateTime?)resultPersonell[4],
                                                        resultPersonell[5] == null ? null : (string)resultPersonell[5],
                                                        resultPersonell[6] == null ? null : (string)resultPersonell[6]);
                    }
                    else if (db.selectEntryExists("management", "company_data", personellIdentifier))
                    {
                        /* Personell data as Person registered */
                        resultPersonell = db.selectOneRow("management", "company_data", new string[] { "name", "address", "contact_email", "webaddress" }, personellIdentifier);
                        resultUserI.PersonData = new CompanyPersonDataInterface((Guid?)personellIdentifier.Last().Value,
                                                        resultPersonell[0] == null ? null : (string)resultPersonell[0],
                                                        resultPersonell[1] == null ? null : (string)resultPersonell[1],
                                                        resultPersonell[2] == null ? null : (string)resultPersonell[2],
                                                        resultPersonell[3] == null ? null : (string)resultPersonell[3]);
                    }
                }
                else
                {
                    resultUserI.PersonData = null;
                }
            }

            return resultUserI;
        }

        /// <summary>
        /// Read the users password, password salt and uuid based on a given username, uuid or social insurance number.
        /// </summary>
        /// <param name="userI"><c>UserInterface</c> containing either <c>Uuid</c>, <c>Username</c> or <c>InsuranceNumberHash</c> as unique identifiers.</param>
        /// <returns></returns>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public UserInterface ReadUserPassword(UserInterface userI)
        {
            DBConnection db;
            List<NpgsqlParameter> selectOptions = new List<NpgsqlParameter>();
            object[] selectedUuid;
            object[] selectedPW;
            UserInterface result = new UserInterface();

            if (userI.Uuid == null && userI.Username == null && userI.InsuranceNumberHash == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                if (userI.Uuid != null)
                {
                    result.Uuid = userI.Uuid;

                    selectOptions = new List<NpgsqlParameter>();
                    selectOptions.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    selectOptions.Last().Value = result.Uuid.Value;
                    selectedPW = db.selectOneRow("management", "user", new string[] { "password_hash", "pwd_salt" }, selectOptions);

                    if (selectedPW == null || selectedPW.Length != 2)
                    {
                        throw new ArgumentNullException();
                    }
                    else
                    {
                        result.PasswordHash = DBConnection.convertHexToByteArray((string)selectedPW[0]);
                        result.PasswordSalt = DBConnection.convertAsciiToByteArray((string)selectedPW[1]);
                    }
                }
                else if (userI.Uuid == null && userI.Username != null)
                {
                    selectOptions = new List<NpgsqlParameter>();
                    selectOptions.Add(new NpgsqlParameter<string>("username", NpgsqlTypes.NpgsqlDbType.Varchar));
                    selectOptions.Last().Value = userI.Username;
                    selectedUuid = db.selectOneRow("management", "user", new string[] { "uuid", "password_hash", "pwd_salt" }, selectOptions);

                    if (selectedUuid == null || selectedUuid.Length != 3)
                    {
                        throw new ArgumentNullException();
                    }
                    else
                    {
                        result.Uuid = (Guid?)selectedUuid[0];
                        result.PasswordHash = DBConnection.convertHexToByteArray((string)selectedUuid[1]);
                        result.PasswordSalt = DBConnection.convertAsciiToByteArray((string)selectedUuid[2]);
                    }
                }
                else if (userI.Uuid == null && userI.PasswordHash != null)
                {
                    selectOptions = new List<NpgsqlParameter>();
                    selectOptions.Add(new NpgsqlParameter<string>("insurance_number_hash", NpgsqlTypes.NpgsqlDbType.Varchar));
                    selectOptions.Last().Value = DBConnection.convertByteArrayToHex(userI.InsuranceNumberHash);
                    selectedUuid = db.selectOneRow("management", "user", new string[] { "uuid", "password_hash", "pwd_salt" }, selectOptions);

                    if (selectedUuid == null || selectedUuid.Length != 3)
                    {
                        throw new ArgumentNullException();
                    }
                    else
                    {
                        result.Uuid = (Guid?)selectedUuid[0];
                        result.PasswordHash = DBConnection.convertHexToByteArray((string)selectedUuid[1]);
                        result.PasswordSalt = DBConnection.convertAsciiToByteArray((string)selectedUuid[2]);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Insert a new user and its personell into Demographic and Vital DB.
        /// </summary>
        /// <param name="userI">
        /// <c>UserInterface</c> containing the <c>Uuid</c>, <c>Username</c>, <c>PasswordHash</c> and <c>PassordSalt</c> as required field, alongside with insertable data and optional <c>PersonData</c>.
        /// </param>
        /// <exception cref="SQLNoEntryFoundException"></exception>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void CreateUser(UserInterface userI)
        {
            DBConnection db;
            List<NpgsqlParameter> personellIdentifier = new List<NpgsqlParameter>();

            if (userI.Uuid == null || userI?.PersonData?.Uuid == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                /* Because of the foreign key references, personell data must be inserted first (if existing) */
                if (userI.PersonData?.Uuid != null)
                {
                    personellIdentifier.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    personellIdentifier.Last().Value = userI.PersonData.Uuid.Value;
                    if (!db.selectEntryExists("management", "personalized_data", personellIdentifier))
                    {
                        db.insertUserPersonalizedData(userI);
                    }
                }
                db.insertUser(userI);
            }
        }

        /// <summary>
        /// Insert personell data only.
        /// </summary>
        /// <param name="userI"></param>
        public void CreateUserPersonell(UserInterface userI)
        {
            DBConnection db;
            List<NpgsqlParameter> personellIdentifier = new List<NpgsqlParameter>();

            if (userI.PersonData?.Uuid == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                /* Because of the foreign key references, personell data must be inserted first (if existing) */
                if (userI.PersonData != null)
                {
                    personellIdentifier.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                    personellIdentifier.Last().Value = userI.PersonData.Uuid.Value;
                    if (!db.selectEntryExists("management", "personalized_data", personellIdentifier))
                    {
                        db.insertUserPersonalizedData(userI);
                    }
                }
            }
        }

        /// <summary>
        /// Update user information and (if not null) user personell information
        /// </summary>
        /// <param name="userI"></param>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void WriteUser(UserInterface userI)
        {
            DBConnection db;
            List<NpgsqlParameter> selectUUIDOptions = new List<NpgsqlParameter>();
            object[] selectedUuid;

            if (userI.Uuid == null)
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                if (userI.PersonData != null)
                {
                    if (userI.PersonData.Uuid == null)
                    {
                        selectUUIDOptions = new List<NpgsqlParameter>();
                        selectUUIDOptions.Add(new NpgsqlParameter<Guid>("uuid", NpgsqlTypes.NpgsqlDbType.Uuid));
                        selectUUIDOptions.Last().Value = userI.Uuid.Value;
                        selectedUuid = db.selectOneRow("management", "user", new string[] { "user_personell_uuid" }, selectUUIDOptions);
                        userI.PersonData.Uuid = selectedUuid[0] != null ? (Guid?)selectedUuid[0] : null;
                    }

                    // if no uuid is registered 
                    if (userI.PersonData.Uuid == null)
                    {
                        userI.PersonData.Uuid = Guid.NewGuid();
                        db.insertUserPersonalizedData(userI);
                    }
                    else
                    {
                        db.updateUserPersonalizedData(userI.PersonData);
                    }
                }

                /* update user afterwards, to insert eventually created user_personell_uuid */
                db.updateUser(userI);
            }
        }

        /// <summary>
        /// Insert the given ICD10 impairment links between existing ICD10 diagnoses and any one patient.
        /// </summary>
        /// <param name="patientI">Contains required <c>uuid</c> and <c>ICD10Impairment</c>s to insert into the given patient.</param>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void CreateICD10Impairments(PatientInterface patientI)
        {
            DBConnection db;

            if (patientI.uuid == null || patientI.ICD10Impairments == null || patientI.ICD10Impairments.Count == 0 || (patientI.ICD10Impairments?.Exists(item => item.code == null) ?? false))
            {
                throw new ArgumentNullException();
            }

            using (db = new DBConnection())
            {
                foreach (ICD10ImpairmentInterface icd10 in patientI.ICD10Impairments)
                {
                    db.InsertICD10References(patientI, icd10);
                }
            }
        }

        /// <summary>
        /// Delete all impairment references between the given patient and ICD10 diseases.
        /// </summary>
        /// <param name="patientI">Contains required <c>uuid</c> and <c>ICD10Impairment</c>s to delete from the given patient.</param>
        /// <exception cref="NpgsqlException"></exception>
        /// <exception cref="PostgresException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void RemoveICD10Impairments(PatientInterface patientI)
        {
            DBConnection db;
            List<PatientInterface> patientIs = new List<PatientInterface>();

            if (patientI.uuid == null)
            {
                throw new ArgumentNullException();
            }

            if (patientI.ICD10Impairments != null && patientI.ICD10Impairments.Count != 0 && !patientI.ICD10Impairments.Exists(item => item.code == null))
            {
                using (db = new DBConnection())
                {
                    patientIs.Add(patientI);
                    db.deleteAlementOfICD10(patientIs);
                }
            }
        }
    }
}