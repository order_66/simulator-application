﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PatientSimulator.VitalParameters;

namespace PatientSimulator.Patient
{
    class ICD10Impairment
    {
        private string hiddenCode;
        public string Code { get { return hiddenCode; } private set { setCode(value); } }
        public Dictionary<VitalParameterType, ICD10ImpairedValues> ImpairedVitalParmeters { get; }

        public ICD10Impairment(string code)
        {
            this.Code = code;
        }
        public ICD10Impairment(string code, Dictionary<VitalParameterType, ICD10ImpairedValues> ImpairedVitalParmeters) : this(code)
        {
            this.ImpairedVitalParmeters = ImpairedVitalParmeters;
        }

        #region Getter and Setter Methods
        private void setCode(string value)
        {
            hiddenCode = value;
        }

        /// <summary>
        /// Set a new Value to the Dictionary <c>ImpairedVitalParmeters</c>.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="referenceValue"></param>
        /// <param name="impairedValue"></param>
        /// <exception cref="ArgumentException">In case <c>type</c> is already present in the Dictionary</exception>
        public void addImpairedVitalParameter(VitalParameterType type, float? referenceValue, float? impairedValue)
        {
            if (ImpairedVitalParmeters.ContainsKey(type))
            {
                throw new ArgumentException(String.Format("The Dictionary ImpairedVitalParmeters does already contains a Key of type {0}.", type.ToString()), "type");
            }
            ImpairedVitalParmeters.Add(type, new ICD10ImpairedValues(referenceValue, impairedValue));
        }
        /// <summary>
        /// Set a new Value to the Dictionary <c>ImpairedVitalParmeters</c>.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="referenceValue"></param>
        /// <param name="impairedValue"></param>
        /// <exception cref="ArgumentException">In case <c>type</c> is already present in the Dictionary</exception>
        public void addImpairedVitalParameter(VitalParameterType type, ICD10ImpairedValues value)
        {
            if (ImpairedVitalParmeters.ContainsKey(type))
            {
                throw new ArgumentException(String.Format("The Dictionary ImpairedVitalParmeters does already contains a Key of type {0}.", type.ToString()), "type");
            }
            ImpairedVitalParmeters.Add(type, value);
        }
        #endregion
    }

    class ICD10ImpairedValues
    {
        private float hiddenReferenceValue;
        public float ReferenceValue { get { return hiddenReferenceValue; } set { setReferenceValue(value); } }
        private float hiddenImpairedValue;
        public float ImpairedValue { get { return hiddenImpairedValue; } set { setImpairedValue(value); } }
        public float RelativeImpairment { get { return ImpairedValue / ReferenceValue; } set { setRelativeImpairment(value); } }

        public ICD10ImpairedValues(float? referenceValue, float? impairedValue)
        {
            ReferenceValue = referenceValue ?? throw new ArgumentNullException("referenceValue");
            ImpairedValue = impairedValue ?? throw new ArgumentNullException("impairedValue");
        }

        #region Getter and Setter Methods
        private void setReferenceValue(float value)
        {
            hiddenReferenceValue = value;
        }
        private void setImpairedValue(float value)
        {
            hiddenImpairedValue = value;
        }
        private void setRelativeImpairment(float value)
        {
            setImpairedValue(ReferenceValue * value);
        }
        #endregion
    }
}
