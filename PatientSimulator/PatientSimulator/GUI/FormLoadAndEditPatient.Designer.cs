﻿using System;

namespace PatientSimulator.GUI
{
    partial class FormLoadAndEditPatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLoadAndEditPatient));
            this.userInformationLabel = new System.Windows.Forms.Label();
            this.tBox_Title = new System.Windows.Forms.TextBox();
            this.tBox_LoadExistingTitle = new System.Windows.Forms.Label();
            this.existingPatientsComboBox = new System.Windows.Forms.ComboBox();
            this.LoadExistingPatientButton = new System.Windows.Forms.Button();
            this.tBox_CreateNewPatientTitle = new System.Windows.Forms.Label();
            this.tBox_PatientModelTitle = new System.Windows.Forms.Label();
            this.tBox_PatientNameTitle = new System.Windows.Forms.Label();
            this.tBox_PatientOwnerTitle = new System.Windows.Forms.Label();
            this.SavePatientButton = new System.Windows.Forms.Button();
            this.PatientNameInputBox = new System.Windows.Forms.TextBox();
            this.PatientRoleModelComboBox = new System.Windows.Forms.ComboBox();
            this.CreatePatientButton = new System.Windows.Forms.Button();
            this.LoadCreatePatientTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.patientRightsLabel = new System.Windows.Forms.Label();
            this.PatientOwnerInputComboBox = new System.Windows.Forms.ComboBox();
            this.PatientRightsComboBox = new System.Windows.Forms.ComboBox();
            this.tBox_SimulatedPatientInfo = new System.Windows.Forms.Label();
            this.tBox_SimulatedPatient = new System.Windows.Forms.Label();
            this.pBox_Vitals1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Vitals1)).BeginInit();
            this.SuspendLayout();
            // 
            // userInformationLabel
            // 
            resources.ApplyResources(this.userInformationLabel, "userInformationLabel");
            this.userInformationLabel.Name = "userInformationLabel";
            this.LoadCreatePatientTooltip.SetToolTip(this.userInformationLabel, resources.GetString("userInformationLabel.ToolTip"));
            // 
            // tBox_Title
            // 
            resources.ApplyResources(this.tBox_Title, "tBox_Title");
            this.tBox_Title.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Title.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Title.Name = "tBox_Title";
            this.tBox_Title.ReadOnly = true;
            this.tBox_Title.TabStop = false;
            this.LoadCreatePatientTooltip.SetToolTip(this.tBox_Title, resources.GetString("tBox_Title.ToolTip"));
            this.tBox_Title.TextChanged += new System.EventHandler(this.tBox_Title_TextChanged);
            // 
            // tBox_LoadExistingTitle
            // 
            resources.ApplyResources(this.tBox_LoadExistingTitle, "tBox_LoadExistingTitle");
            this.tBox_LoadExistingTitle.Name = "tBox_LoadExistingTitle";
            this.LoadCreatePatientTooltip.SetToolTip(this.tBox_LoadExistingTitle, resources.GetString("tBox_LoadExistingTitle.ToolTip"));
            // 
            // existingPatientsComboBox
            // 
            resources.ApplyResources(this.existingPatientsComboBox, "existingPatientsComboBox");
            this.existingPatientsComboBox.FormattingEnabled = true;
            this.existingPatientsComboBox.Name = "existingPatientsComboBox";
            this.LoadCreatePatientTooltip.SetToolTip(this.existingPatientsComboBox, resources.GetString("existingPatientsComboBox.ToolTip"));
            // 
            // LoadExistingPatientButton
            // 
            resources.ApplyResources(this.LoadExistingPatientButton, "LoadExistingPatientButton");
            this.LoadExistingPatientButton.Name = "LoadExistingPatientButton";
            this.LoadCreatePatientTooltip.SetToolTip(this.LoadExistingPatientButton, resources.GetString("LoadExistingPatientButton.ToolTip"));
            this.LoadExistingPatientButton.UseVisualStyleBackColor = true;
            this.LoadExistingPatientButton.Click += new System.EventHandler(this.loadPatientOnClick);
            // 
            // tBox_CreateNewPatientTitle
            // 
            resources.ApplyResources(this.tBox_CreateNewPatientTitle, "tBox_CreateNewPatientTitle");
            this.tBox_CreateNewPatientTitle.Name = "tBox_CreateNewPatientTitle";
            this.LoadCreatePatientTooltip.SetToolTip(this.tBox_CreateNewPatientTitle, resources.GetString("tBox_CreateNewPatientTitle.ToolTip"));
            // 
            // tBox_PatientModelTitle
            // 
            resources.ApplyResources(this.tBox_PatientModelTitle, "tBox_PatientModelTitle");
            this.tBox_PatientModelTitle.Name = "tBox_PatientModelTitle";
            this.LoadCreatePatientTooltip.SetToolTip(this.tBox_PatientModelTitle, resources.GetString("tBox_PatientModelTitle.ToolTip"));
            // 
            // tBox_PatientNameTitle
            // 
            resources.ApplyResources(this.tBox_PatientNameTitle, "tBox_PatientNameTitle");
            this.tBox_PatientNameTitle.Name = "tBox_PatientNameTitle";
            this.LoadCreatePatientTooltip.SetToolTip(this.tBox_PatientNameTitle, resources.GetString("tBox_PatientNameTitle.ToolTip"));
            // 
            // tBox_PatientOwnerTitle
            // 
            resources.ApplyResources(this.tBox_PatientOwnerTitle, "tBox_PatientOwnerTitle");
            this.tBox_PatientOwnerTitle.Name = "tBox_PatientOwnerTitle";
            this.LoadCreatePatientTooltip.SetToolTip(this.tBox_PatientOwnerTitle, resources.GetString("tBox_PatientOwnerTitle.ToolTip"));
            // 
            // SavePatientButton
            // 
            resources.ApplyResources(this.SavePatientButton, "SavePatientButton");
            this.SavePatientButton.Name = "SavePatientButton";
            this.LoadCreatePatientTooltip.SetToolTip(this.SavePatientButton, resources.GetString("SavePatientButton.ToolTip"));
            this.SavePatientButton.UseVisualStyleBackColor = true;
            this.SavePatientButton.Click += new System.EventHandler(this.savePatientOnClick);
            // 
            // PatientNameInputBox
            // 
            resources.ApplyResources(this.PatientNameInputBox, "PatientNameInputBox");
            this.PatientNameInputBox.Name = "PatientNameInputBox";
            this.LoadCreatePatientTooltip.SetToolTip(this.PatientNameInputBox, resources.GetString("PatientNameInputBox.ToolTip"));
            // 
            // PatientRoleModelComboBox
            // 
            resources.ApplyResources(this.PatientRoleModelComboBox, "PatientRoleModelComboBox");
            this.PatientRoleModelComboBox.FormattingEnabled = true;
            this.PatientRoleModelComboBox.Name = "PatientRoleModelComboBox";
            this.LoadCreatePatientTooltip.SetToolTip(this.PatientRoleModelComboBox, resources.GetString("PatientRoleModelComboBox.ToolTip"));
            // 
            // CreatePatientButton
            // 
            resources.ApplyResources(this.CreatePatientButton, "CreatePatientButton");
            this.CreatePatientButton.Name = "CreatePatientButton";
            this.LoadCreatePatientTooltip.SetToolTip(this.CreatePatientButton, resources.GetString("CreatePatientButton.ToolTip"));
            this.CreatePatientButton.UseVisualStyleBackColor = true;
            this.CreatePatientButton.Click += new System.EventHandler(this.createPatientOnClick);
            // 
            // patientRightsLabel
            // 
            resources.ApplyResources(this.patientRightsLabel, "patientRightsLabel");
            this.patientRightsLabel.Name = "patientRightsLabel";
            this.LoadCreatePatientTooltip.SetToolTip(this.patientRightsLabel, resources.GetString("patientRightsLabel.ToolTip"));
            // 
            // PatientOwnerInputComboBox
            // 
            resources.ApplyResources(this.PatientOwnerInputComboBox, "PatientOwnerInputComboBox");
            this.PatientOwnerInputComboBox.FormattingEnabled = true;
            this.PatientOwnerInputComboBox.Name = "PatientOwnerInputComboBox";
            this.LoadCreatePatientTooltip.SetToolTip(this.PatientOwnerInputComboBox, resources.GetString("PatientOwnerInputComboBox.ToolTip"));
            // 
            // PatientRightsComboBox
            // 
            resources.ApplyResources(this.PatientRightsComboBox, "PatientRightsComboBox");
            this.PatientRightsComboBox.FormattingEnabled = true;
            this.PatientRightsComboBox.Name = "PatientRightsComboBox";
            this.LoadCreatePatientTooltip.SetToolTip(this.PatientRightsComboBox, resources.GetString("PatientRightsComboBox.ToolTip"));
            // 
            // tBox_SimulatedPatientInfo
            // 
            resources.ApplyResources(this.tBox_SimulatedPatientInfo, "tBox_SimulatedPatientInfo");
            this.tBox_SimulatedPatientInfo.Name = "tBox_SimulatedPatientInfo";
            this.LoadCreatePatientTooltip.SetToolTip(this.tBox_SimulatedPatientInfo, resources.GetString("tBox_SimulatedPatientInfo.ToolTip"));
            // 
            // tBox_SimulatedPatient
            // 
            resources.ApplyResources(this.tBox_SimulatedPatient, "tBox_SimulatedPatient");
            this.tBox_SimulatedPatient.Name = "tBox_SimulatedPatient";
            this.LoadCreatePatientTooltip.SetToolTip(this.tBox_SimulatedPatient, resources.GetString("tBox_SimulatedPatient.ToolTip"));
            // 
            // pBox_Vitals1
            // 
            resources.ApplyResources(this.pBox_Vitals1, "pBox_Vitals1");
            this.pBox_Vitals1.Name = "pBox_Vitals1";
            this.pBox_Vitals1.TabStop = false;
            this.LoadCreatePatientTooltip.SetToolTip(this.pBox_Vitals1, resources.GetString("pBox_Vitals1.ToolTip"));
            // 
            // FormLoadAndEditPatient
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.tBox_SimulatedPatient);
            this.Controls.Add(this.tBox_SimulatedPatientInfo);
            this.Controls.Add(this.PatientRightsComboBox);
            this.Controls.Add(this.patientRightsLabel);
            this.Controls.Add(this.PatientOwnerInputComboBox);
            this.Controls.Add(this.CreatePatientButton);
            this.Controls.Add(this.PatientRoleModelComboBox);
            this.Controls.Add(this.PatientNameInputBox);
            this.Controls.Add(this.SavePatientButton);
            this.Controls.Add(this.tBox_PatientOwnerTitle);
            this.Controls.Add(this.tBox_PatientNameTitle);
            this.Controls.Add(this.tBox_PatientModelTitle);
            this.Controls.Add(this.tBox_CreateNewPatientTitle);
            this.Controls.Add(this.LoadExistingPatientButton);
            this.Controls.Add(this.existingPatientsComboBox);
            this.Controls.Add(this.tBox_LoadExistingTitle);
            this.Controls.Add(this.tBox_Title);
            this.Controls.Add(this.userInformationLabel);
            this.Controls.Add(this.pBox_Vitals1);
            this.Name = "FormLoadAndEditPatient";
            this.LoadCreatePatientTooltip.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.Load += new System.EventHandler(this.FormLoadAndEditPatient_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Vitals1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pBox_Vitals1;
        private System.Windows.Forms.Label userInformationLabel;
        private System.Windows.Forms.TextBox tBox_Title;
        private System.Windows.Forms.Label tBox_LoadExistingTitle;
        private System.Windows.Forms.ComboBox existingPatientsComboBox;
        private System.Windows.Forms.Button LoadExistingPatientButton;
        private System.Windows.Forms.Label tBox_CreateNewPatientTitle;
        private System.Windows.Forms.Label tBox_PatientModelTitle;
        private System.Windows.Forms.Label tBox_PatientNameTitle;
        private System.Windows.Forms.Label tBox_PatientOwnerTitle;
        private System.Windows.Forms.Button SavePatientButton;
        private System.Windows.Forms.TextBox PatientNameInputBox;
        private System.Windows.Forms.ComboBox PatientRoleModelComboBox;
        private System.Windows.Forms.Button CreatePatientButton;
        private System.Windows.Forms.ToolTip LoadCreatePatientTooltip;
        private System.Windows.Forms.ComboBox PatientOwnerInputComboBox;
        private System.Windows.Forms.Label patientRightsLabel;
        private System.Windows.Forms.ComboBox PatientRightsComboBox;
        private System.Windows.Forms.Label tBox_SimulatedPatientInfo;
        private System.Windows.Forms.Label tBox_SimulatedPatient;
    }
}