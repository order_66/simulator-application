﻿namespace PatientSimulator.GUI
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.MainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.MainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vitalparameterKörperToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ChestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HeartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LungToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AbdomenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.liverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cBox_Search = new System.Windows.Forms.ComboBox();
            this.tBox_Time = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenuStrip
            // 
            this.MainMenuStrip.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.MainMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MainMenuToolStripMenuItem,
            this.ChestToolStripMenuItem,
            this.AbdomenToolStripMenuItem,
            this.settingsToolStripMenuItem});
            resources.ApplyResources(this.MainMenuStrip, "MainMenuStrip");
            this.MainMenuStrip.Name = "MainMenuStrip";
            this.MainMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.MainMenuStrip_ItemClicked);
            // 
            // MainMenuToolStripMenuItem
            // 
            this.MainMenuToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.MainMenuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vitalparameterKörperToolStripMenuItem});
            this.MainMenuToolStripMenuItem.Margin = new System.Windows.Forms.Padding(5);
            this.MainMenuToolStripMenuItem.Name = "MainMenuToolStripMenuItem";
            resources.ApplyResources(this.MainMenuToolStripMenuItem, "MainMenuToolStripMenuItem");
            this.MainMenuToolStripMenuItem.Click += new System.EventHandler(this.MainMenuToolStripMenuItem_Click);
            // 
            // vitalparameterKörperToolStripMenuItem
            // 
            this.vitalparameterKörperToolStripMenuItem.Name = "vitalparameterKörperToolStripMenuItem";
            resources.ApplyResources(this.vitalparameterKörperToolStripMenuItem, "vitalparameterKörperToolStripMenuItem");
            this.vitalparameterKörperToolStripMenuItem.Click += new System.EventHandler(this.vitalparameterKörperToolStripMenuItem_Click);
            // 
            // ChestToolStripMenuItem
            // 
            this.ChestToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ChestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HeartToolStripMenuItem,
            this.LungToolStripMenuItem});
            this.ChestToolStripMenuItem.Margin = new System.Windows.Forms.Padding(5);
            this.ChestToolStripMenuItem.Name = "ChestToolStripMenuItem";
            resources.ApplyResources(this.ChestToolStripMenuItem, "ChestToolStripMenuItem");
            this.ChestToolStripMenuItem.Click += new System.EventHandler(this.test3ToolStripMenuItem_Click);
            // 
            // HeartToolStripMenuItem
            // 
            this.HeartToolStripMenuItem.Name = "HeartToolStripMenuItem";
            resources.ApplyResources(this.HeartToolStripMenuItem, "HeartToolStripMenuItem");
            this.HeartToolStripMenuItem.Click += new System.EventHandler(this.HeartToolStripMenuItem_Click);
            // 
            // LungToolStripMenuItem
            // 
            this.LungToolStripMenuItem.Name = "LungToolStripMenuItem";
            resources.ApplyResources(this.LungToolStripMenuItem, "LungToolStripMenuItem");
            this.LungToolStripMenuItem.Click += new System.EventHandler(this.LungToolStripMenuItem_Click);
            // 
            // AbdomenToolStripMenuItem
            // 
            this.AbdomenToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.AbdomenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.liverToolStripMenuItem});
            this.AbdomenToolStripMenuItem.Margin = new System.Windows.Forms.Padding(5);
            this.AbdomenToolStripMenuItem.Name = "AbdomenToolStripMenuItem";
            resources.ApplyResources(this.AbdomenToolStripMenuItem, "AbdomenToolStripMenuItem");
            this.AbdomenToolStripMenuItem.Click += new System.EventHandler(this.AbdomenToolStripMenuItem_Click);
            // 
            // liverToolStripMenuItem
            // 
            this.liverToolStripMenuItem.Name = "liverToolStripMenuItem";
            resources.ApplyResources(this.liverToolStripMenuItem, "liverToolStripMenuItem");
            this.liverToolStripMenuItem.Click += new System.EventHandler(this.liverToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.patientsToolStripMenuItem,
            this.propertiesToolStripMenuItem});
            this.settingsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(5);
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            resources.ApplyResources(this.settingsToolStripMenuItem, "settingsToolStripMenuItem");
            // 
            // patientsToolStripMenuItem
            // 
            this.patientsToolStripMenuItem.Name = "patientsToolStripMenuItem";
            resources.ApplyResources(this.patientsToolStripMenuItem, "patientsToolStripMenuItem");
            this.patientsToolStripMenuItem.Click += new System.EventHandler(this.patientsToolStripMenuItem_Click);
            // 
            // cBox_Search
            // 
            this.cBox_Search.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cBox_Search.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cBox_Search.FormattingEnabled = true;
            this.cBox_Search.Items.AddRange(new object[] {
            resources.GetString("cBox_Search.Items"),
            resources.GetString("cBox_Search.Items1"),
            resources.GetString("cBox_Search.Items2"),
            resources.GetString("cBox_Search.Items3"),
            resources.GetString("cBox_Search.Items4"),
            resources.GetString("cBox_Search.Items5"),
            resources.GetString("cBox_Search.Items6"),
            resources.GetString("cBox_Search.Items7"),
            resources.GetString("cBox_Search.Items8"),
            resources.GetString("cBox_Search.Items9"),
            resources.GetString("cBox_Search.Items10"),
            resources.GetString("cBox_Search.Items11"),
            resources.GetString("cBox_Search.Items12")});
            resources.ApplyResources(this.cBox_Search, "cBox_Search");
            this.cBox_Search.Name = "cBox_Search";
            this.cBox_Search.Sorted = true;
            this.cBox_Search.TabStop = false;
            this.cBox_Search.SelectedIndexChanged += new System.EventHandler(this.cBox_Search_SelectedIndexChanged);
            this.cBox_Search.SelectionChangeCommitted += new System.EventHandler(this.cBox_Search_SelectionChangeCommitted);
            this.cBox_Search.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cBox_Search_KeyDown);
            this.cBox_Search.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cBox_Search_MouseClick);
            // 
            // tBox_Time
            // 
            this.tBox_Time.BackColor = System.Drawing.SystemColors.Control;
            this.tBox_Time.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.tBox_Time, "tBox_Time");
            this.tBox_Time.Name = "tBox_Time";
            this.tBox_Time.ReadOnly = true;
            this.tBox_Time.TabStop = false;
            this.tBox_Time.TextChanged += new System.EventHandler(this.tBox_Time_TextChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            resources.ApplyResources(this.propertiesToolStripMenuItem, "propertiesToolStripMenuItem");
            this.propertiesToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tBox_Time);
            this.Controls.Add(this.cBox_Search);
            this.Controls.Add(this.MainMenuStrip);
            this.IsMdiContainer = true;
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MainMenuStrip.ResumeLayout(false);
            this.MainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem MainMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ChestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HeartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LungToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AbdomenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vitalparameterKörperToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem liverToolStripMenuItem;
        private System.Windows.Forms.ComboBox cBox_Search;
        private System.Windows.Forms.TextBox tBox_Time;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
    }
}

