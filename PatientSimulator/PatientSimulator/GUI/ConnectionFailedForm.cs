﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace PatientSimulator.GUI
{
    public partial class ConnectionFailedForm : System.Windows.Forms.Form
    {
        private string regexPatternIP = @"^([0-9]{1,3}.){3}([0-9]{1,3}){1}$";
        private string regexPatternHost = @"^(\w+?.)+(\w+)$";


        public ConnectionFailedForm()
        {
            InitializeComponent();
            dbHostInput_textBox.Text = Global.DatabaseHost;

            enableAllOtherForms(false);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.maskedTextBox1.KeyDown += new KeyEventHandler(maskedTextBoxKeyDown);
            //this.maskedTextBox1.KeyUp += new KeyEventHandler(maskedTextBoxKeyUp);
            //this.maskedTextBox1.Leave += new EventHandler(leaveTextBox);

            this.button2.Click += new EventHandler(Submit);
            this.button1.Click += new EventHandler(Exit);
        }

        #region Masked IP input filter
        //private void maskedTextBoxKeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.OemPeriod)  // '.' sign
        //    {
        //        int fieldStr;
        //        maskedTextBox1.Text = fillZerosIPMask(maskedTextBox1.SelectionStart, maskedTextBox1.Text, out fieldStr);
        //        maskedTextBox1.SelectionStart = (fieldStr + 1) * 4;
        //    }
        //    else if (e.KeyCode == Keys.Delete && !maskedTextBox1.Text.Substring(maskedTextBox1.SelectionStart, 1).Equals("."))
        //    {
        //        delKeyReplace = true;
        //    }
        //}
        //private void maskedTextBoxKeyUp(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Back && !maskedTextBox1.Text.Substring(maskedTextBox1.SelectionStart, 1).Equals("."))  // on delete action
        //    {
        //        int cursorPos = maskedTextBox1.SelectionStart;
        //        string[] splitTextAtCursor = { maskedTextBox1.Text.Substring(0, cursorPos), maskedTextBox1.Text.Substring(cursorPos) };
        //        maskedTextBox1.Text = String.Format("{0}{2}{1}", splitTextAtCursor[0], splitTextAtCursor[1], " ");
        //        maskedTextBox1.SelectionStart = cursorPos;

        //    }
        //    else if (e.KeyCode == Keys.Delete && delKeyReplace)  // the 'entf'/'del' key
        //    {
        //        int cursorPos = maskedTextBox1.SelectionStart;
        //        string[] splitTextAtCursor = { maskedTextBox1.Text.Substring(0, cursorPos), maskedTextBox1.Text.Substring(cursorPos) };
        //        maskedTextBox1.Text = String.Format("{0}{2}{1}", splitTextAtCursor[0], splitTextAtCursor[1], " ");
        //        maskedTextBox1.SelectionStart = cursorPos;
        //        delKeyReplace = false;
        //    }
        //    else if (numberKeys.Contains(e.KeyCode))
        //    {
        //        int cursorPos = maskedTextBox1.SelectionStart;
        //        maskedTextBox1.Text = replaceTooHighFieldIPMask(cursorPos, maskedTextBox1.Text);
        //        maskedTextBox1.SelectionStart = cursorPos;
        //    }
        //}
        //private void leaveTextBox(object sender, EventArgs e)
        //{
        //    for (int i = 0; i <= 12; i += 4)
        //    {
        //        maskedTextBox1.Text = fillZerosIPMask(i, maskedTextBox1.Text);
        //        maskedTextBox1.Text = replaceTooHighFieldIPMask(i, maskedTextBox1.Text);
        //    }
        //}
        //private string fillZerosIPMask(int cursorPosition, string ipField)
        //{
        //    string[] fillLeadingZeros = { "", "0", "00", "000" };
        //    // Identify the IP field in which the cursor is located. All IP fields have a length of floor digits (including dot).
        //    int fieldStr = (int)Math.Floor(((float)cursorPosition) / 4f);  // Index of the field containing the cursor (seperated by zeros)
        //    string[] ipFields = ipField.Split('.');
        //    ipFields[fieldStr] = ipFields[fieldStr].Replace(" ", "");
        //    ipFields[fieldStr] = String.Format("{0}{1}", fillLeadingZeros[3 - ipFields[fieldStr].Length], ipFields[fieldStr]);
        //    return String.Join(".", ipFields);
        //}
        //private string fillZerosIPMask(int cursorPosition, string ipField, out int ipFieldIndex)
        //{
        //    string[] fillLeadingZeros = { "", "0", "00", "000" };
        //    // Identify the IP field in which the cursor is located. All IP fields have a length of floor digits (including dot).
        //    ipFieldIndex = (int)Math.Floor(((float)cursorPosition) / 4f);  // Index of the field containing the cursor (seperated by zeros)
        //    string[] ipFields = ipField.Split('.');
        //    ipFields[ipFieldIndex] = ipFields[ipFieldIndex].Replace(" ", "");
        //    ipFields[ipFieldIndex] = String.Format("{0}{1}", fillLeadingZeros[3 - ipFields[ipFieldIndex].Length], ipFields[ipFieldIndex]);
        //    return String.Join(".", ipFields);
        //}
        //private string replaceTooHighFieldIPMask(int cursorPosition, string ipField)
        //{
        //    int ipFieldIndex = (int)Math.Floor(((float)cursorPosition) / 4f);  // Index of the field containing the cursor (seperated by zeros)
        //    string[] ipFields = ipField.Split('.');
        //    if (Int32.Parse(ipFields[ipFieldIndex]) > 255) { ipFields[ipFieldIndex] = "255"; }
        //    return String.Join(".", ipFields);
        //}

        //private bool delKeyReplace = false; // Signals, if a white field can be inserted (so del is not called over a '.')
        //private Keys[] numberKeys = new Keys[] { Keys.D0, Keys.D1, Keys.D2, Keys.D3, Keys.D4, Keys.D5, Keys.D6, Keys.D7, Keys.D8, Keys.D9,
        //    Keys.NumPad0, Keys.NumPad1, Keys.NumPad2, Keys.NumPad3, Keys.NumPad4, Keys.NumPad5, Keys.NumPad6, Keys.NumPad7, Keys.NumPad8, Keys.NumPad9 };
        #endregion

        #region Control buttons
        private void Submit(object sender, EventArgs e)
        {
            label1.ForeColor = Color.Black;

            if (Regex.IsMatch(dbHostInput_textBox.Text, regexPatternIP) || Regex.IsMatch(dbHostInput_textBox.Text, regexPatternHost))  // && test connection
            {
                Global.DatabaseHost = dbHostInput_textBox.Text;
                if (MainApplicationContext.VerifyVersion())
                {
                    enableAllOtherForms(true);
                    // Successfully quit this Form, open the main app
                    EstablishedConnection.Invoke(this, new EventArgs());
                    // close is done by the Context
                }
            }
            else
            {
                markWrongHost();
            }
        }
        private void Exit(object sender, EventArgs e)
        {
            label1.ForeColor = Color.Black;
            // Exit the App
            ExitApplication.Invoke(this, new EventArgs());
        }
        #endregion

        #region Events
        public delegate void UserSubmitEventHandler(object source, EventArgs e);

        public event UserSubmitEventHandler EstablishedConnection;
        public event UserSubmitEventHandler ExitApplication;
        #endregion

        private void enableAllOtherForms(bool setActive)
        {   // disable the other forms, before the DB connection problem is resolved
            foreach(System.Windows.Forms.Form item in Application.OpenForms)
            {
                if(item != null && !item.Name.Equals(this.Name)) { item.Enabled = setActive; }
                else if (item.Name.Equals(this.Name) && !setActive) { item.Focus(); }  // Only if the current Form should be in focus as only interactible
            }
        }
        public void markWrongHost()
        {
            label1.ForeColor = Color.Red;
        }
    }
}
