﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using PatientSimulator.Patient;
using PatientSimulator.Patient.DatabaseRequestInterface;
using PatientSimulator.VitalParameters;

namespace PatientSimulator.GUI
{
    partial class FormLung : ParentVPForm
    {
        EditValueCheck editValueCheck;
        private FormEditValue evForm;
        private bool editValueSubscribed = false;

        public FormLung(PatientModel simulatedPatient) : base(simulatedPatient)
        {
            InitializeComponent();
            if (!Global.EditVitalParamteres)
            {
                b_Lung_Edit1.Enabled = false;
            }
        }


        private void b_Lung_Edit1_Click(object sender, EventArgs e)
        {
            openEditValueCheck(4);
        }

        private void FormLung_Load(object sender, EventArgs e)
        {
            ChangeLanguage();
        }
        private void ChangeLanguage()
        {
            string temp = Global.getCurrentLanguage;
            Console.WriteLine(temp);
            foreach (Control c in Controls)
            {
                Type ty = typeof(FormLung);
                Global.CL(ty, c, temp);
            }
        }
        private void FormLung_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                //Debug.WriteLine("Is Visible");
                simulatedPatient.continuousValueCreated += LungEvent;

                try
                {
                    tBox_Lung_BRValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.BreathRate), 3)) + " " + Global.TranslateVPType2Unit[VitalParameterType.BreathRate];
                }
                catch (ArgumentException)
                {
                    Logger.log(LogLevel.WARNING, "Failed to generate initial Values for the VitalParameters at the Lung-Form in the Main GUI.", LogSource.GENERAL);
                }
            }
            else // Is also called when the Form is closed
            {
                //Debug.WriteLine("Is Not Visible");
                simulatedPatient.continuousValueCreated -= LungEvent;
                if (editValueSubscribed && evForm != null)
                {
                    editValueSubscribed = false;
                    evForm.EditedValue -= RefreshEditedValue;
                }
            }
        }
        private void FormIsClosing(object source, EventArgs e)
        {
            //Debug.WriteLine("Is Closing");
        }

        private void LungEvent(object source, VitalParameterEventArgs e)
        {
            CurrentVitalValue currentVital = (CurrentVitalValue)source;
            if (currentVital.type == VitalParameterType.BreathRate)
            {
                if (this.Visible)
                {
                    float breathRateF = currentVital.value;
                    this.Invoke(new MethodInvoker(delegate { tBox_Lung_BRValue.Text = Convert.ToString(Math.Round(breathRateF,1))+" " + Global.TranslateVPType2Unit[VitalParameterType.BreathRate]; }));
                    //Debug.WriteLine(String.Format("Windows Form with Event: At {0} for {1} recorded Value {2}", DateTime.Now.ToString("HH:mm:ss.ff"), currentVital.type, currentVital.value));
                    //tBox_Lung_BRValue.Text = Convert.ToString(breathRateF);
                }
            }
        }
        private void RefreshEditedValue(object source, VitalParameterEventArgs e)
        {
            if (e.type.Equals(VitalParameterType.BreathRate))
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Lung_BRValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.BreathRate), 3)+" " + Global.TranslateVPType2Unit[VitalParameterType.BreathRate]); }));
            }
        }

        private void openEditValueCheck(int eVNr)
        {
            if (Global.EditVitalParamteres)
            {
                EditValueCheck editValueCheck;
                evForm = (FormEditValue)Application.OpenForms["FormEditValue"];
                if (evForm != null) { }
                else
                {
                    editValueSubscribed = false;
                    editValueCheck = new EditValueCheck();
                    EditValueCheck.editValueNr = eVNr;
                    FormEditValue editValue = new FormEditValue(simulatedPatient);
                    evForm = editValue;

                    if (!editValueSubscribed)
                    {
                        editValueSubscribed = true;
                        evForm.EditedValue += RefreshEditedValue;
                    }

                    editValue.ShowDialog();
                }
            }
        }

        private void LungValuesRefresh()
        {
            if (this.Visible)
            {
                try
                {
                    float breathRateF = simulatedPatient.getLastContinuousValue(VitalParameterType.BreathRate)?.value ?? simulatedPatient.getReferenceValue(VitalParameterType.BreathRate);
                    tBox_Lung_BRValue.Text = Convert.ToString(breathRateF);
                }
                catch (Exceptions.NonExistentContinuousValueException)
                {
                    tBox_Lung_BRValue.Text = Convert.ToString(simulatedPatient.getReferenceValue(VitalParameterType.BreathRate));
                }
            }
        }
    }
}
