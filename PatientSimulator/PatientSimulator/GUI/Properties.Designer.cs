﻿namespace PatientSimulator.GUI
{
    partial class Properties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Properties));
            this.tBox_Title = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tBox_AutomaticDB_Setting = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label_DBAccessHint = new System.Windows.Forms.Label();
            this.label_DBAccessSliderLabel = new System.Windows.Forms.Label();
            this.cB_Window = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.cB_Language = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // tBox_Title
            // 
            resources.ApplyResources(this.tBox_Title, "tBox_Title");
            this.tBox_Title.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Title.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Title.Name = "tBox_Title";
            this.tBox_Title.ReadOnly = true;
            this.tBox_Title.TabStop = false;
            this.toolTip1.SetToolTip(this.tBox_Title, resources.GetString("tBox_Title.ToolTip"));
            this.tBox_Title.TextChanged += new System.EventHandler(this.tBox_Title_TextChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, resources.GetString("pictureBox1.ToolTip"));
            // 
            // tBox_AutomaticDB_Setting
            // 
            resources.ApplyResources(this.tBox_AutomaticDB_Setting, "tBox_AutomaticDB_Setting");
            this.tBox_AutomaticDB_Setting.Name = "tBox_AutomaticDB_Setting";
            this.toolTip1.SetToolTip(this.tBox_AutomaticDB_Setting, resources.GetString("tBox_AutomaticDB_Setting.ToolTip"));
            // 
            // trackBar1
            // 
            resources.ApplyResources(this.trackBar1, "trackBar1");
            this.trackBar1.Maximum = 2;
            this.trackBar1.Name = "trackBar1";
            this.toolTip1.SetToolTip(this.trackBar1, resources.GetString("trackBar1.ToolTip"));
            this.trackBar1.Value = 2;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // label_DBAccessHint
            // 
            resources.ApplyResources(this.label_DBAccessHint, "label_DBAccessHint");
            this.label_DBAccessHint.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_DBAccessHint.Name = "label_DBAccessHint";
            this.toolTip1.SetToolTip(this.label_DBAccessHint, resources.GetString("label_DBAccessHint.ToolTip"));
            // 
            // label_DBAccessSliderLabel
            // 
            resources.ApplyResources(this.label_DBAccessSliderLabel, "label_DBAccessSliderLabel");
            this.label_DBAccessSliderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_DBAccessSliderLabel.Name = "label_DBAccessSliderLabel";
            this.toolTip1.SetToolTip(this.label_DBAccessSliderLabel, resources.GetString("label_DBAccessSliderLabel.ToolTip"));
            this.label_DBAccessSliderLabel.Click += new System.EventHandler(this.label_DBAccessLabel_Click);
            // 
            // cB_Window
            // 
            resources.ApplyResources(this.cB_Window, "cB_Window");
            this.cB_Window.FormattingEnabled = true;
            this.cB_Window.Items.AddRange(new object[] {
            resources.GetString("cB_Window.Items"),
            resources.GetString("cB_Window.Items1"),
            resources.GetString("cB_Window.Items2"),
            resources.GetString("cB_Window.Items3")});
            this.cB_Window.Name = "cB_Window";
            this.toolTip1.SetToolTip(this.cB_Window, resources.GetString("cB_Window.ToolTip"));
            this.cB_Window.SelectedIndexChanged += new System.EventHandler(this.cB_Window_SelectedIndexChanged);
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.Name = "checkBox1";
            this.toolTip1.SetToolTip(this.checkBox1, resources.GetString("checkBox1.ToolTip"));
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cB_Language
            // 
            resources.ApplyResources(this.cB_Language, "cB_Language");
            this.cB_Language.FormattingEnabled = true;
            this.cB_Language.Items.AddRange(new object[] {
            resources.GetString("cB_Language.Items"),
            resources.GetString("cB_Language.Items1")});
            this.cB_Language.Name = "cB_Language";
            this.toolTip1.SetToolTip(this.cB_Language, resources.GetString("cB_Language.ToolTip"));
            this.cB_Language.SelectedIndexChanged += new System.EventHandler(this.cB_Language_SelectedIndexChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.toolTip1.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.toolTip1.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            this.toolTip1.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            // 
            // Properties
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cB_Language);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.cB_Window);
            this.Controls.Add(this.label_DBAccessSliderLabel);
            this.Controls.Add(this.label_DBAccessHint);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.tBox_AutomaticDB_Setting);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tBox_Title);
            this.Name = "Properties";
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.Load += new System.EventHandler(this.Properties_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBox_Title;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label tBox_AutomaticDB_Setting;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label_DBAccessHint;
        private System.Windows.Forms.Label label_DBAccessSliderLabel;
        private System.Windows.Forms.ComboBox cB_Window;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox cB_Language;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}