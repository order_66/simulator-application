﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

using PatientSimulator.Patient;
using PatientSimulator.Patient.DatabaseRequestInterface;
using Npgsql;

namespace PatientSimulator.GUI
{
    class MainApplicationContext : ApplicationContext
    {
        private Form1 MainForm;
        private LoginSystem LoginForm;
        private ConnectionFailedForm DBConnectionFailedForm;
        private int _formCount = 0;

        public MainApplicationContext()
        {
            if (VerifyVersion())
            {
                openMainFormChain();
            }
            else
            {
                openDBConnectionFailedForm();
            }
           
        }

        #region Handle Form Events and functions
        private void onFormClosed(object sender, EventArgs e)
        {
            Debug.WriteLine(_formCount + " - " + Application.OpenForms.Count + " - " + sender.GetType().Name);
            _formCount--;
            if (sender.GetType().Name.Contains("ConnectionFailedForm") && !VerifyVersion())  // In case the connection failed form got closed
            {
                openDBConnectionFailedForm();
            }
            if (sender.GetType().Name.Contains("ConnectionFailedForm") && VerifyVersion())
            {
                openMainFormChain();
            }
            if ((_formCount <= 0 && Application.OpenForms.Count == 0) || sender.GetType().Name.Contains("Form1"))  // No forms handled and no forms displayed
            {
                exitApplication(null, null);
            }
        }

        /// <summary>
        /// Only function entitled to call openMainForm(). Checks any prequisits (eg. user logged in) before opening the main form.
        /// </summary>
        public void openMainFormChain()
        {
            if(LoginSystem.UserAuthentificationToken == null)
            {
                openLoginSystem();
            }
            else
            {
                openMainForm();
            }
        }

        public void openLoginSystem()
        {
            if(LoginForm == null)
            {
                LoginForm = new LoginSystem();
                LoginForm.FormClosed += onFormClosed;
                LoginForm.LoginSuccessEvent += loginFormSuccess;
                LoginForm.DBConnectionFailedEvent += failedDBAtLoginSystem;
                _formCount++;
            }
            LoginForm.Show();
        }

        private void openMainForm()
        {
            if(MainForm == null)
            {
                MainForm = new Form1();
                MainForm.FormClosed += onFormClosed;
                MainForm.FailedDBConnection += failedDBAtPatientModel;
                MainForm.Text = Global.ApplicationName;
                _formCount++;
            }
            MainForm.Show();
        }

        private void openDBConnectionFailedForm()
        {
            if(DBConnectionFailedForm == null)
            {
                DBConnectionFailedForm = new ConnectionFailedForm();
                DBConnectionFailedForm.FormClosed += onFormClosed;
                DBConnectionFailedForm.EstablishedConnection += connectionFailedFormSuccess;
                DBConnectionFailedForm.ExitApplication += exitApplication;
                _formCount++;
            }
            DBConnectionFailedForm.Show();
        }

        private void loginFormSuccess(object sender, EventArgs e)
        {
            if(Application.OpenForms["Form1"] == null)
            {
                openMainFormChain();
            }
            LoginForm.Close();
        }

        private void connectionFailedFormSuccess(object sender, EventArgs e)
        {
            if(Application.OpenForms["Form1"] == null)
            {
                openMainFormChain();
            }
            DBConnectionFailedForm.Close();
        }

        private void failedDBAtPatientModel(object source, EventArgs e)
        {
            // It is expected, that source is the PatientModel and EventArgs was thrown by the patient model
            openDBConnectionFailedForm();
        }

        private void failedDBAtLoginSystem(object source, EventArgs e)
        {
            openDBConnectionFailedForm();
        }

        private void exitApplication(object sender, EventArgs e)
        {
            Logger.log(LogLevel.INFO, "Exiting Application", LogSource.GENERAL);
            Application.Exit();
            ExitThread();
        }
        #endregion

        public static bool VerifyVersion()
        {
            bool feedback = false;
            try
            {
                DBAccess database = new DBAccess();
                VersionInterface dbVersion = database.ReadVersion();
                if (!dbVersion.uuid.Equals(Global.DatabaseVersionUUID) || dbVersion.majorVersion != Global.DatabaseVersionNumber[0] || dbVersion.minorVersion != Global.DatabaseVersionNumber[1])
                {
                    Logger.log(LogLevel.ERROR, "Failed to establish a connection to the Demographic and Vital Database. The databse Version is not supported by this application.", LogSource.GENERAL);
                }
                else
                {
                    Logger.log(LogLevel.INFO, "Successfully established a connection to the Demographic and Vital Database.", LogSource.GENERAL);
                    feedback = true;
                }
            }
            catch(PostgresException e)// Catch the specific Exception thrown by failed DB connection
            {
                string logMessage = "Failed to establish a connection to the Demographic and Vital Database.";
                if (e.SqlState.StartsWith("08")) // IP could not be resolved/DB was not found
                {
                    logMessage = logMessage + " The Databse was not found at the given IP.";
                }
                // Any case, either IP not resloved or missing network connection
                Logger.log(LogLevel.INFO, logMessage, LogSource.GENERAL);
            }
            catch (Exception e)
            {
                Logger.log(LogLevel.ERROR, "Unexpected error at the startup process of the App.\n" + e.StackTrace, LogSource.GENERAL);
            }
            feedback = feedback || Global.ApplicationIsDevelopment;   // Returns always true if application is in development, so the application can start even if the DB is not accessible

            return feedback;  // Otherwise: test if version uuid == Global.DatabaseVersionUUID // && minorVersion == Global.DatabaseVersionNumber[1] && majorVersion == Global.DatabaseVersionNumber[0]
        }
    }
}
