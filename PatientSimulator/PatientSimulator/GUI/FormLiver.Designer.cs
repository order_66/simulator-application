﻿namespace PatientSimulator.GUI
{
    partial class FormLiver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLiver));
            this.tBox_Liver_GlucoseValue = new System.Windows.Forms.TextBox();
            this.tBox_Liver_Info1 = new System.Windows.Forms.TextBox();
            this.b_Liver_Edit1_GlucoseLevel = new System.Windows.Forms.Button();
            this.tBox_Liver1 = new System.Windows.Forms.TextBox();
            this.pBox_Liver1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Liver1)).BeginInit();
            this.SuspendLayout();
            // 
            // tBox_Liver_GlucoseValue
            // 
            resources.ApplyResources(this.tBox_Liver_GlucoseValue, "tBox_Liver_GlucoseValue");
            this.tBox_Liver_GlucoseValue.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Liver_GlucoseValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Liver_GlucoseValue.Name = "tBox_Liver_GlucoseValue";
            this.tBox_Liver_GlucoseValue.ReadOnly = true;
            this.tBox_Liver_GlucoseValue.TabStop = false;
            // 
            // tBox_Liver_Info1
            // 
            resources.ApplyResources(this.tBox_Liver_Info1, "tBox_Liver_Info1");
            this.tBox_Liver_Info1.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Liver_Info1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Liver_Info1.Name = "tBox_Liver_Info1";
            this.tBox_Liver_Info1.ReadOnly = true;
            this.tBox_Liver_Info1.TabStop = false;
            // 
            // b_Liver_Edit1_GlucoseLevel
            // 
            resources.ApplyResources(this.b_Liver_Edit1_GlucoseLevel, "b_Liver_Edit1_GlucoseLevel");
            this.b_Liver_Edit1_GlucoseLevel.Name = "b_Liver_Edit1_GlucoseLevel";
            this.b_Liver_Edit1_GlucoseLevel.UseVisualStyleBackColor = true;
            this.b_Liver_Edit1_GlucoseLevel.Click += new System.EventHandler(this.b_Liver_Edit1_GlucoseLevel_Click);
            // 
            // tBox_Liver1
            // 
            resources.ApplyResources(this.tBox_Liver1, "tBox_Liver1");
            this.tBox_Liver1.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Liver1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Liver1.Name = "tBox_Liver1";
            this.tBox_Liver1.ReadOnly = true;
            this.tBox_Liver1.TabStop = false;
            this.tBox_Liver1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // pBox_Liver1
            // 
            resources.ApplyResources(this.pBox_Liver1, "pBox_Liver1");
            this.pBox_Liver1.Name = "pBox_Liver1";
            this.pBox_Liver1.TabStop = false;
            // 
            // FormLiver
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.tBox_Liver_GlucoseValue);
            this.Controls.Add(this.tBox_Liver_Info1);
            this.Controls.Add(this.b_Liver_Edit1_GlucoseLevel);
            this.Controls.Add(this.pBox_Liver1);
            this.Controls.Add(this.tBox_Liver1);
            this.Name = "FormLiver";
            this.Load += new System.EventHandler(this.FormLiver_Load);
            this.Shown += new System.EventHandler(this.FormLiver_Shown);
            this.VisibleChanged += new System.EventHandler(this.FormLiver_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Liver1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBox_Liver1;
        private System.Windows.Forms.PictureBox pBox_Liver1;
        private System.Windows.Forms.Button b_Liver_Edit1_GlucoseLevel;
        private System.Windows.Forms.TextBox tBox_Liver_Info1;
        private System.Windows.Forms.TextBox tBox_Liver_GlucoseValue;
        //private System.Windows.Forms.Timer timer_Liver;
    }
}