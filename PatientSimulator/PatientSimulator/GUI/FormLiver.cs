﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;
using PatientSimulator.Patient;
using PatientSimulator.Patient.DatabaseRequestInterface;
using PatientSimulator.VitalParameters;

namespace PatientSimulator.GUI
{
    partial class FormLiver : ParentVPForm
    {
        private FormEditValue evForm;
        private bool editValueSubscribed = false;

        public FormLiver(PatientModel simulatedPatient) : base(simulatedPatient)
        {
            InitializeComponent();
            simulatedPatient.continuousValueCreated += LiverEvent;
            if (!Global.EditVitalParamteres)
            {
                b_Liver_Edit1_GlucoseLevel.Enabled = false;
            }
        }

        private void FormLiver_Load(object sender, EventArgs e)
        {
            ChangeLanguage();
        }
        private void ChangeLanguage()
        {
            string temp = Global.getCurrentLanguage;
            Console.WriteLine(temp);
            foreach (Control c in Controls)
            {
                Type ty = typeof(FormLiver);
                Global.CL(ty, c, temp);
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormLiver_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                //Debug.WriteLine("Is Visible");
                simulatedPatient.continuousValueCreated += LiverEvent;

                try
                {
                    tBox_Liver_GlucoseValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.CapillarBloodGlucose), 3)) + " " + Global.TranslateVPType2Unit[VitalParameterType.CapillarBloodGlucose];
                }
                catch (ArgumentException)
                {
                    Logger.log(LogLevel.WARNING, "Failed to generate initial Values for the VitalParameters at the Liver-Form in the Main GUI.", LogSource.GENERAL);
                }
            }
            else // Is also called when the Form is closed
            {
                //Debug.WriteLine("Is Not Visible");
                simulatedPatient.continuousValueCreated -= LiverEvent;
                if (editValueSubscribed && evForm != null)
                {
                    editValueSubscribed = false;
                    evForm.EditedValue -= RefreshEditedValue;
                }
            }
        }
        private void LiverEvent(object source, VitalParameterEventArgs e)
        {
            CurrentVitalValue currentVital = (CurrentVitalValue)source;
            if (currentVital.type == VitalParameterType.CapillarBloodGlucose)
            {
                if (this.Visible)
                {
                    float GlucoseLevelF = currentVital.value;
                    this.Invoke(new MethodInvoker(delegate { tBox_Liver_GlucoseValue.Text = Convert.ToString(Math.Round(GlucoseLevelF,3)) + " " + Global.TranslateVPType2Unit[VitalParameterType.CapillarBloodGlucose]; }));
                }
            }
        }
        private void RefreshEditedValue(object source, VitalParameterEventArgs e)
        {
            if (e.type.Equals(VitalParameterType.CapillarBloodGlucose))
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Liver_GlucoseValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.CapillarBloodGlucose), 3)) + " " + Global.TranslateVPType2Unit[VitalParameterType.CapillarBloodGlucose]; }));
            }
        }

        private void b_Liver_Edit1_GlucoseLevel_Click(object sender, EventArgs e)
        {
            openEditValueCheck(5);
        }
        private void openEditValueCheck(int eVNr)
        {
            if (Global.EditVitalParamteres)
            {
                EditValueCheck editValueCheck;
                evForm = (FormEditValue)Application.OpenForms["FormEditValue"];
                if (evForm != null) { }
                else
                {
                    editValueSubscribed = false;
                    editValueCheck = new EditValueCheck();
                    EditValueCheck.editValueNr = eVNr;
                    FormEditValue editValue = new FormEditValue(simulatedPatient);
                    evForm = editValue;

                    if (!editValueSubscribed)
                    {
                        editValueSubscribed = true;
                        evForm.EditedValue += RefreshEditedValue;
                    }

                    editValue.ShowDialog();
                }
            }
        }
        /*private void presentVP(object source, VitalParameterEventArgs e)
        {
            CurrentVitalValue vp = (CurrentVitalValue)source;
            //Debug.WriteLine("At {0} value {1}", vp.type, vp.value);
        }*/

        /*private void timer_Liver_Tick(object sender, EventArgs e)
        {
            float GlucoseLevelF = simulatedPatient.getLastContinuousValue(VitalParameterType.CapillarBloodGlucose)?.value ?? simulatedPatient.getReferenceValue(VitalParameterType.CapillarBloodGlucose);
            tBox_Liver_GlucoseValue.Text = Convert.ToString(GlucoseLevelF);
            Debug.WriteLine(String.Format("Windows Form timer-triggered: At {0} for {1} recorded Value {2}", DateTime.Now.ToString("HH:mm:ss.ff"), VitalParameterType.CapillarBloodGlucose, GlucoseLevelF));
            if (this.Visible) { }
            else { timer_Liver.Stop(); }
        }*/

        private void FormLiver_Shown(object sender, EventArgs e)
        {
            //timer_Liver.Start();
        }
    }
}
