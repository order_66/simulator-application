﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator.GUI
{
    class ParentVPForm : System.Windows.Forms.Form
    {
        protected Patient.PatientModel simulatedPatient;

        /// <summary>
        /// Do never use this empty constructor, use the overload <c>ParentVPForm(PatientSimulator.Patient.ParentModel patient)</c>.
        /// </summary>
        public ParentVPForm() { }
        public ParentVPForm(Patient.PatientModel patient) : this()
        {
            simulatedPatient = patient;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ParentVPForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Name = "ParentVPForm";
            this.Load += new System.EventHandler(this.ParentVPForm_Load);
            this.ResumeLayout(false);

        }

        private void ParentVPForm_Load(object sender, EventArgs e)
        {

        }
    }
}
