﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatientSimulator.GUI
{
    public partial class LoginSystem : Form
    {
        StringGenerator strgen = new StringGenerator();
        Sha256 shaHash = new Sha256();

        Panel login;
        Panel select;
        Panel register;
        Patient.DBAccess db = new Patient.DBAccess();
        public LoginSystem()
        {
            InitializeComponent();
            this.Name = "LoginSystem";
        }

        private void LoginSystem_Load(object sender, EventArgs e)
        {
            defaultPanel.Visible = false;

            login = CreateLoginPanel();
            login.Visible = true;
            Controls.Add(login);
        }

        #region Interaktions-Events
        private void Login_Registerbtn_Click(object sender, EventArgs e)
        {
            //Register Panel aufmachen
            login.Visible = false;
            select = CreateSelectionPanel();
            select.Visible = true;            
            Controls.Add(select);
        }

        private void Ret_Click(object sender, EventArgs e)
        {
            //go back to selection
            register.Visible = false;            
            login.Visible = true;
            Controls.Remove(register);

        }
        private void Login_PasswordTB_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                Login_Loginbtn_Click(sender,e);
            }
        }

        private void Login_Loginbtn_Click(object sender, EventArgs e)
        {
            Patient.DBAccess db = new Patient.DBAccess();
            Sha256 sha = new Sha256();
            string username = login.Controls.Find("Login_UsernameTB", true)[0].Text;
            string userpw = login.Controls.Find("Login_PasswordTB", true)[0].Text;

            Patient.DatabaseRequestInterface.UserInterface user = new Patient.DatabaseRequestInterface.UserInterface();
            user.Username = username;

            try
            {
                if (db.IsUserExisting(user))
                {
                    user = db.ReadUserPassword(user);
                    string salt = Encoding.ASCII.GetString(user.PasswordSalt);
                    byte[] pw = sha.GetSha256(userpw, salt);

                    if (pw.SequenceEqual(user.PasswordHash))
                    {
                        //Programm starten
                        //token generieren
                        Patient.DatabaseRequestInterface.UserInterface useri = db.ReadFullUser(user);

                        UserAuthentificationToken = new UserInfo(useri.Username, useri.Uuid.Value, DateTime.Now);
                        Controls.Find("loginstatus", true)[0].Text = "Login successfull";

                        LoginSuccessEvent?.Invoke(this, new EventArgs());
                        this.Close();
                    }
                    else
                    {
                        Controls.Find("loginstatus", true)[0].Text = "Login failed, Username and/or password incorrect!";
                    }
                }
                else
                {
                    Controls.Find("loginstatus", true)[0].Text = "Login failed, Username and/or password incorrect!";
                }
            }
            catch (PostgresException ex)
            {
                if (ex.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                {
                    DBConnectionFailedEvent?.Invoke(this, new EventArgs());
                }
                Logger.log(LogLevel.ERROR, "Failed to access the Demographic and VitalDatabase.", LogSource.GENERAL);
            }
        }

        private void Priv_Click(object sender, EventArgs e)
        {
            //Registrierung für Privatperson öffnen
            if (Controls.Contains(register))
            {
                Controls.Remove(register);
                register = CreateRegisterPanel(false);
                register.Visible = true;
                Controls.Add(register);
            }
            else
            {
                register = CreateRegisterPanel(false);
                register.Visible = true;
                Controls.Add(register);
            }
        }
        private void Comp_Click(object sender, EventArgs e)
        {
            //Registrierung für Firma öffnen
            if (Controls.Contains(register))
            {
                Controls.Remove(register);
                register = CreateRegisterPanel(true);
                register.Visible = true;
                Controls.Add(register);
            }
            else
            {
                register = CreateRegisterPanel(true);
                register.Visible = true;
                Controls.Add(register);
            }
        }
        private void Pw_TextChanged(object sender, EventArgs e)
        {
            //checken wie "gut" das Passwort ist              
            string pw = Controls.Find("RegPw", true)[0].Text;
            var withoutSpecial = new string(pw.Where(c => Char.IsLetterOrDigit(c) || Char.IsWhiteSpace(c)).ToArray());
            if (pw.Length <= 8)
            {
                Controls.Find("PwL", true)[0].Text = "Password weak, make it longer and more complex (numbers, upper/lower case, special characters)";
                Controls.Find("PwL", true)[0].ForeColor = Color.Red;
            }
            else if (pw.Length > 8 && pw.Length <= 15 && pw.Any(x => char.IsDigit(x)) && pw.Any(x => char.IsUpper(x)) && pw.Any(x => char.IsLower(x)))
            {
                Controls.Find("PwL", true)[0].Text = "Password semi secure, make it longer and more complex (special characters)";
                Controls.Find("PwL", true)[0].ForeColor = Color.Orange;
            }
            else if (pw.Length > 15 && pw.Any(x => char.IsDigit(x)) && pw.Any(x => char.IsUpper(x)) && pw.Any(x => char.IsLower(x)) && pw != withoutSpecial)
            {
                Controls.Find("PwL", true)[0].Text = "Password very secure";
                Controls.Find("PwL", true)[0].ForeColor = Color.Green;
            }
            else if (pw.Length > 15 && pw.Any(x => char.IsDigit(x)) && pw.Any(x => char.IsUpper(x)) && pw.Any(x => char.IsLower(x)))
            {
                Controls.Find("PwL", true)[0].Text = "Password secure";
                Controls.Find("PwL", true)[0].ForeColor = Color.LightGreen;
            }
            //pw "sehr stark" wenn zahlen, groß/klein, sonderzeichen, länge > 15
            //pw "stark" wenn zahlen, groß/klein, sonderzeichen oder länge > 15 ohne sonderzeichen
            //pw mittelstark wenn zahlen, groß/klein >8
            //pw schwach wenn <8
        }
        private void RegPri_Click(object sender, EventArgs e)
        {
            //person registrieren                     
            if (register.Controls.Find("regUser", true)[0].Text == null && register.Controls.Find("RegPw", true)[0].Text == null)
            {
                Controls.Find("regUserL", true)[0].Text = "You need a username to procede;";
                Controls.Find("regUserL", true)[0].ForeColor = Color.Red;

                Controls.Find("PwL", true)[0].Text = "You need a password to procede;";
                Controls.Find("PwL", true)[0].ForeColor = Color.Red;
            }
            else if (register.Controls.Find("RegPw", true)[0].Text == "")
            {
                Controls.Find("PwL", true)[0].Text = "You need a password to procede;";
                Controls.Find("PwL", true)[0].ForeColor = Color.Red;
            }
            else if (register.Controls.Find("regUser", true)[0].Text == "")
            {
                Controls.Find("regUserL", true)[0].Text = "You need a username to procede;";
                Controls.Find("regUserL", true)[0].ForeColor = Color.Red;
            }
            else
            {
                Patient.DBAccess db = new Patient.DBAccess();
                Patient.DatabaseRequestInterface.UserInterface user = new Patient.DatabaseRequestInterface.UserInterface();
                StringGenerator gen = new StringGenerator();
                Sha256 sha = new Sha256();
                string salt = gen.GetRandString(12);
                string pw = register.Controls.Find("RegPw", true)[0].Text;
                user.PasswordHash = sha.GetSha256(register.Controls.Find("RegPw", true)[0].Text, salt);
                user.Username = register.Controls.Find("regUser", true)[0].Text;
                user.InsuranceNumberHash = register.Controls.Find("regSvnr", true)[0].Text != "" ? sha.GetSha256(register.Controls.Find("regSvnr", true)[0].Text, "") : null;
                Guid uuiduser = Guid.NewGuid();
                Guid uuidpersonel = Guid.NewGuid();

                Patient.DatabaseRequestInterface.RealPersonDataInterface data = new Patient.DatabaseRequestInterface.RealPersonDataInterface();
                try
                {
                    if (!db.IsUserExisting(user))
                    {
                        if (register.Controls.Find("RegPw", true)[0].Text.Length > 8 && pw.Any(x => char.IsDigit(x)) && pw.Any(x => char.IsUpper(x)))
                        {
                            Controls.Find("regUserL", true)[0].ForeColor = Color.Black;
                            Controls.Find("regUserL", true)[0].Text = "Username";
                            Controls.Find("PwL", true)[0].ForeColor = Color.Black;

                            user.PasswordSalt = Encoding.ASCII.GetBytes(salt);
                            user.Uuid = uuiduser;

                            data.Birthdate = DateTime.Parse(register.Controls.Find("regBirth", true)[0].Text);
                            data.Email = register.Controls.Find("regeMail", true)[0].Text != "" ? register.Controls.Find("regeMail", true)[0].Text : null;
                            data.Employer = register.Controls.Find("regEmpl", true)[0].Text != "" ? register.Controls.Find("regEmpl", true)[0].Text : null;
                            data.Firstname = register.Controls.Find("regFName", true)[0].Text != "" ? register.Controls.Find("regFName", true)[0].Text : null;
                            data.Lastname = register.Controls.Find("regLName", true)[0].Text != "" ? register.Controls.Find("regLName", true)[0].Text : null;
                            ComboBox natCodes = (ComboBox)register.Controls.Find("regNat", true)[0];
                            data.NationalityCode = natCodes.SelectedValue.ToString();
                            data.Titles = register.Controls.Find("regPostfix", true)[0].Text + "#" + register.Controls.Find("regPrefix", true)[0].Text != "#" ? register.Controls.Find("regPostfix", true)[0].Text + "#" + register.Controls.Find("regPrefix", true)[0].Text : null;
                            data.Uuid = uuidpersonel;

                            user.PersonData = data;

                            db.CreateUser(user);
                            db.CreateUserPersonell(user);

                            UserAuthentificationToken = new UserInfo(user.Username, user.Uuid.Value, DateTime.Now);
                            LoginSuccessEvent?.Invoke(this, new EventArgs());

                            this.Close();
                        }
                        else
                        {
                            register.Controls.Find("PwL", true)[0].Text = "Password too insecure";
                            register.Controls.Find("PwL", true)[0].ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        register.Controls.Find("regUserL", true)[0].Text = "Username already taken";
                        register.Controls.Find("regUserL", true)[0].ForeColor = Color.Red;
                    }

                    //pw mindestens 8 zeichen kleine warnung? (neue form? mit procede ja / nein?)
                }
                catch (PostgresException ex)
                {
                    MessageBox.Show(ex.ToString());
                    if (ex.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                    {
                        DBConnectionFailedEvent?.Invoke(this, new EventArgs());
                    }
                    Logger.log(LogLevel.ERROR, "Failed to access the Demographic and VitalDatabase.", LogSource.GENERAL);
                }
            }
        }
        private void RegComp_Click(object sender, EventArgs e)
        {
            //firma registrieren
            if (register.Controls.Find("RegPw", true)[0].Text == "")
            {
                Controls.Find("PwL", true)[0].Text = "You need a password to procede;";
                Controls.Find("PwL", true)[0].ForeColor = Color.Red;
            }
            else if (register.Controls.Find("regUser", true)[0].Text == "")
            {
                Controls.Find("regUserL", true)[0].Text = "You need a username to procede;";
                Controls.Find("regUserL", true)[0].ForeColor = Color.Red;
            }
            else
            {
                Patient.DBAccess db = new Patient.DBAccess();
                Patient.DatabaseRequestInterface.UserInterface user = new Patient.DatabaseRequestInterface.UserInterface();
                StringGenerator gen = new StringGenerator();
                Sha256 sha = new Sha256();
                string salt = gen.GetRandString(12);
                string pw = register.Controls.Find("RegPw", true)[0].Text;
                user.PasswordHash = sha.GetSha256(pw, salt);
                user.Username = register.Controls.Find("regUser", true)[0].Text;
                user.InsuranceNumberHash = null;

                Guid uuiduser = Guid.NewGuid();
                Guid uuidpersonel = Guid.NewGuid();

                Patient.DatabaseRequestInterface.CompanyPersonDataInterface data = new Patient.DatabaseRequestInterface.CompanyPersonDataInterface();

                try
                {
                    if (!db.IsUserExisting(user))
                    {
                        if (register.Controls.Find("RegPw", true)[0].Text.Length > 8 && pw.Any(x => char.IsDigit(x)) && pw.Any(x => char.IsUpper(x)))
                        {
                            register.Controls.Find("regUser", true)[0].ForeColor = Color.Black;
                            register.Controls.Find("regUser", true)[0].Text = "Username";
                            register.Controls.Find("RegPw", true)[0].ForeColor = Color.Black;

                            user.PasswordSalt = Encoding.ASCII.GetBytes(salt);
                            user.Uuid = uuiduser;

                            data.Name = register.Controls.Find("regCompName", true)[0].Text != "" ? register.Controls.Find("regCompName", true)[0].Text : null;
                            data.Address = register.Controls.Find("regAdr", true)[0].Text != "" ? register.Controls.Find("regAdr", true)[0].Text : null;
                            data.ContactEMail = register.Controls.Find("regMail", true)[0].Text != "" ? register.Controls.Find("regMail", true)[0].Text : null;
                            data.Webaddress = register.Controls.Find("regWeb", true)[0].Text != "" ? register.Controls.Find("regWeb", true)[0].Text : null;
                            data.Uuid = uuidpersonel;
                            user.PersonData = data;

                            db.CreateUser(user);
                            db.CreateUserPersonell(user);

                            UserAuthentificationToken = new UserInfo(user.Username, user.Uuid.Value, DateTime.Now);
                            LoginSuccessEvent?.Invoke(this, new EventArgs());
                            this.Close();
                        }
                        else
                        {
                            register.Controls.Find("RegPw", true)[0].Text = "Password too insecure";
                            register.Controls.Find("RegPw", true)[0].ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        register.Controls.Find("regUser", true)[0].Text = "Username already taken";
                        register.Controls.Find("regUser", true)[0].ForeColor = Color.Red;
                    }
                }
                catch (PostgresException ex)
                {
                    if (ex.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                    {
                        DBConnectionFailedEvent?.Invoke(this, new EventArgs());
                    }
                    Logger.log(LogLevel.ERROR, "Failed to access the Demographic and VitalDatabase.", LogSource.GENERAL);
                }
            }
        }
        #endregion
        #region panel definitionen
        private Panel CreateRegisterPanel(bool type)
        {
            Panel register = new Panel();

            //creatingPanel
            register.Location = new Point(50, 0);
            register.Name = "Register";
            register.Size = new Size(1000, 1200);

            if (type)
            {
                //firmenkunde
                //firmensachen

                PictureBox logo = new PictureBox();
                TextBox username = new TextBox();
                TextBox pw = new TextBox();
                TextBox compName = new TextBox();
                TextBox adress = new TextBox();
                TextBox eMail = new TextBox();
                TextBox webadress = new TextBox();
                Button regComp = new Button();
                Button ret = new Button();

                Label usernameL = new Label();
                Label pwL = new Label();
                Label compNameL = new Label();
                Label adressL = new Label();
                Label eMailL = new Label();
                Label webadressL = new Label();

                logo.Location = new Point(20, 40);
                logo.SizeMode = PictureBoxSizeMode.StretchImage;
                logo.Image = PatientSimulator.Properties.Resources.Logo;

                usernameL.Location = new Point(20, 100);
                usernameL.Size = new Size(400, 20);
                usernameL.Text = "Username*";
                usernameL.Name = "regUserL";

                username.Location = new Point(20, 115);
                username.Size = new Size(200, 20);
                username.Text = null;
                username.Name = "regUser";
                username.MaxLength = 32;

                pwL.Location = new Point(20, 135);
                pwL.Size = new Size(600, 20);
                pwL.Text = "Password*";
                pwL.Name = "PwL";

                pw.Location = new Point(20, 150);
                pw.Size = new Size(200, 20);
                pw.Text = null;
                pw.PasswordChar = '*';
                pw.Name = "RegPw";
                pw.TextChanged += Pw_TextChanged;

                compNameL.Location = new Point(20, 170);
                compNameL.Size = new Size(200, 20);
                compNameL.Text = "Companyname";

                compName.Location = new Point(20, 185);
                compName.Size = new Size(200, 20);
                compName.Text = null;
                compName.Name = "regCompName";
                compName.MaxLength = 60;

                adressL.Location = new Point(20, 205);
                adressL.Size = new Size(200, 20);
                adressL.Text = "Adress";

                adress.Location = new Point(20, 220);
                adress.Size = new Size(200, 20);
                adress.Text = null;
                adress.Name = "regAdr";
                adress.MaxLength = 60;

                eMailL.Location = new Point(20, 240);
                eMailL.Size = new Size(200, 20);
                eMailL.Text = "eMail";

                eMail.Location = new Point(20, 255);
                eMail.Size = new Size(200, 20);
                eMail.Text = null;
                eMail.Name = "regMail";
                eMail.MaxLength = 60;

                webadressL.Location = new Point(20, 275);
                webadressL.Size = new Size(200, 20);
                webadressL.Text = "Webadress";

                webadress.Location = new Point(20, 290);
                webadress.Size = new Size(200, 20);
                webadress.Text = null;
                webadress.Name = "regWeb";
                webadress.MaxLength = 60;

                regComp.Location = new Point(20, 340);
                regComp.Size = new Size(100, 20);
                regComp.Text = "Register";
                regComp.Click += RegComp_Click;

                ret.Location = new Point(200, 340);
                ret.Size = new Size(100, 20);
                ret.Text = "Return";
                ret.Click += Ret_Click;

                register.Controls.Add(logo);
                register.Controls.Add(username);
                register.Controls.Add(pw);
                register.Controls.Add(compName);
                register.Controls.Add(adress);
                register.Controls.Add(eMail);
                register.Controls.Add(webadress);
                register.Controls.Add(regComp);
                register.Controls.Add(ret);

                register.Controls.Add(usernameL);
                register.Controls.Add(pwL);
                register.Controls.Add(compNameL);
                register.Controls.Add(adressL);
                register.Controls.Add(eMailL);
                register.Controls.Add(webadressL);

            }
            else
            {
                //privatkunde
                //ecard möglich

                PictureBox logo = new PictureBox();
                //PictureBox eCard = new PictureBox();
                TextBox username = new TextBox();
                TextBox pw = new TextBox();
                TextBox svnr = new TextBox(); //auch über Ecard Klick abfragbar
                TextBox fName = new TextBox(); //Ecard
                TextBox lName = new TextBox(); //Ecard
                TextBox prefix = new TextBox(); //Ecard?
                TextBox postfix = new TextBox(); //Ecard?
                TextBox eMail = new TextBox();
                DateTimePicker birthdate = new DateTimePicker(); //Ecard
                ComboBox natCode = new ComboBox(); //Ecard?
                TextBox employer = new TextBox();
                Button regPri = new Button();
                Button ret = new Button();

                Label usernameL = new Label();
                Label pwL = new Label();
                Label svnrL = new Label();
                Label fNameL = new Label();
                Label lNameL = new Label();
                Label prefixL = new Label();
                Label postfixL = new Label();
                Label eMailL = new Label();
                Label birthdateL = new Label();
                Label natCodeL = new Label();
                Label employerL = new Label();

                logo.Location = new Point(20, 40);
                logo.SizeMode = PictureBoxSizeMode.StretchImage;
                logo.Image = PatientSimulator.Properties.Resources.Logo;

                /* eCard.Location = new Point(700, 300);
                 eCard.SizeMode = PictureBoxSizeMode.StretchImage;
                 eCard.Image = Properties.Resources.Ecard;
                */

                usernameL.Location = new Point(20, 100);
                usernameL.Size = new Size(400, 20);
                usernameL.Text = "Username*";
                usernameL.Name = "regUserL";

                username.Location = new Point(20, 115);
                username.Size = new Size(200, 20);
                username.Text = null;
                username.Name = "regUser";
                username.MaxLength = 32;

                pwL.Location = new Point(20, 135);
                pwL.Size = new Size(600, 20);
                pwL.Text = "Password*";
                pwL.Name = "PwL";

                pw.Location = new Point(20, 150);
                pw.Size = new Size(200, 20);
                pw.Text = null;
                pw.PasswordChar = '*';
                pw.Name = "RegPw";
                pw.TextChanged += Pw_TextChanged;

                svnrL.Location = new Point(20, 170);
                svnrL.Size = new Size(200, 20);
                svnrL.Text = "Social Security Number";
                svnrL.Name = "svnrL";

                svnr.Location = new Point(20, 185);
                svnr.Size = new Size(200, 20);
                svnr.Text = null;
                svnr.Name = "regSvnr";

                fNameL.Location = new Point(20, 205);
                fNameL.Size = new Size(200, 20);
                fNameL.Text = "First Name";

                fName.Location = new Point(20, 220);
                fName.Size = new Size(200, 20);
                fName.Text = null;
                fName.Name = "regFName";
                fName.MaxLength = 60;

                lNameL.Location = new Point(20, 240);
                lNameL.Size = new Size(200, 20);
                lNameL.Text = "Last Name";

                lName.Location = new Point(20, 255);
                lName.Size = new Size(200, 20);
                lName.Text = null;
                lName.Name = "regLName";
                lName.MaxLength = 60;

                prefixL.Location = new Point(20, 275);
                prefixL.Size = new Size(200, 20);
                prefixL.Text = "Titles prefix";

                prefix.Location = new Point(20, 290);
                prefix.Size = new Size(200, 20);
                prefix.Text = null;
                prefix.Name = "regPrefix";
                prefix.MaxLength = 29;

                postfixL.Location = new Point(300, 275);
                postfixL.Size = new Size(200, 20);
                postfixL.Text = "Titles, postfix";

                postfix.Location = new Point(300, 290);
                postfix.Size = new Size(200, 20);
                postfix.Text = null;
                postfix.Name = "regPostfix";
                postfix.MaxLength = 30;

                eMailL.Location = new Point(20, 310);
                eMailL.Size = new Size(200, 20);
                eMailL.Text = "E-Mail";

                eMail.Location = new Point(20, 325);
                eMail.Size = new Size(200, 20);
                eMail.Text = null;
                eMail.Name = "regeMail";
                eMail.MaxLength = 60;

                birthdateL.Location = new Point(20, 345);
                birthdateL.Size = new Size(200, 20);
                birthdateL.Text = "Birthdate";

                birthdate.Location = new Point(20, 360);
                birthdate.Size = new Size(200, 20);
                birthdate.Format = DateTimePickerFormat.Long;
                birthdate.MaxDate = DateTime.Now;
                birthdate.MinDate = new DateTime(1920, 1, 1);
                birthdate.Name = "regBirth";

                natCodeL.Location = new Point(20, 380);
                natCodeL.Size = new Size(200, 20);
                natCodeL.Text = "National Code";

                natCode.Location = new Point(20, 395);
                natCode.Size = new Size(200, 20);
                NatCodes codes = new NatCodes();
                natCode.DataSource = new BindingSource(codes.Codes, null);
                natCode.DisplayMember = "Key";
                natCode.ValueMember = "Value";
                natCode.Text = null;
                natCode.Name = "regNat";

                employerL.Location = new Point(20, 415);
                employerL.Size = new Size(200, 20);
                employerL.Text = "Employer";

                employer.Location = new Point(20, 430);
                employer.Size = new Size(200, 20);
                employer.Text = null;
                employer.Name = "regEmpl";
                employer.MaxLength = 60;

                regPri.Location = new Point(20, 500);
                regPri.Size = new Size(100, 20);
                regPri.Text = "Register";
                regPri.Click += RegPri_Click;

                ret.Location = new Point(200, 500);
                ret.Size = new Size(100, 20);
                ret.Text = "Return";
                ret.Click += Ret_Click;

                register.Controls.Add(logo);
                register.Controls.Add(username);
                register.Controls.Add(pw);
                register.Controls.Add(svnr);
                register.Controls.Add(fName);
                register.Controls.Add(lName);
                register.Controls.Add(prefix);
                register.Controls.Add(postfix);
                register.Controls.Add(eMail);
                register.Controls.Add(birthdate);
                register.Controls.Add(natCode);
                register.Controls.Add(employer);
                register.Controls.Add(regPri);
                register.Controls.Add(ret);

                register.Controls.Add(usernameL);
                register.Controls.Add(pwL);
                register.Controls.Add(svnrL);
                register.Controls.Add(fNameL);
                register.Controls.Add(lNameL);
                register.Controls.Add(prefixL);
                register.Controls.Add(postfixL);
                register.Controls.Add(eMailL);
                register.Controls.Add(birthdateL);
                register.Controls.Add(natCodeL);
                register.Controls.Add(employerL);

                //register.Controls.Add(eCard);

                return register;
            }
            return register;
        }

        private Panel CreateSelectionPanel()
        {
            Panel select = new Panel();
            //creatingPanel
            select.Location = new Point(0, 0);
            select.Name = "Select";
            select.Size = new Size(1000, 40);

            //elemente
            Button comp = new Button();
            comp.Location = new Point(20, 20);
            comp.Size = new Size(100, 20);
            comp.Text = "Company";
            comp.Name = "comp";
            comp.Click += Comp_Click;

            Button priv = new Button();                        
            priv.Location = new Point(200, 20);
            priv.Size = new Size(100, 20);
            priv.Text = "Private";
            priv.Name = "priv";
            priv.Click += Priv_Click;
                 
            select.Controls.Add(comp);
            select.Controls.Add(priv);           
            
            return select;
        }
        private Panel CreateLoginPanel()
        {
            Panel login = new Panel();
            TextBox Login_UsernameTB = new TextBox();
            TextBox Login_PasswordTB = new TextBox();
            Label label1 = new Label();
            Label label2 = new Label();
            Label loginstatus = new Label();
            Button Login_Loginbtn = new Button();
            Button Login_Registerbtn = new Button();
            PictureBox Login_Logo = new PictureBox();
            PictureBox Login_ECard = new PictureBox();

            //creatingPanel
            login.Location = new Point(0, 0);
            login.Name = "Login";
            login.Size = new Size(1000, 400);

            //locations

            Login_ECard.Location = new Point(500, 250);
            Login_ECard.SizeMode = PictureBoxSizeMode.StretchImage;
            Login_ECard.Image = PatientSimulator.Properties.Resources.Ecard;
            Login_ECard.Name = "Login_Ecard";

            Login_Logo.Location = new Point(148, 12);
            Login_Logo.SizeMode = PictureBoxSizeMode.StretchImage;
            Login_Logo.Size = new Size(359, 150);
            Login_Logo.Image = PatientSimulator.Properties.Resources.Logo;
            Login_Logo.Name = "Login_Logo";

            label1.Location = new Point(31, 200);
            label1.Size = new Size(58, 13);
            label1.Text = "Username:";
            label1.Name = "label1";

            label2.Location = new Point(31, 244);
            label2.Size = new Size(56, 13);
            label2.Text = "Password:";
            label2.Name = "label2";

            Login_UsernameTB.Location = new Point(148, 197);
            Login_UsernameTB.Size = new Size(359, 20);
            Login_UsernameTB.Text = "";
            Login_UsernameTB.Name = "Login_UsernameTB";

            Login_PasswordTB.Location = new Point(148, 241);
            Login_PasswordTB.Size = new Size(359, 20);
            Login_PasswordTB.Text = "";
            Login_PasswordTB.PasswordChar = '*';
            Login_PasswordTB.Name = "Login_PasswordTB";
            Login_PasswordTB.KeyDown += Login_PasswordTB_KeyDown;

            Login_Loginbtn.Location = new Point(432, 334);
            Login_Loginbtn.Size = new Size(75, 23);
            Login_Loginbtn.Text = "Login";
            Login_Loginbtn.Click += Login_Loginbtn_Click;

            loginstatus.Location = new Point(323, 360);
            loginstatus.Size = new Size(300, 20);
            loginstatus.Text = "";
            loginstatus.Name = "loginstatus";

            Login_Registerbtn.Location = new Point(323, 334);
            Login_Registerbtn.Size = new Size(75, 23);
            Login_Registerbtn.Text = "Register";
            Login_Registerbtn.Click += Login_Registerbtn_Click;

            //login.Controls.Add(Login_ECard);
            login.Controls.Add(Login_Logo);
            login.Controls.Add(label1);
            login.Controls.Add(label2);
            login.Controls.Add(Login_UsernameTB);
            login.Controls.Add(Login_PasswordTB);
            login.Controls.Add(Login_Loginbtn);
            login.Controls.Add(Login_Registerbtn);
            login.Controls.Add(loginstatus);

            return login;
        }
        #endregion

        public event EventHandler LoginSuccessEvent;
        public event EventHandler DBConnectionFailedEvent;
        public static UserInfo UserAuthentificationToken { get; private set; } = null;
    }
    public class UserInfo
    {
        private string username;
        private Guid uuid;
        private DateTime loginTime;

        public string Username { get => username; private set { username = value; } }
        public Guid Uuid { get => uuid; private set { uuid = value; } }
        public DateTime LoginTime { get => loginTime; private set { loginTime = value; } }
        public UserInfo(string username, Guid uuid, DateTime loginTime)
        {
            this.username = username;
            this.uuid = uuid;
            this.loginTime = loginTime;
        }
    }
}
