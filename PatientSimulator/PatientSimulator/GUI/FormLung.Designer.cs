﻿namespace PatientSimulator.GUI
{
    partial class FormLung
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLung));
            this.tBox_Lung1 = new System.Windows.Forms.TextBox();
            this.tBox_Lung_1 = new System.Windows.Forms.TextBox();
            this.tBox_Lung_BRValue = new System.Windows.Forms.TextBox();
            this.b_Lung_Edit1 = new System.Windows.Forms.Button();
            this.pBox_Lung1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Lung1)).BeginInit();
            this.SuspendLayout();
            // 
            // tBox_Lung1
            // 
            resources.ApplyResources(this.tBox_Lung1, "tBox_Lung1");
            this.tBox_Lung1.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Lung1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Lung1.Name = "tBox_Lung1";
            this.tBox_Lung1.ReadOnly = true;
            this.tBox_Lung1.TabStop = false;
            // 
            // tBox_Lung_1
            // 
            resources.ApplyResources(this.tBox_Lung_1, "tBox_Lung_1");
            this.tBox_Lung_1.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Lung_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Lung_1.Name = "tBox_Lung_1";
            this.tBox_Lung_1.ReadOnly = true;
            this.tBox_Lung_1.TabStop = false;
            // 
            // tBox_Lung_BRValue
            // 
            resources.ApplyResources(this.tBox_Lung_BRValue, "tBox_Lung_BRValue");
            this.tBox_Lung_BRValue.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Lung_BRValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Lung_BRValue.Name = "tBox_Lung_BRValue";
            this.tBox_Lung_BRValue.ReadOnly = true;
            this.tBox_Lung_BRValue.TabStop = false;
            // 
            // b_Lung_Edit1
            // 
            resources.ApplyResources(this.b_Lung_Edit1, "b_Lung_Edit1");
            this.b_Lung_Edit1.Name = "b_Lung_Edit1";
            this.b_Lung_Edit1.UseVisualStyleBackColor = true;
            this.b_Lung_Edit1.Click += new System.EventHandler(this.b_Lung_Edit1_Click);
            // 
            // pBox_Lung1
            // 
            resources.ApplyResources(this.pBox_Lung1, "pBox_Lung1");
            this.pBox_Lung1.Name = "pBox_Lung1";
            this.pBox_Lung1.TabStop = false;
            // 
            // FormLung
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.b_Lung_Edit1);
            this.Controls.Add(this.tBox_Lung_BRValue);
            this.Controls.Add(this.tBox_Lung_1);
            this.Controls.Add(this.pBox_Lung1);
            this.Controls.Add(this.tBox_Lung1);
            this.Name = "FormLung";
            this.ShowIcon = false;
            this.Load += new System.EventHandler(this.FormLung_Load);
            this.VisibleChanged += new System.EventHandler(this.FormLung_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Lung1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBox_Lung1;
        private System.Windows.Forms.PictureBox pBox_Lung1;
        private System.Windows.Forms.TextBox tBox_Lung_1;
        private System.Windows.Forms.TextBox tBox_Lung_BRValue;
        private System.Windows.Forms.Button b_Lung_Edit1;
        //private System.Windows.Forms.Timer timer_Lung;
    }
}