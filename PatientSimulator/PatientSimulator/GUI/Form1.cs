﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PatientSimulator.Patient.DatabaseRequestInterface;
using PatientSimulator.Patient;
using System.Diagnostics;

namespace PatientSimulator.GUI
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        private PatientModel simulatedPatient;
        private Form currentOpenForm;

        public Form1()
        {
            if (Global.ApplicationIsDevelopment)  // Loading a patient test model if the application is in developer mode
            {
                simulatedPatient = new PatientModel(TestCases.getGenericPatientInterface());
            }

            InitializeComponent();
            this.FormClosing += thisIsClosing;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            openGeneralParametersChild();
            setTimeDate();

        }
        #region Methods to open all the Child Forms 
        private void openGeneralParametersChild()
        {
            if (simulatedPatient != null)
            {
                currentOpenForm?.Close();
                FormGeneralParameters VitalChild = new FormGeneralParameters(simulatedPatient);
                currentOpenForm = VitalChild;
                VitalChild.MdiParent = this;
                //.ClientSize = new Size(2000,800);
                //this.WindowState = FormWindowState.Maximized;
                VitalChild.FormBorderStyle = FormBorderStyle.None;
                VitalChild.Dock = DockStyle.Fill;
                VitalChild.Show();
            }
            else
            {
                openLoadAndEditPatientChild();
            }
        }

        private void openHeartChild()
        {
            if (simulatedPatient != null)
            {
                currentOpenForm?.Close();
                FormHeart HeartChild = new FormHeart(simulatedPatient);
                currentOpenForm = HeartChild;
                HeartChild.MdiParent = this;
                HeartChild.FormBorderStyle = FormBorderStyle.None;
                HeartChild.Dock = DockStyle.Fill;
                HeartChild.Show();
            }
            else
            {
                openLoadAndEditPatientChild();
            }
        }
        private void openLungChild()
        {
            if (simulatedPatient != null)
            {
                currentOpenForm?.Close();
                FormLung LungChild = new FormLung(simulatedPatient);
                currentOpenForm = LungChild;
                LungChild.MdiParent = this;
                LungChild.FormBorderStyle = FormBorderStyle.None;
                LungChild.Dock = DockStyle.Fill;
                LungChild.Show();
            }
            else
            {
                openLoadAndEditPatientChild();
            }
        }
        private void openLiverChild()
        {
            if(simulatedPatient != null)
            {
                currentOpenForm?.Close();
                FormLiver LiverChild = new FormLiver(simulatedPatient);
                currentOpenForm = LiverChild;
                LiverChild.MdiParent = this;
                LiverChild.FormBorderStyle = FormBorderStyle.None;
                LiverChild.Dock = DockStyle.Fill;
                LiverChild.Show();
            }
            else
            {
                openLoadAndEditPatientChild();
            }
        }
        private void openPropertiesChild()
        {
            if (simulatedPatient != null)
            {
                currentOpenForm?.Close();
                Properties PropertiesChild = new Properties(simulatedPatient);
                currentOpenForm = PropertiesChild;
                PropertiesChild.MdiParent = this;
                PropertiesChild.FormBorderStyle = FormBorderStyle.None;
                PropertiesChild.Dock = DockStyle.Fill;
                PropertiesChild.Show();
            }
            else
            {
                openLoadAndEditPatientChild();
            }
        }
        private void openLoadAndEditPatientChild()
        {
            currentOpenForm?.Close();
            FormLoadAndEditPatient LoadAndEditChild = new FormLoadAndEditPatient(simulatedPatient);
            currentOpenForm = LoadAndEditChild;
            LoadAndEditChild.MdiParent = this;
            LoadAndEditChild.PatientCreated += newPatientCreated;
            LoadAndEditChild.FailedDBConnectionEvent += failedDBAtPatientModel;
            if (simulatedPatient == null)
            {
                LoadAndEditChild.setInfoBox(Color.DarkOrange, "Please load or create a patient model to begin the simulation.");
            }
            //.ClientSize = new Size(2000,800);
            //this.WindowState = FormWindowState.Maximized;
            LoadAndEditChild.FormBorderStyle = FormBorderStyle.None;
            LoadAndEditChild.Dock = DockStyle.Fill;
            LoadAndEditChild.Show();
        }
        #endregion
        private void setTimeDate()
        {
            timer1.Start();
            //tBox_Time.Text = DateTime.Now.ToLongTimeString();
        }
        private void test3ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void newPatientCreated(object sender, PatientCreatedEventArgs e)
        {
            this.Text = String.Format("{0} - Simulating '{1} - {2}'", Global.ApplicationName, e.SimulatedPatient.patientName, e.SimulatedPatient.ViewerRights.ToString());
            if(this.simulatedPatient != null)
            {
                this.simulatedPatient.FailedDBConnection -= failedDBAtPatientModel;
            }
            this.simulatedPatient = e.SimulatedPatient;
            this.simulatedPatient.FailedDBConnection += failedDBAtPatientModel;
        }

        private void vitalparameterKörperToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openGeneralParametersChild();
        }


        private void HeadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void MainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void HeartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openHeartChild();
        }

        private void LungToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openLungChild();
        }

        private void liverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openLiverChild();
        }

        #region Events
        public event EventHandler FailedDBConnection;   // Please invoke this event when you catch the PatientModel.FailedDBConnection event. So the Context can display the connection error form

        private void failedDBAtPatientModel(object source, EventArgs e)  // call this function by PatientModelVariable.FailedDBConnection += new EventHandler(failedDBAtPatientModel);
        {
            FailedDBConnection.Invoke(source, e);
        }

        #endregion

        private void tBox_Time_TextChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tBox_Time.Text = DateTime.Now.ToString("HH:mm:ss  yyyy.MM.dd");
            timer1.Start();
        }
        #region Search-Code
        private void cBox_Search_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cBox_Search.Text.ToLower())
            {
                case "heart":
                        openHeartChild();
                        break;
                    case "mean blood pressure":
                        openHeartChild();
                        break;
                    case "blood carbondioxide pressure":
                        openHeartChild();
                        break;
                    case "blood oxygen pressure":
                        openHeartChild();
                        break;
                    case "blood pressure":
                        openHeartChild();
                        break;
                    case "lung":
                        openLungChild();
                        break;
                    case "breath rate":
                        openLungChild();
                        break;
                    case "liver":
                        openLiverChild();
                        break;
                case "glucose level":
                    openLiverChild();
                    break;
                    case "core temperature":
                        openGeneralParametersChild();
                        break;
                    case "height":
                        openGeneralParametersChild();
                        break;
                    case "weight":
                        openGeneralParametersChild();
                        break;
                    case "general parameters":
                        openGeneralParametersChild();
                        break;

                    default:
                        break;
                }
        }

        private void cBox_Search_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                switch (cBox_Search.Text.ToLower())
                {
                    case "heart":
                        openHeartChild();
                        break;
                    case "blood carbondioxide pressure":
                        openHeartChild();
                        break;
                    case "blood oxygen pressure":
                        openHeartChild();
                        break;
                    case "blood pressure":
                        openHeartChild();
                        break;
                    case "lung":
                        openLungChild();
                        break;
                    case "breath rate":
                        openLungChild();
                        break;
                    case "liver":
                        openLiverChild();
                        break;
                    case "core temperature":
                        openGeneralParametersChild();
                        break;
                    case "height":
                        openGeneralParametersChild();
                        break;
                    case "weight":
                        openGeneralParametersChild();
                        break;
                    case "general parameters":
                        openGeneralParametersChild();
                        break;

                    default:
                        break;
                }
            }
        }
        #endregion
        private void MainMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void ExtremitiesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        
        private void cBox_Search_SelectionChangeCommitted(object sender, EventArgs e)
        {
        }
        
        private void cBox_Search_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void kidneysToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void AbdomenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void patientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openLoadAndEditPatientChild();
        }

        private void thisIsClosing(object sender, EventArgs e)
        {
            if (simulatedPatient != null)
            {
                new Task(simulatedPatient.DirtyThreadAbort).Start();
            }
        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openPropertiesChild();
        }
    }
}
