﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PatientSimulator.Patient;
using PatientSimulator.VitalParameters;

namespace PatientSimulator.GUI
{
    partial class FormEditValue : ParentVPForm 
    {
        
        EditValueCheck editValueCheck;
        public FormEditValue(PatientModel simulatedPatient) : base(simulatedPatient)
        {
            InitializeComponent();
        }

        private void textBox1_MouseClick(object sender, MouseEventArgs e)
        {
            tBox_Edit_Input.Text = null;
            tBox_Edit_Input.ForeColor = SystemColors.WindowText;

        }

        private void FormEditValue_Load(object sender, EventArgs e)
        {
            ChangeLanguage();
        }
        private void ChangeLanguage()
        {
            string temp = Global.getCurrentLanguage;
            Console.WriteLine(temp);
            foreach (Control c in Controls)
            {
                Type ty = typeof(FormEditValue);
                Global.CL(ty, c, temp);
            }
        }
        private void FormEditValue_VisibleChanged(object sender, EventArgs e)
        {
            editValueCheck = new EditValueCheck();
            if (this.Visible)
            {
                switch (EditValueCheck.editValueNr)
                {
                    //Check which value is beeing edited
                    // 1: Blood Carbondioxide Pressure
                    // 2: Blood Oxygen Pressure
                    // 3: Blood Pressure
                    // 4: Breath Rate
                    // 5: Capillar Glucose Level
                    // 6: Core Temperature
                    // 7: Height
                    // 8: Weight

                    // 9: Mean Blood Pressure --- not in VitalParameters?
                    case 1:
                        tBox_Edit_Info.Text = "Blood carbondioxide pressure:";
                        tBox_Edit_Input.Text = Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.VenousBloodCarbondioxidePressure),2).ToString();
                        break;
                    case 2:
                        tBox_Edit_Info.Text = "Blood oxygen pressure:";
                        tBox_Edit_Input.Text = Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.VenousBloodOxygenPressure), 2).ToString();
                        break;
                    case 3:
                        tBox_Edit_Info.Text = "Blood pressure:";
                        tBox_Edit_Info4.Text = "Use / to split systolic and diastolic blood pressure";
                        tBox_Edit_Input.Text = String.Format("{0}/{1}", Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.SystolicBloodPressure), 2).ToString(), Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.DiastolicBloodPressure), 2).ToString());
                        break;
                    case 4:
                        tBox_Edit_Info.Text = "Breath rate:";
                        tBox_Edit_Input.Text = Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.BreathRate), 2).ToString();
                        break;
                    case 5:
                        tBox_Edit_Info.Text = "Capillar Glucose Level";
                        tBox_Edit_Input.Text = Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.CapillarBloodGlucose), 2).ToString();
                        break;
                    case 6:
                        tBox_Edit_Info.Text = "Core Temperature";
                        tBox_Edit_Input.Text = Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.CoreTemperature), 2).ToString();
                        break;
                    case 7:
                        tBox_Edit_Info.Text = "Height";
                        tBox_Edit_Input.Text = Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.Height), 2).ToString();
                        break;
                    case 8:
                        tBox_Edit_Info.Text = "Weight";
                        tBox_Edit_Input.Text = Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.Weight), 2).ToString();
                        break;
                    default:
                        tBox_Edit_Info.Text = Convert.ToString(EditValueCheck.editValueNr);
                        break;
                }
            }
        }

        private void tBox_Edit_Input_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                float inputValue = 0;
                try
                {
                    List<VitalParameterEventArgs> editedEventArgs = new List<VitalParameterEventArgs>();

                    switch (EditValueCheck.editValueNr)
                    {
                        case 1:
                            inputValue = (float)Convert.ToDouble(tBox_Edit_Input.Text);
                            simulatedPatient.setReferenceValue(inputValue, VitalParameterType.VenousBloodCarbondioxidePressure);
                            editedEventArgs.Add(new VitalParameterEventArgs(VitalParameterType.VenousBloodCarbondioxidePressure));
                            break;
                        case 2:
                            inputValue = (float)Convert.ToDouble(tBox_Edit_Input.Text);
                            simulatedPatient.setReferenceValue(inputValue, VitalParameterType.VenousBloodOxygenPressure);
                            editedEventArgs.Add(new VitalParameterEventArgs(VitalParameterType.VenousBloodOxygenPressure));
                            break;
                        case 3:
                            //Systolic and diastolic bp
                            string[] bpSysDia = tBox_Edit_Input.Text.Split('/');
                            simulatedPatient.setReferenceValue((float)Convert.ToDouble(bpSysDia[0]), VitalParameterType.SystolicBloodPressure);
                            simulatedPatient.setReferenceValue((float)Convert.ToDouble(bpSysDia[1].Split(' ')[0]), VitalParameterType.DiastolicBloodPressure);
                            editedEventArgs.Add(new VitalParameterEventArgs(VitalParameterType.SystolicBloodPressure));
                            editedEventArgs.Add(new VitalParameterEventArgs(VitalParameterType.DiastolicBloodPressure));
                            break;
                        case 4:
                            inputValue = (float)Convert.ToDouble(tBox_Edit_Input.Text);
                            simulatedPatient.setReferenceValue(inputValue, VitalParameterType.BreathRate);
                            editedEventArgs.Add(new VitalParameterEventArgs(VitalParameterType.BreathRate));
                            break;
                        case 5:
                            inputValue = (float)Convert.ToDouble(tBox_Edit_Input.Text);
                            simulatedPatient.setReferenceValue(inputValue, VitalParameterType.CapillarBloodGlucose);
                            editedEventArgs.Add(new VitalParameterEventArgs(VitalParameterType.CapillarBloodGlucose));
                            break;
                        case 6:
                            inputValue = (float)Convert.ToDouble(tBox_Edit_Input.Text);
                            simulatedPatient.setReferenceValue(inputValue, VitalParameterType.CoreTemperature);
                            editedEventArgs.Add(new VitalParameterEventArgs(VitalParameterType.CoreTemperature));
                            break;
                        case 7:
                            inputValue = (float)Convert.ToDouble(tBox_Edit_Input.Text);
                            simulatedPatient.setReferenceValue(inputValue, VitalParameterType.Height);
                            editedEventArgs.Add(new VitalParameterEventArgs(VitalParameterType.Height));
                            break;
                        case 8:
                            inputValue = (float)Convert.ToDouble(tBox_Edit_Input.Text);
                            simulatedPatient.setReferenceValue(inputValue, VitalParameterType.Weight);
                            editedEventArgs.Add(new VitalParameterEventArgs(VitalParameterType.Weight));
                            break;
                        default:
                            break;
                    }

                    foreach(VitalParameterEventArgs arg in editedEventArgs)
                    {
                        this.EditedValue?.Invoke(this, arg);
                    }
                }
                catch (FormatException)
                {
                    Logger.log(LogLevel.WARNING, String.Format("Failed to convert a provided value (idx[{0}]: '{1}') into a number, wrong format provided.", EditValueCheck.editValueNr, tBox_Edit_Input.Text), LogSource.GENERAL);
                }
                catch (ArgumentException ex)
                {
                    Logger.log(LogLevel.WARNING,ex.Message,LogSource.GENERAL);
                }
                catch (Exception ex)
                {
                    Logger.log(LogLevel.WARNING, ex.Message, LogSource.GENERAL);
                }
                this.Close();
            }
        }

        private void tBox_Edit_Input_Click(object sender, EventArgs e)
        {
            tBox_Edit_Input.ForeColor = Color.Black;
        }
    }
}
