﻿namespace PatientSimulator.GUI
{
    partial class LoginSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.defaultPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // defaultPanel
            // 
            this.defaultPanel.Location = new System.Drawing.Point(31, 26);
            this.defaultPanel.Name = "defaultPanel";
            this.defaultPanel.Size = new System.Drawing.Size(757, 412);
            this.defaultPanel.TabIndex = 0;
            // 
            // LoginSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 549);
            this.Controls.Add(this.defaultPanel);
            this.Name = "LoginSystem";
            this.Text = "LoginSystem";
            this.Load += new System.EventHandler(this.LoginSystem_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel defaultPanel;
    }
}