﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatientSimulator.GUI
{
    class MenuColors : ProfessionalColorTable
    {
        public MenuColors()
        {
            base.UseSystemColors = false;
        }
        /*
        public override System.Drawing.Color MenuBorder
        {
            get { return Color.White; }
        }
        */
        public override Color MenuItemBorder
        {
            get
            {
                return Color.Black;
            }
        }
    }
}
