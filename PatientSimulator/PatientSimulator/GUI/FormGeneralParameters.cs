﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PatientSimulator.Patient;
using PatientSimulator.Patient.DatabaseRequestInterface;
using PatientSimulator.VitalParameters;

namespace PatientSimulator.GUI
{
    partial class FormGeneralParameters : ParentVPForm
    {
        private bool editValueSubscribed = false;
        private FormEditValue evForm = null;


        public FormGeneralParameters(PatientModel simulatedPatient) : base(simulatedPatient)
        {
            InitializeComponent();
            if (!Global.EditVitalParamteres)
            {
                b_General_Edit1_Temp.Enabled = false;
                b_General_Edit2_Weight.Enabled = false;
                b_General_Edit3_Height.Enabled = false;
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            ChangeLanguage();

        }
        private void ChangeLanguage()
        {
            string temp = Global.getCurrentLanguage;
            Console.WriteLine(temp);
            foreach (Control c in Controls)
            {
                Type ty = typeof(FormGeneralParameters);
                Global.CL(ty, c, temp);
            }
        }

        private void FormGeneralParameters_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                //Debug.WriteLine("Is Visible");
                simulatedPatient.continuousValueCreated += GeneralParametersEvent;

                try
                {
                    tBox_General_TempValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.CoreTemperature), 4)) + " " + Global.TranslateVPType2Unit[VitalParameterType.CoreTemperature];
                    tBox_General_WeightValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.Weight), 1)) + " " + Global.TranslateVPType2Unit[VitalParameterType.Weight];
                    tBox_General_HeightValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.Height), 1)) + " " + Global.TranslateVPType2Unit[VitalParameterType.Height];
                }
                catch (ArgumentException)
                {
                    Logger.log(LogLevel.WARNING, "Failed to generate initial Values for the VitalParameters at the General-Parameters-Form in the Main GUI.", LogSource.GENERAL);
                }
            }
            else // Is also called when the Form is closed
            {
                //Debug.WriteLine("Is Not Visible");
                simulatedPatient.continuousValueCreated -= GeneralParametersEvent;
                if(editValueSubscribed && evForm != null)
                {
                    editValueSubscribed = false;
                    evForm.EditedValue -= RefreshEditedValue;
                }

            }
        }
        private void b_General_Edit1_Click(object sender, EventArgs e)
        {
            openEditValueCheck(6);
        }

        private void b_General_Edit2_Click(object sender, EventArgs e)
        {
            openEditValueCheck(8);
        }

        private void b_General_Edit3_Click(object sender, EventArgs e)
        {
            openEditValueCheck(7);
        }
        private void GeneralParametersEvent(object source, VitalParameterEventArgs e)
        {
            CurrentVitalValue currentVital = (CurrentVitalValue)source;
            if (currentVital.type == VitalParameterType.CoreTemperature)
            {
                if (this.Visible)
                {
                    this.Invoke(new MethodInvoker(delegate { tBox_General_TempValue.Text = Convert.ToString(Math.Round(currentVital.value, 4)) + " " + Global.TranslateVPType2Unit[VitalParameterType.CoreTemperature]; }));
                }
            }
        }
        private void RefreshEditedValue(object source, VitalParameterEventArgs e)
        {
            if (e.type.Equals(VitalParameterType.CoreTemperature))
            {
                this.Invoke(new MethodInvoker(delegate { tBox_General_TempValue.Text = Convert.ToString(Math.Round( simulatedPatient.getReferenceValue(VitalParameterType.CoreTemperature), 4)) + " " + Global.TranslateVPType2Unit[VitalParameterType.CoreTemperature]; }));
            }
            else if (e.type.Equals(VitalParameterType.Weight))
            {
                this.Invoke(new MethodInvoker(delegate { tBox_General_WeightValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.Weight), 1))+" "+ Global.TranslateVPType2Unit[VitalParameterType.Weight]; }));
            }
            else if (e.type.Equals(VitalParameterType.Height))
            {
                this.Invoke(new MethodInvoker(delegate { tBox_General_HeightValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.Height), 1)) + " " + Global.TranslateVPType2Unit[VitalParameterType.Height]; }));
            }
        }

        private void openEditValueCheck(int eVNr)
        {
            if (Global.EditVitalParamteres)
            {
                EditValueCheck editValueCheck;
                evForm = (FormEditValue)Application.OpenForms["FormEditValue"];
                if (evForm != null) { }
                else
                {
                    editValueSubscribed = false;
                    editValueCheck = new EditValueCheck();
                    EditValueCheck.editValueNr = eVNr;
                    FormEditValue editValue = new FormEditValue(simulatedPatient);
                    evForm = editValue;

                    if (!editValueSubscribed)
                    {
                        editValueSubscribed = true;
                        evForm.EditedValue += RefreshEditedValue;
                    }

                    editValue.ShowDialog();
                }
            }
        }

        private void FormGeneralParameters_Shown(object sender, EventArgs e)
        {
        }
    }
}
