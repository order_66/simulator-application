﻿namespace PatientSimulator.GUI
{
    partial class FormGeneralParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGeneralParameters));
            this.tBox_Vitals1 = new System.Windows.Forms.TextBox();
            this.tBox_General_Info1 = new System.Windows.Forms.TextBox();
            this.b_General_Edit1_Temp = new System.Windows.Forms.Button();
            this.b_General_Edit2_Weight = new System.Windows.Forms.Button();
            this.tBox_General_Info2 = new System.Windows.Forms.TextBox();
            this.b_General_Edit3_Height = new System.Windows.Forms.Button();
            this.tBox_General_Info3 = new System.Windows.Forms.TextBox();
            this.tBox_General_HeightValue = new System.Windows.Forms.TextBox();
            this.tBox_General_WeightValue = new System.Windows.Forms.TextBox();
            this.tBox_General_TempValue = new System.Windows.Forms.TextBox();
            this.pBox_Vitals1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Vitals1)).BeginInit();
            this.SuspendLayout();
            // 
            // tBox_Vitals1
            // 
            this.tBox_Vitals1.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Vitals1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.tBox_Vitals1, "tBox_Vitals1");
            this.tBox_Vitals1.Name = "tBox_Vitals1";
            this.tBox_Vitals1.ReadOnly = true;
            this.tBox_Vitals1.TabStop = false;
            // 
            // tBox_General_Info1
            // 
            this.tBox_General_Info1.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_General_Info1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.tBox_General_Info1, "tBox_General_Info1");
            this.tBox_General_Info1.Name = "tBox_General_Info1";
            this.tBox_General_Info1.ReadOnly = true;
            this.tBox_General_Info1.TabStop = false;
            // 
            // b_General_Edit1_Temp
            // 
            resources.ApplyResources(this.b_General_Edit1_Temp, "b_General_Edit1_Temp");
            this.b_General_Edit1_Temp.Name = "b_General_Edit1_Temp";
            this.b_General_Edit1_Temp.UseVisualStyleBackColor = true;
            this.b_General_Edit1_Temp.Click += new System.EventHandler(this.b_General_Edit1_Click);
            // 
            // b_General_Edit2_Weight
            // 
            resources.ApplyResources(this.b_General_Edit2_Weight, "b_General_Edit2_Weight");
            this.b_General_Edit2_Weight.Name = "b_General_Edit2_Weight";
            this.b_General_Edit2_Weight.UseVisualStyleBackColor = true;
            this.b_General_Edit2_Weight.Click += new System.EventHandler(this.b_General_Edit2_Click);
            // 
            // tBox_General_Info2
            // 
            this.tBox_General_Info2.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_General_Info2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.tBox_General_Info2, "tBox_General_Info2");
            this.tBox_General_Info2.Name = "tBox_General_Info2";
            this.tBox_General_Info2.ReadOnly = true;
            this.tBox_General_Info2.TabStop = false;
            // 
            // b_General_Edit3_Height
            // 
            resources.ApplyResources(this.b_General_Edit3_Height, "b_General_Edit3_Height");
            this.b_General_Edit3_Height.Name = "b_General_Edit3_Height";
            this.b_General_Edit3_Height.UseVisualStyleBackColor = true;
            this.b_General_Edit3_Height.Click += new System.EventHandler(this.b_General_Edit3_Click);
            // 
            // tBox_General_Info3
            // 
            this.tBox_General_Info3.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_General_Info3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.tBox_General_Info3, "tBox_General_Info3");
            this.tBox_General_Info3.Name = "tBox_General_Info3";
            this.tBox_General_Info3.ReadOnly = true;
            this.tBox_General_Info3.TabStop = false;
            // 
            // tBox_General_HeightValue
            // 
            this.tBox_General_HeightValue.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_General_HeightValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.tBox_General_HeightValue, "tBox_General_HeightValue");
            this.tBox_General_HeightValue.Name = "tBox_General_HeightValue";
            this.tBox_General_HeightValue.ReadOnly = true;
            this.tBox_General_HeightValue.TabStop = false;
            // 
            // tBox_General_WeightValue
            // 
            this.tBox_General_WeightValue.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_General_WeightValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.tBox_General_WeightValue, "tBox_General_WeightValue");
            this.tBox_General_WeightValue.Name = "tBox_General_WeightValue";
            this.tBox_General_WeightValue.ReadOnly = true;
            this.tBox_General_WeightValue.TabStop = false;
            // 
            // tBox_General_TempValue
            // 
            this.tBox_General_TempValue.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_General_TempValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.tBox_General_TempValue, "tBox_General_TempValue");
            this.tBox_General_TempValue.Name = "tBox_General_TempValue";
            this.tBox_General_TempValue.ReadOnly = true;
            this.tBox_General_TempValue.TabStop = false;
            // 
            // pBox_Vitals1
            // 
            resources.ApplyResources(this.pBox_Vitals1, "pBox_Vitals1");
            this.pBox_Vitals1.Name = "pBox_Vitals1";
            this.pBox_Vitals1.TabStop = false;
            // 
            // FormGeneralParameters
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.tBox_General_TempValue);
            this.Controls.Add(this.tBox_General_WeightValue);
            this.Controls.Add(this.tBox_General_HeightValue);
            this.Controls.Add(this.b_General_Edit3_Height);
            this.Controls.Add(this.tBox_General_Info3);
            this.Controls.Add(this.b_General_Edit2_Weight);
            this.Controls.Add(this.tBox_General_Info2);
            this.Controls.Add(this.b_General_Edit1_Temp);
            this.Controls.Add(this.tBox_General_Info1);
            this.Controls.Add(this.pBox_Vitals1);
            this.Controls.Add(this.tBox_Vitals1);
            this.Name = "FormGeneralParameters";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.Shown += new System.EventHandler(this.FormGeneralParameters_Shown);
            this.VisibleChanged += new System.EventHandler(this.FormGeneralParameters_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Vitals1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tBox_Vitals1;
        private System.Windows.Forms.PictureBox pBox_Vitals1;
        private System.Windows.Forms.TextBox tBox_General_Info1;
        private System.Windows.Forms.Button b_General_Edit1_Temp;
        private System.Windows.Forms.Button b_General_Edit2_Weight;
        private System.Windows.Forms.TextBox tBox_General_Info2;
        private System.Windows.Forms.Button b_General_Edit3_Height;
        private System.Windows.Forms.TextBox tBox_General_Info3;
        private System.Windows.Forms.TextBox tBox_General_HeightValue;
        private System.Windows.Forms.TextBox tBox_General_WeightValue;
        private System.Windows.Forms.TextBox tBox_General_TempValue;
        //private System.Windows.Forms.Timer timer_General;
    }
}