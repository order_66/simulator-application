﻿
using PatientSimulator.Patient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatientSimulator.GUI
{
    partial class Properties : ParentVPForm
    {
        public ComponentResourceManager resourceManager;
        public Properties(PatientModel simulatedPatient) : base(simulatedPatient)
        {
            InitializeComponent();
        }

        private void tBox_Title_TextChanged(object sender, EventArgs e)
        {

        }

        private void label_DBAccessLabel_Click(object sender, EventArgs e)
        {

        }

        private void Properties_Load(object sender, EventArgs e)
        {
            ChangeLanguage();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void cB_Window_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cB_Window.Text.ToLower())
            {
                case "1920x1080":
                    ParentForm.Size = new Size(1920, 1080);
                    break;
                case "1200x900":
                    ParentForm.Size = new Size(1200, 900);
                    break;
                case "1024x768":
                    ParentForm.Size = new Size(1024, 768);
                    break;
                case "800x600":
                    ParentForm.Size = new Size(800, 600);
                    break;
                default:
                    break;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                ParentForm.WindowState = FormWindowState.Normal;
                ParentForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                ParentForm.Bounds = Screen.PrimaryScreen.Bounds;
            }
            else
            {
                ParentForm.WindowState = FormWindowState.Maximized;
                ParentForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;

            }
        }

        private void cB_Language_SelectedIndexChanged(object sender, EventArgs e)
        {
            string lang;
            switch (cB_Language.Text.ToLower())
            {
                case "english":
                    lang = "en";
                    ChangeLanguage("en");
                    Global.getCurrentLanguage = lang;
                    break;
                case "german":
                    lang = "de";
                    ChangeLanguage("de");
                    Global.getCurrentLanguage = lang;
                    break;
                default:
                    break;
            }
        }
        
        private void ChangeLanguage(string temp)
        {
            foreach(Control c in Controls)
            {
                Type ty = typeof(Properties);
                Global.CL(ty,c,temp);
            }
        }
        private void ChangeLanguage()
        {
            string temp = Global.getCurrentLanguage;
            Console.WriteLine(temp);
            foreach (Control c in Controls)
            {
                Type ty = typeof(Properties);
                Global.CL(ty, c, temp);
            }
        }

    }
}
