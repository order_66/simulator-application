﻿namespace PatientSimulator.GUI
{
    partial class FormHeart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHeart));
            this.tBox_Heart_mbpValue = new System.Windows.Forms.TextBox();
            this.tBox_Heart_Info4 = new System.Windows.Forms.TextBox();
            this.tBox_Heart_bcpValue = new System.Windows.Forms.TextBox();
            this.tBox_Heart_bopValue = new System.Windows.Forms.TextBox();
            this.tBox_Heart_bpValue = new System.Windows.Forms.TextBox();
            this.b_Heart_Edit3_bp = new System.Windows.Forms.Button();
            this.tBox_Heart_Info3 = new System.Windows.Forms.TextBox();
            this.b_Heart_Edit2_bop = new System.Windows.Forms.Button();
            this.tBox_Heart_Info2 = new System.Windows.Forms.TextBox();
            this.b_Heart_Edit1_bcp = new System.Windows.Forms.Button();
            this.tBox_Heart_Info1 = new System.Windows.Forms.TextBox();
            this.tBox_Heart1 = new System.Windows.Forms.TextBox();
            this.pBox_Heart1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Heart1)).BeginInit();
            this.SuspendLayout();
            // 
            // tBox_Heart_mbpValue
            // 
            resources.ApplyResources(this.tBox_Heart_mbpValue, "tBox_Heart_mbpValue");
            this.tBox_Heart_mbpValue.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Heart_mbpValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Heart_mbpValue.Name = "tBox_Heart_mbpValue";
            this.tBox_Heart_mbpValue.ReadOnly = true;
            this.tBox_Heart_mbpValue.TabStop = false;
            this.tBox_Heart_mbpValue.TextChanged += new System.EventHandler(this.tBox_Heart_mbpValue_TextChanged);
            // 
            // tBox_Heart_Info4
            // 
            resources.ApplyResources(this.tBox_Heart_Info4, "tBox_Heart_Info4");
            this.tBox_Heart_Info4.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Heart_Info4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Heart_Info4.Name = "tBox_Heart_Info4";
            this.tBox_Heart_Info4.ReadOnly = true;
            this.tBox_Heart_Info4.TabStop = false;
            // 
            // tBox_Heart_bcpValue
            // 
            resources.ApplyResources(this.tBox_Heart_bcpValue, "tBox_Heart_bcpValue");
            this.tBox_Heart_bcpValue.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Heart_bcpValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Heart_bcpValue.Name = "tBox_Heart_bcpValue";
            this.tBox_Heart_bcpValue.ReadOnly = true;
            this.tBox_Heart_bcpValue.TabStop = false;
            // 
            // tBox_Heart_bopValue
            // 
            resources.ApplyResources(this.tBox_Heart_bopValue, "tBox_Heart_bopValue");
            this.tBox_Heart_bopValue.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Heart_bopValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Heart_bopValue.Name = "tBox_Heart_bopValue";
            this.tBox_Heart_bopValue.ReadOnly = true;
            this.tBox_Heart_bopValue.TabStop = false;
            // 
            // tBox_Heart_bpValue
            // 
            resources.ApplyResources(this.tBox_Heart_bpValue, "tBox_Heart_bpValue");
            this.tBox_Heart_bpValue.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Heart_bpValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Heart_bpValue.Name = "tBox_Heart_bpValue";
            this.tBox_Heart_bpValue.ReadOnly = true;
            this.tBox_Heart_bpValue.TabStop = false;
            this.tBox_Heart_bpValue.TextChanged += new System.EventHandler(this.tBox_General_HeightValue_TextChanged);
            // 
            // b_Heart_Edit3_bp
            // 
            resources.ApplyResources(this.b_Heart_Edit3_bp, "b_Heart_Edit3_bp");
            this.b_Heart_Edit3_bp.Name = "b_Heart_Edit3_bp";
            this.b_Heart_Edit3_bp.UseVisualStyleBackColor = true;
            this.b_Heart_Edit3_bp.Click += new System.EventHandler(this.b_Heart_Edit3_bp_Click);
            // 
            // tBox_Heart_Info3
            // 
            resources.ApplyResources(this.tBox_Heart_Info3, "tBox_Heart_Info3");
            this.tBox_Heart_Info3.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Heart_Info3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Heart_Info3.Name = "tBox_Heart_Info3";
            this.tBox_Heart_Info3.ReadOnly = true;
            this.tBox_Heart_Info3.TabStop = false;
            // 
            // b_Heart_Edit2_bop
            // 
            resources.ApplyResources(this.b_Heart_Edit2_bop, "b_Heart_Edit2_bop");
            this.b_Heart_Edit2_bop.Name = "b_Heart_Edit2_bop";
            this.b_Heart_Edit2_bop.UseVisualStyleBackColor = true;
            this.b_Heart_Edit2_bop.Click += new System.EventHandler(this.b_Heart_Edit2_bop_Click);
            // 
            // tBox_Heart_Info2
            // 
            resources.ApplyResources(this.tBox_Heart_Info2, "tBox_Heart_Info2");
            this.tBox_Heart_Info2.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Heart_Info2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Heart_Info2.Name = "tBox_Heart_Info2";
            this.tBox_Heart_Info2.ReadOnly = true;
            this.tBox_Heart_Info2.TabStop = false;
            this.tBox_Heart_Info2.TextChanged += new System.EventHandler(this.tBox_Heart_Info2_TextChanged);
            // 
            // b_Heart_Edit1_bcp
            // 
            resources.ApplyResources(this.b_Heart_Edit1_bcp, "b_Heart_Edit1_bcp");
            this.b_Heart_Edit1_bcp.Name = "b_Heart_Edit1_bcp";
            this.b_Heart_Edit1_bcp.UseVisualStyleBackColor = true;
            this.b_Heart_Edit1_bcp.VisibleChanged += new System.EventHandler(this.b_Heart_Edit1_bcp_VisibleChanged);
            this.b_Heart_Edit1_bcp.Click += new System.EventHandler(this.b_Heart_Edit1_bcp_Click);
            // 
            // tBox_Heart_Info1
            // 
            resources.ApplyResources(this.tBox_Heart_Info1, "tBox_Heart_Info1");
            this.tBox_Heart_Info1.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Heart_Info1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Heart_Info1.Name = "tBox_Heart_Info1";
            this.tBox_Heart_Info1.ReadOnly = true;
            this.tBox_Heart_Info1.TabStop = false;
            // 
            // tBox_Heart1
            // 
            resources.ApplyResources(this.tBox_Heart1, "tBox_Heart1");
            this.tBox_Heart1.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Heart1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Heart1.Name = "tBox_Heart1";
            this.tBox_Heart1.ReadOnly = true;
            this.tBox_Heart1.TabStop = false;
            this.tBox_Heart1.TextChanged += new System.EventHandler(this.tBox_Heart1_TextChanged);
            // 
            // pBox_Heart1
            // 
            resources.ApplyResources(this.pBox_Heart1, "pBox_Heart1");
            this.pBox_Heart1.Name = "pBox_Heart1";
            this.pBox_Heart1.TabStop = false;
            // 
            // FormHeart
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.tBox_Heart_mbpValue);
            this.Controls.Add(this.tBox_Heart_Info4);
            this.Controls.Add(this.tBox_Heart_bcpValue);
            this.Controls.Add(this.tBox_Heart_bopValue);
            this.Controls.Add(this.tBox_Heart_bpValue);
            this.Controls.Add(this.b_Heart_Edit3_bp);
            this.Controls.Add(this.tBox_Heart_Info3);
            this.Controls.Add(this.b_Heart_Edit2_bop);
            this.Controls.Add(this.tBox_Heart_Info2);
            this.Controls.Add(this.b_Heart_Edit1_bcp);
            this.Controls.Add(this.tBox_Heart_Info1);
            this.Controls.Add(this.tBox_Heart1);
            this.Controls.Add(this.pBox_Heart1);
            this.Name = "FormHeart";
            this.Load += new System.EventHandler(this.FormHeart_Load);
            this.Shown += new System.EventHandler(this.FormHeart_Shown);
            this.VisibleChanged += new System.EventHandler(this.FormHeart_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Heart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pBox_Heart1;
        private System.Windows.Forms.TextBox tBox_Heart1;
        private System.Windows.Forms.TextBox tBox_Heart_bcpValue;
        private System.Windows.Forms.TextBox tBox_Heart_bopValue;
        private System.Windows.Forms.TextBox tBox_Heart_bpValue;
        private System.Windows.Forms.Button b_Heart_Edit3_bp;
        private System.Windows.Forms.TextBox tBox_Heart_Info3;
        private System.Windows.Forms.Button b_Heart_Edit2_bop;
        private System.Windows.Forms.TextBox tBox_Heart_Info2;
        private System.Windows.Forms.Button b_Heart_Edit1_bcp;
        private System.Windows.Forms.TextBox tBox_Heart_Info1;
        private System.Windows.Forms.TextBox tBox_Heart_mbpValue;
        private System.Windows.Forms.TextBox tBox_Heart_Info4;
        //private System.Windows.Forms.Timer timer_Heart;
    }
}