﻿namespace PatientSimulator.GUI
{
    partial class FormEditValue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditValue));
            this.tBox_Edit_Info2 = new System.Windows.Forms.TextBox();
            this.tBox_Edit_Input = new System.Windows.Forms.TextBox();
            this.tBox_Edit_Info = new System.Windows.Forms.TextBox();
            this.tBox_Edit_Info3 = new System.Windows.Forms.TextBox();
            this.tBox_Edit_Info4 = new System.Windows.Forms.TextBox();
            this.tBox_Edit_Info5 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tBox_Edit_Info2
            // 
            resources.ApplyResources(this.tBox_Edit_Info2, "tBox_Edit_Info2");
            this.tBox_Edit_Info2.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Edit_Info2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Edit_Info2.Name = "tBox_Edit_Info2";
            this.tBox_Edit_Info2.ReadOnly = true;
            this.tBox_Edit_Info2.TabStop = false;
            // 
            // tBox_Edit_Input
            // 
            resources.ApplyResources(this.tBox_Edit_Input, "tBox_Edit_Input");
            this.tBox_Edit_Input.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Edit_Input.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tBox_Edit_Input.Name = "tBox_Edit_Input";
            this.tBox_Edit_Input.Click += new System.EventHandler(this.tBox_Edit_Input_Click);
            this.tBox_Edit_Input.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox1_MouseClick);
            this.tBox_Edit_Input.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tBox_Edit_Input_KeyPress);
            // 
            // tBox_Edit_Info
            // 
            resources.ApplyResources(this.tBox_Edit_Info, "tBox_Edit_Info");
            this.tBox_Edit_Info.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Edit_Info.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Edit_Info.Name = "tBox_Edit_Info";
            this.tBox_Edit_Info.ReadOnly = true;
            this.tBox_Edit_Info.TabStop = false;
            // 
            // tBox_Edit_Info3
            // 
            resources.ApplyResources(this.tBox_Edit_Info3, "tBox_Edit_Info3");
            this.tBox_Edit_Info3.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Edit_Info3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Edit_Info3.Name = "tBox_Edit_Info3";
            this.tBox_Edit_Info3.ReadOnly = true;
            this.tBox_Edit_Info3.TabStop = false;
            // 
            // tBox_Edit_Info4
            // 
            resources.ApplyResources(this.tBox_Edit_Info4, "tBox_Edit_Info4");
            this.tBox_Edit_Info4.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Edit_Info4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Edit_Info4.Name = "tBox_Edit_Info4";
            this.tBox_Edit_Info4.ReadOnly = true;
            this.tBox_Edit_Info4.TabStop = false;
            // 
            // tBox_Edit_Info5
            // 
            resources.ApplyResources(this.tBox_Edit_Info5, "tBox_Edit_Info5");
            this.tBox_Edit_Info5.BackColor = System.Drawing.SystemColors.Window;
            this.tBox_Edit_Info5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_Edit_Info5.Name = "tBox_Edit_Info5";
            this.tBox_Edit_Info5.ReadOnly = true;
            this.tBox_Edit_Info5.TabStop = false;
            // 
            // FormEditValue
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.tBox_Edit_Info5);
            this.Controls.Add(this.tBox_Edit_Info4);
            this.Controls.Add(this.tBox_Edit_Info3);
            this.Controls.Add(this.tBox_Edit_Info2);
            this.Controls.Add(this.tBox_Edit_Input);
            this.Controls.Add(this.tBox_Edit_Info);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormEditValue";
            this.Load += new System.EventHandler(this.FormEditValue_Load);
            this.VisibleChanged += new System.EventHandler(this.FormEditValue_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBox_Edit_Info;
        private System.Windows.Forms.TextBox tBox_Edit_Input;
        private System.Windows.Forms.TextBox tBox_Edit_Info2;
        private System.Windows.Forms.TextBox tBox_Edit_Info3;
        private System.Windows.Forms.TextBox tBox_Edit_Info4;
        private System.Windows.Forms.TextBox tBox_Edit_Info5;
        public event PatientSimulator.VitalParameters.VitalParameterEventHandler EditedValue;
    }
}