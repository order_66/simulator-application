﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

using PatientSimulator.Patient.DatabaseRequestInterface;
using PatientSimulator.Patient;
using PatientSimulator.Exceptions;

namespace PatientSimulator.GUI
{
    partial class FormLoadAndEditPatient : ParentVPForm
    {
        private List<PatientInterface> alterablePatients = new List<PatientInterface>();
        private List<PatientInterface> nonAlterablePatients = new List<PatientInterface>();
        private List<UserInterface> registeredUsers = new List<UserInterface>();

        public FormLoadAndEditPatient(PatientModel simulatedPatient) : base(simulatedPatient)
        {
            InitializeComponent();

            try
            {
                DBAccess db = new DBAccess();
                List<PatientInterface> allPatients = db.ReadAllPatientInformation();
                alterablePatients = allPatients.FindAll(item => (item.isAlterable ?? false) && 
                    (item.owner.Uuid == LoginSystem.UserAuthentificationToken.Uuid || item.viewerRights.Right != ViewerRightsType.Hidden));
                nonAlterablePatients = allPatients.FindAll(item => !(item.isAlterable ?? true));
                registeredUsers = db.ReadAllUsers();
            }
            catch (PostgresException e)
            {
                if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                {
                    InvokeFailedDBConnectionEvent(this, new EventArgs());
                }
                Logger.log(LogLevel.ERROR, "Failed to access the Demographic and VitalDatabase.", LogSource.GENERAL);
            }

            existingPatientsComboBox.Items.AddRange(alterablePatients.ToArray());
            PatientRoleModelComboBox.Items.AddRange(nonAlterablePatients.ToArray());
            PatientOwnerInputComboBox.Items.AddRange(registeredUsers.ToArray());
            /* Select the currently logged in user and block the combo box */
            if(LoginSystem.UserAuthentificationToken != null)
            {
                PatientOwnerInputComboBox.SelectedItem = registeredUsers.Find(item => item.Uuid.Equals(LoginSystem.UserAuthentificationToken.Uuid));
                PatientOwnerInputComboBox.SelectedText = registeredUsers.Find(item => item.Uuid.Equals(LoginSystem.UserAuthentificationToken.Uuid)).ToString();
                PatientOwnerInputComboBox.Enabled = false;
            }

            List<ViewerRightsInterface> viewerRights = new List<ViewerRightsInterface>();
            foreach(ViewerRightsType right in (ViewerRightsType[])Enum.GetValues(typeof(ViewerRightsType)))
            {
                viewerRights.Add(new ViewerRightsInterface(right, null));
            }
            PatientRightsComboBox.Items.AddRange(viewerRights.ToArray());
        }

        #region Event Handler
        public delegate void PatientCreatedEventHandler(object source, PatientCreatedEventArgs e);
        public event PatientCreatedEventHandler PatientCreated;
        public event EventHandler FailedDBConnectionEvent;

        private void InvokePatientCreatedEvent()
        {
            setInfoBox(Color.Black, "Successfully loaded and simulated the patient model.");
            PatientCreated?.Invoke(null, new PatientCreatedEventArgs(simulatedPatient));
        }
        private void InvokeFailedDBConnectionEvent(object source, EventArgs e)
        {
            FailedDBConnectionEvent?.Invoke(source, e);
        }

        public void savePatientOnClick(object source, EventArgs args)
        {
            if(simulatedPatient != null)
            {
                try
                {
                    DBAccess db = new DBAccess();
                    db.WriteFullPatient(simulatedPatient.getPatientInterface());
                    Logger.log(LogLevel.INFO, $"Successfully wrote the patient model to the remote Demographic and Vital Database at {Global.DatabaseHost}.");
                }
                catch (PostgresException e)
                {
                    if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                    {
                        InvokeFailedDBConnectionEvent(this, new EventArgs());
                    }
                    Debug.WriteLine(e);
                }
                catch (ArgumentNullException e)
                {
                    Debug.WriteLine(e);
                }
            }
        }
        public void loadPatientOnClick(object source, EventArgs args)
        {
            // Load the full patient with all his VP and Organs from DBAccessor
            if(existingPatientsComboBox.SelectedItem != null)
            {
                tBox_LoadExistingTitle.ForeColor = Color.Black;

                PatientModel createdPatient = null;
                try
                {
                    DBAccess db = new DBAccess();
                    PatientInterface loadPatient = db.ReadFullPatient((PatientInterface)existingPatientsComboBox.SelectedItem);

                    createdPatient = new PatientModel(loadPatient);
                }
                catch (PostgresException e)
                {
                    if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                    {
                        InvokeFailedDBConnectionEvent(this, new EventArgs());
                    }
                    Debug.WriteLine(e);
                }
                catch (ArgumentNullException e)
                {
                    Debug.WriteLine(e);
                }
                catch (ArgumentException e)
                {
                    Debug.WriteLine(e);
                }
                catch (VitalParameterNullReferenceException e)
                {
                    Debug.WriteLine(e);
                }
                catch (CreateProtectedPatientModelException e)
                {
                    Debug.WriteLine(e);
                }
                catch (SQLNoEntryFoundException e)
                {
                    Debug.WriteLine(e);
                }

                if (createdPatient != null)  // Creation of the new Patient Sucessfull
                {
                    if (simulatedPatient != null)
                    {
                        new Task(simulatedPatient.ThreadAbort).Start();
                    }
                    simulatedPatient = createdPatient;
                    Global.EditVitalParamteres = simulatedPatient.ViewerRights == ViewerRightsType.Writing || simulatedPatient.ownerUuid == LoginSystem.UserAuthentificationToken.Uuid;
                    InvokePatientCreatedEvent();
                }
            }
            else
            {
                tBox_LoadExistingTitle.ForeColor = Color.Red;
            }
        }
        public void createPatientOnClick(object source, EventArgs args)
        {
            bool isAppliable = true;
            if (PatientNameInputBox.Text.Trim().Equals(""))
            {
                tBox_PatientNameTitle.ForeColor = Color.Red;
                isAppliable = false;
            }
            if (PatientOwnerInputComboBox.SelectedItem == null)
            {
                tBox_PatientOwnerTitle.ForeColor = Color.Red;
                isAppliable = false;
            }
            if (PatientRoleModelComboBox.SelectedItem == null)
            {
                tBox_PatientModelTitle.ForeColor = Color.Red;
                isAppliable = false;
            }
            if(PatientRightsComboBox.SelectedItem == null)
            {
                patientRightsLabel.ForeColor = Color.Red;
                isAppliable = false;
            }

            if(isAppliable)
            {
                tBox_PatientNameTitle.ForeColor = Color.Black;
                tBox_PatientOwnerTitle.ForeColor = Color.Black;
                tBox_PatientModelTitle.ForeColor = Color.Black;
                patientRightsLabel.ForeColor = Color.Black;

                PatientModel createdPatient = null;
                try
                {
                    DBAccess db = new DBAccess();
                    PatientInterface InewPatient = db.ReadFullPatient((PatientInterface)PatientRoleModelComboBox.SelectedItem);

                    InewPatient.patientName = PatientNameInputBox.Text.Trim();
                    InewPatient.owner = (UserInterface)PatientOwnerInputComboBox.SelectedItem;
                    InewPatient.viewerRights = (ViewerRightsInterface)PatientRightsComboBox.SelectedItem;
                    InewPatient.isAlterable = true;
                    InewPatient.createdAt = DateTime.Now;
                    InewPatient.roleModelUuid = InewPatient.uuid;
                    InewPatient.uuid = Guid.NewGuid();
                    foreach (OrganInterface IFace in InewPatient.organs)
                    {
                        IFace.uuid = Guid.NewGuid();
                    }
                    db.CreateFullPatient(InewPatient);
                    createdPatient = new PatientModel(InewPatient);
                }
                catch (PostgresException e)
                {
                    if (e.SqlState.StartsWith("08")) // Failed to resolve the IP where the DB should be
                    {
                        InvokeFailedDBConnectionEvent(this, new EventArgs());
                    }
                    Debug.WriteLine(e);
                }
                catch (ArgumentNullException e)
                {
                    Debug.WriteLine(e);
                }
                catch (ArgumentException e)
                {
                    Debug.WriteLine(e);
                }
                catch (VitalParameterNullReferenceException e)
                {
                    Debug.WriteLine(e);
                }
                catch (CreateProtectedPatientModelException e)
                {
                    Debug.WriteLine(e);
                }
                catch (SQLNoEntryFoundException e)
                {
                    Debug.WriteLine(e);
                }

                if(createdPatient != null)  // Creation of the new Patient Sucessfull
                {
                    if(simulatedPatient != null)
                    {
                        new Task(simulatedPatient.ThreadAbort).Start();
                    }
                    simulatedPatient = createdPatient;
                    Global.EditVitalParamteres = simulatedPatient.ViewerRights == ViewerRightsType.Writing || simulatedPatient.ownerUuid == LoginSystem.UserAuthentificationToken.Uuid;
                    InvokePatientCreatedEvent();
                }
            }
        }
        #endregion

        public void setInfoBox(Color color, string message)
        {
            userInformationLabel.Text = message;
            userInformationLabel.ForeColor = color;
        }

        private class ViewerRightsTypePresenter
        {
            public ViewerRightsType Right { get; }

            public ViewerRightsTypePresenter(ViewerRightsType right)
            {
                Right = right;
            }

            public override string ToString()
            {
                return Right.ToString();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void FormLoadAndEditPatient_Load(object sender, EventArgs e)
        {
            setSimulatedPatientTB();
            PatientCreated += setSimulatedPatientTB;
            ChangeLanguage();
        }

        private void ChangeLanguage()
        {
            string temp = Global.getCurrentLanguage;
            Console.WriteLine(temp);
            foreach (Control c in Controls)
            {
                Type ty = typeof(FormLoadAndEditPatient);
                Global.CL(ty, c, temp);
            }
        }
        private void setSimulatedPatientTB()
        {
            if (simulatedPatient != null)
            {

                if (simulatedPatient.ownerUuid == LoginSystem.UserAuthentificationToken.Uuid)
                {
                    tBox_SimulatedPatient.Text = String.Format("{0} - Access level: {1}", simulatedPatient.patientName, ViewerRightsType.Writing.ToString());
                }
                else
                {
                    tBox_SimulatedPatient.Text = String.Format("{0} - Access level: {1}", simulatedPatient.patientName, simulatedPatient.ViewerRights.ToString());
                }
            }
        }
        private void setSimulatedPatientTB(object sender, PatientCreatedEventArgs e)
        {
            setSimulatedPatientTB();
        }

        private void tBox_Title_TextChanged(object sender, EventArgs e)
        {

        }
    }

    class PatientCreatedEventArgs : EventArgs
    {
        public PatientModel SimulatedPatient { get; private set; }

        public PatientCreatedEventArgs(PatientModel patient) : base()
        {
            SimulatedPatient = patient;
        }
    }
}
