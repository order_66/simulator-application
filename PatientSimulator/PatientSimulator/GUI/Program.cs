﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatientSimulator.GUI
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Logger.log(LogLevel.INFO, String.Format("Started the Application {0} V{1} by {2}", Global.ApplicationName, Global.ApplicationVersion, Global.ApplicationAuthors), LogSource.GENERAL);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainApplicationContext());
            //Application.Run(new Form1());
            Logger.log(LogLevel.INFO, String.Format("Closed the Application {0} V{1} by {2}", Global.ApplicationName, Global.ApplicationVersion, Global.ApplicationAuthors), LogSource.GENERAL);
        }
    }
}
