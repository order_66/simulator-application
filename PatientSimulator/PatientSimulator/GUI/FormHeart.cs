﻿using PatientSimulator.Patient;
using PatientSimulator.Patient.DatabaseRequestInterface;
using PatientSimulator.VitalParameters;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatientSimulator.GUI
{
    partial class FormHeart : ParentVPForm
    {
        float bcpValueF;
        float bopValueF;
        float sysbpValueF;
        float diabpValueF;
        float meanbpValueF;

        private FormEditValue evForm;
        private bool editValueSubscribed = false;
        

        public FormHeart(PatientModel simulatedPatient) : base(simulatedPatient)
        {
            InitializeComponent();
            if (!Global.EditVitalParamteres)
            {
                b_Heart_Edit1_bcp.Enabled = false;
                b_Heart_Edit2_bop.Enabled = false;
                b_Heart_Edit3_bp.Enabled = false;
            }
        }

        private void rtBox_Heart1_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormHeart_Load(object sender, EventArgs e)
        {
            ChangeLanguage();
        }
        private void ChangeLanguage()
        {
            string temp = Global.getCurrentLanguage;
            Console.WriteLine(temp);
            foreach (Control c in Controls)
            {
                Type ty = typeof(FormHeart);
                Global.CL(ty, c, temp);
            }
        }

        private void rtBox_Heart1_VisibleChanged(object sender, EventArgs e)
        {
        }
        private void presentVP(object source, VitalParameterEventArgs e)
        {
            CurrentVitalValue vp = (CurrentVitalValue)source;
            //Debug.WriteLine("At {0} value {1}", vp.type, vp.value);
        }

        private void tBox_Heart_Info2_TextChanged(object sender, EventArgs e)
        {

        }

        private void tBox_General_HeightValue_TextChanged(object sender, EventArgs e)
        {

        }

        private void b_Heart_Edit1_bcp_Click(object sender, EventArgs e)
        {
            openEditValueCheck(1);
        }
        private void b_Heart_Edit2_bop_Click(object sender, EventArgs e)
        {
            openEditValueCheck(2);
        }

        private void b_Heart_Edit3_bp_Click(object sender, EventArgs e)
        {
            openEditValueCheck(3);
        }
        private void FormHeart_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                //Debug.WriteLine("Is Visible");
                simulatedPatient.continuousValueCreated += HeartEvent;

                try
                {
                    tBox_Heart_bcpValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.VenousBloodCarbondioxidePressure), 3))+" "+ Global.TranslateVPType2Unit[VitalParameterType.VenousBloodCarbondioxidePressure];
                    tBox_Heart_bopValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.VenousBloodOxygenPressure), 3))+" "+ Global.TranslateVPType2Unit[VitalParameterType.VenousBloodOxygenPressure];
                    tBox_Heart_bpValue.Text = String.Format("{0}/{1} ", Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.SystolicBloodPressure), 0)), Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.DiastolicBloodPressure), 0)) + Global.TranslateVPType2Unit[VitalParameterType.DiastolicBloodPressure]);
                    tBox_Heart_mbpValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.MeanBloodPressure), 1))+ " " + Global.TranslateVPType2Unit[VitalParameterType.MeanBloodPressure];
                }
                catch (ArgumentException)
                {
                    Logger.log(LogLevel.WARNING, "Failed to generate initial Values for the VitalParameters at the Heart-Form in the Main GUI.", LogSource.GENERAL);
                }
            }
            else // Is also called when the Form is closed
            {
                //Debug.WriteLine("Is Not Visible");
                simulatedPatient.continuousValueCreated -= HeartEvent;
                if (editValueSubscribed && evForm != null)
                {
                    editValueSubscribed = false;
                    evForm.EditedValue -= RefreshEditedValue;
                }
            }
        }
        private void HeartEvent(object source, VitalParameterEventArgs e)
        {
            CurrentVitalValue currentVital = (CurrentVitalValue)source;
            if (currentVital.type == VitalParameterType.VenousBloodCarbondioxidePressure && this.Visible)
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Heart_bcpValue.Text = Convert.ToString(Math.Round(currentVital.value, 3))+" " + Global.TranslateVPType2Unit[VitalParameterType.VenousBloodCarbondioxidePressure]; }));
            }
            else if (currentVital.type == VitalParameterType.VenousBloodOxygenPressure && this.Visible)
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Heart_bopValue.Text = Convert.ToString(Math.Round(currentVital.value, 3))+" " + Global.TranslateVPType2Unit[VitalParameterType.VenousBloodOxygenPressure]; }));
            }
            else if (currentVital.type == VitalParameterType.SystolicBloodPressure && this.Visible)
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Heart_bpValue.Text = String.Format("{0}/{1}",Convert.ToString(Math.Round(currentVital.value, 0)), tBox_Heart_bpValue.Text.Split('/')[1]); }));
            }
            else if (currentVital.type == VitalParameterType.DiastolicBloodPressure && this.Visible)
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Heart_bpValue.Text = String.Format("{1}/{0}", Convert.ToString(Math.Round(currentVital.value, 0))+ " " + Global.TranslateVPType2Unit[VitalParameterType.DiastolicBloodPressure], tBox_Heart_bpValue.Text.Split('/')[0]); }));
            }
            else if (currentVital.type == VitalParameterType.MeanBloodPressure && this.Visible)
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Heart_mbpValue.Text = Convert.ToString(Math.Round(currentVital.value, 1))+ " " + Global.TranslateVPType2Unit[VitalParameterType.MeanBloodPressure]; }));
            }
        }
        private void RefreshEditedValue(object source, VitalParameterEventArgs e)
        {
            if (e.type.Equals(VitalParameterType.VenousBloodCarbondioxidePressure))
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Heart_bcpValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.VenousBloodCarbondioxidePressure), 3))+ " " + Global.TranslateVPType2Unit[VitalParameterType.VenousBloodCarbondioxidePressure]; }));
            }
            else if (e.type.Equals(VitalParameterType.VenousBloodOxygenPressure))
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Heart_bopValue.Text = Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.VenousBloodOxygenPressure), 3))+ " " + Global.TranslateVPType2Unit[VitalParameterType.VenousBloodOxygenPressure]; }));
            }
            else if (e.type.Equals(VitalParameterType.SystolicBloodPressure))
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Heart_bpValue.Text = String.Format("{0}/{1}", Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.SystolicBloodPressure), 0)), tBox_Heart_bpValue.Text.Split('/')[1]); }));
            }
            else if (e.type.Equals(VitalParameterType.DiastolicBloodPressure))
            {
                this.Invoke(new MethodInvoker(delegate { tBox_Heart_bpValue.Text = String.Format("{1}/{0}", Convert.ToString(Math.Round(simulatedPatient.getReferenceValue(VitalParameterType.DiastolicBloodPressure), 0)) + " " + Global.TranslateVPType2Unit[VitalParameterType.DiastolicBloodPressure], tBox_Heart_bpValue.Text.Split('/')[0]); }));
            }
        }

        private void openEditValueCheck(int eVNr)
        {
            if (Global.EditVitalParamteres)
            {
                EditValueCheck editValueCheck;
                evForm = (FormEditValue)Application.OpenForms["FormEditValue"];
                if (evForm != null) { }
                else
                {
                    editValueSubscribed = false;
                    editValueCheck = new EditValueCheck();
                    EditValueCheck.editValueNr = eVNr;
                    FormEditValue editValue = new FormEditValue(simulatedPatient);
                    evForm = editValue;

                    if (!editValueSubscribed)
                    {
                        editValueSubscribed = true;
                        evForm.EditedValue += RefreshEditedValue;
                    }
                    editValue.ShowDialog();
                }
            }
        }
        private void tBox_Heart_mbpValue_TextChanged(object sender, EventArgs e)
        {

        }

        private void b_Heart_Edit1_bcp_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void FormHeart_Shown(object sender, EventArgs e)
        {
            //timer_Heart.Start();
        }

        private void tBox_Heart1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
