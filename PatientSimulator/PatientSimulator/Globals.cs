﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PatientSimulator.VitalParameters;
using PatientSimulator.Organs;
using PatientSimulator.Patient.DatabaseRequestInterface;
using System.ComponentModel;
using System.Windows.Forms;

namespace PatientSimulator
{
    static class Global
    {
        public static string ApplicationName { get { return "Patient Simulator"; } }
        public static string ApplicationVersion { get { return "1.0a"; } }
        public static string ApplicationAuthors { get { return "Order 66"; } }

        public static bool ApplicationIsDevelopment = false;  // Indicates whether or not specific development options are turned on or of (eg. if true, the MainApplicationContext will not test the DB accessability)
        public static bool ApplicationAllowsConstantUpload = true; // Indicates whether or a DB-Connecten can be opened automatically or only at user request.
        public static ComponentResourceManager resourceManager; // resource manager to change languages
        /// <summary>
        /// The compatible version number of the Demographic and Vital DB.
        /// Read as  [majorVersion, minorVersion]
        /// </summary>
        public static int[] DatabaseVersionNumber { get; } = new int[] { 4, 3 };
        /// <summary>
        /// The compatible version UUID of the Demographic and Vital DB.
        /// Stored as string representation after norm RFC4122.
        /// </summary>
        public static Guid DatabaseVersionUUID { get; } = new Guid("25e1d8ea3aea11eba0353747f8066038");

        public static string DatabaseName { get; } = "LSE-G1";

        public static Global.DatabaseUser DatabaseUserInformation { get; private set; } = new DatabaseUser("LSE-G1", "T50ztvGhql0NWIbR5J5fXs86");
        //public static Global.DatabaseUser DatabaseUserInformation { get; private set; } = new DatabaseUser("postgres", "postgres");

        public static string DatabaseHost { get; set; } = "db-n1.ls.technikum-wien.at";

        #region Runtime Variables

        public static bool EditVitalParamteres { get; set; } = false;

        #endregion

        #region Language change
        private static string CurrentLanguage = "en";
        public static string getCurrentLanguage { get { return CurrentLanguage; } set { CurrentLanguage = value; } }

        public static void CL(Type c, Control control, string str)
        {
            resourceManager = new ComponentResourceManager(c);
            resourceManager.ApplyResources(control, control.Name, new System.Globalization.CultureInfo(str));
        }

        #endregion

        #region Translation Dictionaries
        public static Dictionary<VitalParameterType, string> TranslateVPType2ElgaCode
        {
            get
            {
                if (translateVPType2ElgaCode == null)
                {
                    translateVPType2ElgaCode = new Dictionary<VitalParameterType, string>();
                    translateVPType2ElgaCode.Add(VitalParameterType.MeanBloodPressure, "8478-0");
                    translateVPType2ElgaCode.Add(VitalParameterType.SystolicBloodPressure, "8480-6");
                    translateVPType2ElgaCode.Add(VitalParameterType.DiastolicBloodPressure, "8462-4");
                    translateVPType2ElgaCode.Add(VitalParameterType.BreathRate, "9279-1");
                    translateVPType2ElgaCode.Add(VitalParameterType.CapillarBloodGlucose, "32016-8");
                    translateVPType2ElgaCode.Add(VitalParameterType.CoreTemperature, "8310-5");
                    translateVPType2ElgaCode.Add(VitalParameterType.Height, "8302-2");
                    translateVPType2ElgaCode.Add(VitalParameterType.VenousBloodCarbondioxidePressure, "2021-4");
                    translateVPType2ElgaCode.Add(VitalParameterType.VenousBloodOxygenPressure, "2705-2");
                    translateVPType2ElgaCode.Add(VitalParameterType.Weight, "3141-9");
                }
                return translateVPType2ElgaCode;
            }
        }
        private static Dictionary<VitalParameterType, string> translateVPType2ElgaCode;

        public static Dictionary<VitalParameterType, string> TranslateVPType2Unit
        {
            get
            {
                if (translateVPType2Unit == null)
                {
                    translateVPType2Unit = new Dictionary<VitalParameterType, string>();
                    translateVPType2Unit.Add(VitalParameterType.MeanBloodPressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.SystolicBloodPressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.DiastolicBloodPressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.BreathRate, "/min");
                    translateVPType2Unit.Add(VitalParameterType.CapillarBloodGlucose, "mg/dl");
                    translateVPType2Unit.Add(VitalParameterType.CoreTemperature, "°C");
                    translateVPType2Unit.Add(VitalParameterType.Height, "cm");
                    translateVPType2Unit.Add(VitalParameterType.VenousBloodCarbondioxidePressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.VenousBloodOxygenPressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.Weight, "kg");
                }
                return translateVPType2Unit;
            }
        }
        private static Dictionary<VitalParameterType, string> translateVPType2Unit;
        // Get with: string someUnit = Global.TranslateVPType2Unit[VitalParameterType.SomeType];

        /// <summary>
        /// Translate an organ type into the name of the corresponding table in the Demographic and Vital DB
        /// </summary>
        public static Dictionary<OrganType, string> TranslateOType2DemographDB
        {
            get
            {
                if (translateOType2DemographDB == null)
                {
                    translateOType2DemographDB = new Dictionary<OrganType, string>();
                    translateOType2DemographDB.Add(OrganType.PeripheralVein, "peripheral_vein_organ");
                    translateOType2DemographDB.Add(OrganType.PeripheralArtery, "peripheral_artery_organ");
                    translateOType2DemographDB.Add(OrganType.Body, "body_organ");
                    translateOType2DemographDB.Add(OrganType.CapillarBlood, "capillary_blood_organ");
                    translateOType2DemographDB.Add(OrganType.Lung, "lung_organ");
                    translateOType2DemographDB.Add(OrganType.VenousBlood, "venous_blood_organ");
                }
                return translateOType2DemographDB;
            }
        }
        private static Dictionary<OrganType, string> translateOType2DemographDB;

        /// <summary>
        /// Translate an organ type into the corresponding entity name of patientsimulator.patient (where its uuid is stored as foreign key)
        /// </summary>
        public static Dictionary<OrganType, string> TranslateOType2DBPatientReference
        {
            get
            {
                if (translateOType2DBPatientReference == null)
                {
                    translateOType2DBPatientReference = new Dictionary<OrganType, string>();
                    translateOType2DBPatientReference.Add(OrganType.PeripheralVein, "peripheral_vein_uuid");
                    translateOType2DBPatientReference.Add(OrganType.PeripheralArtery, "peripheral_artery_uuid");
                    translateOType2DBPatientReference.Add(OrganType.Body, "body_uuid");
                    translateOType2DBPatientReference.Add(OrganType.CapillarBlood, "capillary_blood_uuid");
                    translateOType2DBPatientReference.Add(OrganType.Lung, "lung_uuid");
                    translateOType2DBPatientReference.Add(OrganType.VenousBlood, "venous_blood_uuid");
                }
                return translateOType2DBPatientReference;
            }
        }
        private static Dictionary<OrganType, string> translateOType2DBPatientReference;

        public static Dictionary<OrganType, VitalParameterType[]> AllowedVPsInOrgans
        {
            get
            {
                if (allowedVPsInOrgans == null)
                {
                    allowedVPsInOrgans = new Dictionary<OrganType, VitalParameterType[]>();
                    allowedVPsInOrgans.Add(OrganType.Body, new VitalParameterType[] { VitalParameterType.Height, VitalParameterType.Weight, VitalParameterType.CoreTemperature });
                    allowedVPsInOrgans.Add(OrganType.CapillarBlood, new VitalParameterType[] { VitalParameterType.CapillarBloodGlucose });
                    allowedVPsInOrgans.Add(OrganType.Lung, new VitalParameterType[] { VitalParameterType.BreathRate });
                    allowedVPsInOrgans.Add(OrganType.PeripheralArtery, new VitalParameterType[] { VitalParameterType.MeanBloodPressure, VitalParameterType.SystolicBloodPressure, VitalParameterType.DiastolicBloodPressure });
                    allowedVPsInOrgans.Add(OrganType.PeripheralVein, new VitalParameterType[] { });
                    allowedVPsInOrgans.Add(OrganType.VenousBlood, new VitalParameterType[] { VitalParameterType.VenousBloodCarbondioxidePressure, VitalParameterType.VenousBloodOxygenPressure });
                }
                return allowedVPsInOrgans;
            }
        }
        private static Dictionary<OrganType, VitalParameterType[]> allowedVPsInOrgans;

        public static Dictionary<int, GUI.AllowedAutomaticDBAccess> TranslatePropertiesBar2AllowedDBAccess
        {
            get
            {
                if (translatePropertiesBar2Enum == null)
                {
                    translatePropertiesBar2Enum = new Dictionary<int, GUI.AllowedAutomaticDBAccess>();
                    translatePropertiesBar2Enum.Add(0, GUI.AllowedAutomaticDBAccess.None);
                    translatePropertiesBar2Enum.Add(1, GUI.AllowedAutomaticDBAccess.Average);
                    translatePropertiesBar2Enum.Add(2, GUI.AllowedAutomaticDBAccess.Full);
                }
                return translatePropertiesBar2Enum;
            }
        }
        private static Dictionary<int, GUI.AllowedAutomaticDBAccess> translatePropertiesBar2Enum;

        public static Dictionary<Patient.ViewerRightsType, int> TranslateViewerRightsType2DBInteger
        {
            get
            {
                if(translateViewerRightsType2DBInteger == null)
                {
                    translateViewerRightsType2DBInteger = new Dictionary<Patient.ViewerRightsType, int>();
                    translateViewerRightsType2DBInteger.Add(Patient.ViewerRightsType.Hidden, 0);
                    translateViewerRightsType2DBInteger.Add(Patient.ViewerRightsType.Reading, 1);
                    translateViewerRightsType2DBInteger.Add(Patient.ViewerRightsType.Writing, 2);
                }
                return translateViewerRightsType2DBInteger;
            }
        }
        private static Dictionary<Patient.ViewerRightsType, int> translateViewerRightsType2DBInteger;

        public static Dictionary<Patient.PatientGender, string> TranslateGender2DBString { get; } = new Dictionary<Patient.PatientGender, string>
        {
            [Patient.PatientGender.Male] = "m",
            [Patient.PatientGender.Female] = "f",
            [Patient.PatientGender.Other] = "o",
            [Patient.PatientGender.Unknown] = "u",
        };

        #endregion

        public class DatabaseUser
        {
            public string Name { get; private set; }
            public string Password { get; private set; }

            public DatabaseUser(string name, string password)
            {
                Name = name;
                Password = password;
            }
        }
    }

    class Locks
    {
        public static readonly object[] ThreadCalculationLocks = new object[] { new object(), new object(), new object(), new object(), new object() };
    }
}

namespace PatientSimulator.VitalParameters
{
    public enum VitalParameterType
    {
        SystolicBloodPressure, DiastolicBloodPressure, MeanBloodPressure, BreathRate, VenousBloodOxygenPressure,
        VenousBloodCarbondioxidePressure, Weight, Height, CoreTemperature, CapillarBloodGlucose
    }

    delegate void VitalParameterEventHandler(object source, VitalParameterEventArgs args);
    class VitalParameterEventArgs : EventArgs
    {
        public VitalParameterType type { get; set; }  // Might be unnessecary, as the type is contained in Current vital value

        public VitalParameterEventArgs(VitalParameterType type) : base()
        {
            this.type = type;
        }
    }
}

namespace PatientSimulator.Organs
{
    /// <summary>
    /// The Organ classes have been reduced to define Organ classes based on physiological properties, a definete instance identification is done by OrganType
    /// </summary>
    public enum OrganType { Lung, Body, CapillarBlood, VenousBlood, PeripheralVein, PeripheralArtery }
}

namespace PatientSimulator.GUI
{
    /// <summary>
    /// States to what extend data is automatically written to the DB (None - None, Average - All except continuously created VP, Full - All)
    /// </summary>
    public enum AllowedAutomaticDBAccess { None, Average, Full }
}

namespace PatientSimulator.Patient
{
    /// <summary>
    /// States how many rights viewer (non-owner) of a patient have over this patient
    /// </summary>
    public enum ViewerRightsType { Hidden, Reading, Writing }

    /// <summary>
    /// The patients gender posibilities (extracted from strucutred HL7 FHIR choices)
    /// </summary>
    public enum PatientGender { Male, Female, Unknown, Other }
}

namespace PatientSimulator.Patient.DatabaseRequestInterface
{
    class TestCases
    {
        /// <summary>
        /// Creates a standard <c>PatientInterface</c> of default and approved values. Use only in test cases, not any exportable solution. 
        /// Be aware that the patient user is not existing in the database on default.
        /// </summary>
        /// <returns>Full <c>PatientInterface</c> with all Organs and vital parameters to initiate a new instance of <c>PatientModel</c></returns>
        public static PatientInterface getGenericPatientInterface()  // !!! Do not delete or change this function for consistency reasons, if a different PatientInterface is needed, create an additional one !!!
        {
            List<VitalParameterInterface> vpinterface = new List<VitalParameterInterface>();
            List<OrganInterface> ointerface = new List<OrganInterface>();
            List<ICD10ImpairmentInterface> icd10interface = new List<ICD10ImpairmentInterface>();
            // PW: 123456 ; Salt: saltsaltsalt ; SVNR: 3354010100
            UserInterface userInterface = new UserInterface(new Guid("ddd72a14-1c60-a2eb-a01a-b75c8491bc2f"), "testuserpleaseignore",
                new byte[32] { 0x51, 0xae, 0x77, 0xe7, 0x73, 0x4b, 0x76, 0x15, 0xf5, 0x8f, 0x51, 0x2b, 0x38, 0x80, 0xc6, 0x60, 0x2d, 0x18, 0x02, 0x63, 0xab, 0x13, 0x07, 0xad, 0x0e, 0xc7, 0x02, 0x48, 0x48, 0xb6, 0xe4, 0x02 },
                new byte[12] { Convert.ToByte('s'), Convert.ToByte('a'), Convert.ToByte('l'), Convert.ToByte('t'), Convert.ToByte('s'), Convert.ToByte('a'), Convert.ToByte('l'), Convert.ToByte('t'),
                    Convert.ToByte('s'), Convert.ToByte('a'), Convert.ToByte('l'), Convert.ToByte('t') },
                new byte[32] { 0xcc, 0x17, 0x47, 0x8c, 0x03, 0x3c, 0x99, 0xd8, 0x9b, 0xcb, 0xed, 0x17, 0xf7, 0x9e, 0x8d, 0x97, 0x47, 0x63, 0x46, 0x83, 0x23, 0xde, 0xcc, 0xb7, 0xf5, 0x90, 0xe1, 0xe9, 0x6f, 0x0a, 0xab, 0x80 },
                new CompanyPersonDataInterface(new Guid("2b6d6694-3c6b-11eb-a41b-1fe3f5f51acf"), "testcompanypleaseignore", "Max Mustergasse 0", "pleaseignore@testcompany.at", "www.testcompany.at"));
            ViewerRightsInterface rightInterface = new ViewerRightsInterface(ViewerRightsType.Writing, "Modification");
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.BreathRate, 10f, null));
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.CapillarBloodGlucose, 100f, null));
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.CoreTemperature, 37f, null));
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.DiastolicBloodPressure, 90f, null));
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.Height, 180f, null));
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.MeanBloodPressure, 100f, null));
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.SystolicBloodPressure, 130f, null));
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.VenousBloodCarbondioxidePressure, 2.8f, null));
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.VenousBloodOxygenPressure, 0.9f, null));
            vpinterface.Add(new VitalParameterInterface(VitalParameterType.Weight, 100f, null));
            ointerface.Add(new OrganInterface(OrganType.Body, new Guid("12e34599-7a89-8a01-02d8-6277a39f4f51")));
            ointerface.Add(new OrganInterface(OrganType.CapillarBlood, new Guid("12e34599-7a89-8a01-02d8-6277a39f4f52")));
            ointerface.Add(new OrganInterface(OrganType.Lung, new Guid("12e34599-7a89-8a01-02d8-6277a39f4f53")));
            ointerface.Add(new OrganInterface(OrganType.PeripheralArtery, new Guid("12e34599-7a89-8a01-02d8-6277a39f4f54")));
            ointerface.Add(new OrganInterface(OrganType.PeripheralVein, new Guid("12e34599-7a89-8a01-02d8-6277a39f4f55")));
            ointerface.Add(new OrganInterface(OrganType.VenousBlood, new Guid("12e34599-7a89-8a01-02d8-6277a39f4f56")));
            icd10interface.Add(new ICD10ImpairmentInterface("E66.0", 2020, null));
            return new PatientInterface(new Guid("c25031ad-7a89-8a01-02d8-6277a39f4f56"), DateTime.Now, new Guid("7a19253b-9b69-11ea-8d67-98e7f42e1ccf"), "Rudolf", PatientGender.Male, true, userInterface, rightInterface, vpinterface, ointerface, icd10interface);
        }
    }
}
