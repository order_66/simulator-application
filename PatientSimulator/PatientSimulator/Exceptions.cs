﻿using System;
using Npgsql;

using PatientSimulator.VitalParameters;

namespace PatientSimulator.Exceptions
{
    /// <summary>
    /// It is an exception that is thrown when a requested DatabaseRequestInterface field can not be filled because of missing information.
    /// </summary>
    class MissingInterfaceDataException : Exception
    {
        string interfaceParameter;

        public MissingInterfaceDataException(string message) : base(message) { }
        public MissingInterfaceDataException(string message, Exception innerException) : base(message, innerException) { }
        public MissingInterfaceDataException(string message, string interfaceParameter) : this(message)
        {
            this.interfaceParameter = interfaceParameter;
        }
        public MissingInterfaceDataException(string message, string interfaceParameter, Exception innerException) : this(message, innerException)
        {
            this.interfaceParameter = interfaceParameter;
        }
    }
    class CreateProtectedPatientModelException : Exception
    {
        public CreateProtectedPatientModelException(string message) : base(message) { }
        public CreateProtectedPatientModelException(string message, Exception innerException) : base(message, innerException) { }
    }
    /// <summary>
    /// This exception is thrown when some kind of error occured handeling a VitalParameter
    /// </summary>
    class VitalParameterException : Exception
    {
        public VitalParameterType? Type { get; protected set; }

        public VitalParameterException() : base()
        {
            this.Type = null;
        }
        public VitalParameterException(string message) : base(message)
        {
            this.Type = null;
        }
        public VitalParameterException(string message, Exception innerException) : base(message, innerException)
        {
            this.Type = null;
        }
        public VitalParameterException(string message, VitalParameterType type) : this(message)
        {
            this.Type = type;  // Will be executed after this(message)
        }
        public VitalParameterException(string message, VitalParameterType type, Exception innerException) : this(message, innerException)
        {
            this.Type = type;  // Will be executed after this(message, innerException)
        }
    }
    /// <summary>
    /// Inherits from VitalParameterException, is used for the case, that a reference to a vitalParameter object is not available
    /// </summary>
    class VitalParameterNullReferenceException : VitalParameterException
    {
        public string parameter { get; protected set; }
        
        public VitalParameterNullReferenceException() : base() { }
        public VitalParameterNullReferenceException(string message) : base(message) { }
        public VitalParameterNullReferenceException(string message, Exception innerException) : base(message, innerException) { }
        public VitalParameterNullReferenceException(string message, VitalParameterType type) : base(message, type) { }
        public VitalParameterNullReferenceException(string message, VitalParameterType type, Exception innerException) : base(message, type, innerException) { }
        public VitalParameterNullReferenceException(string message, VitalParameterType type, string param) : this(message, type)
        {
            parameter = param;
        }
        public VitalParameterNullReferenceException(string message, VitalParameterType type, string param, Exception innerException) : this(message, type, innerException)
        {
            parameter = param;
        }
        public VitalParameterNullReferenceException(string message, string param) : this(message)
        {
            parameter = param;
        }
        public VitalParameterNullReferenceException(string message, string param, Exception innerException) : this(message, innerException)
        {
            parameter = param;
        }
    }

    /// <summary>
    /// Inherits from VitalParameterException, is used for the case, that the continuous values of a vital parameter are disabled by default (doCalculateContinuousValue is false)
    /// Can return the current reference Value as a compensation for the missing continuous values (perform a null check on the parameter befor using it).
    /// </summary>
    class NonExistentContinuousValueException : VitalParameterException
    {
        public float? currentReferenceValue { get; protected set; }
        public NonExistentContinuousValueException() : base() { }
        public NonExistentContinuousValueException(string message, float? referenceValue) : base(message)
        {
            currentReferenceValue = referenceValue;
        }
        public NonExistentContinuousValueException(string message, float? referenceValue, Exception innerException) : base(message, innerException)
        {
            currentReferenceValue = referenceValue;
        }
        public NonExistentContinuousValueException(string message, VitalParameterType type, float? referenceValue) : base(message, type)
        {
            currentReferenceValue = referenceValue;
        }
        public NonExistentContinuousValueException(string message, VitalParameterType type, float? referenceValue, Exception innerException) : base(message, type, innerException)
        {
            currentReferenceValue = referenceValue;
        }
    }

    #region PostgreSQL, DBAccessor and related Exceptions
    /// <summary>
    /// An exception thrown when some sql statemetns could not be executed for some reason during a multi-command function
    /// </summary>
    class SQLCommandIncompleteException : NpgsqlException
    {
        public int FailedSQLCommands { get; protected set; }

        public SQLCommandIncompleteException(int failedCommands) : base($"Failed to execute {failedCommands} SQL Commands.")
        {
            FailedSQLCommands = failedCommands;
        }
        public SQLCommandIncompleteException(int failedCommands, Exception innerException) : base($"Failed to execute {failedCommands} SQL Commands.", innerException)
        {
            FailedSQLCommands = failedCommands;
        }
    }

    /// <summary>
    /// This exception shall be thrown if any SELECT statement on the Demographic and Vital DB returns sucessfully, but without any results.
    /// </summary>
    class SQLNoEntryFoundException : NpgsqlException
    {
        public string SelectQuery { get; protected set; }

        public SQLNoEntryFoundException(string selectQuery) : base($"No entries available at following select statement:\n{selectQuery}")
        {
            SelectQuery = selectQuery;
        }
        public SQLNoEntryFoundException(string selectQuery, Exception innerException) : base($"No entries available at following select statement:\n{selectQuery}", innerException)
        {
            SelectQuery = selectQuery;
        }
    }
    #endregion
}
