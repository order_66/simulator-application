﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PatientSimulator_REST_API.Domain.Models;

namespace PatientSimulator_REST_API.Domain.Services
{
    public interface IPatientService
    {
        /* Async tasks meant to return patient models read from the database */
        Task<IEnumerable<PatientModel>> ListAsync();
        Task<IEnumerable<PatientModel>> ListAsync(int count);
        Task<PatientModel> SingleAsync(Guid patientId);
    }
}
