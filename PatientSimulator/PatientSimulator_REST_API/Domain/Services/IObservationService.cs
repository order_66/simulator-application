﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Services
{
    public interface IObservationService
    {
        IVitalSignService VitalSignService { get; }
    }
}
