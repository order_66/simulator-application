﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Services
{
    public interface IResourceService
    {
        /* tasks from patient and vital sign service will be needed in different controllers, therefore they are wrapped up here (so one controller can recieve one service) */

        IPatientService PatientService { get; }
        IVitalSignService VitalSignService { get; }
    }
}
