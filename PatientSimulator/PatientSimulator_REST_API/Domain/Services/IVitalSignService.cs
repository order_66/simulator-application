﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hl7.Fhir.Model;

namespace PatientSimulator_REST_API.Domain.Services
{
    public interface IVitalSignService
    {
        /* For reference: list all vitalsigns or list all vitalsigns for the given LOINC code */
        System.Threading.Tasks.Task UpdateResourceVitalSignAsync(IEnumerable<Models.ObservationResource> vitalSigns);
        Task<IEnumerable<Observation>> ListResourceVitalSignAsync(Guid patientId);
        Task<IEnumerable<Observation>> ListResourceVitalSignAsync(Guid patientId, IEnumerable<string> loincCodes);
        Task<IEnumerable<Observation>> ListObservationVitalSignAsync(Guid patientId);
        Task<IEnumerable<Observation>> ListObservationVitalSignAsync(Guid patientId, IEnumerable<string> loincCodes);
        Task<IEnumerable<Observation>> ListObservationVitalSignAsync(Guid patientId, DateTime begin, DateTime end);
        Task<IEnumerable<Observation>> ListObservationVitalSignAsync(Guid patientId, IEnumerable<string> loincCodes, DateTime begin, DateTime end);
    }
}
