﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Database
{
    /// <summary>
    /// This class has the aim to store continuously created vital values (like heart rate, etc.) and link them with the time and type of vital parameter they were created.
    /// </summary>
    class CurrentVitalValue
    {
        public DateTime timestamp { private set; get; }
        public float value { private set; get; }
        public VitalParameterType type { private set; get; }

        public CurrentVitalValue(float vital, VitalParameterType vitalParameter) : this(DateTime.Now, vital, vitalParameter) { }
        public CurrentVitalValue(DateTime createdAt, float vital, VitalParameterType vitalParameter)
        {
            timestamp = createdAt;
            value = vital;
            type = vitalParameter;
        }
    }
}
