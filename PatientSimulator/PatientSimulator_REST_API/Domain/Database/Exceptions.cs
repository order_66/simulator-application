﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npgsql;

namespace PatientSimulator_REST_API.Domain.Database
{
    #region PostgreSQL, DBAccessor and related Exceptions
    /// <summary>
    /// An exception thrown when some sql statemetns could not be executed for some reason during a multi-command function
    /// </summary>
    class SQLCommandIncompleteException : NpgsqlException
    {
        public int FailedSQLCommands { get; protected set; }

        public SQLCommandIncompleteException(int failedCommands) : base($"Failed to execute {failedCommands} SQL Commands.")
        {
            FailedSQLCommands = failedCommands;
        }
        public SQLCommandIncompleteException(int failedCommands, Exception innerException) : base($"Failed to execute {failedCommands} SQL Commands.", innerException)
        {
            FailedSQLCommands = failedCommands;
        }
    }

    /// <summary>
    /// This exception shall be thrown if any SELECT statement on the Demographic and Vital DB returns sucessfully, but without any results.
    /// </summary>
    class SQLNoEntryFoundException : NpgsqlException
    {
        public string SelectQuery { get; protected set; }

        public SQLNoEntryFoundException(string selectQuery) : base($"No entries available at following select statement:\n{selectQuery}")
        {
            SelectQuery = selectQuery;
        }
        public SQLNoEntryFoundException(string selectQuery, Exception innerException) : base($"No entries available at following select statement:\n{selectQuery}", innerException)
        {
            SelectQuery = selectQuery;
        }
    }
    #endregion
}
