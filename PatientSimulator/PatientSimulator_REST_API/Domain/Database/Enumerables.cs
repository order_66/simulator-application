﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Database
{
    public enum VitalParameterType
    {
        SystolicBloodPressure, DiastolicBloodPressure, MeanBloodPressure, BreathRate, VenousBloodOxygenPressure,
        VenousBloodCarbondioxidePressure, Weight, Height, CoreTemperature, CapillarBloodGlucose
    }

    /// <summary>
    /// States how many rights viewer (non-owner) of a patient have over this patient
    /// </summary>
    public enum ViewerRightsType { Hidden, Reading, Writing }

    /// <summary>
    /// The Organ classes have been reduced to define Organ classes based on physiological properties, a definete instance identification is done by OrganType
    /// </summary>
    public enum OrganType { Lung, Body, CapillarBlood, VenousBlood, PeripheralVein, PeripheralArtery }

    public enum PersonalizedDataType { CompanyData, PersonData }

    /// <summary>
    /// The patients gender posibilities (extracted from strucutred HL7 FHIR choices)
    /// </summary>
    public enum PatientGender { Male, Female, Unknown, Other }
}
