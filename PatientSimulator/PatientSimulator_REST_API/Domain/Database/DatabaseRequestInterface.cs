﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Database
{
    class PatientInterface  // a non-used member is set to null
    {
        /// <summary>
        /// All vitalparameters (and continuously created vital values) currently linked to the patient
        /// </summary>
        public List<VitalParameterInterface> vitalParameters { get; set; } = new List<VitalParameterInterface>();
        // These Lists are intended to use only when the full patient is loaded from the database, in other cases the single classes might be used
        /// <summary>
        /// All organs currently linked to the patient
        /// </summary>
        public List<OrganInterface> organs { get; set; } = new List<OrganInterface>();
        /// <summary>
        /// All deseases/illnesses currently linked to the patient model
        /// </summary>
        public List<ICD10ImpairmentInterface> ICD10Impairments { get; set; } = new List<ICD10ImpairmentInterface>();
        /// <summary>
        /// One registered user, who is in administrative ownership of the patient
        /// </summary>
        public UserInterface owner { get; set; }
        /// <summary>
        /// The rights, which are granted to everyone beside the owner
        /// </summary>
        public ViewerRightsInterface viewerRights { get; set; }
        /// <summary>
        /// UUID version 1 of the current patient
        /// </summary>
        public Guid? uuid { get; set; }
        /// <summary>
        /// Date and time the model was first crteated
        /// </summary>
        public DateTime? createdAt { get; set; }
        /// <summary>
        /// UUID version 1 of the patient which served as model for the current one
        /// </summary>
        public Guid? roleModelUuid { get; set; }
        /// <summary>
        /// Human readable representation of the patient
        /// </summary>
        public string patientName { get; set; }
        /// <summary>
        /// The gender of the patient model (default PatientGender.Unkown)
        /// </summary>
        public PatientGender gender { get; set; } = PatientGender.Unknown;
        /// <summary>
        /// Administrative flag, if the patient serves as role model (and therefore cannot be altered or directly loaded)
        /// </summary>
        public bool? isAlterable { get; set; }

        [Obsolete("Kept for compatibility reasons, use other overload of this method")]
        public PatientInterface(Guid? uuid, DateTime? createdAt, Guid? roleModelUuid, string patientName, bool? isAlterable, UserInterface owner, ViewerRightsInterface viewerRights, List<VitalParameterInterface> vitalParameters, List<OrganInterface> organs, List<ICD10ImpairmentInterface> ICD10Impairments)
        {
            this.uuid = uuid;
            this.createdAt = createdAt;
            this.roleModelUuid = roleModelUuid;
            this.patientName = patientName;
            this.owner = owner;
            this.viewerRights = viewerRights;
            this.isAlterable = isAlterable;
            this.vitalParameters = vitalParameters;
            this.organs = organs;
            this.ICD10Impairments = ICD10Impairments;
        }
        public PatientInterface(Guid? uuid, DateTime? createdAt, Guid? roleModelUuid, string patientName, PatientGender? gender, bool? isAlterable, UserInterface owner, ViewerRightsInterface viewerRights, List<VitalParameterInterface> vitalParameters, List<OrganInterface> organs, List<ICD10ImpairmentInterface> ICD10Impairments)
        {
            this.uuid = uuid;
            this.createdAt = createdAt;
            this.roleModelUuid = roleModelUuid;
            this.patientName = patientName;
            this.gender = gender ?? PatientGender.Unknown;
            this.owner = owner;
            this.viewerRights = viewerRights;
            this.isAlterable = isAlterable;
            this.vitalParameters = vitalParameters;
            this.organs = organs;
            this.ICD10Impairments = ICD10Impairments;
        }
        public PatientInterface() { }

        public override string ToString()  // Mainly used by some combo boxes at the GUI
        {
            return String.Format("{0}{2}{1}", this.patientName, this.createdAt?.ToString("dd.MMM.yyyy HH:mm"), this.createdAt == null ? "" : (this.patientName == null ? "Patient from " : " from "));
        }
    }

    /// <summary>
    /// An interface to store data regarding a VitalParameter and its continuous values.
    /// </summary>
    class VitalParameterInterface  // a non-used member is set to null, except for type, which is mandatory
    {
        /// <summary>
        /// The type, of which the specific vitalparameter is a member of.
        /// </summary>
        public VitalParameterType type { get; set; }
        /// <summary>
        /// The value identifying the vitalparameters current physiological state
        /// </summary>
        public float? referenceValue { get; set; }
        /// <summary>
        /// Date and time when this field was last changed in the dataabse
        /// </summary>
        public DateTime? lastChanged { get; set; }
        /// <summary>
        /// All <c>CurrentVitalValue</c> objects linked to this vitalparameter.
        /// </summary>
        public List<CurrentVitalValue> continuousValues { get; set; } = new List<CurrentVitalValue>();

        public VitalParameterInterface(VitalParameterType interfaceType, float? interfaceReferenceValue, DateTime? interfaceLastChanged, List<CurrentVitalValue> interfaceContinuousValues)
        {
            type = interfaceType;
            referenceValue = interfaceReferenceValue;
            lastChanged = interfaceLastChanged;
            continuousValues = interfaceContinuousValues == null ? new List<CurrentVitalValue>() : new List<CurrentVitalValue>(interfaceContinuousValues);
        }
        public VitalParameterInterface(VitalParameterType interfaceType, float? interfaceReferenceValue, List<CurrentVitalValue> interfaceContinuousValues)
        {
            type = interfaceType;
            referenceValue = interfaceReferenceValue;
            continuousValues = interfaceContinuousValues == null ? new List<CurrentVitalValue>() : new List<CurrentVitalValue>(interfaceContinuousValues); // So changes to the list will not affect the function
        }
        public VitalParameterInterface() { }
    }

    class OrganInterface  // a non-used member is set to null, except for type, which is mandatory
    {
        public OrganType type { get; set; }
        public Guid? uuid { get; set; }

        public OrganInterface(OrganType interfaceType, Guid? interfaceUuid)
        {
            type = interfaceType;
            uuid = interfaceUuid;
        }
        public OrganInterface() { }
    }

    /// <summary>
    /// An interface to store data regarding an ICD10 coded diagnosis.
    /// </summary>
    class ICD10ImpairmentInterface
    {
        /// <summary>
        /// The ICD10 character code of this diagnosis (corresponding to the given version).
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// The version of ICD10 of which the diagnosis reffers to.
        /// </summary>
        public int? VersionYear { get; set; }
        /// <summary>
        /// All related impairments of vitalparameters linked to this specific diagnosis.
        /// </summary>
        public List<ICD10ImpairedValueInterface> impairedVitalParameters { get; set; } = new List<ICD10ImpairedValueInterface>();

        public ICD10ImpairmentInterface(string code, int? versionYear, List<ICD10ImpairedValueInterface> impairedVitalParameters)
        {
            this.code = code;
            this.VersionYear = versionYear;
            this.impairedVitalParameters = impairedVitalParameters;
        }
        public ICD10ImpairmentInterface() { }
    }

    class ICD10ImpairedValueInterface
    {
        public VitalParameterType type { get; set; }
        public float? referenceValue { get; set; }
        public float? impairedValue { get; set; }

        public ICD10ImpairedValueInterface(VitalParameterType type, float? referenceValue, float? impairedValue)
        {
            this.referenceValue = referenceValue;
            this.impairedValue = impairedValue;
            this.type = type;
        }
        public ICD10ImpairedValueInterface() { }
    }

    /// <summary>
    /// Representation of an entry of the table management.version int he Demographic and Vital DB
    /// </summary>
    class VersionInterface
    {
        /// <summary>
        /// UUID version 1 identifying the current database implementation version
        /// </summary>
        public Guid? uuid { get; set; }
        /// <summary>
        /// Major version number identifying the current database implementation (as in <c>majorVersion</c>.<c>minorVersion</c>)
        /// </summary>
        public int? majorVersion { get; set; }
        /// <summary>
        /// Minor version number identifying minor patches of the current database implementation (as in <c>majorVersion</c>.<c>minorVersion</c>)
        /// </summary>
        public int? minorVersion { get; set; }
        /// <summary>
        /// A flag, indication if the database implementation is in alpha testing
        /// </summary>
        public bool? isAlpha { get; set; }
        /// <summary>
        /// A flag, indication if the database implementation is in alpha testing
        /// </summary>
        public bool? isBeta { get; set; }
        /// <summary>
        /// The Date (not including time) of which this database implementation was enrolled
        /// </summary>
        public DateTime? releaseDate { get; set; }

        public VersionInterface(Guid? uuid, int? majorVersion, int? minorVersion, bool? isAlpha, bool? isBeta, DateTime? releaseDate)
        {
            this.uuid = uuid;
            this.majorVersion = majorVersion;
            this.minorVersion = minorVersion;
            this.isAlpha = isAlpha;
            this.isBeta = isBeta;
            this.releaseDate = releaseDate;
        }
        public VersionInterface() { }
    }

    /// <summary>
    /// Representation of an entry in the table management.user of the Demographic and Vital DB
    /// </summary>
    class UserInterface
    {
        /// <summary>
        /// UUID version 1 identifying the specific user of the desktop application
        /// </summary>
        public Guid? Uuid { get; set; }
        /// <summary>
        /// Unique username of this user used as identification token
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Sha256 hashed password and salt of this user, used as authentification token
        /// </summary>
        public byte[] PasswordHash
        {
            get { return passwordHash_hidden; }
            set
            {
                if (value != null && value.Length != 32)
                {
                    throw new ArgumentOutOfRangeException("For this variable only array lengths of 32 elements are allowed.");
                }
                passwordHash_hidden = value;
            }
        }
        private byte[] passwordHash_hidden = null;
        /// <summary>
        /// 12 byte salt, used to sha256 hash the password of the user and increase security of the hash against rainbow-chart attacks
        /// </summary>
        public byte[] PasswordSalt
        {
            get { return passwordSalt_hidden; }
            set
            {
                if (value != null && value.Length != 12)
                {
                    throw new ArgumentOutOfRangeException("For this variable only array lengths of 12 elements are allowed.");
                }
                passwordSalt_hidden = value;
            }
        }
        private byte[] passwordSalt_hidden = null;
        /// <summary>
        /// Sha256 hashed social insurance number of the user, used as identification token
        /// </summary>
        public byte[] InsuranceNumberHash
        {
            get { return insuranceNumberHash_hidden; }
            set
            {
                if (value != null && value.Length != 32)
                {
                    throw new ArgumentOutOfRangeException("For this variable only array lengths of 32 elements are allowed.");
                }
                insuranceNumberHash_hidden = value;
            }
        }
        private byte[] insuranceNumberHash_hidden = null;
        /// <summary>
        /// A class extending <c>IPersonalizedData</c>, with its field <c>Person</c> indicating if the class is from type <c>RealPersonDataInterface</c> or <c>CompanyPersonDataInterface</c>
        /// </summary>
        public IPersonalizedData PersonData { get; set; }

        public UserInterface(Guid? uuid, string username, byte[] passwordHash, byte[] passwordSalt, byte[] insuranceNumberHash, IPersonalizedData personData)
        {
            this.Uuid = uuid;
            this.Username = username;
            this.PasswordHash = passwordHash;
            this.PasswordSalt = passwordSalt;
            this.InsuranceNumberHash = insuranceNumberHash;
            this.PersonData = personData;
        }
        public UserInterface() { }

        public override string ToString()
        {
            return String.Format("Patientsimulator User {0}", Username ?? "UNKNOWN");
        }
    }

    class RealPersonDataInterface : IPersonalizedData
    {
        public Guid? Uuid { get; set; }
        public PersonalizedDataType Person { get; private set; } = PersonalizedDataType.PersonData;
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Titles { get; set; }
        public string Email { get; set; }
        public DateTime? Birthdate { get; set; }
        public string NationalityCode { get; set; }
        public string Employer { get; set; }

        public RealPersonDataInterface(Guid? uuid, string surname, string lastname, string titles, string email, DateTime? birthdate, string nationalityCode, string employer)
        {
            this.Uuid = uuid;
            this.Firstname = surname;
            this.Lastname = lastname;
            this.Titles = titles;
            this.Email = email;
            this.Birthdate = birthdate;
            this.NationalityCode = nationalityCode;
            this.Employer = employer;
        }
        public RealPersonDataInterface() { }
    }
    class CompanyPersonDataInterface : IPersonalizedData
    {
        public Guid? Uuid { get; set; }
        public PersonalizedDataType Person { get; private set; } = PersonalizedDataType.CompanyData;
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactEMail { get; set; }
        public string Webaddress { get; set; }

        public CompanyPersonDataInterface(Guid? uuid, string name, string address, string contactEMail, string webaddress)
        {
            this.Uuid = uuid;
            this.Name = name;
            this.Address = address;
            this.ContactEMail = contactEMail;
            this.Webaddress = webaddress;
        }
        public CompanyPersonDataInterface() { }
    }
    interface IPersonalizedData
    {
        Guid? Uuid { get; set; }
        PersonalizedDataType Person { get; }
    }

    class ViewerRightsInterface
    {
        public ViewerRightsType Right { set; get; }
        public string Description { get; set; }

        public ViewerRightsInterface(ViewerRightsType right, string description)
        {
            Right = right;
            Description = description;
        }
        public ViewerRightsInterface() { }
        public override string ToString()
        {
            return Right.ToString();
        }
    }
}