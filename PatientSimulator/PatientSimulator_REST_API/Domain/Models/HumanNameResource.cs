﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hl7.Fhir.Model;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class HumanNameResource
    {
        public string Use { get; set; }
        public string  Family { get; set; }
        public IEnumerable<string> Given { get; set; }
        public IEnumerable<string> Prefix { get; set; }
        public IEnumerable<string> Suffix { get; set; }
        public string Text { get; set; }
    }
}
