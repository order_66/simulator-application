﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class IsAlifeResource
    {
        public bool IsAlife { get; set; } = true;
    }
}
