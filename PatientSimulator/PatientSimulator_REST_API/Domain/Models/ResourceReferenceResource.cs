﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class ResourceReferenceResource
    {
        public string Reference { get; set; }
        public string Type { get; set; }
        public IdentifierResource Identifier { get; set; }
        public string Display { get; set; }
    }
}
