﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hl7.Fhir.Model;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class PatientModel : Patient
    {
        public List<ResourceReference> vitalSign { get; } = new List<ResourceReference>();
    }
}
