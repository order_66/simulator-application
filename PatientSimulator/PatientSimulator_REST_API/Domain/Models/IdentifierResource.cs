﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hl7.Fhir.Model;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class IdentifierResource
    {
        public string System { get; set; }
        public string Value { get; set; }
        public string Use { get; set; }
        public CodeableConceptResource Type { get; set; }
        public ResourceReferenceResource Assigner { get; set; }
    }
}
