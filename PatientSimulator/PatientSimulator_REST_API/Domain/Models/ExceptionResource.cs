﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class ExceptionDevResource
    {
        public string Exception { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string ExceptionSource { get; set; }
    }

    public class ExceptionResource
    {
        public string Exception { get; set; }
    }
}
