﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hl7.Fhir.Model;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class CodingResource
    {
        public string System { get; set; }
        public string Version { get; set; }
        public string Code { get; set; }
        public string Display { get; set; }
    }
}
