﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hl7.Fhir.Model;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class AddressResource
    {

        public string Type { get; set; }
        public string Use { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string PostalCode { get; set; }
        public IEnumerable<string> Line { get; set; }
        public string Text { get; set; }
    }
}
