﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class CodeableConceptResource
    {
        public List<CodingResource> Coding { get; set; }
        public string ElementId { get; set; }
        public string TypeName { get; set; }
        public string Text { get; set; }
    }
}
