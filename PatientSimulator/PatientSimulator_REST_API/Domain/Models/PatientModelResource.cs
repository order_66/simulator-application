﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hl7.Fhir.Model;

namespace PatientSimulator_REST_API.Domain.Models
{
    /// <summary>
    /// Used by auto mapper to return a resource containing only suported elements
    /// </summary>
    public class PatientModelResource
    {
        public string Id { get; set; }
        public bool? Active { get; set; }
        public List<HumanNameResource> Name { get; set; } = new List<HumanNameResource>();
        public string Gender { get; set; }
        public string BirthDate { get; set; }
        public bool? DeceasedBoolean { get; set; }
        public List<AddressResource> Address { get; set; }
        public ResourceReferenceResource ManagingOrganization { get; set; }
        public List<ResourceReferenceResource> VitalSign { get; set; } = new List<ResourceReferenceResource>();
    }
}
