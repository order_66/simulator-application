﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Domain.Models
{
    public class ObservationResource
    {
        //[JsonProperty("Status")]
        public string Status { get; set; }
        public List<CodeableConceptResource> Category { get; set; }
        public CodeableConceptResource Code { get; set; }
        public ResourceReferenceResource Subject { get; set; }
        public string EffectiveDateTime { get; set; }
        public float? ValueQuantity { get; set; }
        public CodeableConceptResource DataAbsentReason { get; set; }
        public List<Hl7.Fhir.Model.Observation.ComponentComponent> Component { get; set; }
    }
}
