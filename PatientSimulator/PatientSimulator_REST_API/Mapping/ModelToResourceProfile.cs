﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

using PatientSimulator_REST_API.Domain.Models;

namespace PatientSimulator_REST_API.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            // Nested mapping is done automatically by the defined maps for inner parameters.
            CreateMap<PatientModel, PatientModelResource>()
                .ForMember(pr => pr.DeceasedBoolean, opt => opt.MapFrom(p => p.Deceased.TypeName.Equals("boolean") ? ((Hl7.Fhir.Model.FhirBoolean)p.Deceased).Value : null ))
                .ForMember(pr => pr.Gender, opt => opt.MapFrom(p => p.Gender.HasValue ? p.Gender.Value.ToString() : null ));
            CreateMap<Hl7.Fhir.Model.HumanName, HumanNameResource>()
                .ForMember(hr => hr.Use, opt => opt.MapFrom(h => h.Use.HasValue ? h.Use.ToString(): null ));
            CreateMap<Hl7.Fhir.Model.Address, AddressResource>()
                .ForMember(ar => ar.Type, opt => opt.MapFrom(a => a.Type.HasValue ? a.Type.ToString() : null))
                .ForMember(ar => ar.Use, opt => opt.MapFrom(a => a.Use.HasValue ? a.Use.ToString() : null));
            CreateMap<Hl7.Fhir.Model.ResourceReference, ResourceReferenceResource>();
            CreateMap<Hl7.Fhir.Model.Identifier, IdentifierResource>()
                .ForMember(ir => ir.Use, opt => opt.MapFrom(i => i.Use.HasValue ? i.Use.ToString() : null ));

            CreateMap<Hl7.Fhir.Model.Observation, ObservationResource>()
                .ForMember(or => or.Status, opt => opt.MapFrom(o => o.Status.HasValue ? o.Status.ToString() : null ))
                .ForMember(or => or.EffectiveDateTime, opt => opt.MapFrom(o => o.Effective.TypeName.Equals("dateTime") ? ((Hl7.Fhir.Model.FhirDateTime)o.Effective).Value : null ))
                .ForMember(or => or.ValueQuantity, opt => opt.MapFrom(o => o.Value.TypeName.Equals("decimal") ? (float?)((Hl7.Fhir.Model.FhirDecimal)o.Value).Value : null ));
            CreateMap<Hl7.Fhir.Model.CodeableConcept, CodeableConceptResource>();
            CreateMap<Hl7.Fhir.Model.Coding, CodingResource>();

            CreateMap<Exception, ExceptionDevResource>()
                .ForMember(er => er.Exception, opt => opt.MapFrom(e => e.GetType().Name))
                .ForMember(er => er.ExceptionSource, opt => opt.MapFrom(e => e.Source))
                .ForMember(er => er.ExceptionStackTrace, opt => opt.MapFrom(e => e.StackTrace))
                .ForMember(er => er.ExceptionMessage, opt => opt.MapFrom(e => e.Message));

            CreateMap<Exception, ExceptionResource>()
                .ForMember(er => er.Exception, opt => opt.MapFrom(e => e.GetType().Name));
        }
    }
}
