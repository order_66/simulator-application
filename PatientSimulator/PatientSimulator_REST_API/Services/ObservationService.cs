﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PatientSimulator_REST_API.Domain.Services;

namespace PatientSimulator_REST_API.Services
{
    public class ObservationService : IObservationService
    {
        public IVitalSignService VitalSignService { get; } = new VitalSignService();
    }
}
