﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hl7.Fhir.Model;

using PatientSimulator_REST_API.Domain.Models;
using PatientSimulator_REST_API.Domain.Services;
using PatientSimulator_REST_API.Domain.Database;

namespace PatientSimulator_REST_API.Services
{
    public class VitalSignService : IVitalSignService
    {
        public async System.Threading.Tasks.Task UpdateResourceVitalSignAsync(IEnumerable<ObservationResource> vitalSigns)
        {
            if(vitalSigns == null)
            {
                throw new RequestFormatException(null, "At least one parameter containes an unexpected null value.");
            }

            try
            {
                DBAccess db = new DBAccess();
                List<Tuple<Guid, VitalParameterInterface>> vitalSignUpdates = new List<Tuple<Guid, VitalParameterInterface>>();  // Links a vital parameter with its patient guid

                /* Check each for required values first */
                foreach (ObservationResource observation in vitalSigns)
                {
                    Guid patientUuid;

                    if (observation.Code.Coding.Count < 1 ||
                        observation.Code.Coding.First().System != Global.FhirIdentification_VitalSignLoincSystem ||
                        !Global.TranslateVPType2ElgaCode.ContainsValue(observation.Code.Coding.First().Code) || 
                        observation.Subject.Type != Global.FhirSubject_PatientType ||
                        !Guid.TryParse(observation.Subject.Identifier.Value, out patientUuid) ||
                        observation.ValueQuantity == null )
                    {
                        throw new RequestFormatException(observation, "The paramter values are in conflict with offical requirements.");
                    }
                    else
                    {
                        vitalSignUpdates.Add(new Tuple<Guid, VitalParameterInterface>(patientUuid,
                                new VitalParameterInterface(Global.TranslateVPType2ElgaCode.ToList().Find(item => item.Value.Equals(observation.Code.Coding.First().Code)).Key, observation.ValueQuantity, null, null)));
                    }
                }

                foreach(Tuple<Guid, VitalParameterInterface> vp in vitalSignUpdates)
                {
                    db.WriteReferenceValue(vp.Item1, vp.Item2);
                }
            }
            catch (NullReferenceException e)
            {
                throw new RequestFormatException(null, "At least one parameter containes an unexpected null value.", e);
            }
        }

        public async Task<IEnumerable<Observation>> ListResourceVitalSignAsync(Guid patientUuid)
        {
            try
            {
                DBAccess db = new DBAccess();
                List<VitalParameterInterface> vitalParameters = new List<VitalParameterInterface>();
                List<Observation> result = new List<Observation>();

                vitalParameters = db.ReadAllReferenceValues(new PatientInterface(patientUuid, null, null, null, null, null, null, null, null, null, null));

                foreach (VitalParameterInterface vp in vitalParameters)
                {
                    result.Add(transposeVitalParameter(vp, patientUuid));
                }

                return result;
            }
            catch (SQLNoEntryFoundException)
            {
                return new List<Observation>();
            }
        }

        public async Task<IEnumerable<Observation>> ListResourceVitalSignAsync(Guid patientUuid, IEnumerable<string> loincCodes)
        {
            try
            {
                DBAccess db = new DBAccess();
                List<VitalParameterInterface> vitalParameters = new List<VitalParameterInterface>();
                List<Observation> result = new List<Observation>();

                vitalParameters = db.ReadAllReferenceValues(new PatientInterface(patientUuid, null, null, null, null, null, null, null, null, null, null));

                foreach (VitalParameterInterface vp in vitalParameters.FindAll(item => loincCodes.Contains(Global.TranslateVPType2ElgaCode[item.type])))
                {
                    result.Add(transposeVitalParameter(vp, patientUuid));
                }

                return result;
            }
            catch (SQLNoEntryFoundException)
            {
                return new List<Observation>();
            }
        }

        public async Task<IEnumerable<Observation>> ListObservationVitalSignAsync(Guid patientUuid)
        {
            try
            {
                DBAccess db = new DBAccess();
                List<VitalParameterInterface> continuousValues = new List<VitalParameterInterface>();
                List<Observation> result = new List<Observation>();

                continuousValues = db.ReadAllContinuousValues(new PatientInterface(patientUuid, null, null, null, null, null, null, null, null, null, null), null);

                foreach (VitalParameterInterface vp in continuousValues)
                {
                    foreach (CurrentVitalValue cv in vp.continuousValues)
                    {
                        result.Add(transposeVitalParameter(cv, patientUuid));
                    }
                }

                return result;
            }
            catch (SQLNoEntryFoundException)
            {
                return new List<Observation>();
            }
        }

        public async Task<IEnumerable<Observation>> ListObservationVitalSignAsync(Guid patientUuid, IEnumerable<string> loincCodes)
        {
            try
            {
                DBAccess db = new DBAccess();
                List<VitalParameterInterface> readingParameters = new List<VitalParameterInterface>();
                List<VitalParameterInterface> continuousValues = new List<VitalParameterInterface>();
                List<Observation> result = new List<Observation>();

                loincCodes.ToList().FindAll(item => Global.TranslateVPType2ElgaCode.ContainsValue(item))
                    .ForEach(item => readingParameters.Add(new VitalParameterInterface(Global.TranslateVPType2ElgaCode.ToList().Find(tran => tran.Value.Equals(item)).Key, null, null, null)));
                continuousValues = db.ReadAllContinuousValues(new PatientInterface(patientUuid, null, null, null, null, null, null, null, null, null, null), null);

                foreach (VitalParameterInterface vp in continuousValues)
                {
                    foreach (CurrentVitalValue cv in vp.continuousValues)
                    {
                        result.Add(transposeVitalParameter(cv, patientUuid));
                    }
                }

                return result;
            }
            catch (SQLNoEntryFoundException)
            {
                return new List<Observation>();
            }
        }

        public async Task<IEnumerable<Observation>> ListObservationVitalSignAsync(Guid patientUuid, DateTime begin, DateTime end)
        {
            try
            {
                DBAccess db = new DBAccess();
                List<VitalParameterInterface> continuousValues = new List<VitalParameterInterface>();
                List<Observation> result = new List<Observation>();

                continuousValues = db.ReadTimedContinuousValues(new PatientInterface(patientUuid, null, null, null, null, null, null, null, null, null, null), begin, end, null);

                foreach (VitalParameterInterface vp in continuousValues)
                {
                    foreach (CurrentVitalValue cv in vp.continuousValues)
                    {
                        result.Add(transposeVitalParameter(cv, patientUuid));
                    }
                }

                return result;
            }
            catch (SQLNoEntryFoundException)
            {
                return new List<Observation>();
            }
        }

        public async Task<IEnumerable<Observation>> ListObservationVitalSignAsync(Guid patientUuid, IEnumerable<string> loincCodes, DateTime begin, DateTime end)
        {
            try
            {
                DBAccess db = new DBAccess();
                List<VitalParameterInterface> continuousValues = new List<VitalParameterInterface>();
                List<VitalParameterInterface> readingParameters = new List<VitalParameterInterface>();
                List<Observation> result = new List<Observation>();

                loincCodes.ToList().FindAll(item => Global.TranslateVPType2ElgaCode.ContainsValue(item))
                    .ForEach(item => readingParameters.Add(new VitalParameterInterface(Global.TranslateVPType2ElgaCode.ToList().Find(tran => tran.Value.Equals(item)).Key, null, null, null)));
                continuousValues = db.ReadTimedContinuousValues(new PatientInterface(patientUuid, null, null, null, null, null, null, null, null, null, null), begin, end, readingParameters);

                foreach (VitalParameterInterface vp in continuousValues)
                {
                    foreach (CurrentVitalValue cv in vp.continuousValues)
                    {
                        result.Add(transposeVitalParameter(cv, patientUuid));
                    }
                }

                return result;
            }
            catch (SQLNoEntryFoundException)
            {
                return new List<Observation>();
            }
        }

        private Observation transposeVitalParameter(VitalParameterInterface vitalI, Guid patientUuid)
        {
            Observation result = new Observation();

            result.Status = ObservationStatus.Final;
            result.Category.Add(new CodeableConcept());
            result.Category.Last().Coding.Add(new Coding(Global.FhirCoding_ObservationSystem, Global.FhirCoding_VitalSignCode));
            result.Code = new CodeableConcept(null, null, null, "");
            result.Code.Coding.Add(new Coding(Global.FhirIdentification_VitalSignLoincSystem, Global.TranslateVPType2ElgaCode[vitalI.type]));
            result.Subject = new ResourceReference($"Resource?{Global.HttpQueryKey_Category}={Global.HttpQueryValue_PatientCategory}&{Global.HttpQueryKey_PatientId}={patientUuid.ToString("N")}");
            result.Subject.Type = "Patient";
            result.Subject.Identifier = new Identifier(Global.FhirIdentification_PatientUuidSystem, patientUuid.ToString("N"));
            result.Effective = vitalI.lastChanged != null ? new FhirDateTime((DateTime)vitalI.lastChanged) : null;
            result.Value = new FhirDecimal((decimal?)vitalI.referenceValue);
            result.DataAbsentReason = null;
            result.Component = null;

            return result;
        }

        private Observation transposeVitalParameter(CurrentVitalValue vitalI, Guid patientUuid)
        {
            Observation result = new Observation();

            result.Status = ObservationStatus.Final;
            result.Category.Add(new CodeableConcept());
            result.Category.Last().Coding.Add(new Coding(Global.FhirCoding_ObservationSystem, Global.FhirCoding_VitalSignCode));
            result.Code = new CodeableConcept(null, null, null, "");
            result.Code.Coding.Add(new Coding(Global.FhirIdentification_VitalSignLoincSystem, Global.TranslateVPType2ElgaCode[vitalI.type]));
            result.Subject = new ResourceReference($"Resource?{Global.HttpQueryKey_Category}={Global.HttpQueryValue_PatientCategory}&{Global.HttpQueryKey_PatientId}={patientUuid.ToString("N")}");
            result.Subject.Type = "Patient";
            result.Subject.Identifier = new Identifier(Global.FhirIdentification_PatientUuidSystem, patientUuid.ToString("N"));
            result.Effective = vitalI.timestamp != null ? new FhirDateTime(vitalI.timestamp) : null;
            result.Value = new FhirDecimal((decimal?)vitalI.value);
            result.DataAbsentReason = null;
            result.Component = null;

            return result;
        }
    }
}
