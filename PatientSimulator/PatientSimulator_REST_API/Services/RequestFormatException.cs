﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Services
{
    /// <summary>
    /// This exception is thrown when the body of a request (eg. from a PUT or POST request) does not match pre-defined conditions
    /// </summary>
    public class RequestFormatException : Exception
    {
        public object RequestBody { get; }

        public RequestFormatException(): base() { }
        public RequestFormatException(string message) : base(message) { }
        public RequestFormatException(string message, Exception innerException) : base(message, innerException) { }
        public RequestFormatException(object requestBody) : base()
        {
            RequestBody = requestBody;
        }
        public RequestFormatException(object requestBody, string message) : base(message)
        {
            RequestBody = requestBody;
        }
        public RequestFormatException(object requestBody, string message, Exception innerException) : base(message, innerException)
        {
            RequestBody = requestBody;
        }
    }
}
