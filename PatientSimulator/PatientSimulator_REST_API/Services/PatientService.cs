﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hl7.Fhir.Model;

using PatientSimulator_REST_API.Domain.Services;
using PatientSimulator_REST_API.Domain.Models;
using PatientSimulator_REST_API.Domain.Database;
using System.Diagnostics;

namespace PatientSimulator_REST_API.Services
{
    //[ExceptionFilter.DatabaseExceptionFilter]
    public class PatientService : IPatientService
    {

        public async Task<PatientModel> SingleAsync(Guid patientUuid)
        {
            try
            {
                DBAccess db = new DBAccess();

                // Some exceptions might be thrown, they are caught in the controller to prepare apropriate http responses
                return transposePatient(db.ReadFullPatient(new PatientInterface(patientUuid, null, null, null, null, null, null, null, null, null, null)));
            }
            catch (SQLNoEntryFoundException)
            {
                return null;
            }
        }

        public async Task<IEnumerable<PatientModel>> ListAsync()
        {
            List<PatientModel> patients = new List<PatientModel>();
            try
            {
                DBAccess db = new DBAccess();

                List<PatientInterface> patientIs = db.ReadAllPatientInformation();

                foreach (PatientInterface patientInterface in patientIs)
                {
                    patients.Add(transposePatient(db.ReadFullPatient(patientInterface)));
                }

                return patients;
            }
            catch (SQLNoEntryFoundException)
            {
                return new List<PatientModel>();
            }
        }

        public async Task<IEnumerable<PatientModel>> ListAsync(int count)
        {
            try
            {
                List<PatientModel> patients = new List<PatientModel>();
                DBAccess db = new DBAccess();

                List<PatientInterface> patientIs = db.ReadAllPatientInformation();

                int elementCount = count < patientIs.Count ? count : patientIs.Count;

                for (int i = 0; i < elementCount; i++)
                {
                    patients.Add(transposePatient(db.ReadFullPatient(patientIs[i])));
                }

                return patients;
            }
            catch (SQLNoEntryFoundException)
            {
                return new List<PatientModel>();
            }
        }


        private PatientModel transposePatient(PatientInterface patientI)
        {
            PatientModel patientM = new PatientModel();

            patientM.Id = patientI.uuid.Value.ToString("N");
            patientM.Active = true;
            patientM.Name.Add(new HumanName());
            patientM.Name.Last().Use = HumanName.NameUse.Usual;
            patientM.Name.Last().Family = null;
            patientM.Name.Last().Given = null;
            patientM.Name.Last().Prefix = null;
            patientM.Name.Last().Suffix = null;
            patientM.Name.Last().Text = patientI.patientName;
            patientM.Gender = Global.TranslateGender2Hl7FhirGender[patientI.gender];
            patientM.BirthDate = patientI.createdAt?.ToString("yyyy-MM-dd");
            //patientM.BirthDateElement = patientI.createdAt == null ? null : new Date(patientI.createdAt.Value.Year, patientI.createdAt.Value.Month, patientI.createdAt.Value.Day);
            //patientM.Deceased = new FhirDateTime("2015-04-23");  // In these instances, fill in the abstract DataType with the apropriate Fhir data type
            patientM.Deceased = new FhirBoolean(false);
            patientM.Address = new List<Address>();
            patientM.ManagingOrganization = null;
            
            foreach(VitalParameterInterface vp in patientI.vitalParameters)
            {
                patientM.vitalSign.Add(new ResourceReference($"/Resource?{Global.HttpQueryKey_Category}={Global.HttpQueryValue_VitalSignCategory}&{Global.HttpQueryKey_PatientId}={patientI.uuid}&{Global.HttpQueryKey_LoincCode}=[{Global.TranslateVPType2ElgaCode[vp.type]}]"));
                patientM.vitalSign.Last().Type = "Observation";
                patientM.vitalSign.Last().Identifier = new Identifier(Global.FhirIdentification_VitalSignLoincSystem, Global.TranslateVPType2ElgaCode[vp.type]);
                patientM.vitalSign.Last().Identifier.Use = Identifier.IdentifierUse.Official;
            }

            return patientM;
        }
    }
}
