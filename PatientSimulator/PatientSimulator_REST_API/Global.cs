﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PatientSimulator_REST_API.Domain.Database;

namespace PatientSimulator_REST_API
{
    public static class Global
    {
        #region HTTP query parameters

        public static string HttpQueryKey_Category { get; } = "category";
        public static string HttpQueryKey_Count { get; } = "count";
        public static string HttpQueryKey_PatientId { get; } = "patient";
        public static string HttpQueryKey_LoincCode { get; } = "code";
        public static string HttpQueryKey_TimeSelectBegin { get; } = "dateStart";
        public static string HttpQueryKey_TimeSelectEnd { get; } = "dateStop";

        public static string HttpQueryValue_PatientCategory { get; } = "patient";
        public static string HttpQueryValue_VitalSignCategory { get; } = "vital-signs";

        #endregion

        #region Regex patterns to check input values

        /// <summary>
        /// Is a basic uuid pattern
        /// </summary>
        public static string Regex_PatientId { get; } = @"^([0-9a-fA-F]{32})|([0-9a-fA-F]{8}(-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12})$";

        /// <summary>
        /// Checks for basic LOINC code arrays (eg. [2222,2-345])
        /// </summary>
        public static string Regex_LoincCodeArray { get; } = @"^\[([0-9\-]+,)*?([0-9\-]+){1}\]$";

        /// <summary>
        /// Use in Regex.Matsches() to extract all elements fromt he provided LOINC code array
        /// </summary>
        public static string Regex_ExtractLioncCode { get; } = @"\b[0-9\-]+\b";

        /// <summary>
        /// Checks for a match to the pattern needed in http query datetime parameter (e.g. "2020-12-12.13:50:05.000+02:00")
        /// </summary>
        public static string Regex_DateTimeTimezone { get; } = @"^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])\.([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])\.([0-9]{3})(\+|-)([01][0-9]|2[0-3]):([0-5][0-9])$";
        
        /// <summary>
        /// Checks for a match to the pattern needed in http query date parameter (e.g. "2020-12-12")
        /// </summary>
        public static string Regex_Date { get; } = @"^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$";

        public static string Pattern_DateTimeTimezone { get; } = "yyyy-MM-dd.HH:mm:ss.FFFzzzz";

        public static string Pattern_Date { get; } = "yyyy-MM-dd";

        #endregion

        #region HL7 FHIR model values

        public static string FhirIdentification_PatientUuidSystem { get; } = "https://tools.ietf.org/html/rfc4122";

        public static string FhirIdentification_VitalSignLoincSystem { get; } = "https://loinc.org";

        public static string FhirSubject_PatientType { get; } = "Patient";

        public static string FhirCoding_ObservationSystem { get; } = "http://terminology.hl7.org/CodeSystem/observation-category";

        public static string FhirCoding_VitalSignCode { get; } = "vital-signs";

        #endregion

        #region Database connection

        /// <summary>
        /// The compatible version number of the Demographic and Vital DB.
        /// Read as  [majorVersion, minorVersion]
        /// </summary>
        public static int[] DatabaseVersionNumber { get; } = new int[] { 4, 3 };

        /// <summary>
        /// The compatible version UUID of the Demographic and Vital DB.
        /// Stored as string representation after norm RFC4122.
        /// </summary>
        public static Guid DatabaseVersionUUID { get; } = new Guid("25e1d8ea3aea11eba0353747f8066038");

        public static string DatabaseName { get; set; } = "LSE-G1";

        public static string DatabaseUserName { get; set; } = "LSE-G1";
        public static string DatabaseUserPassword { get; set; } = "T50ztvGhql0NWIbR5J5fXs86";
        //public static Global.DatabaseUser DatabaseUserInformation { get; private set; } = new DatabaseUser("postgres", "postgres");

        public static string DatabaseHost { get; set; } = "db-n1.ls.technikum-wien.at";

        #endregion

        #region Translation Dictionaries
        public static Dictionary<VitalParameterType, string> TranslateVPType2ElgaCode
        {
            get
            {
                if (translateVPType2ElgaCode == null)
                {
                    translateVPType2ElgaCode = new Dictionary<VitalParameterType, string>();
                    translateVPType2ElgaCode.Add(VitalParameterType.MeanBloodPressure, "8478-0");
                    translateVPType2ElgaCode.Add(VitalParameterType.SystolicBloodPressure, "8480-6");
                    translateVPType2ElgaCode.Add(VitalParameterType.DiastolicBloodPressure, "8462-4");
                    translateVPType2ElgaCode.Add(VitalParameterType.BreathRate, "9279-1");
                    translateVPType2ElgaCode.Add(VitalParameterType.CapillarBloodGlucose, "32016-8");
                    translateVPType2ElgaCode.Add(VitalParameterType.CoreTemperature, "8310-5");
                    translateVPType2ElgaCode.Add(VitalParameterType.Height, "8302-2");
                    translateVPType2ElgaCode.Add(VitalParameterType.VenousBloodCarbondioxidePressure, "2021-4");
                    translateVPType2ElgaCode.Add(VitalParameterType.VenousBloodOxygenPressure, "2705-2");
                    translateVPType2ElgaCode.Add(VitalParameterType.Weight, "3141-9");
                }
                return translateVPType2ElgaCode;
            }
        }
        private static Dictionary<VitalParameterType, string> translateVPType2ElgaCode;

        public static Dictionary<VitalParameterType, string> TranslateVPType2Unit
        {
            get
            {
                if (translateVPType2Unit == null)
                {
                    translateVPType2Unit = new Dictionary<VitalParameterType, string>();
                    translateVPType2Unit.Add(VitalParameterType.MeanBloodPressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.SystolicBloodPressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.DiastolicBloodPressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.BreathRate, "/min");
                    translateVPType2Unit.Add(VitalParameterType.CapillarBloodGlucose, "mg/dl");
                    translateVPType2Unit.Add(VitalParameterType.CoreTemperature, "°C");
                    translateVPType2Unit.Add(VitalParameterType.Height, "cm");
                    translateVPType2Unit.Add(VitalParameterType.VenousBloodCarbondioxidePressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.VenousBloodOxygenPressure, "mmHg");
                    translateVPType2Unit.Add(VitalParameterType.Weight, "kg");
                }
                return translateVPType2Unit;
            }
        }
        private static Dictionary<VitalParameterType, string> translateVPType2Unit;
        // Get with: string someUnit = Global.TranslateVPType2Unit[VitalParameterType.SomeType];

        /// <summary>
        /// Translate an organ type into the name of the corresponding table in the Demographic and Vital DB
        /// </summary>
        public static Dictionary<OrganType, string> TranslateOType2DemographDB
        {
            get
            {
                if (translateOType2DemographDB == null)
                {
                    translateOType2DemographDB = new Dictionary<OrganType, string>();
                    translateOType2DemographDB.Add(OrganType.PeripheralVein, "peripheral_vein_organ");
                    translateOType2DemographDB.Add(OrganType.PeripheralArtery, "peripheral_artery_organ");
                    translateOType2DemographDB.Add(OrganType.Body, "body_organ");
                    translateOType2DemographDB.Add(OrganType.CapillarBlood, "capillary_blood_organ");
                    translateOType2DemographDB.Add(OrganType.Lung, "lung_organ");
                    translateOType2DemographDB.Add(OrganType.VenousBlood, "venous_blood_organ");
                }
                return translateOType2DemographDB;
            }
        }
        private static Dictionary<OrganType, string> translateOType2DemographDB;

        /// <summary>
        /// Translate an organ type into the corresponding entity name of patientsimulator.patient (where its uuid is stored as foreign key)
        /// </summary>
        public static Dictionary<OrganType, string> TranslateOType2DBPatientReference
        {
            get
            {
                if (translateOType2DBPatientReference == null)
                {
                    translateOType2DBPatientReference = new Dictionary<OrganType, string>();
                    translateOType2DBPatientReference.Add(OrganType.PeripheralVein, "peripheral_vein_uuid");
                    translateOType2DBPatientReference.Add(OrganType.PeripheralArtery, "peripheral_artery_uuid");
                    translateOType2DBPatientReference.Add(OrganType.Body, "body_uuid");
                    translateOType2DBPatientReference.Add(OrganType.CapillarBlood, "capillary_blood_uuid");
                    translateOType2DBPatientReference.Add(OrganType.Lung, "lung_uuid");
                    translateOType2DBPatientReference.Add(OrganType.VenousBlood, "venous_blood_uuid");
                }
                return translateOType2DBPatientReference;
            }
        }
        private static Dictionary<OrganType, string> translateOType2DBPatientReference;

        public static Dictionary<OrganType, VitalParameterType[]> AllowedVPsInOrgans
        {
            get
            {
                if (allowedVPsInOrgans == null)
                {
                    allowedVPsInOrgans = new Dictionary<OrganType, VitalParameterType[]>();
                    allowedVPsInOrgans.Add(OrganType.Body, new VitalParameterType[] { VitalParameterType.Height, VitalParameterType.Weight, VitalParameterType.CoreTemperature });
                    allowedVPsInOrgans.Add(OrganType.CapillarBlood, new VitalParameterType[] { VitalParameterType.CapillarBloodGlucose });
                    allowedVPsInOrgans.Add(OrganType.Lung, new VitalParameterType[] { VitalParameterType.BreathRate });
                    allowedVPsInOrgans.Add(OrganType.PeripheralArtery, new VitalParameterType[] { VitalParameterType.MeanBloodPressure, VitalParameterType.SystolicBloodPressure, VitalParameterType.DiastolicBloodPressure });
                    allowedVPsInOrgans.Add(OrganType.PeripheralVein, new VitalParameterType[] { });
                    allowedVPsInOrgans.Add(OrganType.VenousBlood, new VitalParameterType[] { VitalParameterType.VenousBloodCarbondioxidePressure, VitalParameterType.VenousBloodOxygenPressure });
                }
                return allowedVPsInOrgans;
            }
        }
        private static Dictionary<OrganType, VitalParameterType[]> allowedVPsInOrgans;

        public static Dictionary<ViewerRightsType, int> TranslateViewerRightsType2DBInteger
        {
            get
            {
                if (translateViewerRightsType2DBInteger == null)
                {
                    translateViewerRightsType2DBInteger = new Dictionary<ViewerRightsType, int>();
                    translateViewerRightsType2DBInteger.Add(ViewerRightsType.Hidden, 0);
                    translateViewerRightsType2DBInteger.Add(ViewerRightsType.Reading, 1);
                    translateViewerRightsType2DBInteger.Add(ViewerRightsType.Writing, 2);
                }
                return translateViewerRightsType2DBInteger;
            }
        }
        private static Dictionary<ViewerRightsType, int> translateViewerRightsType2DBInteger;

        public static Dictionary<PatientGender, Hl7.Fhir.Model.AdministrativeGender> TranslateGender2Hl7FhirGender { get; } = new Dictionary<PatientGender, Hl7.Fhir.Model.AdministrativeGender>
        {
            [PatientGender.Male] = Hl7.Fhir.Model.AdministrativeGender.Male,
            [PatientGender.Female] = Hl7.Fhir.Model.AdministrativeGender.Female,
            [PatientGender.Other] = Hl7.Fhir.Model.AdministrativeGender.Other,
            [PatientGender.Unknown] = Hl7.Fhir.Model.AdministrativeGender.Unknown,
        };

        public static Dictionary<PatientGender, string> TranslateGender2DBString { get; } = new Dictionary<PatientGender, string>
        {
            [PatientGender.Male] = "m",
            [PatientGender.Female] = "f",
            [PatientGender.Other] = "o",
            [PatientGender.Unknown] = "u",
        };

        #endregion
    }
}
