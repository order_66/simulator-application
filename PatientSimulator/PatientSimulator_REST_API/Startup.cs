using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using AutoMapper;

using PatientSimulator_REST_API.Domain.Services;
using PatientSimulator_REST_API.Services;
using PatientSimulator_REST_API.Domain.Database;

namespace PatientSimulator_REST_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            /* Load database configuration from appsettings.json (!Must be provided) */
            try
            {
                Global.DatabaseName = Configuration.GetSection("Database").GetValue<string>("DatabaseName");
                Global.DatabaseHost = Configuration.GetSection("Database").GetValue<string>("Host");
                Global.DatabaseUserName = Configuration.GetSection("Database").GetValue<string>("UserName");
                Global.DatabaseUserPassword = Configuration.GetSection("Database").GetValue<string>("Password");

                /* Check, if the correct DBVersion is provided. */
                DBAccess db = new DBAccess();
                VersionInterface version = db.ReadVersion();
                if (!(version.uuid == Global.DatabaseVersionUUID))
                {
                    throw new NotSupportedException($"Attempt to access Demographic and Vital Database version {Global.DatabaseVersionNumber[0]}.{Global.DatabaseVersionNumber[1]} (with credentials provided in appsettings.json) failed.\n Version id ({version.uuid}) does not match the required {Global.DatabaseVersionUUID}");
                }
            }
            catch (Exception)
            {
                throw new NotSupportedException($"Attempt to access Demographic and Vital Database version {Global.DatabaseVersionNumber[0]}.{Global.DatabaseVersionNumber[1]} (with credentials provided in appsettings.json) failed.");
            }
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddJsonOptions(options => {  options.JsonSerializerOptions.IgnoreNullValues = true;  })
                .AddJsonOptions(options => { options.JsonSerializerOptions.PropertyNamingPolicy = null;  });

            /* Add database controller */
            //services.AddDbContext<AppDbContext>(options => {
            //    options.UseInMemoryDatabase("supermarket-api-in-memory");
            //});

            /* Add services, ready to use by the controllers */
            services.AddScoped<IResourceService, ResourceService>();
            services.AddScoped<IObservationService, ObservationService>();

            services.AddAutoMapper(typeof(Startup));
            //services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                app.UseExceptionHandler("/error-local-development");
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            //app.UseHttpsRedirection();
            // Enable https redirection only when a trusted certificate can be provided (any application ohter than a browser will have problems)

            app.UseRouting();

            app.UseAuthorization();

            // Add a static route not working
            //app.UsePathBase("/REST/R4");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
