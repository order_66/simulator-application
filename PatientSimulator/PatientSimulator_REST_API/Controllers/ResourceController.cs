﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Hl7.Fhir.Model;
using AutoMapper;

using PatientSimulator_REST_API.Domain.Models;
using PatientSimulator_REST_API.Domain.Services;

namespace PatientSimulator_REST_API.Controllers
{

    // Route /REST/R4/Resource
    [Route("/REST/R4/[controller]")]
    public class ResourceController : Controller
    {
        private readonly IResourceService _resourceService;
        private readonly IMapper _mapper;

        public ResourceController(IResourceService resourceService, IMapper mapper)
        {
            _resourceService = resourceService;
            _mapper = mapper;
        }

        // GET /REST/R4/Resource?category=[]
        // GET /REST/R4/Resource?category=[]&count=[]
        // GET /REST/R4/Resource?category=[]&patient=[]
        // GET /REST/R4/Resource?category=[]&patient=[]&code=[]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            /* Returns IEnumerable<PatientModel> */
            int count = -1;
            bool patientIdProvided = Request.Query.ContainsKey(Global.HttpQueryKey_PatientId) && Regex.IsMatch(Request.Query[Global.HttpQueryKey_PatientId], Global.Regex_PatientId);

            /* Check for bad querys (must contain category, can contain count (which is of type integer), ...) including bad values for non-string queries (those with defined formats) */
            if (!Request.Query.ContainsKey(Global.HttpQueryKey_Category)
                || (Request.Query.ContainsKey(Global.HttpQueryKey_Count) && !Int32.TryParse(Request.Query[Global.HttpQueryKey_Count], out count))
                || (Request.Query.ContainsKey(Global.HttpQueryKey_PatientId) && !Regex.IsMatch(Request.Query[Global.HttpQueryKey_PatientId], Global.Regex_PatientId)) 
                || (Request.Query.ContainsKey(Global.HttpQueryKey_LoincCode) && !Regex.IsMatch(Request.Query[Global.HttpQueryKey_LoincCode], Global.Regex_LoincCodeArray)) )
            {
                return StatusCode(400);
            }

            // Remember: Less specific matches to the query must be processed after more specific ones.
            if (Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_PatientCategory) && patientIdProvided)
            {
                /* Return specific patient model */
                return Ok(_mapper.Map<PatientModel, PatientModelResource>(await getSinglePatientAsync(new Guid(Request.Query[Global.HttpQueryKey_PatientId]))));
            }
            else if (Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_PatientCategory))
            {
                /* Return all patient models */
                return Ok(_mapper.Map<IEnumerable<PatientModel>, IEnumerable<PatientModelResource>>(await getAllPatientsAsync(count > 0 ? count : (int?)null)));
            }
            else if (Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_VitalSignCategory) && patientIdProvided)
            {
                /* Return all vital signs */
                List<string> loincCodes = new List<string>();
                if (Request.Query.ContainsKey(Global.HttpQueryKey_LoincCode))
                {
                    foreach(Match m in Regex.Matches(Request.Query[Global.HttpQueryKey_LoincCode], Global.Regex_ExtractLioncCode))
                    {
                        loincCodes.Add(m.Value);
                    }
                }

                /* Check if all given LOINC codes are supported */
                if (loincCodes.Exists(item => !Global.TranslateVPType2ElgaCode.ContainsValue(item)))
                {
                    throw new Services.RequestFormatException(loincCodes, "At least one of the given LOINC codes is not supported.");
                }

                return Ok(_mapper.Map<IEnumerable<Observation>, IEnumerable<ObservationResource>>(await getVitalSignReferenceAsync(new Guid(Request.Query[Global.HttpQueryKey_PatientId]), loincCodes)));
            }
            else
            {
                /* Combination of query keys with specific values not found */
                return StatusCode(400);
            }
        }

        // POST /REST/R4/Resource?category=[]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]IEnumerable<ObservationResource> vitalSignResource)
        {
            if (!Request.Query.ContainsKey(Global.HttpQueryKey_Category))
            {
                return StatusCode(400);
            }

            if (Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_VitalSignCategory))
            {
                await _resourceService.VitalSignService.UpdateResourceVitalSignAsync(vitalSignResource);
                return Ok();
            }
            else
            {
                return StatusCode(400);
            }
        }

        #region Hidden controller functions
        /* These functions are not associated with HTTP verbs, because they depend on parameters passed as variables in URLs */
        private async Task<IEnumerable<PatientModel>> getAllPatientsAsync(int? count)
        {
            if(count != null)
            {
                return await _resourceService.PatientService.ListAsync((int)count);
            }
            else
            {
                return await _resourceService.PatientService.ListAsync();
            }
        }

        private async Task<PatientModel> getSinglePatientAsync(Guid patientId)
        {
            return await _resourceService.PatientService.SingleAsync(patientId);
        }

        private async Task<IEnumerable<Observation>> getVitalSignReferenceAsync(Guid patientId, IEnumerable<string> loincCodes)
        {
            // Only select different loinc codes if the resource is provided
            if(loincCodes != null && loincCodes.Count() != 0)
            {
                return await _resourceService.VitalSignService.ListResourceVitalSignAsync(patientId, loincCodes);
            }
            else
            {
                return await _resourceService.VitalSignService.ListResourceVitalSignAsync(patientId);
            }
        }

        #endregion
    }
}
