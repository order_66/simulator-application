﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientSimulator_REST_API.Controllers
{
    [Route("/[controller]")]
    public class IsAlifeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(new Domain.Models.IsAlifeResource() { IsAlife = true });
        }
    }
}
