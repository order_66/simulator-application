﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using AutoMapper;
using Hl7.Fhir.Model;

using PatientSimulator_REST_API.Domain.Services;
using PatientSimulator_REST_API.Domain.Models;
using System.Globalization;
using System.Diagnostics;

namespace PatientSimulator_REST_API.Controllers
{
    // Route /REST/R4/Controller
    [Route("/REST/R4/[controller]")]
    public class ObservationController : Controller
    {
        private IObservationService _observationService;
        private IMapper _mapper;

        public ObservationController(IObservationService observationService, IMapper mapper)
        {
            _observationService = observationService;
            _mapper = mapper;
        }

        // GET /REST/R4/Observation?category=[]&patient=[]
        // GET /REST/R4/Observation?category=[]&patient=[]&dateStart=[]
        // GET /REST/R4/Observation?category=[]&patient=[]&dateStart=[]&dateStop=[]
        // GET /REST/R4/Observation?category=[]&patient=[]&code=[]
        // GET /REST/R4/Observation?category=[]&patient=[]&code=[]&dateStart=[]
        // GET /REST/R4/Observation?category=[]&patient=[]&code=[]&dateStart=[]&dateStop=[]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            
            if(!Request.Query.ContainsKey(Global.HttpQueryKey_Category) 
                || !Request.Query.ContainsKey(Global.HttpQueryKey_PatientId) || !Regex.IsMatch(Request.Query[Global.HttpQueryKey_PatientId], Global.Regex_PatientId)
                || (Request.Query.ContainsKey(Global.HttpQueryKey_LoincCode) && !Regex.IsMatch(Request.Query[Global.HttpQueryKey_LoincCode], Global.Regex_LoincCodeArray))
                || (Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectBegin) && !Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectEnd) &&
                    !Regex.IsMatch(Request.Query[Global.HttpQueryKey_TimeSelectBegin], Global.Regex_Date)) 
                || (Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectBegin) && Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectEnd) && 
                    (!Regex.IsMatch(Request.Query[Global.HttpQueryKey_TimeSelectBegin], Global.Regex_DateTimeTimezone) || !Regex.IsMatch(Request.Query[Global.HttpQueryKey_TimeSelectEnd], Global.Regex_DateTimeTimezone)))
                || (Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectEnd) && !Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectBegin)) )
            {
                return StatusCode(400);
            }


            if (Request.Query.ContainsKey(Global.HttpQueryKey_Category) && Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_VitalSignCategory) &&
                Request.Query.ContainsKey(Global.HttpQueryKey_PatientId) && Request.Query.ContainsKey(Global.HttpQueryKey_LoincCode) && 
                Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectBegin) && Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectEnd))
            {
                /* Return continuous values for specific loinc codes and timespan */
                List<string> loincCodes = new List<string>();
                if (Request.Query.ContainsKey(Global.HttpQueryKey_LoincCode))
                {
                    foreach (Match m in Regex.Matches(Request.Query[Global.HttpQueryKey_LoincCode], Global.Regex_ExtractLioncCode))
                    {
                        loincCodes.Add(m.Value);
                    }
                }

                /* Check if all given LOINC codes are supported */
                if(loincCodes.Exists(item => !Global.TranslateVPType2ElgaCode.ContainsValue(item)))
                {
                    throw new Services.RequestFormatException(loincCodes, "At least one of the given LOINC codes is not supported.");
                }

                DateTimeOffset beginOffset = DateTimeOffset.ParseExact(Request.Query[Global.HttpQueryKey_TimeSelectBegin], Global.Pattern_DateTimeTimezone, CultureInfo.InvariantCulture);
                DateTime begin = beginOffset.UtcDateTime.AddHours(1);
                DateTimeOffset endOffset = DateTimeOffset.ParseExact(Request.Query[Global.HttpQueryKey_TimeSelectEnd], Global.Pattern_DateTimeTimezone, CultureInfo.InvariantCulture);
                DateTime end = endOffset.UtcDateTime.AddHours(1);

                return Ok(_mapper.Map<IEnumerable<Observation>, IEnumerable<ObservationResource>>(await getVitalSigns(new Guid(Request.Query[Global.HttpQueryKey_PatientId]), loincCodes, begin, end)));
            }
            else 
            if (Request.Query.ContainsKey(Global.HttpQueryKey_Category) && Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_VitalSignCategory) && 
                Request.Query.ContainsKey(Global.HttpQueryKey_PatientId) && Request.Query.ContainsKey(Global.HttpQueryKey_LoincCode) && Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectBegin))
            {
                /* Return continuous values for specific loinc codes and one date */
                List<string> loincCodes = new List<string>();
                if (Request.Query.ContainsKey(Global.HttpQueryKey_LoincCode))
                {
                    foreach (Match m in Regex.Matches(Request.Query[Global.HttpQueryKey_LoincCode], Global.Regex_ExtractLioncCode))
                    {
                        loincCodes.Add(m.Value);
                    }
                }

                /* Check if all given LOINC codes are supported */
                if (loincCodes.Exists(item => !Global.TranslateVPType2ElgaCode.ContainsValue(item)))
                {
                    throw new Services.RequestFormatException(loincCodes, "At least one of the given LOINC codes is not supported.");
                }

                DateTime begin = DateTime.ParseExact(Request.Query[Global.HttpQueryKey_TimeSelectBegin], Global.Pattern_Date, CultureInfo.InvariantCulture);
                DateTime end = begin.AddDays(1);

                return Ok(_mapper.Map<IEnumerable<Observation>, IEnumerable<ObservationResource>>(await getVitalSigns(new Guid(Request.Query[Global.HttpQueryKey_PatientId]), loincCodes, begin, end)));
            }
            else
            if (Request.Query.ContainsKey(Global.HttpQueryKey_Category) && Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_VitalSignCategory) && 
                Request.Query.ContainsKey(Global.HttpQueryKey_PatientId) && Request.Query.ContainsKey(Global.HttpQueryKey_LoincCode))
            {
                /* Return continuous values for specific loinc codes only */
                List<string> loincCodes = new List<string>();
                if (Request.Query.ContainsKey(Global.HttpQueryKey_LoincCode))
                {
                    foreach (Match m in Regex.Matches(Request.Query[Global.HttpQueryKey_LoincCode], Global.Regex_ExtractLioncCode))
                    {
                        loincCodes.Add(m.Value);
                    }
                }

                /* Check if all given LOINC codes are supported */
                if (loincCodes.Exists(item => !Global.TranslateVPType2ElgaCode.ContainsValue(item)))
                {
                    throw new Services.RequestFormatException(loincCodes, "At least one of the given LOINC codes is not supported.");
                }

                return Ok(_mapper.Map<IEnumerable<Observation>, IEnumerable<ObservationResource>>(await getAllVitalSigns(new Guid(Request.Query[Global.HttpQueryKey_PatientId]), loincCodes)));
            }
            else 
            if (Request.Query.ContainsKey(Global.HttpQueryKey_Category) && Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_VitalSignCategory) && 
                Request.Query.ContainsKey(Global.HttpQueryKey_PatientId) && Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectBegin) && Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectEnd))
            {
                /* Return continuous values for specific timespan */

                DateTimeOffset beginOffset = DateTimeOffset.ParseExact(Request.Query[Global.HttpQueryKey_TimeSelectBegin], Global.Pattern_DateTimeTimezone, CultureInfo.InvariantCulture);
                DateTime begin = beginOffset.UtcDateTime.AddHours(1);
                DateTimeOffset endOffset = DateTimeOffset.ParseExact(Request.Query[Global.HttpQueryKey_TimeSelectEnd], Global.Pattern_DateTimeTimezone, CultureInfo.InvariantCulture);
                DateTime end = endOffset.UtcDateTime.AddHours(1);

                return Ok(_mapper.Map<IEnumerable<Observation>, IEnumerable<ObservationResource>>(await getVitalSigns(new Guid(Request.Query[Global.HttpQueryKey_PatientId]), null, begin, end)));
            }
            else 
            if (Request.Query.ContainsKey(Global.HttpQueryKey_Category) && Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_VitalSignCategory) && 
                Request.Query.ContainsKey(Global.HttpQueryKey_PatientId) && Request.Query.ContainsKey(Global.HttpQueryKey_TimeSelectBegin))
            {
                /* Return continuous values for specific date */

                DateTime begin = DateTime.ParseExact(Request.Query[Global.HttpQueryKey_TimeSelectBegin], Global.Pattern_Date, CultureInfo.InvariantCulture);
                DateTime end = begin.AddDays(1);

                return Ok(_mapper.Map<IEnumerable<Observation>, IEnumerable<ObservationResource>>(await getVitalSigns(new Guid(Request.Query[Global.HttpQueryKey_PatientId]), null, begin, end)));
            }
            else
            if(Request.Query.ContainsKey(Global.HttpQueryKey_Category) && Request.Query[Global.HttpQueryKey_Category].Equals(Global.HttpQueryValue_VitalSignCategory) && 
                Request.Query.ContainsKey(Global.HttpQueryKey_PatientId))
            {
                /* Return all continuous values*/
                return Ok(_mapper.Map<IEnumerable<Observation>, IEnumerable<ObservationResource>>(await getAllVitalSigns(new Guid(Request.Query[Global.HttpQueryKey_PatientId]), null)));
            }
            else
            {
                /* Combination of query keys with specific values not found */
                return StatusCode(400);
            }
        }

        #region Hidden controller functions
        /* These functions are not associated with HTTP verbs, because they depend on parameters passed as variables in URLs */

        private async Task<IEnumerable<Observation>> getAllVitalSigns(Guid patientId, List<string> loincCodes)
        {
            if(loincCodes == null || loincCodes.Count == 0)
            {
                return await _observationService.VitalSignService.ListObservationVitalSignAsync(patientId);
            }
            else
            {
                return await _observationService.VitalSignService.ListObservationVitalSignAsync(patientId, loincCodes);
            }
        }
        private async Task<IEnumerable<Observation>> getVitalSigns(Guid patientId, List<string> loincCodes, DateTime begin, DateTime end)
        {
            if (loincCodes == null || loincCodes.Count == 0)
            {
                return await _observationService.VitalSignService.ListObservationVitalSignAsync(patientId, begin, end);
            }
            else
            {
                return await _observationService.VitalSignService.ListObservationVitalSignAsync(patientId, loincCodes, begin, end);
            }
        }
        #endregion

        void Application_Error(object sender, EventArgs e)
        {
            Response.StatusCode = 444;
            Response.StartAsync();
        }
    }
}
