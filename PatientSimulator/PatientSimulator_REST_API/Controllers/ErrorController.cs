﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace PatientSimulator_REST_API.Controllers
{
    [ApiController]
    public class ErrorController : ControllerBase
    {
        private readonly IMapper _mapper;

        public ErrorController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [Route("/error-local-development")]
        public IActionResult ErrorLocalDevelopment()
        {
            IExceptionHandlerFeature context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            IActionResult result;

            /* Make sure NotImplemented Exception cannot slip through testing */
            if(context.Error is NotImplementedException)
            {
                throw context.Error;
            }

            if(getExceptionAction(context, out result))
            {
                return result;
            }

            return StatusCode(500,  _mapper.Map<Exception, Domain.Models.ExceptionDevResource>(context.Error));
        }

        [Route("/error")]
        public IActionResult Error()
        {
            IExceptionHandlerFeature context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            IActionResult result;

            if (getExceptionAction(context, out result))
            {
                return result;
            }

            return StatusCode(500, _mapper.Map<Exception, Domain.Models.ExceptionResource>(context.Error));
        }

        #region Context hidden functions

        /// <summary>
        /// Out the action for a registered exception
        /// </summary>
        /// <param name="context">Context containing the Exception</param>
        /// <param name="action">If this function returns true, contains the action apropriate to the found exception</param>
        /// <returns>True, if the the exception was registered and the apropriate action is set, false otherwise</returns>
        private bool getExceptionAction(IExceptionHandlerFeature context, out IActionResult action)
        {
            action = null;

            if (context.Error is Services.RequestFormatException)
            {
                action = StatusCode(406, ((Services.RequestFormatException)context.Error).RequestBody);
                return true;
            }

            return false;
        }

        #endregion
    }
}
